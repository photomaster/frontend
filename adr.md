# Architecture decision record

This is the template in [Documenting architecture decisions - Michael Nygard](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions).

## ADR1: State Management Solution Redux

### Status

accepted

### Context

The application is growing bigger and bigger and therefore managing the state in one central place is a good idea to not provoke 
errors trying to save the state locally for each component. It makes development and state management easier for everyone involved. 
Additionally, this project is being developed as part of a study programme and learning something new (like Redux) should be the 
goal of a masters project.

### Decision

We will integrate the state management library "Redux" in our project.

We will update our Webpack setup accordingly

Prior to this we will write more unit tests in order to ease the integration of Redux

### Consequences

Positive consequences:
* less error prone
* central solution for state management, single source of truth
* we can move functionality that fetches data and populates state from the individual components to a centralized state unit
* testability increases, because fewer components have to be mocked for testing purposes. Components can be better tested in isolation as state and component are more loosely-coupled
* Redux is under active development and lots of resources exist across the internet
* we can export state and share it for debugging purposes
* it becomes more clear why the application behaves in a certain way => documentation of state changes

Negative consequences:
* novel technology for the project and project members, team members have to invest time to learn about Redux
* additional code needed to implement Redux
* tests need to be rewritten after implementation of Redux
* a lot of components will have to be changed and adapted to the new state management library.
* a majority of existing components are currently not covered by tests.
* we have to decide if data belongs to the component state or global state