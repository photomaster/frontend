export const baseUrl =
  process.env.PORT_FRONTEND !== undefined
    ? `http://127.0.0.1:${process.env.PORT_FRONTEND}`
    : `http://127.0.0.1:8081`

/**
 * @see https://gitlab.mediacube.at/photo-master/backend/-/blob/master/photo/locations/fixtures/e2e.yaml
 */
export const User = {
  id: 99,
  email: 'test@test.at',
  username: 'testuser',
  displayName: 'a user',
  description: 'a description',
  password: '1234',
}

export const EditUser = {
  id: 100,
  email: 'edit@test.at',
  username: 'anotheruser',
  displayName: 'another user',
  description: 'a description',
  password: '1234',
}

export async function loginUser(page, useEditUser = false) {
  const UserToLogin = useEditUser ? EditUser : User

  await page.goto(`${baseUrl}/login`)

  const username = await page.waitForSelector('#unique')
  await username.click()
  await username.type(UserToLogin.username)

  const password = await page.waitForSelector('#password')
  await password.click()
  await password.type(UserToLogin.password)

  const button = await page.waitForSelector(
    'button[type="submit"]:not([disabled])',
  )

  await Promise.all([
    button.click(),
    page.waitForNavigation({ waitUntil: 'networkidle0' }),
  ])

  expect(page.url()).toEqual(`${baseUrl}/users/${UserToLogin.id}`)
}

export async function registerUser(page) {
  const date = new Date()
  const TestUsername = `Testuser${date.getTime()}`
  const TestDisplayName = `Testuser`
  const TestEmail = `${date.getTime()}@fakemail.com`
  const TestPassword = '@_T0p_$ecreT_p@ssw0rd'

  await page.goto(`${baseUrl}/register`)

  // fill in values
  const username = await page.waitForSelector('#username')
  await username.click()
  await username.type(TestUsername)

  const displayName = await page.waitForSelector('#displayName')
  await displayName.click()
  await displayName.type(TestDisplayName)

  const email = await page.waitForSelector('#email')
  await email.click()
  await email.type(TestEmail)

  const password = await page.waitForSelector('#password')
  await password.click()
  await password.type(TestPassword)

  const password2 = await page.waitForSelector('#password2')
  await password2.click()
  await password2.type(TestPassword)

  const button = await page.waitForSelector(
    'button[type="submit"]:not([disabled])',
  )

  // click submit and wait for navigation
  await Promise.all([
    button.click(),
    page.waitForNavigation({ waitUntil: 'networkidle0' }),
  ])
}

/* see what's happening in puppeteer (console, requests, responses etc.)
page
  .on('console', message =>
    console.log(
      `${message
        .type()
        .substr(0, 3)
        .toUpperCase()} ${message.text()}`,
    ),
  )
  .on('pageerror', ({ message }) => console.log(message))

page
  .on('request', req => console.log(req))
  .on('response', response => {
    console.log(`${response.status()} ${response.url()}`)
    console.log(response)
  })
  .on('requestfailed', request =>
    console.log(`${request.failure().errorText} ${request.url()}`),
  )
*/
