import { baseUrl, User } from '../helpers'

const puppeteer = require('puppeteer')

let browser
beforeAll(async done => {
  browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  })
  done()
})

afterAll(async done => {
  await browser.close()

  done()
})

describe('Sign in', () => {
  it('enables user to log in via username', async () => {
    const page = await browser.newPage()

    await page.goto(`${baseUrl}/login`)

    // fill in values
    const username = await page.waitForSelector('#unique')
    await username.click()
    await username.type(User.username)

    const password = await page.waitForSelector('#password')
    await password.click()
    await password.type(User.password)

    // wait until the button becomes clickable
    const button = await page.waitForSelector(
      'button[type="submit"]:not([disabled])',
    )

    // submit form and wait for navigation
    await Promise.all([button.click(), page.waitForNavigation()])

    // check that it's really the user page and displaying the correct values
    expect(page.url()).toEqual(`${baseUrl}/users/${User.id}`)

    const headline = await page.evaluate(
      () => document.querySelector('h1').textContent,
    )
    expect(headline).toEqual(User.displayName)

    const bio = await page.evaluate(
      () => document.querySelector('[data-testid="user--desc"]').textContent,
    )
    expect(bio).toEqual(User.description)
  }, 900000)

  it('enables user to log in via email', async () => {
    const page = await browser.newPage()

    await page.goto(`${baseUrl}/login`)

    // fill in values
    const email = await page.waitForSelector('#unique')
    await email.click()
    await email.type(User.email)

    const password = await page.waitForSelector('#password')
    await password.click()
    await password.type(User.password)

    // wait until the button becomes clickable
    const button = await page.waitForSelector(
      'button[type="submit"]:not([disabled])',
    )

    // submit form and wait for navigation
    await Promise.all([button.click(), page.waitForNavigation()])

    // check that it's really the user page and displaying the typed in values
    expect(page.url()).toEqual(`${baseUrl}/users/${User.id}`)

    const headline = await page.evaluate(
      () => document.querySelector('h1').textContent,
    )
    expect(headline).toEqual(User.displayName)

    const bio = await page.evaluate(
      () => document.querySelector('[data-testid="user--desc"]').textContent,
    )
    expect(bio).toEqual(User.description)
  }, 900000)

  it('displays an error message if the credentials are wrong', async () => {
    const page = await browser.newPage()

    await page.goto(`${baseUrl}/login`)

    // fill in values
    const username = await page.waitForSelector('#unique')
    await username.click()
    await username.type('Test_User')

    const password = await page.waitForSelector('#password')
    await password.click()
    await password.type('no_real_password')

    // wait until the button becomes clickable
    const button = await page.waitForSelector(
      'button[type="submit"]:not([disabled])',
    )

    // submit form
    await button.click()

    // wait for error message to be displayed
    const message = await page.waitForSelector('[data-testid="alert"]')
    expect(message).toBeTruthy()

    // check that no navigation happened
    expect(page.url()).toMatch(`${baseUrl}/login`)
  }, 900000)
})
