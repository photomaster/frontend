import { baseUrl, loginUser } from '../helpers'

const puppeteer = require('puppeteer')

let browser
beforeAll(async done => {
  browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
    ],
  })
  done()
})

afterAll(async done => {
  await browser.close()

  done()
})
describe('Use case', () => {
  it('user wants to upload a photo', async () => {
    const page = await browser.newPage()

    await loginUser(page)

    // navigate to upload page
    const linkUpload = await page.waitForSelector(
      'a[data-testid="header--/upload"]',
    )
    await linkUpload.click()

    // select file and fill in values
    const input = await page.$('input[type=file]')
    await input.uploadFile('e2e/src/testphoto.JPG')

    const buttonNext1 = await page.waitForSelector(
      'button[data-testid="upload--button--next"]:not([disabled])',
    )
    await buttonNext1.click()

    const title = await page.waitForSelector('#title')
    await title.click()
    await title.type('My cool photo')

    const desc = await page.waitForSelector('#desc')
    await desc.click()
    await desc.type('This photo is awesome because...')

    const buttonNext2 = await page.waitForSelector(
      'button[data-testid="upload--button--next"]:not([disabled])',
    )
    await buttonNext2.click()

    await page.waitForSelector('[data-testid="upload--tags--container"]')

    const tags = await page.waitForSelector('input[type=text]')
    await tags.click()
    await tags.type('Testphoto')

    const buttonTags = await page.waitForSelector(
      '[data-testid="containedButton"]',
    )
    await buttonTags.click()

    // click submit and wait for navigation
    await Promise.all([
      page.click('[data-testid="upload--button--submit"]'),
      page.waitForNavigation({ waitUntil: 'networkidle0' }),
    ])

    await page.waitForSelector('[data-testid="photo--headline"]')

    // check that now on photo page
    expect(page.url()).toMatch(`${baseUrl}/photos/`)
    const headline = await page.$eval('h1', e => e.innerHTML)
    expect(headline).toEqual('My cool photo')

    await page.close()
  }, 900000)
  xit('user wants to edit a photo', async () => {
    const page = await browser.newPage()

    await loginUser(page)

    // TODO make a photo available by default (without upload) (copy from test above)
    const linkUpload = await page.waitForSelector(
      'a[data-testid="header--/upload"]',
    )
    await linkUpload.click()
    const input = await page.$('input[type=file]')
    await input.uploadFile('e2e/src/testphoto.JPG')
    const buttonNext1 = await page.waitForSelector(
      'button[data-testid="upload--button--next"]:not([disabled])',
    )
    await buttonNext1.click()
    const title = await page.waitForSelector('#title')
    await title.click()
    await title.type('My cool photo')
    const desc = await page.waitForSelector('#desc')
    await desc.click()
    await desc.type('This photo is awesome because...')
    const buttonNext2 = await page.waitForSelector(
      'button[data-testid="upload--button--next"]:not([disabled])',
    )
    await buttonNext2.click()
    await page.waitForSelector('[data-testid="upload--tags--container"]')
    const tags = await page.waitForSelector('input[type=text]')
    await tags.click()
    await tags.type('Testphoto')
    const buttonTags = await page.waitForSelector(
      '[data-testid="containedButton"]',
    )
    await buttonTags.click()
    await Promise.all([
      page.click('[data-testid="upload--button--submit"]'),
      page.waitForNavigation({ waitUntil: 'networkidle0' }),
    ])
    expect(page.url()).toMatch(`${baseUrl}/photos/`)

    page
      .on('console', message =>
        console.log(
          `${message
            .type()
            .substr(0, 3)
            .toUpperCase()} ${message.text()}`,
        ),
      )
      .on('pageerror', ({ message }) => console.log(message))

    // navigate to edit page
    const buttonEdit = await page.waitForSelector(
      '[data-testid="navigationIconButton--button"]',
    )
    await Promise.all([
      buttonEdit.click(),
      page.waitForNavigation({ waitUntil: 'networkidle0' }),
    ])
    
    await page.waitForSelector('[data-testid="photoEdit"]')

    // fill in values
    const newTitle = await page.waitForSelector('#title')
    await newTitle.click({ clickCount: 3 })
    await newTitle.type('A new title')

    const newTemp = await page.waitForSelector('#temp')
    await newTemp.click({ clickCount: 3 })
    await newTemp.type('20')

    const newMake = await page.waitForSelector('#make')
    await newMake.click({ clickCount: 3 })
    await newMake.type('Sony')

    const newFNumber = await page.waitForSelector('#fNumber')
    await newFNumber.click({ clickCount: 3 })
    await newFNumber.type('f/2.8')

    const buttonSubmit = await page.waitForSelector(
      '[data-testid="photoEdit--submitButton"]:not([disabled])',
    )

    // click submit and wait for navigation
    await Promise.all([
      buttonSubmit.click(),
      page.waitForNavigation({ waitUntil: 'networkidle0' }),
    ])

    await page.waitForSelector('[data-testid="photo--headline"]')
    await page.waitForFunction('document.querySelector("h1")')
    //await page.waitForFunction('document.querySelector("h1").innerText === "A new title')

    // check that values have been changed
    const headline = await page.$eval('h1', e => e.innerHTML)
    expect(headline).toEqual('A new title')

    const weatherTile = await page.$eval(
      '[data-testid="tileWeather--content"]',
      e => e.textContent,
    )
    expect(weatherTile).toContain('20 °C')

    const tiles = page.$$('[data-testid="tile"]')
    expect(tiles[3].textContent).toContain('Make: Sony')
    expect(tiles[3].textContent).toContain('fNumber: f/2.8')

    await page.close()
  }, 900000)
  it('user wants to edit profile', async () => {
    const page = await browser.newPage()

    await loginUser(page, true)

    // navigate to edit page
    const buttonEdit = await page.waitForSelector(
      '[data-testid="navigationIconButton--button"]',
    )
    await buttonEdit.click()

    // fill in values
    const displayName = await page.waitForSelector('#displayName')
    // there is already input --> click three times to select it
    await displayName.click({ clickCount: 3 })
    await displayName.type('A_new_displayName')

    const bio = await page.waitForSelector('#bio')
    await bio.click({ clickCount: 3 })
    await bio.type('My awesome bio')

    const button = await page.waitForSelector(
      'button[type="submit"]:not([disabled])',
    )

    // click submit and wait for navigation
    await Promise.all([
      button.click(),
      page.waitForNavigation({ waitUntil: 'networkidle0' }),
    ])

    // check that values have been changed
    const headline = await page.evaluate(
      () => document.querySelector('h1').textContent,
    )
    expect(headline).toEqual('A_new_displayName')

    const bioOnPage = await page.evaluate(
      () => document.querySelector('[data-testid="user--desc"]').textContent,
    )
    expect(bioOnPage).toEqual('My awesome bio')

    await page.close()
  }, 900000)
  it('user wants to log out', async () => {
    const page = await browser.newPage()

    await loginUser(page)

    // open the menu and wait for links
    const linkOpenMenu = await page.waitForSelector(
      'a[data-testid="header--func"]',
    )
    await linkOpenMenu.click()

    await page.waitForSelector('.profileMenu.show')
    const linkLogout = await page.waitForSelector(
      'a[data-testid="profileMenuEntry--func"]',
    )

    // click link and wait for navigation
    await Promise.all([
      linkLogout.click(),
      page.waitForNavigation({ waitUntil: 'networkidle0' }),
    ])

    // check that login link is displayed
    const linkOpenMenu2 = await page.waitForSelector(
      'a[data-testid="header--func"]',
    )
    await linkOpenMenu2.click()

    await page.waitForSelector('.profileMenu.show')

    const loginLink = await page.waitForSelector(
      'a[data-testid="profileMenuEntry--/login"]',
    )
    expect(loginLink).toBeTruthy()

    await page.close()
  }, 900000)
})
