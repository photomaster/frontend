import { baseUrl } from '../helpers'

const date = new Date()
const TestUsername = `Testuser${date.getTime()}`
const TestDisplayName = `Testuser`
const TestEmail = `${date.getTime()}@fakemail.com`
const TestPassword = '@_T0p_$ecreT_p@ssw0rd'

const puppeteer = require('puppeteer')

let browser
beforeAll(async done => {
  browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  })
  done()
})

afterAll(async done => {
  await browser.close()

  done()
})
describe('Sign up', () => {
  it('disables the submit button before filling in the form', async () => {
    const page = await browser.newPage()

    await page.goto(`${baseUrl}/register`)

    await page.waitForSelector('button[type="submit"]')
    const submitButtonDisabled = await page.$eval(
      'button[type="submit"]',
      button => button.disabled,
    )
    expect(submitButtonDisabled).toBeTruthy()

    await page.close()
  }, 900000)
  it('enables the submit button if all is valid', async () => {
    const page = await browser.newPage()

    await page.goto(`${baseUrl}/register`)

    // fill in values
    const username = await page.waitForSelector('#username')
    await username.click()
    await username.type(TestUsername)

    const displayName = await page.waitForSelector('#displayName')
    await displayName.click()
    await displayName.type(TestDisplayName)

    const email = await page.waitForSelector('#email')
    await email.click()
    await email.type(TestEmail)

    const password = await page.waitForSelector('#password')
    await password.click()
    await password.type(TestPassword)

    const password2 = await page.waitForSelector('#password2')
    await password2.click()
    await password2.type(TestPassword)

    // button should now be enabled
    await page.waitForSelector('button[type="submit"]:not([disabled])')
    const submitButtonDisabled = await page.$eval(
      'button[type="submit"]',
      button => button.disabled,
    )
    expect(submitButtonDisabled).toBeFalsy()

    await page.close()
  }, 900000)
  it('enables the user to register', async () => {
    const page = await browser.newPage()

    await page.goto(`${baseUrl}/register`)

    // fill in values
    const username = await page.waitForSelector('#username')
    await username.click()
    await username.type(TestUsername)

    const displayName = await page.waitForSelector('#displayName')
    await displayName.click()
    await displayName.type(TestDisplayName)

    const email = await page.waitForSelector('#email')
    await email.click()
    await email.type(TestEmail)

    const password = await page.waitForSelector('#password')
    await password.click()
    await password.type(TestPassword)

    const password2 = await page.waitForSelector('#password2')
    await password2.click()
    await password2.type(TestPassword)

    const button = await page.waitForSelector(
      'button[type="submit"]:not([disabled])',
    )

    // click submit and wait for navigation
    await Promise.all([
      button.click(),
      page.waitForNavigation({ waitUntil: 'networkidle0' }),
    ])

    // check that it's really the user page and displaying the typed in values
    expect(page.url()).not.toEqual('http://127.0.0.1:3000/register')
    const headline = await page.$eval('h1', e => e.innerHTML)
    expect(headline).toEqual(TestDisplayName)

    await page.close()
  }, 900000)
})
