# Client Side Web Engineering: About our Projekt

## Welches Problem löst unser Projekt?
Fotografen suchen schöne und für sie neue Orte, an denen sie Fotos schießen können. Sie werden inspiriert durch die hochgeladenen Fotos anderer UserInnen. 
Zu jedem Foto/Ort werden nützliche Zusatzinformationen angezeigt, wie die genaue Location, das Wetter zum Aufnahmezeitpunkt bzw. an dieser Location und die Zeiten des Sonnenauf- und -untergangs. Das soll dem Fotografen helfen, Fotos besser zu verstehen und seine Fähigkeiten zu verbessern. Außerdem können über Fotos, die dem User gefallen, neue, nicht bekannte Orte in seiner Umgebung empfohlen werden. Die gesammelten Daten dienen auch als Suchkriterien.

### Wer benutzt das?
* Hobby- und semiprofessionelle FotografInnen
* Social Media InfluencerInnen
* WanderInnen

### Auf welchen Devices?
Desktop und Smartphone (Web-App). Fokus liegt aber auf Smartphone.

### In welcher Situation (Sprache, Umgebung, Licht,…) wird es benutzt?
im Freien
* Sprachen sind Deutsch und Englisch
* Wetter hat Einfluss auf die Benutzung auf dem Smartphone, da Fotos in unterschiedlichen Bedingungen geschossen werden können (Sonnenschein, Regen, Nebel, ...)
* Smartphones haben unterschiedliche Größen und Auflösungen
* wir vermuten, dass Tablets nicht dazu genutzt werden, um Fotos aufzunehmen. Allerdings könnten sie benutzt werden, um alle anderen Funktionen der Applikation zu nutzen (Suche nach Orten, Fotos betrachten, ...).

in Räumen
* wir gehen von stabilen Bedingungen bei der Benutzung via Desktop aus: helle Lichtverhältnisse, FullHD Auflösung, ...

### Gab es schon Tests mit echten Usern? Wenn ja:
#### Mit welcher Methode?
Usability-Test, die UserInnen bekamen den Task ein Foto hochzuladen und dabei den Screen zu sharen. So konnten wir verfolgen wie sie dabei vorgingen, ob etwas unklar war und ob es ohne Probleme funktioniert hat. Anschließend haben wir sie dazu befragt und zusätzliches Feedback eingeholt wie Feature-Wünsche. 

Wenn noch Zeit übrig blieb baten wir sie zudem einen Blick auf die noch unfertigen Teile des Projektes zu werfen und zu kommentieren. Hier sind wir jedoch nicht anhand eines Leitfadens oder Ähnlichem vorgegangen. 

#### Wer hat getestet?
* 4 MMT-StudentInnen aus den unteren Jahrgängen
* 1 Person aus Deutschland, die über Social Media auf das User-Testing aufmerksam wurde

---

In einer anderen Lehrveranstaltung mussten wir unser Projekt sehr ausführlich beschreiben, falls dir noch etwas unklar an unserem Konzept ist kannst du es dir gerne [hier](https://drive.google.com/open?id=1_NJon3ZHq7gP4arblHAJK1WvAzDHO9bt) durchlesen.

---

*Hinweis: Vermutlich war es unser Projekt, dass du über die URL von der FH nicht öffnen konntest, da gibt es Probleme mit der Firewall. Du kannst dir das Projekt hier ansehen: https://photomaster.rocks/*
