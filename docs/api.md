# Photomaster Client Snippets

## Fotos

### Exif-Daten laden
```javascript
const photo = await photoService.getPhoto(id)
try {
    const photoData = await photo.photoData
} catch(e) {
    // es gibt keine Exif-Daten
}
```

### Exif-Daten ändern und speichern
```javascript
const photoData = await photo.photoData
photoData.model = "fancy camera"
photoData.make = "Nikkon"
await photoData.save()
```

## Lieblingsfotos

### ein Foto merken

```javascript
const photo = await photoService.getPhoto(42)
const favorite = await photoService.addPhotoToFavorites(photo)
```

### alle löschen

```javascript
const favorites = await photoService.getFavoritePhotos()
favorites.forEach(favorite => {
    await favorite.delete()
})
```

### ein bestimmtes Lieblingsfoto löschen
```javascript
const photo = await photoService.getPhoto(2776)
const favorites = await photoService.getFavoritePhotos()

const favorite = favorites.find(f => f.photo.id === photo.id)
if (favorite) {
    favorite.delete()
}
```

### Fotos sortieren

#### nach Likes
```javascript
const photos = await photoService.getPhotos({
    ordering: 'popularity'
})
```

#### aufsteigend nach Datum
```javascript
const photos = await photoService.getPhotos({
    ordering: 'originalCreationDate'
})
```

#### absteigend nach Datum
```javascript
const photos = await photoService.getPhotos({
    ordering: '-originalCreationDate'
})
```