# UI-Probleme PhotoMaster

1. Welche UI-Probleme hat unser Projekt?

    1. Ist es benutzbar?

        * Die Applikation ist grundsätzlich benutzbar, die Testuser kommen im Allgemeinen gut damit zurecht. Manche kleinere Details, wie die Möglichkeit das Banner zu editieren, werden jedoch nicht bemerkt. 
    1. Ist die Benutzung klar? Kann sich kein User verlaufen?

        * Suchfunktion nach oben in die Menüleiste

        * Vielleicht noch mehr placeholder einbauen (ich glaube der Unterschied zwischen Username und Display Name war nicht jedem klar, bzw. was einzigartig sein soll und was nicht)
        * Profil bearbeiten => Banner nicht wahrgenommen (Icon/Hinweis vielleicht in die Mitte), "change banner" Hinweis
        * Ausschnitt des Hintergrunds selbst auswählen
    1. Verliert der User Zeit oder muss nachdenken?
        * Login/Logout-Button durch eindeutige Buttons ersetzen
    1. Wird der User belohnt, erfreut, emotional positiv verstärkt?
        * Eigentlich nicht, die visuellen Rückmeldungen sind alle eher negativer Natur (Fehler-Hinweis bei Eingabefeldern, Fehler-Hinweis bei Login, Ladeicon, …)
1. Welche Priorität haben diese Probleme?
    1. strukturelle Probleme
        * Header hüpft manchmal
        * Upload-Form: Title und Desc hüpfen wenn Fehlermeldung angezeigt wird
        * Drag&Drop bei Uploadformular ermöglichen
        * aktuell viele Duplizierungen von Klassen (z. B. ‘center’) → häufig verwendete Klassen zentral einmalig definieren und importieren
    1. visuelle Probleme
        * Kontrast etwas zu niedrig bei RecommendationCards
        * “Schritte” auf Homepage werden als Buttons interpretiert
        * optisch ansprechende Karte verwenden
        * vorausgefüllte Labels im Uploadform werden nicht wahrgenommen
1. Wie lösen wir diese Probleme?
        * strukturelle/technische Probleme lassen sich relativ leicht lösen
        * die visuellen Probleme können auch angepasst werden, allerdings müssen wir danach wieder Testen, ob es als Verbesserung wahrgenommen wird. Durch das User-Testing erfahren wir nur was nicht gut ist, aber nicht auf welche Weise es genau zu ändern ist bzw. ob die Vorschläge einer Person auch für andere Personen gelten.  
1. Aufwandsschätzung
    * 1-2 Tage
