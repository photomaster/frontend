FROM buildkite/puppeteer

ENV PATH="${PATH}:/node_modules/.bin"
ENV NODE_ENV=production

RUN apt-get update && \
    apt-get install -yf curl && \
    rm -rf /var/lib/apt/lists/*

RUN curl -o /usr/local/bin/wait-for-it.sh https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh && \
    chmod +x /usr/local/bin/wait-for-it.sh 

RUN mkdir /code
WORKDIR /code

COPY package.json /code
COPY package-lock.json /code
RUN npm ci && npm i jest-puppeteer

COPY . /code

ENTRYPOINT ["/code/docker-builder-entrypoint.sh"]