import { userActions } from 'actions'

let buffer = []

export const jwt = store => next => action => {
  buffer.push(action)
  if (action.type === 'apiCall/invalidToken') {
    const theStore = store.getState()
    if (theStore.currentUser.loggedIn) {
      if (!theStore.currentUser.pendingRefreshToken) {
        console.info('refreshing is not pending')
        store.dispatch(userActions.refreshToken()).then(() => {
          const pos =
            buffer.map(e => e.type).indexOf('apiCall/invalidToken') - 1
          for (let i = pos; i > -1; i -= 1) {
            if (typeof buffer[i] === 'function') {
              store.dispatch({
                type: 'resend',
                action: buffer[i](store.dispatch),
              })
            }
            if (
              typeof buffer[i] !== 'function' &&
              buffer[i].type === 'refreshToken/success'
            ) {
              break
            }
          }
          buffer = []
        })
      }
    }
  } else if (action.type === 'refreshToken/expired') {
    buffer = []
    store.dispatch(userActions.logout())
  } else {
    if (buffer.length > 20) {
      buffer.splice(0, buffer.length - 20)
    }
    return next(action)
  }
}

export default jwt
