import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import * as Sentry from '@sentry/react'
import { Integrations } from '@sentry/tracing'
import './index.scss'
import { Provider } from 'react-redux'
import App from './App'
// eslint-disable-next-line no-unused-vars
import i18nextConfig from './i18n'
import store from './store'
import * as serviceWorker from './serviceWorker'

import 'abortcontroller-polyfill/dist/abortcontroller-polyfill-only'

if (process !== undefined && process.env.NODE_ENV === 'production') {
  Sentry.init({
    dsn:
      'https://5cd6994cfa59493aba7c03d068c8d026@o478288.ingest.sentry.io/5520778',
    integrations: [new Integrations.BrowserTracing()],

    tracesSampleRate: 1.0,
  })
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()

ReactDOM.render(
  <Suspense fallback={null}>
    <Provider store={store}>
      <App />
    </Provider>
  </Suspense>,
  document.getElementById('root'),
)
