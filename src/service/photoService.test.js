import { PhotoService, DEFAULT_LIMIT } from './photoService'
import Photo from 'models/photo'
import PhotoData from 'models/photoData'
import User from 'models/user'

describe('PhotoService', () => {
  it('should load photos from api', async () => {
    const mockPhotoProperties = [
      { id: 1, name: 'Image-1' },
      { id: 2, name: 'Image-2' },
      { id: 3, name: 'Image-3' },
    ]

    const fetchMock = jest.fn().mockImplementation((path, init) => {
      return Promise.resolve({
        ok: true,
        json: () => {
          return mockPhotoProperties.map(p => {
            return { ...p }
          })
        },
      })
    })

    const photoService = new PhotoService(fetchMock)
    const photos = await photoService.getPhotos()
    expect(photos).toHaveLength(3)
    expect(photos.filter(p => p instanceof Photo)).toHaveLength(3)
    photos.forEach((photo, i) => {
      expect(photo).toEqual(expect.objectContaining(mockPhotoProperties[i]))
    })
  })

  it('should load photos from api using the assigned parameters', async () => {
    const fetchMock = jest.fn().mockImplementation((path, init) => {
      return Promise.resolve({
        ok: true,
        json: () => {
          return []
        },
      })
    })
    const photoService = new PhotoService(fetchMock)

    await photoService.getPhotos({
      limit: 10,
      offset: 5,
    })
    expect(fetchMock.mock.calls[0][0]).toContain('limit=10')
    expect(fetchMock.mock.calls[0][0]).toContain('offset=5')

    await photoService.getPhotos({
      limit: 10,
    })
    expect(fetchMock.mock.calls[1][0]).toContain('limit=10')
    expect(fetchMock.mock.calls[1][0]).toContain('offset=0')

    await photoService.getPhotos({
      offset: 99,
    })
    expect(fetchMock.mock.calls[2][0]).toContain('limit=' + DEFAULT_LIMIT)
    expect(fetchMock.mock.calls[2][0]).toContain('offset=99')

    await photoService.getPhotos({
      labels: [1, 2, 3, 4, 5],
    })
    expect(fetchMock.mock.calls[3][0]).toContain(
      'labels=' + encodeURIComponent('1,2,3,4,5'),
    )

    await photoService.getPhotos({
      labels: 'Star Wars,Foo&Bar',
    })
    expect(fetchMock.mock.calls[4][0]).toContain('labels=Star+Wars%2CFoo%26Bar')
  })

  it('should combine attributes returned from the api with local attributes after initially saving the object', async () => {
    const fetchMock = jest.fn().mockImplementation((path, init) => {
      return Promise.resolve({
        ok: true,
        json: () => {
          if (init.method === 'POST') {
            return {
              id: 42,
              name: 'say my name',
              labels: [
                { title: 'Snow', score: 1 },
                { title: 'Space Ship', score: 1 },
                { title: 'Star Wars', score: 1 },
              ],
              image: 'http://localhost/test.png',
              user: null,
              visibility: 1,
            }
          }

          return {
            ...JSON.parse(init.body),
            image: 'http://localhost/test.png',
          }
        },
      })
    })
    const photoService = new PhotoService(fetchMock)
    const initialData = {
      name: 'hello there',
      visibility: 3,
      image: new File(
        [
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5AMUEzQZGPsPMgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAMSURBVAjXY2BgYAAAAAQAASc0JwoAAAAASUVORK5CYII=',
        ],
        'test.png',
        {
          type: 'image/png',
        },
      ),
    }

    const photo = await photoService.save(initialData)
    expect(photo.id).toBeGreaterThan(0)
    expect(photo.name).toEqual(initialData.name)
    expect(photo.labels).toHaveLength(3)
    expect(photo.visibility).toEqual(initialData.visibility)
    expect(photo.image).toMatch(/.+\/test\.png/)
  })

  it('should load photos of a user', async () => {
    const fetchMock = jest.fn().mockImplementation((path, init) => {
      return Promise.resolve({
        ok: true,
        json: () => {
          return []
        },
      })
    })
    const photoService = new PhotoService(fetchMock)

    await photoService.getPhotosOfUser(5, {
      limit: 10,
      offset: 5,
      labels: [1, 2, 3, 4, 5],
    })
    expect(fetchMock.mock.calls[0][0]).toContain('/users/5/')
    expect(fetchMock.mock.calls[0][0]).toContain('limit=10')
    expect(fetchMock.mock.calls[0][0]).toContain('offset=5')
    expect(fetchMock.mock.calls[0][0]).toContain(
      'labels=' + encodeURIComponent('1,2,3,4,5'),
    )

    await photoService.getPhotosOfUser(
      new User({
        id: 1337,
        name: 'Obi-Wan Kenobi',
      }),
    )
    expect(fetchMock.mock.calls[1][0]).toContain('/users/1337/')
  })

  it('should load solar data of a photo', async () => {
    const mockData = {
      sunrise: '2020-09-22T04:55:54+00:00',
      sunset: '2020-09-22T17:04:36+00:00',
      solarNoon: '2020-09-22T11:00:15+00:00',
      dayLength: 43722,
      civilTwilightBegin: '2020-09-22T04:25:09+00:00',
      civilTwilightEnd: '2020-09-22T17:35:21+00:00',
      nauticalTwilightBegin: '2020-09-22T03:48:51+00:00',
      nauticalTwilightEnd: '2020-09-22T18:11:38+00:00',
      astronomicalTwilightBegin: '2020-09-22T03:48:51+00:00',
      astronomicalTwilightEnd: '2020-09-22T18:11:38+00:00',
      date: '2013-02-25',
    }
    const fetchMock = jest.fn().mockImplementation((path, init) => {
      return Promise.resolve({
        ok: true,
        json: () => {
          return mockData
        },
      })
    })
    const photoService = new PhotoService(fetchMock)

    const solarData = await photoService.getSolarData(42)
    expect(mockData).toEqual(solarData)
    expect(fetchMock).toHaveBeenCalledTimes(1)
    expect(fetchMock.mock.calls[0][0]).toMatch(/\/42\//)
    expect(fetchMock.mock.calls[0][0]).not.toMatch(/date=/)
    expect(fetchMock.mock.calls[0][1].method.toUpperCase()).toBe('GET')

    await photoService.getSolarData(43, null, 'de')
    expect(fetchMock).toHaveBeenCalledTimes(2)
    expect(fetchMock.mock.calls[1][0]).toContain('locale=de')

    await photoService.getSolarData(43, new Date('1995-12-17T03:24:00'))
    expect(fetchMock).toHaveBeenCalledTimes(3)
    expect(fetchMock.mock.calls[2][0]).toMatch(/date=1995-12-17/)
  })

  it('should load photoData', async () => {
    const fetchMock = jest.fn().mockImplementation((path, init) => {
      return Promise.resolve({
        ok: true,
        json: () => {
          return {
            url: 'http://localhost:8080/api/photosData/1/',
            photo: 'http://localhost:8080/api/photos/1/',
            make: 'Apple Inc.',
            model: 'Apple iPhone 42',
          }
        },
      })
    })
    const photo = new Photo({
      id: 1,
      photodata: 'http://localhost:8080/api/photosData/1/',
    })
    const photoService = new PhotoService(fetchMock)
    const photoData = await photoService.getPhotoData(photo)
    expect(photoData).toBeInstanceOf(PhotoData)
    expect(photoData).toMatchObject({
      make: 'Apple Inc.',
      model: 'Apple iPhone 42',
    })
  })

  describe('getNearPhotos', () => {
    it('should load nearby photos from api', async () => {
      const mockPhotoProperties = [
        { id: 1, name: 'Image-1' },
        { id: 2, name: 'Image-2' },
      ]

      const fetchMock = jest.fn().mockImplementation(() => {
        return Promise.resolve({
          ok: true,
          json: () => mockPhotoProperties,
        })
      })

      const photoService = new PhotoService(fetchMock)
      const photos = await photoService.getNearPhotos(13, 47)

      expect(fetchMock).toHaveBeenCalledTimes(1)
      expect(photos).toHaveLength(mockPhotoProperties.length)

      expect(photos[0]).toBeInstanceOf(Photo)
      expect(photos[1]).toBeInstanceOf(Photo)

      expect(photos[0]).toEqual(expect.objectContaining(mockPhotoProperties[0]))
      expect(photos[1]).toEqual(expect.objectContaining(mockPhotoProperties[1]))
    })

    it('should include lat and long parameters', async () => {
      const fetchMock = jest.fn().mockImplementation(() => {
        return Promise.resolve({
          ok: true,
          json: () => [],
        })
      })
      const lat = 13
      const lon = 47

      const photoService = new PhotoService(fetchMock)
      await photoService.getNearPhotos(lat, lon)

      expect(fetchMock.mock.calls[0][0]).toContain('lat=' + lat)
      expect(fetchMock.mock.calls[0][0]).toContain('long=' + lon)
    })

    it('should include a default distance parameter', async () => {
      const fetchMock = jest.fn().mockImplementation(() => {
        return Promise.resolve({
          ok: true,
          json: () => [],
        })
      })

      const photoService = new PhotoService(fetchMock)
      await photoService.getNearPhotos(1, 2)

      expect(fetchMock.mock.calls[0][0]).toMatch(/distance=\d+/)
    })

    it('should include a distance parameter if provided with a custom distance', async () => {
      const fetchMock = jest.fn().mockImplementation(() => {
        return Promise.resolve({
          ok: true,
          json: () => [],
        })
      })

      const photoService = new PhotoService(fetchMock)
      await photoService.getNearPhotos(1, 2, 42.99)

      expect(fetchMock.mock.calls[0][0]).toContain('distance=42.99')
    })
  })
})
