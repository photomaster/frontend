import User from 'models/user'
import authService from 'service/authService'
import { ApiError } from './baseService'

afterEach(() => {
  global.fetch.mockClear()
  authService.setTokens(null)
  localStorage.clear()
  sessionStorage.clear()
})

describe('AuthService', () => {
  it('correctly creates user model from response', async () => {
    expect.assertions(2)

    const userMock = {
      id: 1,
      username: 'username',
      email: 'test@test.de',
      displayName: 'Test Name',
    }
    const mockSuccessResponse = {
      access: 'token1234',
      refresh: 'refreshToken1234',
    }

    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            json: () => userMock,
          }),
        )
        return Promise.resolve(mockSuccessResponse)
      },
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)

    const user = await authService.login('user', 'password')
    expect(user).toBeInstanceOf(User)
    expect(user._props).toEqual(userMock)
  })

  it('rejects login if profile cannot be loaded', async () => {
    expect.assertions(1)

    const mockSuccessResponse = {
      access: 'token1234',
      refresh: 'refreshToken1234',
    }

    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: false,
          }),
        )
        return Promise.resolve(mockSuccessResponse)
      },
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)

    try {
      await authService.login('user', 'password')
    } catch (e) {
      expect(e).toBeInstanceOf(Error)
    }
  })

  it('rejects login if credentials are invalid', async () => {
    expect.assertions(1)
    const mockFetchPromise = Promise.resolve({
      ok: false,
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)

    try {
      await authService.login('user', 'password')
    } catch (e) {
      expect(e).toBeInstanceOf(Error)
    }
  })

  it('injects authentication header into every request after a successful login', async () => {
    expect.assertions(4)

    const tokenResponseMock = {
      access: 'token1234',
      refresh: 'refreshToken1234',
    }

    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            json: () => {},
          }),
        )
        return Promise.resolve(tokenResponseMock)
      },
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)

    await authService.login('user', 'password')
    await authService.loadData('/')
    await authService.loadData('/foobar')

    // previous two calls
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers.Authorization,
    ).toEqual(`Bearer ${tokenResponseMock.access}`)
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 2][1].headers.Authorization,
    ).toEqual(`Bearer ${tokenResponseMock.access}`)
    // profile call
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 3][1].headers.Authorization,
    ).toEqual(`Bearer ${tokenResponseMock.access}`)
    // login call
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 4][1].headers.Authorization,
    ).toBeUndefined()
  })

  it('successfully registers a user', async () => {
    expect.assertions(10)

    const registerParams = {
      username: 'user1',
      password: '1234',
      email: 'test@test.at',
    }

    const tokenResponseMock = {
      access: 'token1234',
      refresh: 'refreshToken1234',
    }

    const userMock = {
      id: 42,
      ...registerParams,
    }

    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            json: () => tokenResponseMock,
          }),
        )
        return Promise.resolve(userMock)
      },
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)

    expect(authService.isSignedIn()).toBeFalsy()
    const user = await authService.register(registerParams)

    expect(user).toBeInstanceOf(User)
    expect(user.username).toEqual(userMock.username)
    expect(user.email).toEqual(userMock.email)
    expect(user.id).toEqual(userMock.id)

    await authService.loadData('/')
    await authService.loadData('/foobar')

    // previous two calls
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers.Authorization,
    ).toEqual(`Bearer ${tokenResponseMock.access}`)
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 2][1].headers.Authorization,
    ).toEqual(`Bearer ${tokenResponseMock.access}`)
    // token call
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 3][1].headers.Authorization,
    ).toEqual(`Bearer ${tokenResponseMock.access}`)
    // register call
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 4][1].headers.Authorization,
    ).toBeUndefined()

    expect(authService.isSignedIn()).toBeTruthy()
  })

  it('saves token to local storage if remember me function is enabled', async () => {
    expect.assertions(1)

    const userMock = {
      id: 1,
      username: 'username',
      email: 'test@test.de',
      displayName: 'Test Name',
    }
    const mockSuccessResponse = {
      access: 'token1234',
      refresh: 'refreshToken1234',
    }

    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            json: () => userMock,
          }),
        )
        return Promise.resolve(mockSuccessResponse)
      },
    })

    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)
    await authService.login('foo', 'bar', true)
    expect(localStorage.getItem('refresh-token')).toEqual(
      mockSuccessResponse.refresh,
    )
  })

  it('does not save token to local storage if remember me function is disabled', async () => {
    expect.assertions(1)

    const userMock = {
      id: 1,
      username: 'username',
      email: 'test@test.de',
      displayName: 'Test Name',
    }
    const mockSuccessResponse = {
      access: 'token1234',
      refresh: 'refreshToken1234',
    }

    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            json: () => userMock,
          }),
        )
        return Promise.resolve(mockSuccessResponse)
      },
    })

    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)
    await authService.login('foo', 'bar')
    expect(localStorage.getItem('token')).toBeNull()
  })

  it('saves token to session storage after logging in', async () => {
    expect.assertions(3)

    const userMock = {
      id: 1,
      username: 'username',
      email: 'test@test.de',
      displayName: 'Test Name',
    }
    const mockSuccessResponse = {
      access: 'token1234',
      refresh: 'refreshToken1234',
    }

    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            json: () => userMock,
          }),
        )
        return Promise.resolve(mockSuccessResponse)
      },
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)

    expect(JSON.parse(sessionStorage.getItem('refresh-token'))).toEqual(null)
    await authService.login('foo', 'bar', true)
    expect(sessionStorage.getItem('refresh-token')).toEqual(
      mockSuccessResponse.refresh,
    )
    expect(sessionStorage.getItem('token')).toEqual(mockSuccessResponse.access)
  })

  it('refreshes access token', async () => {
    expect.assertions(2)

    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () =>
        Promise.resolve({
          access: 'refreshedToken',
        }),
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)

    localStorage.setItem('refresh-token', 'refreshToken')
    await authService.refreshToken()
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers.Authorization,
    ).toBeUndefined()
    expect(sessionStorage.getItem('token')).toEqual('refreshedToken')
  })

  it('fetches the user object if a token is present', async () => {
    const userMock = {
      username: 'foobar123',
      displayName: 'Foo Bar',
      email: 'test@test.at',
    }
    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => Promise.resolve(userMock),
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)

    const user = await authService.getUser()
    expect(user._props).toEqual(expect.objectContaining(userMock))
  })

  it('rejects promise if no token was set', async () => {
    expect.assertions(1)
    const mockFetchPromise = Promise.resolve({
      ok: false,
      json: () => {
        return {
          detail: 'authentication required',
        }
      },
    })
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise)

    try {
      await authService.getUser()
    } catch (e) {
      expect(e).toBeInstanceOf(ApiError)
    }
  })
})
