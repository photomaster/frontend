import { configureRefreshFetch } from 'refresh-fetch'
import { history } from 'helpers/history'
import { buildUrl } from './routes'

const retrieveToken = () => sessionStorage.getItem('token')
const retrieveRefreshToken = () =>
  sessionStorage.getItem('refresh-token') ??
  localStorage.getItem('refresh-token')

const saveToken = token => {
  if ('refresh' in token) {
    sessionStorage.setItem('refresh-token', token.refresh)
  }
  sessionStorage.setItem('token', token.access)
}
const clearToken = () => {
  sessionStorage.removeItem('token')
  sessionStorage.removeItem('refresh-token')
  localStorage.removeItem('refresh-token')
}

class ResponseError extends Error {
  constructor(status, response, body) {
    super('ResponseError')
    this.name = 'ResponseError'
    this.status = status
    this.response = response
    this.body = body
  }
}

const fetchJSONAdapter = async (url, optionsWithToken) => {
  const response = await fetch(url, {
    headers: {
      'Content-Type': 'application/json',
    },
    ...optionsWithToken,
  })
  const body = response.status === 204 ? null : await response.json()
  if (response.ok) {
    return {
      ok: true,
      response: response,
      body: body,
      json: () => Promise.resolve(body),
    }
  }
  throw new ResponseError(response.status, response, body)
}

const fetchJSONWithToken = (url, options = {}) => {
  const token = retrieveToken()

  let optionsWithToken = options
  if (token != null) {
    optionsWithToken = {
      ...options,
      headers: {
        ...('headers' in options ? options.headers : {}),
        Authorization: `Bearer ${token}`,
      },
    }
  }

  return fetchJSONAdapter(url, optionsWithToken)
}

const shouldRefreshToken = error => {
  if (error.name === 'AbortError') {
    return false
  }

  return (
    error.response &&
    error.response.status === 401 &&
    error.body &&
    error.body.code === 'token_not_valid'
  )
}

const refreshToken = () => {
  sessionStorage.removeItem('token')
  return fetchJSONWithToken(buildUrl('/api/auth/refresh/'), {
    method: 'POST',
    body: JSON.stringify({ refresh: retrieveRefreshToken() }),
  })
    .then(r => r.json())
    .then(response => {
      saveToken(response)
    })
    .catch(error => {
      console.error('refresh error', error)
      clearToken()
      history.push({ pathname: `/`, state: { logout: true } })
      throw error
    })
}

const fetchPatched = configureRefreshFetch({
  fetch: fetchJSONWithToken,
  shouldRefreshToken: shouldRefreshToken,
  refreshToken: refreshToken,
})

export { refreshToken }

export default fetchPatched
