import { AutocompleteService } from './autocompleteService'

describe('AutocompleteService', () => {
  it('uses array data from fetch without modifing it.', async () => {
    const mockReturn = ['a', 'b', 'c']
    const fetchMock = jest.fn(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(mockReturn),
      }),
    )
    const service = new AutocompleteService(fetchMock)

    const suggestions = await service.getSuggestions('foobar')

    expect(suggestions).toEqual(expect.arrayContaining(['a', 'b', 'c']))
    expect(suggestions).toHaveLength(mockReturn.length)
  })

  it('includes query parameter when fetching suggestions from server.', async () => {
    const fetchMock = jest.fn(() =>
      Promise.resolve({ ok: true, json: () => {} }),
    )
    const service = new AutocompleteService(fetchMock)

    service.getSuggestions('foobar')

    expect(fetchMock.mock.calls[0][0]).toContain('q=foobar')
  })

  it('includes an empty query parameter when fetching suggestions from server.', async () => {
    const fetchMock = jest.fn(() =>
      Promise.resolve({ ok: true, json: () => {} }),
    )
    const service = new AutocompleteService(fetchMock)

    service.getSuggestions('')

    expect(fetchMock.mock.calls[0][0]).toContain('q=&')
  })

  it('includes the default type parameter "name" if no type parameter is explicitly provided.', async () => {
    const fetchMock = jest.fn(() =>
      Promise.resolve({ ok: true, json: () => {} }),
    )
    const service = new AutocompleteService(fetchMock)

    service.getSuggestions('foobar')

    expect(fetchMock.mock.calls[0][0]).toContain('type=name')
  })

  it('includes the type parameter if type parameter is provided.', async () => {
    const fetchMock = jest.fn(() =>
      Promise.resolve({ ok: true, json: () => {} }),
    )
    const service = new AutocompleteService(fetchMock)

    service.getSuggestions('foobar', 'photodata.make')

    expect(fetchMock.mock.calls[0][0]).toContain('type=photodata.make')
  })
})
