import { UserService } from 'service/userService'
import User from 'models/user'

describe('UserService', () => {
  it('should send a patch request to the user profile route', async () => {
    const mockUserResponse = {
      id: 42,
      displayName: 'mock-user',
      email: 'test@test.at',
    }

    const fetchMock = jest.fn().mockImplementation((path, init) => {
      return Promise.resolve({
        ok: true,
        json: () => {
          return mockUserResponse
        },
      })
    })

    const userService = new UserService(fetchMock)
    const user = new User({
      id: 5,
      displayName: 'mock-user',
      email: 'test@test.at',
    })
    const newUser = await userService.save(user)

    const fetchArguments = fetchMock.mock.calls[0][1]
    const body = JSON.parse(fetchArguments.body)

    expect(fetchArguments.method).toEqual('PATCH')
    expect(fetchArguments.headers).toEqual(
      expect.objectContaining({
        'Content-Type': 'application/json',
      }),
    )
    expect(user).toEqual(expect.objectContaining(body))
    expect(newUser).toEqual(expect.objectContaining(mockUserResponse))
  })

  it('should send mulipart formdata if user objects contains files', async () => {
    const mockUserResponse = {
      id: 42,
      displayName: 'mock-user',
      email: 'test@test.at',
      avatarImage: 'http://localhost/foo.png',
      bannerImage: 'http://localhost/foo.png',
    }
    const fileMock = new File(
      [
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5AMUEzQZGPsPMgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAMSURBVAjXY2BgYAAAAAQAASc0JwoAAAAASUVORK5CYII=',
      ],
      'test.png',
      {
        type: 'image/png',
      },
    )

    const fetchMock = jest.fn().mockImplementation((path, init) => {
      return Promise.resolve({
        ok: true,
        json: () => {
          return mockUserResponse
        },
      })
    })
    const userService = new UserService(fetchMock)

    const testLastCallIsCorrect = () => {
      const lastIdx = fetchMock.mock.calls.length - 1
      expect(fetchMock.mock.calls[lastIdx][1].method).toEqual('PATCH')
      expect(fetchMock.mock.calls[lastIdx][1].headers).toMatchObject({})
      expect(fetchMock.mock.calls[lastIdx][1].body).toBeInstanceOf(FormData)
    }

    let newUser = await userService.save(
      new User({
        id: 5,
        displayName: 'mock-user',
        email: 'test@test.at',
        avatarImage: fileMock,
      }),
    )
    testLastCallIsCorrect()
    expect(newUser).toMatchObject({ avatarImage: mockUserResponse.avatarImage })

    newUser = await userService.save(
      new User({
        id: 5,
        displayName: 'mock-user',
        email: 'test@test.at',
        bannerImage: fileMock,
      }),
    )
    testLastCallIsCorrect()
    expect(newUser).toMatchObject({ bannerImage: mockUserResponse.bannerImage })

    /* test using two images */
    newUser = await userService.save(
      new User({
        id: 5,
        displayName: 'mock-user',
        email: 'test@test.at',
        avatarImage: fileMock,
        bannerImage: fileMock,
      }),
    )
    testLastCallIsCorrect()
    expect(newUser).toMatchObject({
      bannerImage: mockUserResponse.bannerImage,
      avatarImage: mockUserResponse.avatarImage,
    })
  })

  it('should refuse to create new users', async () => {
    expect.assertions(1)

    const userService = new UserService(() => {})
    const user = new User({
      displayName: 'mock-user',
      email: 'test@test.at',
    })
    try {
      await userService.save(user)
    } catch (e) {
      expect(e).toBeInstanceOf(Error)
    }
  })

  it('should load a specific user', async () => {
    expect.assertions(2)

    const mockUserResponse = {
      id: 42,
      displayName: 'mock-user',
      email: 'test@test.at',
    }

    const fetchMock = jest.fn().mockImplementation((path, init) => {
      return Promise.resolve({
        ok: true,
        json: () => {
          return mockUserResponse
        },
      })
    })

    const userService = new UserService(fetchMock)
    const user = await userService.getUser(42)

    expect(user).toBeInstanceOf(User)
    expect(user.id).toEqual(mockUserResponse.id)
  })
})
