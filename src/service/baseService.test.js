import BaseService from './baseService'
import {
  ROUTE_PHOTOS_DETAIL,
  ROUTE_AUTH_LOGIN,
  ROUTE_PHOTOS,
  routes,
} from './routes'

describe('BaseService', () => {
  it('chooses the right content-type headers', async () => {
    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () => Promise.resolve({}),
    })
    const fetch = jest.fn(() => mockFetchPromise)
    const baseService = new BaseService(fetch)

    const jsonData = {
      test: 1,
      foo: {
        bar: 'hello world',
      },
      a: [1, 2, 3, 4, 5],
    }

    await baseService.post('/foo', jsonData)
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers['Content-Type'],
    ).toEqual('application/json')
    expect(fetch.mock.calls[fetch.mock.calls.length - 1][1].body).toEqual(
      JSON.stringify(jsonData),
    )

    await baseService.post('/foo', JSON.stringify(jsonData))
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers['Content-Type'],
    ).toEqual('application/json')
    expect(fetch.mock.calls[fetch.mock.calls.length - 1][1].body).toEqual(
      JSON.stringify(jsonData),
    )

    await baseService.post('/foo', new FormData())
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers['Content-Type'],
    ).toBeUndefined()
    expect(() =>
      JSON.parse(fetch.mock.calls[fetch.mock.calls.length - 1][1].body),
    ).toThrowError()

    await baseService.loadData('/foo', 'GET')
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers['Content-Type'],
    ).toEqual('application/json')
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].body,
    ).toBeUndefined()

    await baseService.loadData('/foo', 'POST')
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers['Content-Type'],
    ).toEqual('application/json')
    await baseService.loadData('/foo', 'POST', { foo: 'bar' })
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers['Content-Type'],
    ).toEqual('application/json')
    expect(fetch.mock.calls[fetch.mock.calls.length - 1][1].body).toEqual(
      JSON.stringify({ foo: 'bar' }),
    )

    await baseService.loadData('/foo', 'POST', new FormData())
    expect(
      fetch.mock.calls[fetch.mock.calls.length - 1][1].headers['Content-Type'],
    ).toBeUndefined()
    expect(() =>
      JSON.parse(fetch.mock.calls[fetch.mock.calls.length - 1][1].body),
    ).toThrowError()
  })

  it('should delete a model having a "url" property', () => {
    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () =>
        Promise.resolve({
          success: 1,
        }),
    })
    const fetchMock = jest.fn(() => mockFetchPromise)

    const model = {
      _props: {
        url: 'https://foobar.com/photo/1.json',
      },
      id: 1,
      name: 'test photo',
    }
    const baseService = new BaseService(fetchMock)

    baseService.deleteModel(model)
    expect(fetchMock).toHaveBeenCalledTimes(1)
    expect(fetchMock).toHaveBeenLastCalledWith(
      model._props.url,
      expect.objectContaining({
        method: 'DELETE',
      }),
    )
  })

  it('should throw an error if asked to delete an unsaved model', () => {
    const baseService = new BaseService(jest.fn(() => {}))
    const model = {
      _props: {
        url: 'https://foobar.com/photo/1.json',
      },
      name: 'test photo',
    }
    expect(() => baseService.deleteModel(model)).toThrow()
  })

  it('should throw an error if no route is provided and the model does not contain a url attribute', () => {
    const baseService = new BaseService(jest.fn(() => {}))
    const model = {
      id: 42,
      name: 'test photo',
    }
    expect(() => baseService.deleteModel(model)).toThrow()
  })

  it('should build the route if provided with a route name', () => {
    const mockFetchPromise = Promise.resolve({
      ok: true,
      json: () =>
        Promise.resolve({
          success: 1,
        }),
    })
    const fetchMock = jest.fn(() => mockFetchPromise)
    const baseService = new BaseService(fetchMock)
    const model = {
      _props: {
        foo: 'bar',
      },
      id: 42,
      name: 'test photo',
    }
    baseService.deleteModel(model, ROUTE_PHOTOS_DETAIL)
    expect(fetchMock).toHaveBeenLastCalledWith(
      expect.stringContaining('/api/photos/42/'),
      expect.objectContaining({
        method: 'DELETE',
      }),
    )
  })

  it('should not decode body of 200 http responses', async () => {
    const mockFetchPromise = Promise.resolve({
      ok: true,
      response: {
        status: 200,
      },
      json: () =>
        Promise.resolve({
          success: 1,
        }),
    })
    const fetchMock = jest.fn(() => mockFetchPromise)
    const baseService = new BaseService(fetchMock)
    const r = await baseService.loadData('/foo', 'GET')
    expect(r).toHaveProperty('success', 1)
  })

  it('should not decode body of 204 (No-Content) http responses', async () => {
    const mockFetchPromise = Promise.resolve({
      ok: true,
      response: {
        status: 204,
      },
      json: () => {
        throw new Error('')
      },
    })
    const fetchMock = jest.fn(() => mockFetchPromise)
    const baseService = new BaseService(fetchMock)
    const r = await baseService.loadData('/foo', 'GET')
    expect(r).toBeNull()
  })

  it('should be able to deal with paginated results', async () => {
    const mockFetchPromise = Promise.resolve({
      ok: true,
      response: {
        status: 200,
      },
      json: () => ({
        next: 'http://foobar.com/?p=2',
        previous: 'http://foobar.com/?p=1',
        count: 145,
        results: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }],
      }),
    })

    const fetchMock = jest.fn(() => mockFetchPromise)
    const baseService = new BaseService(fetchMock)
    const r = await baseService.loadData('/foo', 'GET')

    expect(r).toHaveLength(5)
    expect(r[0]['id']).toEqual(1)
    expect(r[4]['id']).toEqual(5)
  })

  it('should save the paginated result for later access', async () => {
    const mockFetchPromise = Promise.resolve({
      ok: true,
      response: {
        status: 200,
      },
      json: () => ({
        next: 'http://foobar.com/?p=2',
        previous: 'http://foobar.com/?p=1',
        count: 145,
        results: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }],
      }),
    })

    const fetchMock = jest.fn(() => mockFetchPromise)
    const baseService = new BaseService(fetchMock)
    await baseService.loadData('/foo', 'GET')

    expect(baseService._lastResult).toHaveProperty('results')
    expect(baseService._lastResult).toHaveProperty('next')
    expect(baseService._lastResult).toHaveProperty('previous')
  })

  describe('resolveRoute', () => {
    const baseService = new BaseService()

    it('should resolve a route name to a correct route path', () => {
      const path = baseService.resolveRoute(ROUTE_AUTH_LOGIN)
      expect(path).toEqual(routes[ROUTE_AUTH_LOGIN])
    })

    it('should resolve a route name to a correct route path and replace all parameters', () => {
      const path = baseService.resolveRoute(ROUTE_PHOTOS, {
        offset: 0,
        limit: 25,
        locale: 'de',
        ordering: 'name',
      })
      expect(path).toContain('offset=0')
      expect(path).toContain('limit=25')
      expect(path).toContain('locale=de')
      expect(path).toContain('ordering=name')
    })

    it('should not include placeholder if parameters are missing', () => {
      const path = baseService.resolveRoute(ROUTE_PHOTOS, {
        offset: 0,
      })
      expect(path).not.toMatch(/=<.+>/)
    })

    it('should not include empty parameters', () => {
      const path = baseService.resolveRoute(ROUTE_PHOTOS, {
        limit: false,
        locale: null,
      })
      expect(path).not.toContain('limit=')
      expect(path).not.toContain('locale=')
    })
  })
})
