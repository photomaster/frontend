import url from 'url'
import { routes, buildUrl } from './routes'
import refreshFetch from './fetch'

export class ApiError extends Error {
  constructor(message, errors = []) {
    super(message)
    this.errors = errors
  }

  json() {
    if (typeof this.errors !== 'object') {
      this.errors = JSON.parse(this.errors)
    }
    return this.errors
  }
}

const abortControllers = new Map()

function abortRunningRequests(path, abortPathLike = false) {
  if (abortPathLike) {
    const newPathname = url.parse(path).pathname

    abortControllers.forEach((controller, key) => {
      if (url.parse(key).pathname === newPathname) {
        controller.abort()
      }
    })
  } else if (abortControllers.has(path)) {
    console.info(`canceling request to ${path}`)
    abortControllers.get(path).abort()
  }

  const controller = new AbortController()
  abortControllers.set(path, controller)

  return controller.signal
}

export default class BaseService {
  constructor(fetch = null) {
    this.fetch = fetch
    this._lastResult = null
  }

  buildUrl(path) {
    return buildUrl(path)
  }

  resolveRoute(routeName, params = {}) {
    if (!(routeName in routes)) {
      throw new Error(
        `Unkown route name "${routeName}"! Use one of [${Object.keys(
          routes,
        ).join(', ')}]`,
      )
    }

    let path = routes[routeName]
    Object.entries(params).forEach(([key, value]) => {
      if (path.indexOf(`<${key}>`) !== -1) {
        path = path.replace(`<${key}>`, value)
        delete params[key]
      }
    })

    const builtURL = new URL(buildUrl(path))
    // eslint-disable-next-line prefer-const
    for (let [key, value] of Object.entries(params)) {
      if (Array.isArray(value)) {
        value = value.join(',')
      }

      if (value === 0 || value) {
        builtURL.searchParams.append(key, value)
      }
    }

    return builtURL.pathname + builtURL.search
  }

  deleteModel(instance, routeName = null) {
    if (!instance.id) {
      throw new Error(
        "Model cannot be deleted because it hasn't been saved yet.",
      )
    }
    if (!('url' in instance._props) && !routeName) {
      throw new Error(
        'Model does not contain a "url" property. In this case you must provide a route name!',
      )
    }

    this.loadData(
      instance._props.url ??
        baseService.resolveRoute(routeName, { id: instance.id }),
      'DELETE',
    )
  }

  patch(path, data = {}) {
    return this.loadData(path, 'PATCH', data)
  }

  put(path, data = {}) {
    return this.loadData(path, 'PUT', data)
  }

  delete(path = {}) {
    return this.loadData(path, 'DELETE')
  }

  post(path, data = {}) {
    return this.loadData(path, 'POST', data)
  }

  loadData(
    path,
    method = 'GET',
    body = null,
    { abortRequests = false, abortPathLike = false } = {},
  ) {
    body =
      !(body instanceof FormData) && !(typeof body === 'string')
        ? JSON.stringify(body)
        : body

    let signal = false
    if (abortRequests) {
      signal = abortRunningRequests(path, abortPathLike)
    }
    const requestInit = {
      ...(signal ? { signal } : {}),
      ...(method === 'POST' || method === 'PUT' || method === 'PATCH'
        ? { body }
        : {}),
      method: method,
      headers: !(body instanceof FormData)
        ? {
            'Content-Type': 'application/json',
          }
        : {},
    }

    const fetchMethod = this.fetch ?? refreshFetch
    if (path.indexOf('http') !== 0) {
      path = this.buildUrl(path)
    }

    return fetchMethod(path, requestInit)
      .then(r => {
        if (r.response && r.response.status && r.response.status === 204) {
          return null
        }
        return r.json()
      })
      .then(data => {
        this._lastResult = data
        if (data && 'results' in data) {
          return data.results
        }
        return data
      })
      .catch(err => {
        if ('status' in err) {
          throw new ApiError(`Api returned error ${err.status}`, err.body)
        }
        throw err
      })
  }

  setTokens(token) {
    if (!token) {
      const lang = localStorage.getItem('i18nextLng')
      sessionStorage.clear()
      localStorage.clear()
      localStorage.setItem('i18nextLng', lang)
      return
    }

    if ('refresh' in token) {
      sessionStorage.setItem('refresh-token', token.refresh)
    }
    sessionStorage.setItem('token', token.access)
  }

  saveUserInLocalStorage(userObj, rememberMe = false) {
    sessionStorage.setItem('user', JSON.stringify(userObj._props))
    if (rememberMe) {
      localStorage.setItem('user', JSON.stringify(userObj._props))
    }
  }

  isSignedIn() {
    return sessionStorage.getItem('token') !== null
  }

  getFormDataFromProperties(object) {
    const formData = new FormData()

    for (const key in object) {
      if (object[key] === null) {
        // eslint-disable-next-line no-continue
        continue
      }

      if (typeof object[key] === 'object' && !(object[key] instanceof File)) {
        formData.append(key, JSON.stringify(object[key]))
      } else {
        formData.append(key, object[key])
      }
    }

    return formData
  }
}

export const baseService = new BaseService()
