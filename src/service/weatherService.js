import WeatherState from 'models/weatherState'
import BaseService from './baseService'
import { ROUTE_WEATHER_DETAIL } from './routes'

class WeatherService extends BaseService {
  async getWeather(id) {
    return new WeatherState(
      await this.loadData(this.resolveRoute(ROUTE_WEATHER_DETAIL, { id })),
    )
  }

  async save(instance) {
    if (!instance._props.url || !instance.id) {
      throw new Error(
        'You should not try to create weatherState objects yourself. Use await photo.{....} instead.',
      )
    }

    return new WeatherState(
      await this.loadData(
        this.resolveRoute(ROUTE_WEATHER_DETAIL, { id: instance.id }),
        'PATCH',
        instance._props,
      ),
    )
  }
}

export default new WeatherService()
