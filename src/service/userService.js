import User from 'models/user'
import BaseService from './baseService'
import { ROUTE_PROFILE, ROUTE_USER_DETAIL } from './routes'

class UserService extends BaseService {
  getUserLocation = new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      position => {
        resolve({
          access: true,
          position: [position.coords.latitude, position.coords.longitude],
        })
      },
      error => {
        reject(error)
      },
    )
  }).catch(error => {
    console.error(
      `error ${error.code}: ${error.message}\nUsing Salzburg as fallback`,
    )
    return { access: false, position: [47.811195, 13.033229] }
  })

  /**
   *
   * @param {number} id
   * @returns {Promise<User>}
   */
  async getUser(id) {
    return new User(
      await this.loadData(this.resolveRoute(ROUTE_USER_DETAIL, { id })),
    )
  }

  async save(object) {
    if (!('id' in object) || !object.id) {
      throw new Error('you are not allowed to explicitly create user objects!')
    }

    const user = object instanceof User ? { ...object._props } : { ...object }

    const method = 'PATCH'
    const route = this.resolveRoute(ROUTE_PROFILE)

    let body = null
    if (Object.values(user).some(v => v instanceof File)) {
      body = this.getFormDataFromProperties(user)
    } else {
      body = JSON.stringify(user)
    }

    const data = await this.loadData(route, method, body)
    return new User(data)
  }
}

export { UserService }

export default new UserService()
