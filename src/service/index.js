export { default as locationService } from './locationService'
export { default as photoService } from './photoService'
export { default as weatherService } from './weatherService'
