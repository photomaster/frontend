import BaseService from './baseService'
import { ROUTE_AUTOCOMPLETE } from './routes'

const lang = () => localStorage.getItem('i18nextLng')

export class AutocompleteService extends BaseService {
  async getSuggestions(query, type = 'name', locale = lang()) {
    return this.loadData(
      this.resolveRoute(ROUTE_AUTOCOMPLETE, {
        query,
        type,
        locale,
      }),
    )
  }
}

export default new AutocompleteService()
