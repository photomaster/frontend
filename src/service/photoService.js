import Photo from 'models/photo'
import User from 'models/user'
import Favorite from 'models/favorite'
import PhotoData from 'models/photoData'
import {
  ROUTE_USER_PHOTOS,
  ROUTE_PHOTOS,
  ROUTE_PHOTOS_DETAIL,
  ROUTE_PHOTOS_SIMILAR,
  ROUTE_PHOTOS_NEAR,
  ROUTE_SOLAR_DATA_DETAIL,
  ROUTE_PROFILE_FAVORITES,
  ROUTE_PROFILE_FAVORITES_DETAIL,
  ROUTE_PHOTOS_SEARCH,
} from './routes'
import BaseService from './baseService'

export const VISIBILITY_STATES = Object.freeze({
  draft: 1,
  private: 2,
  public: 3,
})

const lang = () => localStorage.getItem('i18nextLng')

export const DEFAULT_LIMIT = 9

export class PhotoService extends BaseService {
  async getNearPhotos(
    lat,
    long,
    distance = 10,
    { limit = DEFAULT_LIMIT, offset = 0, labels = [], locale = lang() } = {},
  ) {
    const props = await this.loadData(
      this.resolveRoute(ROUTE_PHOTOS_NEAR, {
        lat,
        long,
        distance,
        labels,
        limit,
        offset,
        locale,
      }),
    )
    return props.map(prop => new Photo(prop))
  }

  async getSimilarPhotos(photo, locale = lang()) {
    const props = await this.loadData(
      this.resolveRoute(ROUTE_PHOTOS_SIMILAR, { id: photo.id, locale: locale }),
    )
    return props.map(prop => new Photo(prop))
  }

  async getPhotos({
    limit = DEFAULT_LIMIT,
    offset = 0,
    labels = [],
    locale = lang(),
    ordering = null,
  } = {}) {
    const props = await this.loadData(
      this.resolveRoute(ROUTE_PHOTOS, {
        limit,
        offset,
        labels,
        locale,
        ordering,
      }),
    )
    return props.map(prop => new Photo(prop))
  }

  /**
   * Get list of photos of a specific user
   * @param {User|number} user
   */
  async getPhotosOfUser(
    user,
    { limit = DEFAULT_LIMIT, offset = 0, labels = [], locale = lang() } = {},
  ) {
    const props = await this.loadData(
      this.resolveRoute(ROUTE_USER_PHOTOS, {
        user: user instanceof User ? user.id : user,
        limit: limit,
        offset: offset,
        labels: labels,
        locale: locale,
      }),
    )
    return props.map(prop => new Photo(prop))
  }

  async getFavoritePhotos(locale = lang()) {
    const props = await this.loadData(
      this.resolveRoute(ROUTE_PROFILE_FAVORITES, {
        locale,
      }),
    )
    return props.map(prop => new Favorite(prop))
  }

  async addPhotoToFavorites(photo) {
    const props = await this.post(this.resolveRoute(ROUTE_PROFILE_FAVORITES), {
      photo: {
        id: photo.id,
      },
    })
    return new Favorite(props)
  }

  /**
   * @deprecated use favorite.delete() instead
   */
  async removeFavorite(favorite) {
    return this.loadData(
      this.resolveRoute(ROUTE_PROFILE_FAVORITES_DETAIL, {
        id: favorite.id,
      }),
      'DELETE',
    )
  }

  async getPhoto(id, locale = lang()) {
    return new Photo(
      await this.loadData(
        this.resolveRoute(ROUTE_PHOTOS_DETAIL, { id, locale }),
      ),
    )
  }

  async getPhotoData(photo) {
    return new PhotoData(await this.loadData(photo.photoDataUrl))
  }

  getSolarData(photoId, date = null, locale = lang()) {
    if (date instanceof Date) {
      date = new Date(date.getTime() - date.getTimezoneOffset() * 60000)
        .toISOString()
        .split('T')[0]
    }
    return this.loadData(
      this.resolveRoute(ROUTE_SOLAR_DATA_DETAIL, {
        photoId,
        date,
        locale,
      }),
    )
  }

  async getPhotosByQuery(
    query,
    {
      locale = lang(),
      offset = 0,
      season = '',
      weather = '',
      make = '',
      model = '',
      temperatureMin = '',
      temperatureMax = '',
    } = {},
  ) {
    const props = await this.loadData(
      this.resolveRoute(ROUTE_PHOTOS_SEARCH, {
        q: query,
        locale: locale,
        offset: offset,
        season: season,
        weather: weather,
        make: make,
        model: model,
        temperatureMax: temperatureMax,
        temperatureMin: temperatureMin,
      }),
    )
    return props.map(data => new Photo(data))
  }

  publish(photo) {
    photo.visibility = VISIBILITY_STATES.public
    return this.save(photo)
  }

  unpublish(photo) {
    photo.visibility = VISIBILITY_STATES.draft
    return this.save(photo)
  }

  async save(object, locale = lang()) {
    let photoProps =
      object instanceof Photo ? { ...object._props } : { ...object }

    if ('image' in photoProps) {
      if (photoProps.image instanceof File) {
        const updatedProps = await this.uploadPhoto(
          '',
          0,
          0,
          photoProps.image,
          photoProps.id,
          'imageRotateDegrees' in photoProps
            ? photoProps.imageRotateDegrees
            : 0,
        )

        if (!photoProps.id) {
          photoProps = { ...updatedProps._props, ...photoProps }
        }
      }
      delete photoProps.image
    }

    if (
      'geo' in object &&
      Array.isArray(object.geo) &&
      object.geo.length === 2
    ) {
      photoProps.geo = {
        type: 'Point',
        coordinates: [object.geo[1], object.geo[0]],
      }
    }

    let route = this.resolveRoute(ROUTE_PHOTOS, { locale })
    let method = 'POST'

    if ('id' in photoProps && photoProps.id > 0) {
      method = 'PATCH'
      route = this.resolveRoute(ROUTE_PHOTOS_DETAIL, {
        id: photoProps.id,
        locale: locale,
      })
    }

    const data = await this.loadData(route, method, JSON.stringify(photoProps))
    return new Photo(data)
  }

  async uploadPhoto(
    name,
    lat,
    lng,
    file,
    id = null,
    rotate = 0,
    locale = lang(),
  ) {
    const formData = new FormData()

    const geo = {
      type: 'Point',
      coordinates: [lng, lat],
    }

    if (rotate > 0) {
      formData.append('imageRotateDegrees', rotate)
    }
    formData.append('name', name || 'DRAFT TITLE')
    formData.append('image', file)
    formData.append('geo', JSON.stringify(geo))

    const route =
      id === null
        ? this.resolveRoute(ROUTE_PHOTOS, { locale })
        : this.resolveRoute(ROUTE_PHOTOS_DETAIL, {
            id,
            locale,
          })
    const method = id === null ? 'POST' : 'PUT'

    const data = await this.loadData(route, method, formData)
    return new Photo(data)
  }
}

export default new PhotoService()
