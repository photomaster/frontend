export const ROUTE_AUTH_LOGIN = 'auth_login'
export const ROUTE_AUTH_REFRESH = 'auth_refresh'
export const ROUTE_AUTH_REGISTER = 'auth_register'
export const ROUTE_AUTOCOMPLETE = 'autocomplete'
export const ROUTE_LOCATIONS = 'locations'
export const ROUTE_PHOTOS = 'photos'
export const ROUTE_PHOTOS_DETAIL = 'photos_detail'
export const ROUTE_PHOTOS_SEARCH = 'photos_search'
export const ROUTE_PHOTOS_SIMILAR = 'photos_similar'
export const ROUTE_PHOTOS_NEAR = 'photos_near'
export const ROUTE_PROFILE = 'profile'
export const ROUTE_PROFILE_FAVORITES = 'profile_favorites'
export const ROUTE_PROFILE_FAVORITES_DETAIL = 'profile_favorites_detail'
export const ROUTE_SOLAR_DATA_DETAIL = 'solar_data'
export const ROUTE_USER_PHOTOS = 'user_photos'
export const ROUTE_MAP_CLUSTERS = 'map_clusters'
export const ROUTE_USER_DETAIL = 'user_detail'
export const ROUTE_WEATHER = 'weatherState'
export const ROUTE_WEATHER_DETAIL = 'weatherState_detail'

const hostname = process.env.REACT_APP_HOSTNAME

export const routes = {
  auth_login: '/api/auth/',
  auth_refresh: '/api/auth/refresh/',
  auth_register: '/api/auth/register/',
  autocomplete: '/api/photos/autocomplete/?q=<query>',
  locations: '/api/locations/',
  photos: '/api/photos/',
  photos_search: '/api/photos/search/',
  photos_detail: '/api/photos/<id>/',
  photos_near: '/api/photos/',
  photos_similar: '/api/photos/<id>/similar/',
  solar_data: '/api/photos/<photoId>/solar_data/',
  user_photos: '/api/users/<user>/photos/',
  profile: '/api/profile/',
  profile_favorites: '/api/profile/favorites/',
  profile_favorites_detail: '/api/profile/favorites/<id>/',
  map_clusters: '/api/photos/map/',
  user_detail: '/api/users/<id>/',
  weatherState: '/api/weatherState/',
  weatherState_detail: '/api/weatherState/<id>/',
}

export function buildUrl(path) {
  return hostname + path
}
