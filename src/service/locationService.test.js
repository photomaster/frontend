import LocationService from './locationService'

const Locations = [
  {
    centroid: {
      coordinates: [47.8115, 13.033],
      type: 'Point',
    },
    id: 1,
    photos: [
      {
        id: 1,
        imageThumbnailTeaser: '/url',
        name: 'name',
        geo: {
          coordinates: [47.7563, 13.066499],
        },
      },
    ],
    size: 1,
  },
  {
    centroid: {
      coordinates: [47.811, 13.0335],
      type: 'Point',
    },
    id: 2,
    photos: [],
    size: 51,
  },
]

describe('LocationService', () => {
  it('loads clusters', async () => {
    const mockLoadData = jest.fn((path, method, body) =>
      Promise.resolve(Locations),
    )
    LocationService.loadData = mockLoadData

    const clusters = await LocationService.getClusters(
      { lat: 1, lng: 2 },
      { lat: 3, lng: 4 },
      5,
    )

    expect(clusters).toBeTruthy()
    expect(clusters).toEqual(Locations)
  })
})
