import WeatherService from './weatherService'
import WeatherState from 'models/weatherState'
import Photo from 'models/photo'

describe('WeatherService', () => {
  it('loads weather data', async () => {
    const mockLoadData = jest.fn((path, method, body) =>
      Promise.resolve({
        description: 'scattered clouds',
        icon: '01d',
        id: 1,
        locationId: 2760770,
        locationName: 'Somewhere',
        sunrise: '06:21:36',
        sunset: '15:13:41',
        temperature: 10,
        url: 'http://localhost:8080/api/weatherState/1/',
      }),
    )
    WeatherService.loadData = mockLoadData

    const photo = new Photo({
      id: 1,
      weatherAtCreation: 'http://localhost:8080/api/weatherState/1/',
    })
    const weatherState = await photo.weatherAtCreation

    expect(weatherState).toBeInstanceOf(WeatherState)
    expect(weatherState).toMatchObject({
      _props: {
        description: 'scattered clouds',
        icon: '01d',
        id: 1,
        locationId: 2760770,
        locationName: 'Somewhere',
        sunrise: '06:21:36',
        sunset: '15:13:41',
        temperature: 10,
        url: 'http://localhost:8080/api/weatherState/1/',
      },
    })
  })
  it('saves weather data', async () => {
    const mockLoadData = jest.fn((path, method, body) =>
      Promise.resolve({
        description: 'scattered clouds',
        icon: '01d',
        id: 1,
        locationId: 2760770,
        locationName: 'Somewhere',
        sunrise: '06:21:36',
        sunset: '15:13:41',
        temperature: 10,
        url: 'http://localhost:8080/api/weatherState/1/',
      }),
    )
    WeatherService.loadData = mockLoadData

    const photo = new Photo({
      id: 1,
      weatherAtCreation: 'http://localhost:8080/api/weatherState/1/',
    })
    let weatherState = await photo.weatherAtCreation

    const mockEditPhoto = jest.fn((path, method, body) =>
      Promise.resolve({
        description: 'a new weather description',
        temperature: 100,
        url: 'http://localhost:8080/api/weatherState/1/',
      }),
    )
    WeatherService.loadData = mockEditPhoto

    const updatedWeatherState = await WeatherService.save(weatherState)

    expect(updatedWeatherState).toBeInstanceOf(WeatherState)
    expect(updatedWeatherState).toMatchObject({
      _props: {
        description: 'a new weather description',
        temperature: 100,
        url: 'http://localhost:8080/api/weatherState/1/',
      },
    })
  })
  it('throws error saving locally created object', async () => {
    const weatherState = new WeatherState({
      description: 'sunny sky',
      temperature: 20,
    })
    expect(weatherState).toBeInstanceOf(WeatherState)

    try {
      await WeatherService.save(weatherState)
      expect(false).toBeTruthy()
    } catch (e) {
      expect(e.message).toEqual(
        'You should not try to create weatherState objects yourself. Use await photo.{....} instead.',
      )
    }
  })
})
