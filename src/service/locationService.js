import BaseService from './baseService'
import { ROUTE_MAP_CLUSTERS } from './routes'

class LocationService extends BaseService {
  getClusters(topLeft, bottomRight, zoom) {
    return this.loadData(
      this.resolveRoute(ROUTE_MAP_CLUSTERS, {
        lat1: topLeft.lat,
        long1: topLeft.lng,
        lat2: bottomRight.lat,
        long2: bottomRight.lng,
        zoom: zoom,
      }),
      'GET',
      null,
      {
        abortRequests: true,
        abortPathLike: true,
      },
    )
  }
}

export default new LocationService()
