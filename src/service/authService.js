import User from 'models/user'
import BaseService from './baseService'
import { ROUTE_AUTH_LOGIN, ROUTE_PROFILE, ROUTE_AUTH_REGISTER } from './routes'
import { refreshToken } from './fetch'

const lang = () => localStorage.getItem('i18nextLng')

export class AuthService extends BaseService {
  /**
   *
   * @param {string} username
   * @param {string} password
   * @param {boolean} rememberMe stay signed in
   * @throws {Error}
   * @returns {User} user object
   */
  async login(username, password, rememberMe = false) {
    const url = this.resolveRoute(ROUTE_AUTH_LOGIN)

    const tokenData = await this.post(url, {
      username,
      password,
    })

    this.setTokens(tokenData)
    if (rememberMe) {
      localStorage.setItem('refresh-token', tokenData.refresh)
    }

    const user = new User(await this.loadData(this.resolveRoute(ROUTE_PROFILE)))

    this.saveUserInLocalStorage(user, rememberMe)

    return user
  }

  async getUser() {
    return new User(await this.loadData(this.resolveRoute(ROUTE_PROFILE)))
  }

  async register({ ...params }, locale = lang()) {
    const user = new User(
      await this.post(
        this.resolveRoute(ROUTE_AUTH_REGISTER, { locale }),
        params,
      ),
    )

    await this.login(params.username, params.password)

    this.saveUserInLocalStorage(user)

    return user
  }

  /**
   * @deprecated use /service/fetch::refreshToken()
   */
  refreshToken() {
    return refreshToken()
  }

  getRefreshToken() {
    const currentRefreshToken =
      sessionStorage.getItem('refresh-token') ??
      localStorage.getItem('refresh-token')
    if (!currentRefreshToken) {
      throw new Error('refresh token not available')
    }
    return currentRefreshToken
  }
}

export default new AuthService()
