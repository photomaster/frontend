import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import moment from 'moment'
import translationsEN from '../public/assets/translations/en/main.json'

i18n.use(initReactI18next).init({
  resources: {
    en: {
      main: translationsEN,
    },
  },
  lng: 'en',
  fallbackLng: 'en',
  debug: false,
  ns: ['main'],
  defaultNS: 'main',
  keySeparator: '.',
  interpolation: {
    escapeValue: false,
    formatSeparator: ',',
    format: function(value, format) {
      if (value instanceof Date && format === 'dateLong')
        return moment(value).format('LL')
      if (value instanceof Date && format === 'dateShort')
        return moment(value).format('l')
      if (value instanceof Date && format === 'timeLong')
        return moment(value).format('LTS')
      if (value instanceof Date && format === 'timeShort')
        return moment(value).format('LT')
      return value
    },
  },
  react: {
    wait: true,
  },
})

export default i18n
