import { cleanup } from '@testing-library/react'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { userActions } from 'actions/'
import authService from 'service/authService'
import userService from 'service/userService'
import UserModel from 'models/user'
import { ApiError } from 'service/baseService'

const TestUser = new UserModel({
  id: 1,
  username: 'username',
  displayName: 'displayName',
  email: 'test@test.com',
  description: 'description',
  avatarImage: '/avatar',
  bannerImage: '/banner',
})

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

const mockConsole = jest.fn(msg => msg)
console.error = mockConsole

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('user actions', () => {
  describe('user location', () => {
    it('creates getUserLocation/success when requesting location has succeeded', async () => {
      userService.getUserLocation = new Promise((resolve, reject) => {
        resolve({ access: true, position: [1, 2] })
      })

      const store = mockStore({})
      const expectedActions = [
        'getUserLocation/initiated',
        'getUserLocation/success',
      ]

      await store.dispatch(userActions.getUserLocation()).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
    })
    it('creates getUserLocation/denied when requesting location was denied', async () => {
      userService.getUserLocation = new Promise((resolve, reject) => {
        resolve({ access: false, position: [1, 2] })
      })

      const store = mockStore({})
      const expectedActions = [
        'getUserLocation/initiated',
        'getUserLocation/denied',
      ]

      await store.dispatch(userActions.getUserLocation()).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
    })
    it('creates getUserLocation/error when requesting location resulted in error', async () => {
      userService.getUserLocation = new Promise((resolve, reject) => {
        reject({ code: 'xx', message: 'some error' })
      })

      const store = mockStore({})
      const expectedActions = [
        'getUserLocation/initiated',
        'getUserLocation/error',
      ]

      await store.dispatch(userActions.getUserLocation()).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockConsole).toHaveBeenLastCalledWith('geolocation error', {
        code: 'xx',
        message: 'some error',
      })
    })
  })
  describe('register user', () => {
    it('creates register/success when registering has succeeded', async () => {
      const mockRegister = jest.fn(() => Promise.resolve({ _props: '', id: 1 }))
      authService.register = mockRegister

      const store = mockStore({})
      const expectedActions = ['register/initiated', 'register/success']

      await store.dispatch(userActions.register('user')).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockRegister).toHaveBeenLastCalledWith('user')
    })
    it('creates register/error when registering has failed', async () => {
      const mockRegister = jest.fn(() => {
        throw Error()
      })
      authService.register = mockRegister

      const store = mockStore({})
      const expectedActions = ['register/initiated', 'register/error']

      await store.dispatch(userActions.register('user')).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockRegister).toHaveBeenLastCalledWith('user')
      expect(mockConsole).toHaveBeenLastCalledWith('register error', Error())
    })
    it('creates register/error when APIErrors appear', async () => {
      const mockRegister = jest.fn(() => {
        throw new ApiError('Api returned error xx register', 'body')
      })
      authService.register = mockRegister

      const store = mockStore({})
      const expectedActions = ['register/initiated', 'register/error']

      await store.dispatch(userActions.register('user')).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockRegister).toHaveBeenLastCalledWith('user')
      expect(mockConsole).toHaveBeenCalledWith(
        'register error',
        Error('Api returned error xx register'),
      )
      expect(mockConsole).toHaveBeenLastCalledWith('errors in fields', 'body')
    })
  })
  describe('login user', () => {
    it('creates login/success when logging in with username has succeeded', async () => {
      const mockLogin = jest.fn(() => Promise.resolve({ _props: '', id: 1 }))
      authService.login = mockLogin

      const store = mockStore({})
      const expectedActions = ['login/initiated', 'login/success']

      const userObj = {
        username: 'username',
        password: 'secret',
        rememberMe: true,
      }

      await store.dispatch(userActions.login(userObj)).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockLogin).toHaveBeenLastCalledWith('username', 'secret', true)
    })
    it('creates login/success when logging in with email has succeeded', async () => {
      const mockLogin = jest.fn(() => Promise.resolve({ _props: '', id: 1 }))
      authService.login = mockLogin

      const store = mockStore({})
      const expectedActions = ['login/initiated', 'login/success']

      const userObj = {
        email: 'some@mail.com',
        password: 'secret',
        rememberMe: true,
      }

      await store.dispatch(userActions.login(userObj)).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockLogin).toHaveBeenLastCalledWith(
        'some@mail.com',
        'secret',
        true,
      )
    })
    it('creates login/error when logging in has failed', async () => {
      const mockLogin = jest.fn(() => {
        throw Error()
      })
      authService.login = mockLogin

      const store = mockStore({})
      const expectedActions = ['login/initiated', 'login/error']

      const userObj = {
        username: 'username',
        password: 'secret',
        rememberMe: true,
      }

      await store.dispatch(userActions.login(userObj)).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockLogin).toHaveBeenLastCalledWith('username', 'secret', true)
      expect(mockConsole).toHaveBeenLastCalledWith('login error', Error())
    })
    it('creates login/error when no valid userObj is provided', async () => {
      const mockLogin = jest.fn(() => {
        throw Error()
      })
      authService.login = mockLogin

      const store = mockStore({})
      const expectedActions = ['login/initiated', 'login/error']

      await store.dispatch(userActions.login({})).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockLogin).toHaveBeenCalledTimes(0)
      expect(mockConsole).toHaveBeenLastCalledWith(
        'login error',
        Error('no valid userObj provided'),
      )
    })
  })
  describe('logout user', () => {
    it('creates logout/success when logging out has succeeded', async () => {
      const mockSetTokens = jest.fn()
      authService.setTokens = mockSetTokens

      const store = mockStore({})
      const expectedActions = ['updateFavourites/remove', 'logout/success']

      await store.dispatch(userActions.logout()).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockSetTokens).toHaveBeenLastCalledWith(null)
    })
  })
  describe('update user', () => {
    it('creates update/user/success when updating profile has succeeded', async () => {
      const mockSave = jest.fn(() => Promise.resolve(TestUser))
      userService.save = mockSave

      const store = mockStore({})
      const expectedActions = ['update/user/initiated', 'update/user/success']

      await store.dispatch(userActions.update(TestUser)).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockSave).toHaveBeenLastCalledWith(TestUser)
    })
    it('creates update/user/error when updating profile has failed', async () => {
      const mockSave = jest.fn(() => {
        throw Error()
      })
      userService.save = mockSave

      const store = mockStore({})
      const expectedActions = ['update/user/initiated', 'update/user/error']

      await store.dispatch(userActions.update(TestUser)).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockSave).toHaveBeenLastCalledWith(TestUser)
      expect(mockConsole).toHaveBeenLastCalledWith('update error', Error())
    })
  })
})
