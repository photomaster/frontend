import { cleanup } from '@testing-library/react'
import { applicationActions } from 'actions/'

const mockConsole = jest.fn(msg => msg)
console.error = mockConsole

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('application actions', () => {
  describe('lightbox', () => {
    it('should create an action to open the lightbox', () => {
      const expectedAction = {
        type: 'lightboxInteraction/open',
        payload: {
          open: true,
        },
      }
      expect(applicationActions.openLightbox()).toEqual(expectedAction)
    })
    it('should create an action to close the lightbox', () => {
      const expectedAction = {
        type: 'lightboxInteraction/close',
        payload: {
          open: false,
        },
      }
      expect(applicationActions.closeLightbox()).toEqual(expectedAction)
    })
  })
})
