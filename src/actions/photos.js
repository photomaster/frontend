import photoService from '../service/photoService'
import weatherService from '../service/weatherService'
import { history } from '../helpers/history'

const getAllNearPhotos = (
  coordinates = [47.7563, 13.066498],
) => async dispatch => {
  dispatch({ type: 'getAllNearPhotos/initiated', payload: {} })
  try {
    // TODO helper function
    let photos = []
    let radius = 15
    // if there are no photos nearby enlarge search radius gradually
    while (photos.length === 0 && radius < 60) {
      photos = await photoService.getNearPhotos(
        coordinates[0],
        coordinates[1],
        radius,
      )
      radius += 15
    }
    dispatch({
      type: 'getAllNearPhotos/success',
      payload: photos,
    })
  } catch (e) {
    console.error('get near photos error', e)
    dispatch({ type: 'getAllNearPhotos/error', payload: e })
  }
}

const getNearPhotosOffset = (
  coordinates = [47.7563, 13.066498],
  limit = 9,
  offset = 0,
  labels = [],
) => async dispatch => {
  dispatch({ type: 'getNearPhotosOffset/initiated', payload: {} })
  try {
    let photos = []
    let radius = 15
    // if there are no photos nearby enlarge search radius gradually
    while (photos.length === 0 && radius <= 60) {
      photos = await photoService.getNearPhotos(
        coordinates[0],
        coordinates[1],
        radius,
        { limit, offset, labels },
      )
      radius += 15
    }
    dispatch({
      type: 'getNearPhotosOffset/success',
      payload: photos,
    })
  } catch (e) {
    console.error('get near photos offset error', e)
    dispatch({ type: 'getNearPhotosOffset/error', payload: e })
  }
}

const setActivePhoto = photo => {
  return {
    type: 'activePhoto/set',
    payload: photo,
  }
}

const unsetActivePhoto = () => {
  return {
    type: 'activePhoto/unset',
  }
}

const update = (photoObj, weatherObj, photoData) => async dispatch => {
  let success = 0
  dispatch({ type: 'update/photo/initiated', payload: photoObj })
  try {
    const updatedPhoto = await photoService.save(photoObj)

    dispatch({
      type: 'update/photo/success',
      payload: updatedPhoto._props,
    })
    success++
  } catch (e) {
    console.error('photo update error', e)
    dispatch({ type: 'update/photo/error', payload: e.errors })
  }

  if (weatherObj) {
    dispatch({ type: 'update/weather/initiated', payload: weatherObj })
    try {
      const updatedWeather = await weatherService.save(weatherObj)

      dispatch({
        type: 'update/weather/success',
        payload: updatedWeather._props,
      })
      success++
    } catch (e) {
      console.error('weather update error', e)
      dispatch({ type: 'update/weather/error', payload: e.errors })
    }
  } else {
    success++
  }

  if (photoData) {
    dispatch({ type: 'update/camera/initiated', payload: photoData })
    try {
      const updatedCamera = await photoData.save()

      dispatch({
        type: 'update/camera/success',
        payload: updatedCamera._props,
      })
      success++
    } catch (e) {
      console.error('camera update error', e)
      dispatch({ type: 'update/camera/error', payload: e.errors })
    }
  } else {
    success++
  }

  if (success === 3) history.push(`/photos/${photoObj.id}`)
}

const remove = photoObj => async dispatch => {
  dispatch({ type: 'remove/photo/initiated', payload: photoObj })
  const userID = photoObj.user.id
  try {
    await photoObj.delete()

    dispatch({
      type: 'remove/photo/success',
    })
    history.push(`/users/${userID}`)
  } catch (e) {
    console.error('remove error', e)
    dispatch({ type: 'remove/photo/error', payload: e.errors })
  }
}

const updateFavourites = () => async dispatch => {
  dispatch({ type: 'updateFavourites/initiated', payload: {} })
  try {
    const favourites = await photoService.getFavoritePhotos()

    dispatch({
      type: 'updateFavourites/success',
      payload: favourites,
    })
  } catch (e) {
    console.error('update favourites error', e)
    dispatch({ type: 'updateFavourites/error', payload: e })
  }
}

export default {
  getAllNearPhotos,
  getNearPhotosOffset,
  setActivePhoto,
  unsetActivePhoto,
  update,
  remove,
  updateFavourites,
}
