const openLightbox = () => {
  return {
    type: 'lightboxInteraction/open',
    payload: {
      open: true,
    },
  }
}

const closeLightbox = () => {
  return {
    type: 'lightboxInteraction/close',
    payload: {
      open: false,
    },
  }
}

export default {
  openLightbox,
  closeLightbox,
}
