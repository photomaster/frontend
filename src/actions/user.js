import authService from 'service/authService'
import { ApiError } from 'service/baseService'
import userService from '../service/userService'
import { history } from '../helpers/history'

const getUserLocation = () => async dispatch => {
  dispatch({ type: 'getUserLocation/initiated', payload: {} })
  try {
    const userLocation = await userService.getUserLocation
    const status = userLocation.access === true ? 'success' : 'denied'
    dispatch({
      type: `getUserLocation/${status}`,
      payload: {
        access: userLocation.access,
        coordinates: userLocation.position,
      },
    })
  } catch (e) {
    console.error('geolocation error', e)
    dispatch({
      type: 'getUserLocation/error',
      payload: e,
    })
  }
}

const register = userObj => async dispatch => {
  dispatch({ type: 'register/initiated', payload: userObj })
  try {
    const user = await authService.register(userObj)
    dispatch({
      type: 'register/success',
      payload: user._props,
    })
    history.push(`/users/${user.id}`)
  } catch (e) {
    console.error('register error', e)
    if (e instanceof ApiError) {
      console.error('errors in fields', e.errors)
    }
    dispatch({ type: 'register/error', payload: e.errors })
  }
}

const login = userObj => async dispatch => {
  dispatch({ type: 'login/initiated', payload: userObj })
  try {
    const userIdentifier = 'email' in userObj ? userObj.email : userObj.username
    if (!userIdentifier) throw Error('no valid userObj provided')
    const user = await authService.login(
      userIdentifier,
      userObj.password,
      userObj.rememberMe,
    )
    dispatch({
      type: 'login/success',
      payload: user._props,
    })
    history.push(`/users/${user.id}`)
  } catch (e) {
    console.error('login error', e)
    dispatch({ type: 'login/error', payload: e.errors })
  }
}

const logout = () => async dispatch => {
  authService.setTokens(null)
  dispatch({ type: 'updateFavourites/remove' })
  dispatch({ type: 'logout/success' })
  history.push(`/`)
}

const update = userObj => async dispatch => {
  dispatch({ type: 'update/user/initiated', payload: userObj })
  try {
    const updatedUser = await userObj.save()

    // todo: how do we store the updated user model?
    sessionStorage.setItem('user', JSON.stringify(updatedUser._props))

    dispatch({
      type: 'update/user/success',
      payload: updatedUser._props,
    })
    history.push(`/users/${userObj.id}`)
  } catch (e) {
    console.error('update error', e)
    dispatch({ type: 'update/user/error', payload: e.errors })
  }
}

export default {
  getUserLocation,
  register,
  login,
  logout,
  update,
}
