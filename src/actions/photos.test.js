import { cleanup } from '@testing-library/react'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { photosActions } from 'actions/'
import UserModel from 'models/user'
import PhotoModel from 'models/photo'
import PhotoDataModel from 'models/photoData'
import WeatherModel from 'models/weatherState'
import { photoService, weatherService } from 'service'

const TestUser = new UserModel({
  id: 1,
  username: 'username',
  displayName: 'displayName',
  email: 'test@test.com',
  description: 'description',
  avatarImage: '/avatar',
  bannerImage: '/banner',
})
const TestPhoto = new PhotoModel({
  id: 1,
  url: '/photo1',
  name: 'title1',
  description: 'desc1',
  geo: {
    type: 'Point',
    coordinates: [47.811195, 13.033229],
  },
  photodata: '/photodata1',
  solarData: '/some/url/to/solar/data/1/',
  currentWeather: null,
  originalCreationDate: new Date('2020-11-25T05:09:04Z'),
  weatherAtCreation: '/some/url/to/weatherState/33/',
  image: '/image1',
  imageThumbnailTeaser: '/thumbnail1',
  imageThumbnailTeaserSmall: '/thumbnailsmall1',
  imageThumbnailPreview: '/thumbnailpreview1',
  labels: [
    {
      title: 'label1',
      score: 1,
    },
    {
      title: 'label2',
      score: 1,
    },
  ],
  user: TestUser,
})
const TestWeatherState = new WeatherModel({
  description: 'clear sky',
  temperature: 10,
})
const TestPhotoData = new PhotoDataModel({
  id: 1,
  url: '/photosData/1',
  photo: '/photo/1',
  make: 'make',
  model: 'model',
  lensMake: 'lensMake',
  lensModel: 'lensModel',
  focalLength: 'focalLength',
  fNumber: 'fNumber',
  whiteBalance: 'whiteBalance',
  lightSource: 'lightSource',
  exposureTime: 'exposureTime',
  exposureProgram: 'exposureProgram',
  exposureMode: 'exposureMode',
  isoSpeed: 'isoSpeed',
  flash: 'flash',
  pixelXDimension: 200,
  pixelYDimension: 100,
  orientation: 3,
  software: 'software',
})

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

const mockConsole = jest.fn(msg => msg)
console.error = mockConsole

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('photos actions', () => {
  describe('active photo', () => {
    it('should create an action to set the active photo', () => {
      const photo = 'in reality a photo object'
      const expectedAction = {
        type: 'activePhoto/set',
        payload: photo,
      }
      expect(photosActions.setActivePhoto(photo)).toEqual(expectedAction)
    })
    it('should create an action to unset the active photo', () => {
      const expectedAction = {
        type: 'activePhoto/unset',
      }
      expect(photosActions.unsetActivePhoto()).toEqual(expectedAction)
    })
  })
  describe('get near photos', () => {
    it('creates getAllNearPhotos/success when fetching photos has succeeded (coordinates given)', async () => {
      const mockGetNearPhotos = jest.fn(() => Promise.resolve([{ id: 1 }]))
      photoService.getNearPhotos = mockGetNearPhotos

      const store = mockStore({})
      const expectedActions = [
        'getAllNearPhotos/initiated',
        'getAllNearPhotos/success',
      ]

      await store.dispatch(photosActions.getAllNearPhotos([1, 2])).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockGetNearPhotos).toHaveBeenLastCalledWith(1, 2, 15)
    })
    it('creates getAllNearPhotos/error when fetching photos has failed', async () => {
      const mockGetNearPhotos = jest.fn(() => {
        throw Error()
      })
      photoService.getNearPhotos = mockGetNearPhotos

      const store = mockStore({})
      const expectedActions = [
        'getAllNearPhotos/initiated',
        'getAllNearPhotos/error',
      ]

      await store.dispatch(photosActions.getAllNearPhotos()).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockGetNearPhotos).toHaveBeenLastCalledWith(47.7563, 13.066498, 15)
      expect(mockConsole).toHaveBeenLastCalledWith(
        'get near photos error',
        Error(),
      )
    })
  })
  describe('get near photos offset', () => {
    it('creates getNearPhotosOffset/success when fetching photos has succeeded', async () => {
      const mockGetNearPhotos = jest.fn(() => Promise.resolve([{ id: 1 }]))
      photoService.getNearPhotos = mockGetNearPhotos

      const store = mockStore({})
      const expectedActions = [
        'getNearPhotosOffset/initiated',
        'getNearPhotosOffset/success',
      ]

      await store
        .dispatch(photosActions.getNearPhotosOffset([1, 2]))
        .then(() => {
          const actualActions = store.getActions().map(action => action.type)
          expect(actualActions).toEqual(expectedActions)
        })
      expect(mockGetNearPhotos).toHaveBeenLastCalledWith(1, 2, 15, {
        labels: [],
        limit: 9,
        offset: 0,
      })
    })
    it('creates getNearPhotosOffset/error when fetching photos has failed', async () => {
      const mockGetNearPhotos = jest.fn(() => {
        throw Error()
      })
      photoService.getNearPhotos = mockGetNearPhotos

      const store = mockStore({})
      const expectedActions = [
        'getNearPhotosOffset/initiated',
        'getNearPhotosOffset/error',
      ]

      await store.dispatch(photosActions.getNearPhotosOffset()).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockGetNearPhotos).toHaveBeenLastCalledWith(
        47.7563,
        13.066498,
        15,
        { labels: [], limit: 9, offset: 0 },
      )
      expect(mockConsole).toHaveBeenLastCalledWith(
        'get near photos offset error',
        Error(),
      )
    })
  })
  describe('update photo', () => {
    it('creates update/photo/success when updating photo has succeeded', async () => {
      const mockSave = jest.fn(() => Promise.resolve(TestPhoto))
      photoService.save = mockSave

      const store = mockStore({})
      const expectedActions = ['update/photo/initiated', 'update/photo/success']

      await store.dispatch(photosActions.update(TestPhoto)).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockSave).toHaveBeenLastCalledWith(TestPhoto)
    })
    it('creates update/photo/error when updating photo has failed', async () => {
      const mockSave = jest.fn(() => {
        throw Error()
      })
      photoService.save = mockSave

      const store = mockStore({})
      const expectedActions = ['update/photo/initiated', 'update/photo/error']

      await store.dispatch(photosActions.update(TestPhoto)).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockSave).toHaveBeenLastCalledWith(TestPhoto)
      expect(mockConsole).toHaveBeenLastCalledWith(
        'photo update error',
        Error(),
      )
    })
    it('creates update/weather/success when updating weather has succeeded', async () => {
      const mockSavePhoto = jest.fn(() => Promise.resolve(TestPhoto))
      photoService.save = mockSavePhoto

      const mockSaveWeather = jest.fn(() => Promise.resolve(TestWeatherState))
      weatherService.save = mockSaveWeather

      const store = mockStore({})
      const expectedActions = [
        'update/photo/initiated',
        'update/photo/success',
        'update/weather/initiated',
        'update/weather/success',
      ]

      await store
        .dispatch(photosActions.update(TestPhoto, TestWeatherState))
        .then(() => {
          const actualActions = store.getActions().map(action => action.type)
          expect(actualActions).toEqual(expectedActions)
        })
      expect(mockSaveWeather).toHaveBeenLastCalledWith(TestWeatherState)
    })
    it('creates update/weather/error when updating weather has failed', async () => {
      const mockSavePhoto = jest.fn(() => Promise.resolve(TestPhoto))
      photoService.save = mockSavePhoto

      const mockSaveWeather = jest.fn(() => {
        throw Error()
      })
      weatherService.save = mockSaveWeather

      const store = mockStore({})
      const expectedActions = [
        'update/photo/initiated',
        'update/photo/success',
        'update/weather/initiated',
        'update/weather/error',
      ]

      await store
        .dispatch(photosActions.update(TestPhoto, TestWeatherState))
        .then(() => {
          const actualActions = store.getActions().map(action => action.type)
          expect(actualActions).toEqual(expectedActions)
        })
      expect(mockSaveWeather).toHaveBeenLastCalledWith(TestWeatherState)
      expect(mockConsole).toHaveBeenLastCalledWith(
        'weather update error',
        Error(),
      )
    })
    it('creates update/camera/success when updating photo data has succeeded', async () => {
      const mockSavePhoto = jest.fn(() => Promise.resolve(TestPhoto))
      photoService.save = mockSavePhoto

      const mockSaveWeather = jest.fn(() => Promise.resolve(TestWeatherState))
      weatherService.save = mockSaveWeather

      const mockSavePhotoData = jest.fn(() => Promise.resolve(TestPhotoData))
      TestPhotoData.save = mockSavePhotoData

      const store = mockStore({})
      const expectedActions = [
        'update/photo/initiated',
        'update/photo/success',
        'update/weather/initiated',
        'update/weather/success',
        'update/camera/initiated',
        'update/camera/success',
      ]

      await store
        .dispatch(
          photosActions.update(TestPhoto, TestWeatherState, TestPhotoData),
        )
        .then(() => {
          const actualActions = store.getActions().map(action => action.type)
          expect(actualActions).toEqual(expectedActions)
        })
      expect(mockSavePhotoData).toHaveBeenCalledTimes(1)
    })
    it('creates update/camera/error when updating photo data has failed', async () => {
      const mockSavePhoto = jest.fn(() => Promise.resolve(TestPhoto))
      photoService.save = mockSavePhoto

      const mockSaveWeather = jest.fn(() => Promise.resolve(TestWeatherState))
      weatherService.save = mockSaveWeather

      const mockSavePhotoData = jest.fn(() => {
        throw Error()
      })
      TestPhotoData.save = mockSavePhotoData

      const store = mockStore({})
      const expectedActions = [
        'update/photo/initiated',
        'update/photo/success',
        'update/weather/initiated',
        'update/weather/success',
        'update/camera/initiated',
        'update/camera/error',
      ]

      await store
        .dispatch(
          photosActions.update(TestPhoto, TestWeatherState, TestPhotoData),
        )
        .then(() => {
          const actualActions = store.getActions().map(action => action.type)
          expect(actualActions).toEqual(expectedActions)
        })
      expect(mockSavePhotoData).toHaveBeenCalledTimes(1)
      expect(mockConsole).toHaveBeenLastCalledWith(
        'camera update error',
        Error(),
      )
    })
  })
  describe('remove photo', () => {
    it('creates remove/photo/success when removing photo has succeeded', async () => {
      const mockDelete = jest.fn(() => Promise.resolve())
      TestPhoto.delete = mockDelete

      const store = mockStore({})
      const expectedActions = ['remove/photo/initiated', 'remove/photo/success']

      await store.dispatch(photosActions.remove(TestPhoto)).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockDelete).toHaveBeenCalledTimes(1)
    })
    it('creates remove/photo/error when removing photo has failed', async () => {
      const mockDelete = jest.fn(() => {
        throw Error()
      })
      TestPhoto.delete = mockDelete

      const store = mockStore({})
      const expectedActions = ['remove/photo/initiated', 'remove/photo/error']

      await store.dispatch(photosActions.remove(TestPhoto)).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockDelete).toHaveBeenCalledTimes(1)
      expect(mockConsole).toHaveBeenLastCalledWith('remove error', Error())
    })
  })
  describe('update favourites', () => {
    it('creates updateFavourites/success when updating favourites has succeeded', async () => {
      const mockGetFavoritePhotos = jest.fn(() => Promise.resolve([]))
      photoService.getFavoritePhotos = mockGetFavoritePhotos

      const store = mockStore({})
      const expectedActions = [
        'updateFavourites/initiated',
        'updateFavourites/success',
      ]

      await store.dispatch(photosActions.updateFavourites()).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockGetFavoritePhotos).toHaveBeenCalledTimes(1)
    })
    it('creates updateFavourites/error when updating favourites has failed', async () => {
      const mockGetFavoritePhotos = jest.fn(() => {
        throw Error()
      })
      photoService.getFavoritePhotos = mockGetFavoritePhotos

      const store = mockStore({})
      const expectedActions = [
        'updateFavourites/initiated',
        'updateFavourites/error',
      ]

      await store.dispatch(photosActions.updateFavourites()).then(() => {
        const actualActions = store.getActions().map(action => action.type)
        expect(actualActions).toEqual(expectedActions)
      })
      expect(mockGetFavoritePhotos).toHaveBeenCalledTimes(1)
      expect(mockConsole).toHaveBeenLastCalledWith(
        'update favourites error',
        Error(),
      )
    })
  })
})
