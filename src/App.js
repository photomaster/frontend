import React from 'react'
import { Route, Router, Switch } from 'react-router-dom'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'
import { history } from './helpers/history'
import HomePage from './views/home-page/home-page'
import Upload from './views/upload/upload'
import User from './views/user/user'
import UserEdit from './views/user/userEdit'
import Header from './components/header/header'
import Footer from './components/footer/footer'
import SignIn from './views/sign-in/sign-in'
import SignUp from './views/sign-up/sign-up'
import Photo from './views/photo/photo'
import PhotoRadiusPage from './views/photo/photoRadius'
import PhotoEdit from './views/photo/photoEdit'
import Privacy from './views/privacy/privacy'
import Conditions from './views/conditions/conditions'
import NotFound from './views/not-found/not-found'
import Tag from './views/tag/tag'
import InfoPage from './views/steps-page/steps-page'
import SearchPage from './views/search-page/search-page'
import Favourites from './views/favourites/favourites'
import Feedback from './views/feedback/feedback'
import theme from './theme'
import './App.scss'

export default function App() {
  return (
    <Router history={history}>
      <MuiThemeProvider theme={theme}>
        <div className="content">
          <Header />
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/upload" component={Upload} />
            <Route path="/login" component={SignIn} />
            <Route path="/register" component={SignUp} />
            <Route path="/privacy" component={Privacy} />
            <Route path="/info" component={InfoPage} />
            <Route path="/search" component={SearchPage} />
            <Route path="/conditions" component={Conditions} />
            <Route path="/favorites" component={Favourites} />
            <Route path="/feedback" component={Feedback} />
            <Route
              path="/photos/tags/:tag"
              render={props => <Tag {...props} data={props} />}
            />
            <Route
              path="/photos/radius/:lat-:long"
              component={PhotoRadiusPage}
            />
            <Route
              path="/photos/:id/edit"
              render={props => <PhotoEdit {...props} data={props} />}
            />
            <Route
              path="/photos/:id"
              render={props => <Photo {...props} data={props} />}
            />
            <Route path="/photos" />
            <Route
              path="/users/:id/edit"
              render={props => <UserEdit {...props} data={props} />}
            />
            <Route
              path="/users/:id"
              render={props => <User {...props} data={props} />}
            />
            <Route component={NotFound} />
          </Switch>
        </div>
        <Footer />
      </MuiThemeProvider>
    </Router>
  )
}
