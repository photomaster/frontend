import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import LostImage from 'assets/undraw/lost.svg'

const useStyles = makeStyles(theme => ({
  image: {
    width: '100%',
    height: 'auto',
    margin: `${theme.spacing(4)}px 0`,
  },
}))

export default function NotFound() {
  const classes = useStyles()
  const { t } = useTranslation()

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <Typography component="h1" variant="h1">
        {t('notFound.header')}
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="body1">
            {t('notFound.text1')}
            <NavLink exact to="/">
              {t('notFound.link')}
            </NavLink>
            {t('notFound.text2')}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <img
            className={classes.image}
            src={LostImage}
            alt={t('notFound.alt')}
          />
        </Grid>
      </Grid>
    </Container>
  )
}
