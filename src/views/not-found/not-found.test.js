import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import i18nextConfig from '../../i18nTesting'
import NotFound from './not-found'

function renderContainer() {
  const result = render(
    <Router>
      <NotFound />
    </Router>,
  )
  return result
}

afterEach(cleanup)
describe('Not-Found View', () => {
  it('renders without crashing', () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('contains a link to the homepage', async () => {
    const { container } = renderContainer()
    const link = await waitForElement(() => container.querySelector('a'))

    expect(link).toBeTruthy()
    expect(link.getAttribute('href')).toMatch('/')
  })
})
