import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  wait,
  waitForElement,
} from '@testing-library/react'
import Favourites from './favourites'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import photoService from 'service/photoService'

const OtherUser = {
  id: 100,
  displayName: 'someone else',
  avatarImage: 'url',
}

const FavPhoto = {
  id: 10,
  url: '/photo10',
  user: OtherUser,
  name: 'name',
  description: 'desc',
  imageThumbnailTeaser: '/url',
}

const noop = () => {}
Object.defineProperty(window, 'scrollTo', { value: noop, writable: true })

const mockTranslation = jest.fn(str => str)
const mockDispatch = jest.fn()
const mockGetFavoritePhotos = jest.fn(() =>
  Promise.resolve([{ photo: FavPhoto }]),
)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

jest.mock('service/photoService', () => jest.fn())
photoService.getFavoritePhotos = mockGetFavoritePhotos

function renderContainer(loggedIn = true) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: {
            loggedIn: loggedIn,
          },
        })}
      >
        <Favourites />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Favourites View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()
    await wait(() =>
      expect(container.textContent).toContain('favourites.introduction'),
    )

    expect(mockGetFavoritePhotos).toHaveBeenCalledTimes(1)
    expect(container).toBeTruthy()
  })
  it('does not display favourites if not logged in', async () => {
    const { container } = renderContainer(false)
    const favouritesContainer = container.querySelector(
      '[data-testid="favourites--container"]',
    )

    expect(container).toBeTruthy()
    expect(favouritesContainer).toBeFalsy()
  })
  it('translates the text', async () => {
    const { container } = renderContainer()
    await wait(() =>
      expect(container.textContent).toContain('favourites.introduction'),
    )

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('favourites.headline')
    expect(mockTranslation).toHaveBeenCalledWith('favourites.introduction')
  })
  it('can handle no results for favourites', async () => {
    const mockGetFavoritePhotosEmpty = jest.fn(() => Promise.resolve([]))
    photoService.getFavoritePhotos.mockImplementationOnce(
      mockGetFavoritePhotosEmpty,
    )

    const { container } = renderContainer(true)
    await wait(() =>
      expect(container.textContent).toContain('favourites.noResults'),
    )

    expect(mockGetFavoritePhotosEmpty).toHaveBeenCalledTimes(1)
    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('favourites.headline')
    expect(mockTranslation).toHaveBeenCalledWith('favourites.noResults')
  })
  it('does not crash on errors of photoService', async () => {
    const mockConsole = jest.fn(str => str)
    console.error = mockConsole
    const mockReject = jest.fn(() => Promise.reject('error'))
    photoService.getFavoritePhotos.mockImplementationOnce(mockReject)

    const { container, findByTestId } = renderContainer(true)
    const loading = await waitForElement(() => findByTestId('loading'))

    expect(mockReject).toHaveBeenCalledTimes(1)
    expect(mockConsole).toHaveBeenLastCalledWith('error')
    expect(container).toBeTruthy()
    expect(loading).toBeTruthy()
  })
})
