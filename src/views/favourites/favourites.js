import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import RecommendationsGrid from '../../components/recommendationCard/recommendationsGrid'
import photoService from '../../service/photoService'
import RecommendationCard from '../../components/recommendationCard/recommendationCard'
import Loading from '../../components/utilities/loading'

export default function Favourites() {
  const baseClasses = baseStyles()
  const { t } = useTranslation()

  const [photos, setPhotos] = useState([])
  const [loading, setLoading] = useState(true)
  const loggedIn = useSelector(state => state.currentUser.loggedIn)

  useEffect(() => {
    if (loggedIn) {
      setLoading(true)
      window.scrollTo(0, 0)
      fetchData()
    }
  }, [loggedIn])

  async function fetchData() {
    try {
      const favourites = await photoService.getFavoritePhotos()
      setPhotos(favourites.map(fav => fav.photo))
      setLoading(false)
    } catch (e) {
      console.error(e)
    }
  }

  if (!loggedIn) return <Redirect to={`/`} />

  return (
    <Container
      component="main"
      maxWidth="md"
      className="upload"
      data-testid={'favourites--container'}
    >
      <CssBaseline />
      <Typography component="h1" variant="h1">
        {t('favourites.headline')}
      </Typography>
      {loading ? (
        <Loading />
      ) : photos.length == 0 ? (
        <Typography
          variant="body1"
          className={baseClasses.justifyContentCenter}
          data-testid={'favourites--noResults'}
        >
          {t('favourites.noResults')}
        </Typography>
      ) : (
        <React.Fragment>
          <Typography
            variant="body1"
            className={`${baseClasses.justifyContentCenter} ${baseClasses.marginBottom2}`}
          >
            {t('favourites.introduction')}
          </Typography>
          <RecommendationsGrid>
            {photos.map(photo => (
              <RecommendationCard
                key={photo.id}
                photo={photo}
                user={photo.user}
              />
            ))}
          </RecommendationsGrid>
        </React.Fragment>
      )}
    </Container>
  )
}
