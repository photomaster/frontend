import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import InfoPage from './steps-page'

const mockTranslation = jest.fn(str => str)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer(hash = null) {
  const result = render(
    <Router>
      <InfoPage history={{ location: { hash: hash } }} />
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Steps Page/Infopage View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('scrolls to given topic', async () => {
    const mockScrollTo = jest.fn()
    window.scrollTo = mockScrollTo
    renderContainer('#inspiration')

    expect(mockScrollTo).toHaveBeenCalledTimes(1)
  })
  it('provides links to further information', async () => {
    const { findByTestId } = renderContainer()
    const link1 = await waitForElement(() => findByTestId('stepsPage--link--/'))
    const link2 = await waitForElement(() =>
      findByTestId('stepsPage--link--/search'),
    )
    const link3 = await waitForElement(() =>
      findByTestId('stepsPage--link--/upload'),
    )

    expect(link1.getAttribute('href')).toEqual('/')
    expect(link2.getAttribute('href')).toEqual('/search')
    expect(link3.getAttribute('href')).toEqual('/upload')
  })
  it('translates the text', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('explanation.page.header')
    expect(mockTranslation).toHaveBeenCalledWith(
      'explanation.page.introduction',
    )
    expect(mockTranslation).toHaveBeenCalledWith(
      'explanation.topics.locations.header',
    )
    expect(mockTranslation).toHaveBeenCalledWith(
      'explanation.topics.explore.text',
    )
    expect(mockTranslation).toHaveBeenCalledWith(
      'explanation.topics.inspiration.button',
    )
  })
  it('renders 5 info boxes', async () => {
    const { findAllByTestId } = renderContainer()
    const boxes = await waitForElement(() => findAllByTestId('imageTextSlide'))

    expect(boxes).toBeTruthy()
    expect(boxes.length).toEqual(5)
  })
})
