import React, { useEffect } from 'react'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import Button from '@material-ui/core/Button'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import ImageTextSlide from 'components/container/image-text-slide'
import ExploreImage from 'assets/undraw/explore.svg'
import IdeasImage from 'assets/undraw/ideas.svg'
import SearchImage from 'assets/undraw/searching.svg'
import PhotoImage from 'assets/undraw/photo.svg'
import CameraImage from 'assets/undraw/camera.svg'

export default function InfoPage(props) {
  const { t } = useTranslation()

  useEffect(() => {
    const hash = props.history.location.hash
    if (hash && document.getElementById(hash.substr(1))) {
      const yOffset = -50
      const element = document.getElementById(hash.substr(1))
      const y =
        element.getBoundingClientRect().top + window.pageYOffset + yOffset
      window.scrollTo({ top: y, behavior: 'smooth' })
    }
  }, [props.history.location.hash])

  const getLinkButton = (text, to) => {
    return (
      <NavLink
        to={to}
        style={{ textDecoration: 'none' }}
        data-testid={`stepsPage--link--${to}`}
      >
        <Button variant="contained" size="large" color="primary">
          {text}
        </Button>
      </NavLink>
    )
  }

  return (
    <Container component="main" style={{ overflow: 'hidden' }}>
      <CssBaseline />
      <Typography component="h1" variant="h1">
        {t('explanation.page.header')}
      </Typography>
      <p>{t('explanation.page.introduction')}</p>

      <ImageTextSlide
        title={t('explanation.topics.locations.header')}
        image={ExploreImage}
        text={t('explanation.topics.locations.text')}
        reverse
        button={getLinkButton(t('explanation.topics.locations.button'), '/')}
        id={'locations'}
        mt={false}
      />
      <ImageTextSlide
        title={t('explanation.topics.explore.header')}
        text={t('explanation.topics.explore.text')}
        image={CameraImage}
        id={'explore'}
      />
      <ImageTextSlide
        title={t('explanation.topics.external.header')}
        text={t('explanation.topics.external.text')}
        image={SearchImage}
        reverse
        id={'external'}
      />
      <ImageTextSlide
        title={t('explanation.topics.inspiration.header')}
        text={t('explanation.topics.inspiration.text')}
        image={IdeasImage}
        button={getLinkButton(
          t('explanation.topics.inspiration.button'),
          '/search',
        )}
        id={'inspiration'}
      />
      <ImageTextSlide
        title={t('explanation.topics.share.header')}
        text={t('explanation.topics.share.text')}
        image={PhotoImage}
        reverse
        button={getLinkButton(t('explanation.topics.share.button'), '/upload')}
        id={'share'}
      />
    </Container>
  )
}
