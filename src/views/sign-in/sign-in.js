import React, { useState, useEffect } from 'react'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import CircularProgress from '@material-ui/core/CircularProgress'
import { NavLink } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import validators from 'helpers/validators'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import ErrorMessage from 'components/formElements/errorMessage'
import Avatar from 'components/utilities/avatar'
import { userActions } from '../../actions'

export default function SignIn() {
  const baseClasses = baseStyles()
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const [unique, setUnique] = useState({ value: '' })
  const [password, setPassword] = useState('')
  const [remember, setRemember] = useState(false)
  const active = useSelector(state => state.currentUser.loggingIn)
  const apiErrors = useSelector(state => state.currentUser.errors)

  useEffect(() => {
    return function cleanup() {
      dispatch({ type: 'reset/errors' })
    }
  }, [])

  async function _handleSubmit(e) {
    e.preventDefault()

    try {
      if (unique.email) {
        dispatch(
          userActions.login({
            email: unique.value,
            password: password,
            rememberMe: remember,
          }),
        )
      } else {
        dispatch(
          userActions.login({
            username: unique.value,
            password: password,
            rememberMe: remember,
          }),
        )
      }
    } catch (error) {
      console.error(error)
    }
  }

  const _filledInCompletely = () => {
    if (!unique.value || !password) return false
    return true
  }

  const _handleUniqueChange = event => {
    const value = event.target.value
    const validEmail = validators.emailIsValid(value)
    setUnique({ value: value, email: validEmail })
  }

  const _handlePasswordChange = event => {
    setPassword(event.target.value)
  }

  const _handleRememberChange = event => {
    setRemember(event.target.checked)
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={baseClasses.paper}>
        <Avatar
          alt={t('forms.login.header')}
          additionalClasses={baseClasses.avatar}
        >
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h2">
          {t('forms.login.header')}
        </Typography>
        <form
          className={`${baseClasses.fullWidth} ${baseClasses.marginTop1}`}
          noValidate
        >
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="unique"
            label={t('forms.labels.unique')}
            name="unique"
            autoComplete="email"
            autoFocus
            value={unique.value}
            onChange={_handleUniqueChange}
            data-testid={'signIn--unique'}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label={t('forms.labels.password')}
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            onChange={_handlePasswordChange}
            data-testid={'signIn--password'}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label={t('forms.labels.remember')}
            onChange={_handleRememberChange}
            data-testid={'signIn--remember'}
          />
          {apiErrors && apiErrors.login && (
            <ErrorMessage
              text={'forms.login.errorMessage'}
              noTranslate={false}
            />
          )}
          <span className={baseClasses.relative}>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={baseClasses.submit}
              disabled={!_filledInCompletely() || active}
              onClick={e => _handleSubmit(e)}
              data-testid={'signIn--submitButton'}
            >
              {t('forms.login.button')}
            </Button>
            {active && (
              <CircularProgress
                size={24}
                className={baseClasses.buttonProgress}
                data-testid={'signIn--loading'}
              />
            )}
          </span>
          <Grid container justify="flex-end">
            {/* <Grid item xs>
              <Link href="TODO" variant="body2">
                {t('forms.login.links.resetPassword')}
              </Link>
            </Grid> */}
            <Grid item>
              <NavLink to="/register" variant="body2">
                {t('forms.login.links.signUp')}
              </NavLink>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8} />
    </Container>
  )
}
