import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import SignIn from './sign-in'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import { userActions } from 'actions'
import validators from 'helpers/validators'

const User = {
  username: 'test_username',
  email: 'test@mail.com',
  password: 'test_password',
}

const mockTranslation = jest.fn(str => str)
const mockDispatch = jest.fn()
const mockLogin = jest.fn(_ => Promise.resolve(true))
const mockValidatorTrue = jest.fn(_ => true)
const mockValidatorFalse = jest.fn(_ => false)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

function renderContainer(active = false, errors = {}) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: { loggingIn: active, errors: errors },
        })}
      >
        <SignIn />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Sign Up View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  describe('sets new value for', () => {
    it('unique (including call of type check)', async () => {
      validators.emailIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const unique = await waitForElement(() => findByTestId('signIn--unique'))

      fireEvent.change(unique.querySelector('input'), {
        target: { value: User.email },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(unique.querySelector('input').getAttribute('value')).toEqual(
        'test@mail.com',
      )
    })
    it('password', async () => {
      const { findByTestId } = renderContainer()
      const password = await waitForElement(() =>
        findByTestId('signIn--password'),
      )

      fireEvent.change(password.querySelector('input'), {
        target: { value: User.password },
      })

      expect(password.querySelector('input').getAttribute('value')).toEqual(
        'test_password',
      )
    })
    it('rememberMe', async () => {
      const { findByTestId } = renderContainer()
      const checkbox = await waitForElement(() =>
        findByTestId('signIn--remember'),
      )

      expect(checkbox.querySelector('.Mui-checked')).toBeFalsy()

      fireEvent.click(checkbox)

      expect(checkbox.querySelector('.Mui-checked')).toBeTruthy()
    })
  })
  it('displays api errors', async () => {
    const { findByTestId } = renderContainer(false, {
      login: { password: ['password error'], username: ['username error'] },
    })
    const message = await waitForElement(() => findByTestId('alert'))

    expect(message).toBeTruthy()
    expect(mockTranslation).toHaveBeenLastCalledWith('forms.login.errorMessage')
    expect(message.textContent).toEqual('forms.login.errorMessage')
  })
  describe('dispatches action to login', () => {
    it('with username', async () => {
      validators.emailIsValid = mockValidatorFalse
      userActions.login = mockLogin

      const { findByTestId } = renderContainer()
      const unique = await waitForElement(() => findByTestId('signIn--unique'))
      const password = await waitForElement(() =>
        findByTestId('signIn--password'),
      )
      const button = await waitForElement(() =>
        findByTestId('signIn--submitButton'),
      )

      fireEvent.change(unique.querySelector('input'), {
        target: { value: User.username },
      })
      fireEvent.change(password.querySelector('input'), {
        target: { value: User.password },
      })

      fireEvent.click(button)

      expect(mockDispatch).toHaveBeenCalledTimes(1)
      expect(mockLogin).toHaveBeenCalledWith({
        username: 'test_username',
        password: 'test_password',
        rememberMe: false,
      })
    })
    it('with email', async () => {
      validators.emailIsValid = mockValidatorTrue
      userActions.login = mockLogin

      const { findByTestId } = renderContainer()
      const unique = await waitForElement(() => findByTestId('signIn--unique'))
      const password = await waitForElement(() =>
        findByTestId('signIn--password'),
      )
      const button = await waitForElement(() =>
        findByTestId('signIn--submitButton'),
      )

      fireEvent.change(unique.querySelector('input'), {
        target: { value: User.email },
      })
      fireEvent.change(password.querySelector('input'), {
        target: { value: User.password },
      })

      fireEvent.click(button)

      expect(mockDispatch).toHaveBeenCalledTimes(1)
      expect(mockLogin).toHaveBeenCalledWith({
        email: 'test@mail.com',
        password: 'test_password',
        rememberMe: false,
      })
    })
  })
  it('does not crash if dispatching does not work', async () => {
    mockDispatch.mockImplementationOnce(() => {
      throw 'some error'
    })
    const mockConsole = jest.fn(msg => msg)
    console.error = mockConsole

    const { container, findByTestId } = renderContainer()
    const unique = await waitForElement(() => findByTestId('signIn--unique'))
    const password = await waitForElement(() =>
      findByTestId('signIn--password'),
    )
    const button = await waitForElement(() =>
      findByTestId('signIn--submitButton'),
    )

    fireEvent.change(unique.querySelector('input'), {
      target: { value: User.username },
    })
    fireEvent.change(password.querySelector('input'), {
      target: { value: User.password },
    })

    fireEvent.click(button)

    expect(mockDispatch).toHaveBeenCalledTimes(1)
    expect(mockConsole).toHaveBeenCalledWith('some error')
    expect(container).toBeTruthy()
  })
  it('displays loading icon while logging in', async () => {
    const { findByTestId } = renderContainer(true)
    const loading = await waitForElement(() => findByTestId('signIn--loading'))

    expect(loading).toBeTruthy()
  })
  it('translates the text', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('forms.login.header')
    expect(mockTranslation).toHaveBeenCalledWith('forms.labels.password')
    expect(mockTranslation).toHaveBeenCalledWith('forms.labels.remember')
    expect(mockTranslation).toHaveBeenCalledWith('forms.login.button')
    expect(mockTranslation).toHaveBeenCalledWith('forms.login.links.signUp')
  })
})
