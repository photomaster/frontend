import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import SignUp from './sign-up'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import { userActions } from 'actions'
import validators from 'helpers/validators'

const User = {
  username: 'test_username',
  displayName: 'test_displayName',
  email: 'test@mail.com',
  password: 'test_password',
  password2: 'test_password',
}

const mockTranslation = jest.fn(str => str)
const mockDispatch = jest.fn()
const mockRegister = jest.fn(_ => Promise.resolve(true))
const mockValidatorTrue = jest.fn(_ => true)
const mockValidatorFalse = jest.fn(_ => false)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

function renderContainer(active = false, errors = {}) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: { registering: active, errors: errors },
        })}
      >
        <SignUp />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Sign Up View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  describe('validates and sets new value for', () => {
    it('username', async () => {
      validators.usernameIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const username = await waitForElement(() =>
        findByTestId('signUp--username'),
      )

      fireEvent.change(username.querySelector('input'), {
        target: { value: User.username },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(username.querySelector('input').getAttribute('value')).toEqual(
        'test_username',
      )
    })
    it('displayName', async () => {
      validators.displayNameIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const displayName = await waitForElement(() =>
        findByTestId('signUp--displayName'),
      )

      fireEvent.change(displayName.querySelector('input'), {
        target: { value: User.displayName },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(displayName.querySelector('input').getAttribute('value')).toEqual(
        'test_displayName',
      )
    })
    it('email', async () => {
      validators.emailIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const email = await waitForElement(() => findByTestId('signUp--email'))

      fireEvent.change(email.querySelector('input'), {
        target: { value: User.email },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(email.querySelector('input').getAttribute('value')).toEqual(
        'test@mail.com',
      )
    })
    it('password (password2 empty)', async () => {
      validators.passwordIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const password = await waitForElement(() =>
        findByTestId('signUp--password'),
      )

      fireEvent.change(password.querySelector('input'), {
        target: { value: User.password },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(password.querySelector('input').getAttribute('value')).toEqual(
        'test_password',
      )
    })
    it('password (password2 given)', async () => {
      validators.passwordIsValid = mockValidatorTrue
      validators.passwordsMatch = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const password = await waitForElement(() =>
        findByTestId('signUp--password'),
      )
      const password2 = await waitForElement(() =>
        findByTestId('signUp--password2'),
      )

      fireEvent.change(password2.querySelector('input'), {
        target: { value: User.password2 },
      })
      fireEvent.change(password.querySelector('input'), {
        target: { value: User.password },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(2)
      expect(password.querySelector('input').getAttribute('value')).toEqual(
        'test_password',
      )
    })
    it('password2', async () => {
      const { findByTestId } = renderContainer()
      const password2 = await waitForElement(() =>
        findByTestId('signUp--password2'),
      )

      fireEvent.change(password2.querySelector('input'), {
        target: { value: User.password2 },
      })

      expect(password2.querySelector('input').getAttribute('value')).toEqual(
        'test_password',
      )
    })
  })
  describe('displays error for', () => {
    it('username', async () => {
      validators.usernameIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const username = await waitForElement(() =>
        findByTestId('signUp--username'),
      )

      fireEvent.change(username.querySelector('input'), {
        target: { value: User.username },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(username.querySelector('.Mui-error')).toBeTruthy()
    })
    it('displayName', async () => {
      validators.displayNameIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const displayName = await waitForElement(() =>
        findByTestId('signUp--displayName'),
      )

      fireEvent.change(displayName.querySelector('input'), {
        target: { value: User.displayName },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(displayName.querySelector('.Mui-error')).toBeTruthy()
    })
    it('email', async () => {
      validators.emailIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const email = await waitForElement(() => findByTestId('signUp--email'))

      fireEvent.change(email.querySelector('input'), {
        target: { value: User.email },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(email.querySelector('.Mui-error')).toBeTruthy()
    })
    it('password (password2 empty)', async () => {
      validators.passwordIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const password = await waitForElement(() =>
        findByTestId('signUp--password'),
      )

      fireEvent.change(password.querySelector('input'), {
        target: { value: User.password },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(password.querySelector('.Mui-error')).toBeTruthy()
    })
    it('password (password2 given)', async () => {
      validators.passwordIsValid = mockValidatorTrue
      validators.passwordsMatch = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const password = await waitForElement(() =>
        findByTestId('signUp--password'),
      )
      const password2 = await waitForElement(() =>
        findByTestId('signUp--password2'),
      )

      fireEvent.change(password2.querySelector('input'), {
        target: { value: User.password2 },
      })
      fireEvent.change(password.querySelector('input'), {
        target: { value: User.password },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(password2.querySelector('.Mui-error')).toBeTruthy()
    })
    it('password2', async () => {
      const { findByTestId } = renderContainer()
      const password2 = await waitForElement(() =>
        findByTestId('signUp--password2'),
      )

      fireEvent.change(password2.querySelector('input'), {
        target: { value: User.password2 },
      })

      expect(password2.querySelector('.Mui-error')).toBeTruthy()
    })
    it('api errors', async () => {
      const { findByTestId } = renderContainer(false, {
        register: { email: ['email error'], username: ['username error'] },
      })
      const message = await waitForElement(() => findByTestId('alert'))

      expect(message).toBeTruthy()
      expect(message.textContent).toContain('email error', 'username error')
    })
  })
  it('dispatches action to register', async () => {
    validators.usernameIsValid = mockValidatorTrue
    validators.displayNameIsValid = mockValidatorTrue
    validators.emailIsValid = mockValidatorTrue
    validators.passwordIsValid = mockValidatorTrue
    validators.passwordsMatch = mockValidatorTrue
    userActions.register = mockRegister

    const { findByTestId } = renderContainer()
    const username = await waitForElement(() =>
      findByTestId('signUp--username'),
    )
    const displayName = await waitForElement(() =>
      findByTestId('signUp--displayName'),
    )
    const email = await waitForElement(() => findByTestId('signUp--email'))
    const password = await waitForElement(() =>
      findByTestId('signUp--password'),
    )
    const password2 = await waitForElement(() =>
      findByTestId('signUp--password2'),
    )
    const button = await waitForElement(() =>
      findByTestId('signUp--submitButton'),
    )

    fireEvent.change(username.querySelector('input'), {
      target: { value: User.username },
    })
    fireEvent.change(displayName.querySelector('input'), {
      target: { value: User.displayName },
    })
    fireEvent.change(email.querySelector('input'), {
      target: { value: User.email },
    })
    fireEvent.change(password.querySelector('input'), {
      target: { value: User.password },
    })
    fireEvent.change(password2.querySelector('input'), {
      target: { value: User.password2 },
    })

    fireEvent.click(button)

    expect(mockDispatch).toHaveBeenCalledTimes(1)
    expect(mockRegister).toHaveBeenCalledWith({
      username: 'test_username',
      displayName: 'test_displayName',
      email: 'test@mail.com',
      password: 'test_password',
    })
  })
  it('does not crash if dispatching does not work', async () => {
    validators.usernameIsValid = mockValidatorTrue
    validators.displayNameIsValid = mockValidatorTrue
    validators.emailIsValid = mockValidatorTrue
    validators.passwordIsValid = mockValidatorTrue
    validators.passwordsMatch = mockValidatorTrue
    mockDispatch.mockImplementationOnce(() => {
      throw 'some error'
    })
    const mockConsole = jest.fn(msg => msg)
    console.error = mockConsole

    const { container, findByTestId } = renderContainer()
    const username = await waitForElement(() =>
      findByTestId('signUp--username'),
    )
    const displayName = await waitForElement(() =>
      findByTestId('signUp--displayName'),
    )
    const email = await waitForElement(() => findByTestId('signUp--email'))
    const password = await waitForElement(() =>
      findByTestId('signUp--password'),
    )
    const password2 = await waitForElement(() =>
      findByTestId('signUp--password2'),
    )
    const button = await waitForElement(() =>
      findByTestId('signUp--submitButton'),
    )

    fireEvent.change(username.querySelector('input'), {
      target: { value: User.username },
    })
    fireEvent.change(displayName.querySelector('input'), {
      target: { value: User.displayName },
    })
    fireEvent.change(email.querySelector('input'), {
      target: { value: User.email },
    })
    fireEvent.change(password.querySelector('input'), {
      target: { value: User.password },
    })
    fireEvent.change(password2.querySelector('input'), {
      target: { value: User.password2 },
    })

    fireEvent.click(button)

    expect(mockDispatch).toHaveBeenCalledTimes(1)
    expect(mockConsole).toHaveBeenCalledWith('some error')
    expect(container).toBeTruthy()
  })
  it('displays loading icon while registering', async () => {
    const { findByTestId } = renderContainer(true)
    const loading = await waitForElement(() => findByTestId('signUp--loading'))

    expect(loading).toBeTruthy()
  })
  it('translates the text', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('forms.register.header')
    expect(mockTranslation).toHaveBeenCalledWith('forms.labels.username')
    expect(mockTranslation).toHaveBeenCalledWith(
      'forms.helperTexts.displayName',
    )
    expect(mockTranslation).toHaveBeenCalledWith('forms.register.button')
    expect(mockTranslation).toHaveBeenCalledWith('forms.register.links.signIn')
  })
})
