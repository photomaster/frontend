import React, { useState, useEffect } from 'react'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import CircularProgress from '@material-ui/core/CircularProgress'
import { NavLink } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import validators from 'helpers/validators'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import ErrorMessage from 'components/formElements/errorMessage'
import Avatar from 'components/utilities/avatar'
import { userActions } from '../../actions'

export default function SignUp() {
  const baseClasses = baseStyles()
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const [username, setUsername] = useState('')
  const [displayName, setDisplayname] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [password2, setPassword2] = useState('')
  const [errors, setErrors] = useState({
    displayName: false,
    username: false,
    email: false,
    password: false,
    password2: false,
  })
  const active = useSelector(state => state.currentUser.registering)
  const apiErrors = useSelector(state => state.currentUser.errors)

  useEffect(() => {
    return function cleanup() {
      dispatch({ type: 'reset/errors' })
    }
  }, [])

  async function _handleSubmit(e) {
    e.preventDefault()

    try {
      dispatch(
        userActions.register({
          username,
          displayName,
          email,
          password,
        }),
      )
    } catch (error) {
      console.error(error)
    }
  }

  const _handleUsernameChange = event => {
    const value = event.target.value
    let errorUsername = false
    if (value && !validators.usernameIsValid(value)) {
      errorUsername = true
    }
    setUsername(value)
    setErrors({ ...errors, username: errorUsername })
  }

  const _handleDisplaynameChange = event => {
    const value = event.target.value
    let errorDisplayname = false
    if (value && !validators.displayNameIsValid(value)) {
      errorDisplayname = true
    }
    setDisplayname(value)
    setErrors({ ...errors, displayName: errorDisplayname })
  }

  const _handleEmailChange = event => {
    const value = event.target.value
    let errorEmail = false
    if (value && !validators.emailIsValid(value)) {
      errorEmail = true
    }
    setEmail(value)
    setErrors({ ...errors, email: errorEmail })
  }

  const _handlePasswordChange = event => {
    const value = event.target.value
    let errorPassword = false
    if (value && !validators.passwordIsValid(value)) {
      errorPassword = true
    }
    setPassword(value)

    let errorPassword2 = false
    if (password2 && !validators.passwordsMatch(value, password2)) {
      errorPassword2 = true
    }
    setErrors({ ...errors, password: errorPassword, password2: errorPassword2 })
  }

  const _handlePasswort2Change = event => {
    const value = event.target.value
    let errorPassword2 = false
    if (value && value != password) {
      errorPassword2 = true
    }
    setPassword2(value)
    setErrors({ ...errors, password2: errorPassword2 })
  }

  const _filledInCompletely = () => {
    if (!username || !displayName || !email || !password || !password2)
      return false
    return true
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={baseClasses.paper}>
        <Avatar
          alt={t('forms.register.header')}
          additionalClasses={baseClasses.avatar}
        >
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h2">
          {t('forms.register.header')}
        </Typography>
        <form
          className={`${baseClasses.fullWidth} ${baseClasses.marginTop3}`}
          noValidate
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoComplete="username"
                name="username"
                variant="outlined"
                required
                fullWidth
                id="username"
                label={t('forms.labels.username')}
                autoFocus
                value={username}
                onChange={_handleUsernameChange}
                error={errors.username}
                helperText={
                  errors.username
                    ? validators.errorMessages.username
                    : t('forms.helperTexts.username')
                }
                disabled={active}
                data-testid={'signUp--username'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                autoComplete="nickname"
                name="displayName"
                variant="outlined"
                required
                fullWidth
                id="displayName"
                label={t('forms.labels.displayName')}
                value={displayName}
                onChange={_handleDisplaynameChange}
                error={errors.displayName}
                helperText={
                  errors.displayName
                    ? validators.errorMessages.displayName
                    : t('forms.helperTexts.displayName')
                }
                disabled={active}
                data-testid={'signUp--displayName'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label={t('forms.labels.email')}
                name="email"
                autoComplete="email"
                value={email}
                onChange={_handleEmailChange}
                error={errors.email}
                helperText={errors.email ? validators.errorMessages.email : ''}
                disabled={active}
                data-testid={'signUp--email'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label={t('forms.labels.password')}
                type="password"
                id="password"
                autoComplete="current-password"
                value={password}
                onChange={_handlePasswordChange}
                error={errors.password}
                helperText={
                  errors.password ? validators.errorMessages.password : ''
                }
                disabled={active}
                data-testid={'signUp--password'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password2"
                label={t('forms.labels.password2')}
                type="password"
                id="password2"
                value={password2}
                onChange={_handlePasswort2Change}
                error={errors.password2}
                helperText={
                  errors.password2
                    ? validators.errorMessages.passwordsMatch
                    : ''
                }
                disabled={active}
                data-testid={'signUp--password2'}
              />
            </Grid>
          </Grid>
          {apiErrors && apiErrors.register && (
            <ErrorMessage text={apiErrors.register} noTranslate mt />
          )}
          <span className={baseClasses.relative}>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={baseClasses.submit}
              disabled={
                !_filledInCompletely() ||
                Object.values(errors).includes(true) ||
                active
              }
              onClick={e => _handleSubmit(e)}
              data-testid={'signUp--submitButton'}
            >
              {t('forms.register.button')}
            </Button>
            {active && (
              <CircularProgress
                size={24}
                className={baseClasses.buttonProgress}
                data-testid={'signUp--loading'}
              />
            )}
          </span>
          <Grid container justify="flex-end">
            <Grid item>
              <NavLink to="/login" variant="body2">
                {t('forms.register.links.signIn')}
              </NavLink>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5} />
    </Container>
  )
}
