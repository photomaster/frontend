import React from 'react'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import { useTranslation } from 'react-i18next'
import baseStyles from 'styles/baseClasses'
import SharedWorkspaceImage from 'assets/undraw/backup/shared_workspace.svg'

export default function Feedback() {
  const baseClasses = baseStyles()
  const { t } = useTranslation()
  const subject = 'Feedback%3A%20'
  const body = 'Hi%20Lisa%20%26%20Lukas!%0D%0A%0D%0A'

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <Typography component="h1" variant="h1">
        {t('feedback.header')}
      </Typography>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Typography variant="body1">{t('feedback.introduction')}</Typography>
        </Grid>
        <Grid container justify={'center'} style={{ display: 'flex' }}>
          <Button
            variant="contained"
            color="primary"
            href={`mailto:photomailfhs+feedback@gmail.com?subject=${subject}&body=${body}`}
            data-testid={'button--email'}
          >
            {t('privacy.button')}
          </Button>
        </Grid>
        <Grid item xs={12}>
          <img
            className={baseClasses.image}
            src={SharedWorkspaceImage}
            alt={t('feedback.alt')}
          />
        </Grid>
      </Grid>
    </Container>
  )
}
