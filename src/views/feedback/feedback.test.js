import React from 'react'
import { render, cleanup, waitForElement } from '@testing-library/react'
import i18nextConfig from '../../i18nTesting'
import Feedback from './feedback'

function renderContainer() {
  const result = render(<Feedback />)
  return result
}

afterEach(cleanup)
describe('Feedback View', () => {
  it('renders without crashing', () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('contains an email button', async () => {
    const { getByTestId } = renderContainer()
    const button = await waitForElement(() => getByTestId('button--email'))

    expect(button).toBeTruthy()
    expect(button.getAttribute('href')).toContain(
      'mailto:photomailfhs+feedback@gmail.com',
    )
  })
})
