import React from 'react'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import './privacy.scss'
import { useTranslation } from 'react-i18next'
import baseStyles from 'styles/baseClasses'
import PrivacyImage from 'assets/undraw/privacy_protection.svg'

export default function Privacy() {
  const { t } = useTranslation()
  const baseClasses = baseStyles()
  const body = 'Hi%20Lisa%20%26%20Lukas!%0D%0A%0D%0A'

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <Typography component="h1" variant="h1" className={baseClasses.wordBreak}>
        {t('pages.privacy')}
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="body1">{t('privacy.introduction')}</Typography>
        </Grid>
        <Grid container justify="center" style={{ display: 'flex' }}>
          <Button
            variant="contained"
            color="primary"
            href={`mailto:photomailfhs+inquiry@gmail.com?body=${body}`}
            data-testid={'button--email'}
          >
            {t('privacy.button')}
          </Button>
        </Grid>
        <Grid item xs={12}>
          <img
            className={baseClasses.image}
            src={PrivacyImage}
            alt={t('privacy.alt')}
          />
        </Grid>
      </Grid>
    </Container>
  )
}
