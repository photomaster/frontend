import React, { useState, useEffect } from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Container from '@material-ui/core/Container'
import Collapse from '@material-ui/core/Collapse'
import Button from '@material-ui/core/Button'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import CssBaseline from '@material-ui/core/CssBaseline'
import RecommendationCard from 'components/recommendationCard/recommendationCard'
import RecommendationsGrid from 'components/recommendationCard/recommendationsGrid'
import Box from '@material-ui/core/Box'
import { useTranslation } from 'react-i18next'
import { photoService } from 'service'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Paper from '@material-ui/core/Paper'
import IconButton from '@material-ui/core/IconButton'
import SearchIcon from '@material-ui/icons/Search'
import FilterListIcon from '@material-ui/icons/FilterList'
import ServerErrorImage from 'assets/undraw/server_error.svg'
import WbSunnyIcon from '@material-ui/icons/WbSunny'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import FlashOnIcon from '@material-ui/icons/FlashOn'
import GrainIcon from '@material-ui/icons/Grain'
import CloudIcon from '@material-ui/icons/Cloud'
import AcUnitIcon from '@material-ui/icons/AcUnit'
import Slider from '@material-ui/core/Slider'
import { Skeleton } from '@material-ui/lab'
import RemoteAutocompleteInput from 'components/remoteAutocompleteInput/remoteAutocompleteInput'

const useStyles = makeStyles(theme => ({
  searchButton: {
    [theme.breakpoints.down('xs')]: {
      justifyContent: 'center',
      width: '100%',
    },
  },
  searchButtonBox: {
    [theme.breakpoints.down('xs')]: {
      marginLeft: theme.spacing(3),
      marginRight: theme.spacing(3),
    },
  },
  advandedFilters: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(3),
  },
  advandedFiltersPaper: {
    padding: theme.spacing(1),
  },
  noResults: {
    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
    },
  },
  root: {
    padding: theme.spacing(1),
    display: 'flex',
    alignItems: 'center',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  formControl: {
    display: 'block',
  },
  formSelect: {
    width: '100%',
  },
  errorImage: {
    marginBottom: '4em',
    display: 'block',
    width: '100%',
  },
}))

const temperatureMin = -30
const temperatureMax = 50
const temperatureMarks = [
  {
    value: temperatureMin,
    label: `${temperatureMin}°C`,
  },
  {
    value: 0,
    label: '0°C',
  },
  {
    value: 10,
    label: '10°C',
  },
  {
    value: 20,
    label: '20°C',
  },
  {
    value: 30,
    label: '30°C',
  },
  {
    value: temperatureMax,
    label: `${temperatureMax}°C`,
  },
]

export default function SearchPage(props) {
  const { t } = useTranslation()
  const classes = useStyles(props)

  const [query, setQuery] = useState('')
  const [make, setMake] = useState('')
  const [model, setModel] = useState('')
  const [photos, setPhotos] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [loading, setLoading] = useState(true)
  const [hasError, setHasError] = useState(false)
  const [usingInitialPhotos, setUsingInitialPhotos] = useState(true)
  const [showAdvancedFilters, setShowAdvancedFilters] = useState(false)
  const [season, setSeason] = useState(false)
  const [weather, setWeather] = useState(false)
  const [temperature, setTemperature] = useState([
    temperatureMin,
    temperatureMax,
  ])
  const [internalTemperature, setInternalTemperature] = useState([
    temperatureMin,
    temperatureMax,
  ])

  useEffect(() => {
    _applySearch()
  }, [season, weather, currentPage, make, model, query, temperature])

  async function _applySearch() {
    setLoading(true)
    try {
      const params = {
        offset: (currentPage - 1) * 9,
      }
      if (season) {
        params.season = season
      }
      if (weather) {
        params.weather = weather
      }
      if (make) {
        params.make = make
      }
      if (model) {
        params.model = model
      }
      if (
        temperature &&
        (temperature[0] !== temperatureMin || temperature[1] !== temperatureMax)
      ) {
        params.temperatureMin = temperature[0]
        params.temperatureMax = temperature[1]
      }
      const fetchedPhotos = await photoService.getPhotosByQuery(query, params)
      if (hasError) {
        setHasError(false)
      }
      setPhotos(fetchedPhotos)
      if (query && query.length !== 0) {
        setUsingInitialPhotos(false)
      }
    } catch (e) {
      console.error(e)
      setHasError(true)
      setPhotos([])
    }
    setLoading(false)
  }

  function _updatePage(v) {
    setCurrentPage(currentPage + v)
  }

  const _backDisabled = () => {
    return currentPage <= 1
  }

  const _nextDisabled = () => {
    return photos.length <= 8
  }

  const _getWeatherIcon = () => {
    if (!weather || weather === 'clear') {
      return (
        <WbSunnyIcon
          style={{ marginRight: '.25em' }}
          data-testid={'searchPage--icon--clear'}
        />
      )
    }

    if (weather === 'rain' || weather === 'drizzle') {
      return (
        <GrainIcon
          style={{ marginRight: '.25em' }}
          data-testid={'searchPage--icon--rain'}
        />
      )
    }

    if (weather === 'snow') {
      return (
        <AcUnitIcon
          style={{ marginRight: '.25em' }}
          data-testid={'searchPage--icon--snow'}
        />
      )
    }

    if (weather === 'thunderstorm') {
      return (
        <FlashOnIcon
          style={{ marginRight: '.25em' }}
          data-testid={'searchPage--icon--thunderstorm'}
        />
      )
    }

    if (weather === 'clouds') {
      return (
        <CloudIcon
          style={{ marginRight: '.25em' }}
          data-testid={'searchPage--icon--clouds'}
        />
      )
    }
  }

  const _results = () => {
    if (hasError) {
      return (
        <Grid container justify="center">
          <Grid item xs={10} md={6} data-testid={'searchPage--error'}>
            <img
              className={classes.errorImage}
              src={ServerErrorImage}
              alt={''}
            />
            <Typography component="h3" variant="h2">
              {t('search.results.errorHeadline')}
            </Typography>
          </Grid>
        </Grid>
      )
    }
    if (loading) {
      return (
        <Grid item xs={12} data-testid={'searchPage--loading'}>
          <Box my={2}>
            {Array(4).map(() => (
              <Skeleton variant="rect" width={210} height={118} />
            ))}
          </Box>
        </Grid>
      )
    }
    if (photos.length === 0) {
      return (
        <Grid item xs={12} data-testid={'searchPage--noResults'}>
          <Box m={4} className={classes.noResults}>
            <Typography components="h3" variant="body2">
              {t('search.results.noResults')}
            </Typography>
          </Box>
        </Grid>
      )
    }
    if (photos.length > 0) {
      return (
        <Grid item xs={12} data-testid={'searchPage--results'}>
          {!usingInitialPhotos && (
            <Box mb={2}>
              <Typography components="h3" variant="h3">
                {t('search.results.headline')} ({t('search.results.page')}{' '}
                {currentPage})
              </Typography>
            </Box>
          )}
          <RecommendationsGrid>
            {photos.map(photo => (
              <RecommendationCard
                key={photo.id}
                photo={photo}
                user={photo.user}
              />
            ))}
          </RecommendationsGrid>
        </Grid>
      )
    }
  }

  return (
    <Container component="main">
      <CssBaseline />
      <Typography component="h1" variant="h1">
        {t('search.page.header')}
      </Typography>
      <Grid container>
        <Grid container justify="center">
          <Grid item xs={12} sm={9} md={10} data-testid={'searchPage--query'}>
            <Paper component="div" className={classes.root}>
              <RemoteAutocompleteInput
                fieldName="name"
                placeholder={t('search.form.input.placeholder')}
                value={query}
                onChange={(e, curQuery) => {
                  setQuery(curQuery)
                }}
                autoFocus
              />
              <IconButton
                type="button"
                className={classes.iconButton}
                aria-label={t('search.aria.search')}
                onClick={() => _applySearch()}
                data-testid={'searchPage--buttonSearch'}
              >
                <SearchIcon />
              </IconButton>
              <IconButton
                className={classes.iconButton}
                onClick={() => setShowAdvancedFilters(!showAdvancedFilters)}
                aria-label={t('search.aria.filter')}
                data-testid={'searchPage--buttonShowFilters'}
              >
                <FilterListIcon />
              </IconButton>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={9} md={10} className={classes.advandedFilters}>
            <Collapse in={showAdvancedFilters}>
              <Paper className={classes.advandedFiltersPaper}>
                <Grid
                  container
                  spacing={3}
                  alignItems="center"
                  justify="center"
                >
                  <Grid item xs={6} md={3} data-testid={'searchPage--make'}>
                    <RemoteAutocompleteInput
                      fieldName="photodata.make"
                      variant="outlined"
                      size="small"
                      label={t('search.form.filter.make')}
                      value={make}
                      onChange={(e, curMake) => {
                        setMake(curMake)
                      }}
                    />
                  </Grid>
                  <Grid item xs={6} md={3} data-testid={'searchPage--model'}>
                    <RemoteAutocompleteInput
                      fieldName="photodata.model"
                      variant="outlined"
                      size="small"
                      label={t('search.form.filter.model')}
                      value={model}
                      onChange={(e, curModel) => {
                        setModel(curModel)
                      }}
                    />
                  </Grid>
                  <Grid item xs={6} md={3}>
                    <FormControl className={classes.formControl}>
                      <InputLabel id="season-label">
                        <Box
                          display="flex"
                          alignContent="center"
                          alignItems="center"
                        >
                          <CalendarTodayIcon style={{ marginRight: '.25em' }} />{' '}
                          {t('search.form.filter.season')}
                        </Box>
                      </InputLabel>
                      <Select
                        labelId="season-label"
                        value={season}
                        onChange={e => setSeason(e.target.value)}
                        className={classes.formSelect}
                        data-testid={'searchPage--select--season'}
                      >
                        <MenuItem value={false}>
                          <em>&nbsp;</em>
                        </MenuItem>
                        <MenuItem
                          value="winter"
                          data-testid={'searchPage--select--winter'}
                        >
                          {t('search.form.filter.values.season.winter')}
                        </MenuItem>
                        <MenuItem value="spring">
                          {t('search.form.filter.values.season.spring')}
                        </MenuItem>
                        <MenuItem value="summer">
                          {t('search.form.filter.values.season.summer')}
                        </MenuItem>
                        <MenuItem value="fall">
                          {t('search.form.filter.values.season.fall')}
                        </MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={6} md={3}>
                    <FormControl className={classes.formControl}>
                      <InputLabel id="weather-label">
                        <Box
                          display="flex"
                          alignContent="center"
                          alignItems="center"
                        >
                          {_getWeatherIcon()} {t('search.form.filter.weather')}
                        </Box>
                      </InputLabel>
                      <Select
                        labelId="weather-label"
                        value={weather}
                        onChange={e => setWeather(e.target.value)}
                        className={classes.formSelect}
                        data-testid={'searchPage--select--weather'}
                      >
                        <MenuItem value={false}>
                          <em>&nbsp;</em>
                        </MenuItem>
                        <MenuItem
                          value="thunderstorm"
                          data-testid={'searchPage--select--thunderstorm'}
                        >
                          {t('search.form.filter.values.weather.thunderstorm')}
                        </MenuItem>
                        <MenuItem
                          value="drizzle"
                          data-testid={'searchPage--select--drizzle'}
                        >
                          {t('search.form.filter.values.weather.drizzle')}
                        </MenuItem>
                        <MenuItem
                          value="rain"
                          data-testid={'searchPage--select--rain'}
                        >
                          {t('search.form.filter.values.weather.rain')}
                        </MenuItem>
                        <MenuItem
                          value="snow"
                          data-testid={'searchPage--select--snow'}
                        >
                          {t('search.form.filter.values.weather.snow')}
                        </MenuItem>
                        <MenuItem
                          value="clear"
                          data-testid={'searchPage--select--clear'}
                        >
                          {t('search.form.filter.values.weather.clear')}
                        </MenuItem>
                        <MenuItem
                          value="clouds"
                          data-testid={'searchPage--select--clouds'}
                        >
                          {t('search.form.filter.values.weather.clouds')}
                        </MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid container justify="center">
                    <Grid item xs={12} md={6}>
                      <FormControl className={classes.formControl}>
                        <Typography id="discrete-slider-custom" gutterBottom>
                          {t('search.form.filter.temperature')}
                        </Typography>
                        <Slider
                          data-testid={'searchPage--temperature-slider'}
                          value={internalTemperature}
                          min={temperatureMin}
                          max={temperatureMax}
                          marks={temperatureMarks}
                          onChange={(e, v) => setInternalTemperature(v)}
                          onChangeCommitted={(e, v) => setTemperature(v)}
                          valueLabelDisplay="auto"
                          getAriaValueText={() => {
                            return `${internalTemperature[0]}-${internalTemperature[1]}`
                          }}
                          aria-labelledby="range-slider"
                        />
                      </FormControl>
                    </Grid>
                  </Grid>
                </Grid>
              </Paper>
            </Collapse>
          </Grid>
        </Grid>
        {_results()}
        {!_backDisabled() || !_nextDisabled() ? (
          <Grid container spacing={4} justify="center" style={{ marginTop: 0 }}>
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                onClick={() => _updatePage(-1)}
                disabled={_backDisabled()}
                data-testid={'searchPage--buttonLastPage'}
              >
                {t('search.form.buttons.back')}
              </Button>
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                onClick={() => _updatePage(1)}
                disabled={_nextDisabled()}
                data-testid={'searchPage--buttonNextPage'}
              >
                {t('search.form.buttons.next')}
              </Button>
            </Grid>
          </Grid>
        ) : (
          ''
        )}
      </Grid>
    </Container>
  )
}
