import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
  wait,
  act,
} from '@testing-library/react'
import SearchPage from './search-page'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import photoService from 'service/photoService'
import autocompleteService from 'service/autocompleteService'

const User = {
  id: 1,
  username: 'test_username',
  displayName: 'test_displayName',
  email: 'test@mail.com',
  password: 'test_password',
  password2: 'test_password',
}

const Photos = [
  {
    id: 1,
    url: '/photo1',
    user: User,
    name: 'name1',
    description: 'desc1',
    imageThumbnailTeaser: '/url1',
  },
  {
    id: 2,
    url: '/photo2',
    user: User,
    name: 'name2',
    description: 'desc2',
    imageThumbnailTeaser: '/url2',
  },
]

const ManyPhotos = Array(...Array(10)).map((_, i) => {
  return {
    id: i,
    url: `/photo${i}`,
    user: User,
    name: `name${i}`,
    description: `desc${i}`,
    imageThumbnailTeaser: `/url${i}`,
  }
})

const mockTranslation = jest.fn(str => str)
const mockGetPhotosByQuery = jest.fn((query, params) => Promise.resolve(Photos))
const mockGetPhotosByQueryNone = jest.fn((query, params) => Promise.resolve([]))
const mockGetSuggestions = jest.fn(() =>
  Promise.resolve(['sug1', 'sug2', 'sug3']),
)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

jest.mock('service/photoService', () => jest.fn())
photoService.getPhotosByQuery = mockGetPhotosByQuery

jest.mock('service/autocompleteService', () => jest.fn())
autocompleteService.getSuggestions = mockGetSuggestions

function renderContainer() {
  const result = render(
    <Router>
      <Provider store={createStore(rootReducer, {})}>
        <SearchPage />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('SearchPage View', () => {
  it('renders without crashing', async () => {
    const { container, getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('searchPage--results')).toBeTruthy())

    expect(mockGetPhotosByQuery).toHaveBeenCalledTimes(1)
    expect(container).toBeTruthy()
  })
  describe('sets param in request', () => {
    it('query', async () => {
      const { findByTestId } = renderContainer()
      const buttonSearch = await waitForElement(() =>
        findByTestId('searchPage--buttonSearch'),
      )
      const buttonFilters = await waitForElement(() =>
        findByTestId('searchPage--buttonShowFilters'),
      )
      const queryContainer = await waitForElement(() =>
        findByTestId('searchPage--query'),
      )
      const autocomplete = await waitForElement(() =>
        queryContainer.querySelector('[data-testid="autocomplete"]'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.change(autocomplete.querySelector('input'), {
          target: { value: 'Rainy Forest' },
        })
      })

      act(() => {
        fireEvent.blur(autocomplete)
      })

      act(() => {
        fireEvent.click(buttonSearch)
      })

      await wait(() => expect(findByTestId('searchPage--results')).toBeTruthy())

      expect(mockGetPhotosByQuery).toHaveBeenLastCalledWith('Rainy Forest', {
        offset: 0,
      })
    })
    it('make', async () => {
      const { findByTestId } = renderContainer()
      const buttonSearch = await waitForElement(() =>
        findByTestId('searchPage--buttonSearch'),
      )
      const buttonFilters = await waitForElement(() =>
        findByTestId('searchPage--buttonShowFilters'),
      )
      const makeContainer = await waitForElement(() =>
        findByTestId('searchPage--make'),
      )
      const autocomplete = await waitForElement(() =>
        makeContainer.querySelector('[data-testid="autocomplete"]'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.change(autocomplete.querySelector('input'), {
          target: { value: 'Sony' },
        })
      })

      act(() => {
        fireEvent.blur(autocomplete)
      })

      act(() => {
        fireEvent.click(buttonSearch)
      })

      await wait(() => expect(findByTestId('searchPage--results')).toBeTruthy())

      expect(mockGetPhotosByQuery).toHaveBeenLastCalledWith('', {
        offset: 0,
        make: 'Sony',
      })
    })
    it('model', async () => {
      const { findByTestId } = renderContainer()
      const buttonSearch = await waitForElement(() =>
        findByTestId('searchPage--buttonSearch'),
      )
      const buttonFilters = await waitForElement(() =>
        findByTestId('searchPage--buttonShowFilters'),
      )
      const modelContainer = await waitForElement(() =>
        findByTestId('searchPage--model'),
      )
      const autocomplete = await waitForElement(() =>
        modelContainer.querySelector('[data-testid="autocomplete"]'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.change(autocomplete.querySelector('input'), {
          target: { value: 'D750' },
        })
      })

      act(() => {
        fireEvent.blur(autocomplete)
      })

      act(() => {
        fireEvent.click(buttonSearch)
      })

      await wait(() => expect(findByTestId('searchPage--results')).toBeTruthy())

      expect(mockGetPhotosByQuery).toHaveBeenLastCalledWith('', {
        offset: 0,
        model: 'D750',
      })
    })
    it('season', async () => {
      const baseDom = renderContainer()
      const buttonFilters = await waitForElement(() =>
        baseDom.getByTestId('searchPage--buttonShowFilters'),
      )
      const select = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--season'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.mouseDown(select.querySelector('[role="button"]'))
      })

      const option = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--winter'),
      )

      act(() => {
        fireEvent.click(option)
      })
      await wait(() =>
        expect(baseDom.getByTestId('searchPage--results')).toBeTruthy(),
      )

      expect(mockGetPhotosByQuery).toHaveBeenLastCalledWith('', {
        offset: 0,
        season: 'winter',
      })
    })
    it('weather', async () => {
      const baseDom = renderContainer()
      const buttonFilters = await waitForElement(() =>
        baseDom.getByTestId('searchPage--buttonShowFilters'),
      )
      const select = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--weather'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.mouseDown(select.querySelector('[role="button"]'))
      })

      const option = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--thunderstorm'),
      )

      act(() => {
        fireEvent.click(option)
      })
      await wait(() =>
        expect(baseDom.getByTestId('searchPage--results')).toBeTruthy(),
      )

      expect(mockGetPhotosByQuery).toHaveBeenLastCalledWith('', {
        offset: 0,
        weather: 'thunderstorm',
      })
    })
  })
  it('provides button to go to next page', async () => {
    const mockGetPhotosByQueryMany = jest.fn((query, params) =>
      Promise.resolve(ManyPhotos),
    )
    photoService.getPhotosByQuery = mockGetPhotosByQueryMany

    const { getByTestId } = renderContainer()
    const buttonNextPage = await waitForElement(() =>
      getByTestId('searchPage--buttonNextPage'),
    )
    await wait(() => expect(getByTestId('searchPage--results')).toBeTruthy())

    expect(mockGetPhotosByQueryMany).toHaveBeenCalledTimes(1)

    act(() => {
      fireEvent.click(buttonNextPage)
    })
    await wait(() => expect(getByTestId('searchPage--results')).toBeTruthy())

    expect(mockGetPhotosByQueryMany).toHaveBeenCalledTimes(2)
  })
  it('provides button to go to last page', async () => {
    const mockGetPhotosByQueryMany = jest.fn((query, params) =>
      Promise.resolve(ManyPhotos),
    )
    photoService.getPhotosByQuery = mockGetPhotosByQueryMany

    const { getByTestId } = renderContainer()
    const buttonNextPage = await waitForElement(() =>
      getByTestId('searchPage--buttonNextPage'),
    )
    await wait(() => expect(getByTestId('searchPage--results')).toBeTruthy())

    act(() => {
      fireEvent.click(buttonNextPage)
    })
    await wait(() => expect(getByTestId('searchPage--results')).toBeTruthy())

    const buttonLastPage = await waitForElement(() =>
      getByTestId('searchPage--buttonLastPage'),
    )

    act(() => {
      fireEvent.click(buttonLastPage)
    })
    await wait(() => expect(getByTestId('searchPage--results')).toBeTruthy())

    expect(mockGetPhotosByQueryMany).toHaveBeenCalledTimes(3)
  })
  describe('gets the right weather icon for', () => {
    it('clear', async () => {
      const baseDom = renderContainer()
      const buttonFilters = await waitForElement(() =>
        baseDom.getByTestId('searchPage--buttonShowFilters'),
      )
      const select = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--weather'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.mouseDown(select.querySelector('[role="button"]'))
      })

      const option = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--clear'),
      )

      act(() => {
        fireEvent.click(option)
      })
      await wait(() =>
        expect(baseDom.getByTestId('searchPage--results')).toBeTruthy(),
      )

      const icon = await waitForElement(() =>
        baseDom.getByTestId('searchPage--icon--clear'),
      )

      expect(icon).toBeTruthy()
    })
    it('rain', async () => {
      const baseDom = renderContainer()
      const buttonFilters = await waitForElement(() =>
        baseDom.getByTestId('searchPage--buttonShowFilters'),
      )
      const select = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--weather'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.mouseDown(select.querySelector('[role="button"]'))
      })

      const option = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--rain'),
      )

      act(() => {
        fireEvent.click(option)
      })
      await wait(() =>
        expect(baseDom.getByTestId('searchPage--results')).toBeTruthy(),
      )

      const icon = await waitForElement(() =>
        baseDom.getByTestId('searchPage--icon--rain'),
      )

      expect(icon).toBeTruthy()
    })
    it('snow', async () => {
      const baseDom = renderContainer()
      const buttonFilters = await waitForElement(() =>
        baseDom.getByTestId('searchPage--buttonShowFilters'),
      )
      const select = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--weather'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.mouseDown(select.querySelector('[role="button"]'))
      })

      const option = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--snow'),
      )

      act(() => {
        fireEvent.click(option)
      })
      await wait(() =>
        expect(baseDom.getByTestId('searchPage--results')).toBeTruthy(),
      )

      const icon = await waitForElement(() =>
        baseDom.getByTestId('searchPage--icon--snow'),
      )

      expect(icon).toBeTruthy()
    })
    it('thunderstorm', async () => {
      const baseDom = renderContainer()
      const buttonFilters = await waitForElement(() =>
        baseDom.getByTestId('searchPage--buttonShowFilters'),
      )
      const select = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--weather'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.mouseDown(select.querySelector('[role="button"]'))
      })

      const option = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--thunderstorm'),
      )

      act(() => {
        fireEvent.click(option)
      })
      await wait(() =>
        expect(baseDom.getByTestId('searchPage--results')).toBeTruthy(),
      )

      const icon = await waitForElement(() =>
        baseDom.getByTestId('searchPage--icon--thunderstorm'),
      )

      expect(icon).toBeTruthy()
    })
    it('clouds', async () => {
      const baseDom = renderContainer()
      const buttonFilters = await waitForElement(() =>
        baseDom.getByTestId('searchPage--buttonShowFilters'),
      )
      const select = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--weather'),
      )

      act(() => {
        fireEvent.click(buttonFilters)
      })

      act(() => {
        fireEvent.mouseDown(select.querySelector('[role="button"]'))
      })

      const option = await waitForElement(() =>
        baseDom.getByTestId('searchPage--select--clouds'),
      )

      act(() => {
        fireEvent.click(option)
      })
      await wait(() =>
        expect(baseDom.getByTestId('searchPage--results')).toBeTruthy(),
      )

      const icon = await waitForElement(() =>
        baseDom.getByTestId('searchPage--icon--clouds'),
      )

      expect(icon).toBeTruthy()
    })
  })
  it('translates the text', async () => {
    const { getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('searchPage--results')).toBeTruthy())

    expect(mockTranslation).toHaveBeenCalledWith('search.page.header')
    expect(mockTranslation).toHaveBeenCalledWith(
      'search.form.input.placeholder',
    )
    expect(mockTranslation).toHaveBeenCalledWith('search.aria.search')
  })
  it('displays error message', async () => {
    const mockReject = jest.fn((query, params) => Promise.reject('some error'))
    photoService.getPhotosByQuery = mockReject
    const mockConsole = jest.fn(msg => msg)
    console.error = mockConsole

    const { container, getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('searchPage--error')).toBeTruthy())

    expect(mockReject).toHaveBeenCalledTimes(1)
    expect(mockConsole).toHaveBeenLastCalledWith('some error')
    expect(mockTranslation).toHaveBeenCalledWith('search.results.errorHeadline')
  })
  it('removes error message after next query', async () => {
    const mockReject = jest.fn((query, params) => Promise.reject('some error'))
    photoService.getPhotosByQuery = mockReject

    const { container, getByTestId, findByTestId } = renderContainer()
    const buttonSearch = await waitForElement(() =>
      getByTestId('searchPage--buttonSearch'),
    )
    await wait(() => expect(getByTestId('searchPage--error')).toBeTruthy())

    photoService.getPhotosByQuery = mockGetPhotosByQuery

    act(() => {
      fireEvent.click(buttonSearch)
    })

    const results = await waitForElement(() =>
      getByTestId('searchPage--results'),
    )
    const error = container.querySelector('[data-testid="searchPage--error"]')

    expect(mockReject).toHaveBeenCalledTimes(1)
    expect(mockGetPhotosByQuery).toHaveBeenCalledTimes(1)
    expect(results).toBeTruthy()
    expect(error).toBeFalsy()
  })
  it('can handle no photo results', async () => {
    photoService.getPhotosByQuery = mockGetPhotosByQueryNone

    const { getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('searchPage--noResults')).toBeTruthy())

    expect(mockGetPhotosByQueryNone).toHaveBeenCalledTimes(1)
    expect(mockTranslation).toHaveBeenCalledWith('search.results.noResults')
  })
  xit('displays loading icon', async () => {
    const mockGetPhotosByQueryTimeout = jest.fn((query, params) =>
      Promise.reject('some error'),
    )
    photoService.getPhotosByQuery = mockGetPhotosByQueryTimeout

    const { getByTestId } = renderContainer()
    const loading = await waitForElement(() =>
      getByTestId('searchPage--loading'),
    )

    expect(mockGetPhotosByQueryTimeout).toHaveBeenCalledTimes(1)
    expect(loading).toBeTruthy()
  })
})
