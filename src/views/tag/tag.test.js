import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement, wait } from '@testing-library/react'
import Tag from './tag'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import photoService from 'service/photoService'
import useInfiniteScroll from 'helpers/scrollInfinite.js'

const User = {
  id: 100,
  displayName: 'someone else',
  avatarImage: 'url',
}

const Photo1 = {
  id: 1,
  url: '/photo1',
  user: User,
  name: 'name1',
  description: 'desc1',
  imageThumbnailTeaser: '/url1',
}

const Photo2 = {
  id: 2,
  url: '/photo2',
  user: User,
  name: 'name2',
  description: 'desc2',
  imageThumbnailTeaser: '/url2',
}

const mockTranslation = jest.fn(str => str)
const mockScrollTo = jest.fn()
const mockGetPhotos = jest.fn(() => Promise.resolve([Photo1, Photo2]))

window.scrollTo = mockScrollTo
jest.mock('service/photoService', () => jest.fn())

jest.mock('helpers/scrollInfinite.js')
useInfiniteScroll.mockReturnValue([false, jest.fn()])

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer(tag = 'someTag') {
  const result = render(
    <Router>
      <Provider store={createStore(rootReducer, {})}>
        <Tag match={{ params: { tag: tag } }} />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
  photoService.getPhotos = mockGetPhotos
})
afterEach(cleanup)
describe('Tag View', () => {
  it('renders without crashing', async () => {
    const { container, findByTestId } = renderContainer()
    await wait(() => expect(findByTestId('tag--introduction')).toBeTruthy())

    expect(mockGetPhotos).toHaveBeenCalledTimes(1)
    expect(container).toBeTruthy()
  })
  it('translates the text', async () => {
    const { findByTestId } = renderContainer()
    await wait(() => expect(findByTestId('tag--introduction')).toBeTruthy())

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('tags.introduction', {
      tag: 'someTag',
    })
  })
  it('can handle no photo results', async () => {
    const mockNoResults = jest.fn(() => Promise.resolve([]))
    photoService.getPhotos = mockNoResults

    const { findByTestId } = renderContainer()
    await wait(() => expect(findByTestId('tag--noResults')).toBeTruthy())

    expect(mockNoResults).toHaveBeenCalledTimes(1)
    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('tags.noResults', {
      tag: 'someTag',
    })
  })
  it('requests photos with given tag', async () => {
    const { findByTestId } = renderContainer()
    await wait(() => expect(findByTestId('tag--introduction')).toBeTruthy())

    expect(mockGetPhotos).toHaveBeenCalledTimes(1)
    expect(mockGetPhotos).toHaveBeenCalledWith({
      limit: 9,
      labels: ['someTag'],
    })
  })
  xit('requests more photos with given tag', async () => {
    useInfiniteScroll.mockImplementation((cb, _) => {
      cb()
    })

    const { findByTestId } = renderContainer()
    await wait(() => expect(findByTestId('tag--introduction')).toBeTruthy())

    // TOOD trigger cb of useInfiniteScroll

    expect(mockGetPhotos).toHaveBeenCalledTimes(1)
    expect(mockGetPhotos).toHaveBeenCalledWith({
      limit: 9,
      offset: 9,
      labels: ['someTag'],
    })
  })
  it('does not crash on photoService error', async () => {
    const mockReject = jest.fn(() => Promise.reject('some error'))
    const mockConsole = jest.fn(msg => msg)
    photoService.getPhotos = mockReject
    console.error = mockConsole

    const { container, findByTestId } = renderContainer()
    await wait(() => expect(findByTestId('tag--introduction')).toBeTruthy())

    expect(container).toBeTruthy()
    expect(mockReject).toHaveBeenCalledTimes(1)
    expect(mockConsole).toHaveBeenCalledWith('some error')
  })
  it('displays the photos with the tag', async () => {
    const { findAllByTestId } = renderContainer()
    const photos = await waitForElement(() =>
      findAllByTestId('recommendationCard--card'),
    )

    expect(photos).toBeTruthy()
    expect(photos.length).toEqual(2)
  })
})
