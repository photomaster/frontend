import React, { useState, useEffect } from 'react'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import photoService from '../../service/photoService'
import RecommendationCard from '../../components/recommendationCard/recommendationCard'
import RecommendationsGrid from '../../components/recommendationCard/recommendationsGrid'
import Loading from '../../components/utilities/loading'
import useInfiniteScroll from '../../helpers/scrollInfinite'

export default function Tag(props) {
  const baseClasses = baseStyles()
  const { t } = useTranslation()

  const [tag, setTag] = useState(props.match.params.tag)
  const [photos, setPhotos] = useState([])
  const [loading, setLoading] = useState(true)
  const [loadingAdditionalPhotos, setLoadingAdditionalPhotos] = useState(false)
  const limit = useState(9)
  const [offset, setOffset] = useState(9)
  const [counter, setCounter] = useState(0)
  const [fetchedAll, setFetchedAll] = useState(false)
  // eslint-disable-next-line no-unused-vars
  const [isFetching, setIsFetching] = useInfiniteScroll(
    fetchMoreData,
    fetchedAll,
  )

  if (tag != props.match.params.tag) {
    setTag(props.match.params.tag)
  }

  useEffect(() => {
    setLoading(true)
    window.scrollTo(0, 0)
    fetchInitialData()
  }, [tag])

  useEffect(() => {
    if (!loadingAdditionalPhotos) setIsFetching(false)
    if (photos && photos.length > 0) {
      if (counter === photos.length) setFetchedAll(true)
      else {
        setCounter(photos.length)
        setFetchedAll(false)
      }
    }
  }, [loadingAdditionalPhotos])

  async function fetchInitialData() {
    try {
      const fetchedPhotos = await photoService.getPhotos({
        limit: 9,
        labels: [tag],
      })
      setPhotos(fetchedPhotos)
      setLoading(false)
    } catch (e) {
      console.error(e)
    }
  }

  async function fetchMoreData() {
    if (loading || loadingAdditionalPhotos) return
    setLoadingAdditionalPhotos(true)
    try {
      const additionalPhotos = await photoService.getPhotos({
        limit: limit,
        offset: offset,
        labels: [tag],
      })
      setPhotos(photos.concat(additionalPhotos))
      setOffset(parseInt(offset) + parseInt(limit))
    } catch (e) {
      console.error(e)
    }
    setLoadingAdditionalPhotos(false)
  }

  return (
    <Container component="main" maxWidth="md" className="upload">
      <CssBaseline />
      <Typography component="h1" variant="h1">
        {tag}
      </Typography>
      {loading ? (
        <Loading />
      ) : photos.length == 0 ? (
        <Typography
          variant="body1"
          className={baseClasses.justifyContentCenter}
          data-testid={'tag--noResults'}
        >
          {t('tags.noResults', { tag })}
        </Typography>
      ) : (
        <React.Fragment>
          <Typography
            variant="body1"
            className={`${baseClasses.justifyContentCenter} ${baseClasses.marginBottom2}`}
            data-testid={'tag--introduction'}
          >
            {t('tags.introduction', { tag })}
          </Typography>
          <RecommendationsGrid>
            {photos.map(photo => (
              <RecommendationCard
                key={photo.id}
                photo={photo}
                user={photo.user}
              />
            ))}
          </RecommendationsGrid>
        </React.Fragment>
      )}
    </Container>
  )
}
