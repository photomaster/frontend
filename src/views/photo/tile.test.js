import React from 'react'
import { render, cleanup, waitForElement } from '@testing-library/react'
import Tile from './tile'
import TestIcon from '@material-ui/icons/PhotoCamera'

function renderContainer() {
  const result = render(
    <Tile headline={'a headline'} icon={TestIcon}>
      <div data-testid={'children'}></div>
    </Tile>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Tile', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('renders headline', async () => {
    const { getByTestId } = renderContainer()
    const headline = await waitForElement(() =>
      getByTestId('tile').querySelector('h4'),
    )

    expect(headline.textContent).toEqual('a headline')
  })
  it('renders icon', async () => {
    const { getByTestId } = renderContainer()
    const icon = await waitForElement(() => getByTestId('tile--icon'))

    expect(icon).toBeTruthy()
  })
  it('renders children', async () => {
    const { getByTestId } = renderContainer()
    const children = await waitForElement(() => getByTestId('children'))

    expect(children).toBeTruthy()
  })
})
