import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
  sunriseSunset: {
    '& div:first-child': {
      borderRadius: '2px 2px 0 0',
    },
    '& div:last-child': {
      borderRadius: '0 0 2px 2px',
    },
  },
  entry: {
    padding: theme.spacing(1),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'column',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
    },
  },
  text: {
    flexBasis: 0,
    flexGrow: 1,
    margin: `0 ${theme.spacing(2)}px`,
    wordBreak: 'break-word',
    whiteSpace: 'normal',
  },
  time: {
    flexBasis: 0,
    fontWeight: 'bold',
    margin: `0 ${theme.spacing(2)}px`,
  },
}))

export default function SunriseSunset(props) {
  const classes = useStyles()
  const { t } = useTranslation()

  // eslint-disable-next-line no-shadow
  const Entry = props => {
    return (
      <div
        className={classes.entry}
        style={{ backgroundColor: props.color }}
        data-testid={`sunriseSunset--entry--${props.text}`}
      >
        <span className={classes.text}>{props.text}</span>
        <span className={classes.time}>{props.time}</span>
      </div>
    )
  }

  return (
    <div className={classes.sunriseSunset} data-testid={'sunriseSunset'}>
      <Entry
        text={t('photo.tiles.sunriseSunset.civil_twilight_begin')}
        time={t('utilities.time.long', {
          time: new Date(props.data.civilTwilightBegin),
        })}
        color={'lightsteelblue'}
      />
      <Entry
        text={t('photo.tiles.sunriseSunset.nautical_twilight_begin')}
        time={t('utilities.time.long', {
          time: new Date(props.data.nauticalTwilightBegin),
        })}
        color={'lightsteelblue'}
      />
      <Entry
        text={t('photo.tiles.sunriseSunset.astronomical_twilight_begin')}
        time={t('utilities.time.long', {
          time: new Date(props.data.astronomicalTwilightBegin),
        })}
        color={'lightsteelblue'}
      />
      <Entry
        text={t('photo.tiles.sunriseSunset.sunrise')}
        time={t('utilities.time.long', {
          time: new Date(props.data.sunrise),
        })}
        color={'lightgoldenrodyellow'}
      />
      <Entry
        text={t('photo.tiles.sunriseSunset.solar_noon')}
        time={t('utilities.time.long', {
          time: new Date(props.data.solarNoon),
        })}
        color={'lightyellow'}
      />
      <Entry
        text={t('photo.tiles.sunriseSunset.sunset')}
        time={t('utilities.time.long', {
          time: new Date(props.data.sunset),
        })}
        color={'bisque'}
      />
      <Entry
        text={t('photo.tiles.sunriseSunset.civil_twilight_end')}
        time={t('utilities.time.long', {
          time: new Date(props.data.civilTwilightEnd),
        })}
        color={'lightsteelblue'}
      />
      <Entry
        text={t('photo.tiles.sunriseSunset.nautical_twilight_end')}
        time={t('utilities.time.long', {
          time: new Date(props.data.nauticalTwilightEnd),
        })}
        color={'lightsteelblue'}
      />
      <Entry
        text={t('photo.tiles.sunriseSunset.astronomical_twilight_end')}
        time={t('utilities.time.long', {
          time: new Date(props.data.astronomicalTwilightEnd),
        })}
        color={'lightsteelblue'}
      />
    </div>
  )
}

SunriseSunset.propTypes = {
  data: PropTypes.shape({
    astronomicalTwilightBegin: PropTypes.string.isRequired,
    astronomicalTwilightEnd: PropTypes.string.isRequired,
    civilTwilightBegin: PropTypes.string.isRequired,
    civilTwilightEnd: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    dayLength: PropTypes.number.isRequired,
    nauticalTwilightBegin: PropTypes.string.isRequired,
    nauticalTwilightEnd: PropTypes.string.isRequired,
    solarNoon: PropTypes.string.isRequired,
    sunrise: PropTypes.string.isRequired,
    sunset: PropTypes.string.isRequired,
  }).isRequired,
}
