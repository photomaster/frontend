import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
} from '@testing-library/react'
import PhotoRadiusPage from './photoRadius'
import photoService from 'service/photoService'
import { act } from 'react-dom/test-utils'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from 'reducers'
import Photo from 'models/photo'
jest.mock('service/photoService')

afterAll(() => {
  photoService.getNearPhotos.mockClear()
  cleanup()
})

function renderContainer(props) {
  const baseState = {
    currentUser: {
      loggedIn: false,
    },
  }
  return render(
    <Provider store={createStore(rootReducer, baseState)}>
      <Router>
        <PhotoRadiusPage {...props} />
      </Router>
    </Provider>,
  )
}

describe('PhotoRadius', () => {
  it('fetches nearby photos from the photo service', async () => {
    const lat = 1
    const long = 5
    photoService.getNearPhotos.mockReturnValue(
      Promise.resolve([
        new Photo({
          id: 1,
          name: 'photo-1',
          description: '',
          imageThumbnailTeaser: 'http://....',
        }),
      ]),
    )

    const { getByTestId } = renderContainer({
      match: {
        params: {
          lat: lat,
          long: long,
        },
      },
    })
    await waitForElement(() => getByTestId('recommendationsGrid'))

    expect(photoService.getNearPhotos).toHaveBeenCalledWith(
      lat,
      long,
      expect.anything(),
      expect.anything(),
    )
  })

  it('fetches images within a small radius of a few meters', async () => {
    photoService.getNearPhotos.mockReturnValue(Promise.resolve([]))
    const twentyMeters = 0.02

    const { getByTestId } = renderContainer({
      match: {
        params: {
          lat: 1,
          long: 1,
        },
      },
    })
    await waitForElement(() => getByTestId('recommendationsGrid'))

    expect(photoService.getNearPhotos.mock.calls[0][2]).toBeLessThanOrEqual(
      twentyMeters,
    )
  })

  it('renders a list of photos', async () => {
    const mockPhotos = [
      new Photo({
        id: 1,
        name: 'photo-1',
        description: '',
        imageThumbnailTeaser: 'http://....',
      }),
      new Photo({
        id: 2,
        name: 'photo-2',
        description: '',
        imageThumbnailTeaser: 'http://....',
      }),
      new Photo({
        id: 3,
        name: 'photo-3',
        description: '',
        imageThumbnailTeaser: 'http://....',
      }),
    ]
    photoService.getNearPhotos.mockReturnValue(Promise.resolve(mockPhotos))

    const { getByTestId } = renderContainer({
      match: {
        params: {
          lat: 1,
          long: 1,
        },
      },
    })
    const grid = await waitForElement(() => getByTestId('recommendationsGrid'))

    expect(grid.children).toHaveLength(mockPhotos.length)
  })
})
