import React, { useState, useEffect, lazy, Suspense } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'
import { Map, Marker, TileLayer } from 'react-leaflet'
import PropTypes from 'prop-types'
import './photo.scss'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Box from '@material-ui/core/Box'
import Paper from '@material-ui/core/Paper'
import WbSunnyIcon from '@material-ui/icons/WbSunny'
import WbCloudyIcon from '@material-ui/icons/WbCloudy'
import ImageSearchIcon from '@material-ui/icons/ImageSearch'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle'
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import Tooltip from '@material-ui/core/Tooltip'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import { photoService } from 'service'
import Loading from 'components/utilities/loading'
import Lightbox from 'components/lightbox/lightbox'
import Chip from 'components/chip/chip'
import UserCredit from 'components/utilities/userCredit'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import NavigationIconButton from 'components/utilities/navigationIconButton'

import PhotoModel from 'models/photo'
import Avatar from 'components/utilities/avatar'
import LicenseCredit from 'components/utilities/licenseCredit'
import Tile from './tile'
import { applicationActions, photosActions } from '../../actions'

import Favourite from '../../components/favourite/favourite'

const TileWeather = lazy(() => import('./tileWeather'))
const SunriseSunset = lazy(() => import('./sunriseSunset'))
const RecommendationsGrid = lazy(() =>
  import('components/recommendationCard/recommendationsGrid'),
)
const RecommendationCard = lazy(() =>
  import('components/recommendationCard/recommendationCard.js'),
)

const useStyles = makeStyles(theme => ({
  photo_preview: {
    maxWidth: '100%',
    maxHeight: '80vh',
    '&:hover': {
      cursor: 'zoom-in',
    },
  },
  margin: {
    margin: theme.spacing(0),
    [theme.breakpoints.up('sm')]: {
      margin: theme.spacing(1),
    },
  },
  width50: {
    width: '50%',
  },
}))

export default function Photo(props) {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const dispatch = useDispatch()
  const { t, i18n } = useTranslation()

  const [id, setId] = useState(props.match.params.id)
  const [photo, setPhoto] = useState(null)
  const activePhoto = useSelector(state => state.photos.active)
  const [similarPhotos, setSimilarPhotos] = useState([])
  const [nearbyPhotos, setNearbyPhotos] = useState([])
  const [weather, setWeather] = useState(null)
  const [currentWeather, setCurrentWeather] = useState(null)
  const [loading, setLoading] = useState({
    photo: true,
    tags: true,
    weather: true,
    solar: true,
    solarToday: true,
    cameraData: true,
    near: true,
    similar: true,
  })
  const [curTab, setCurTab] = useState(0)
  const [tagsExpanded, setTagsExpanded] = useState(false)
  const [relevantTags, setRelevantTags] = useState(null)
  const [solarData, setSolarData] = useState(null)
  const [solarDataToday, setSolarDataToday] = useState(null)
  const [curLng, setCurLng] = useState(i18n.language)
  const loggedIn = useSelector(state => state.currentUser.loggedIn)
  const user = useSelector(state => state.currentUser.user)
  const [make, setMake] = useState('')
  const [model, setModel] = useState('')
  const [lensMake, setLensMake] = useState('')
  const [lensModel, setLensModel] = useState('')
  const [focalLength, setFocalLength] = useState('')
  const [fNumber, setFNumber] = useState('')
  const [whiteBalance, setWhiteBalance] = useState('')
  const [exposureTime, setExposureTime] = useState('')
  const [exposureMode, setExposureMode] = useState('')
  const [iso, setIso] = useState('')
  const [flash, setFlash] = useState('')
  const [dimensionX, setDimensionX] = useState('')
  const [dimensionY, setDimensionY] = useState('')
  const [software, setSoftware] = useState('')
  const [validWeather, setValidWeather] = useState({
    photo: false,
    now: false,
  })
  const [solarDataPage, setSolarDataPage] = useState(0)

  if (id != props.match.params.id) {
    setId(props.match.params.id)
  }

  i18n.on('languageChanged', function handleLanguageChange(lng) {
    // fired multiple times + state update too slow --> called in useEffect once
    if (!loading.photo && curLng != i18n.language) {
      setCurLng(lng)
    }
  })

  useEffect(() => {
    if (!loading.tags) {
      fetchTags()
    }
  }, [curLng])

  useEffect(() => {
    setLoading({
      photo: true,
      tags: true,
      weather: true,
      solar: true,
      solarToday: true,
      cameraData: true,
      near: true,
      similar: true,
    })
    setSimilarPhotos([])
    setSolarDataToday(null)
    setCurTab(0)
    window.scrollTo(0, 0)
    let mounted = true
    if (mounted) fetchData()
    return function cleanup() {
      mounted = false
    }
  }, [id])

  useEffect(() => {
    if (photo) {
      tagsExpanded
        ? setRelevantTags(photo.labels)
        : setRelevantTags(photo.labels.slice(0, 5))
    }
  }, [tagsExpanded])

  useEffect(() => {
    _containsWeatherData(weather, 'photo')
  }, [weather])

  useEffect(() => {
    _containsWeatherData(currentWeather, 'now')
  }, [currentWeather])

  function TabPanel(properties) {
    const { children, value, index, ...other } = properties

    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`${index}`}
        aria-labelledby={`${index}`}
        {...other}
      >
        {value === index && <Box p={3}>{children}</Box>}
      </Typography>
    )
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  }

  async function fetchData() {
    try {
      let curPhoto = null
      if (activePhoto && activePhoto.id == id) {
        curPhoto = new PhotoModel(activePhoto)
      } else {
        curPhoto = await photoService.getPhoto(id)
      }
      setPhoto(curPhoto)
      setRelevantTags(curPhoto.labels.slice(0, 5))
      setLoading(loading => ({ ...loading, photo: false, tags: false }))

      try {
        setNearbyPhotos(
          await photoService.getNearPhotos(curPhoto.lat, curPhoto.long, 10),
        )
        setLoading(loading => ({ ...loading, near: false }))
      } catch (e) {
        console.error('nearby', e)
        setLoading(loading => ({ ...loading, near: false }))
      }

      try {
        setSolarData(await curPhoto.solarData)
        setLoading(loading => ({ ...loading, solar: false }))
      } catch (e) {
        console.error('solar', e)
        setLoading(loading => ({ ...loading, solar: false }))
      }

      try {
        setWeather(await curPhoto.weatherAtCreation)
      } catch (e) {
        console.error('weather', e)
      }
      try {
        setCurrentWeather(await curPhoto.currentWeather)
      } catch (e) {
        console.error('weather', e)
      }
      setLoading(loading => ({ ...loading, weather: false }))

      try {
        const photoData = await curPhoto.photoData
        setMake(photoData.make || '')
        setModel(photoData.model || '')
        setWhiteBalance(photoData.whiteBalance || '')
        setFNumber(photoData.fNumber || '')
        setFocalLength(photoData.focalLength || '')
        setExposureTime(photoData.exposureTime || '')
        setLensMake(photoData.lensMake || '')
        setLensModel(photoData.lensModel || '')
        setFlash(photoData.flash || '')
        setSoftware(photoData.software || '')
        setExposureMode(photoData.exposureMode || '')
        setIso(photoData.isoSpeed || '')
        setDimensionX(photoData.pixelXDimension || '')
        setDimensionY(photoData.pixelYDimension || '')
        setLoading(loading => ({ ...loading, cameraData: false }))
      } catch (e) {
        console.error('camera data', e)
        setLoading(loading => ({ ...loading, cameraData: false }))
      }

      dispatch(photosActions.setActivePhoto(curPhoto._props))
    } catch (e) {
      console.error(e)
    }
  }

  async function fetchSimilar() {
    try {
      setSimilarPhotos(await photoService.getSimilarPhotos(photo))
      setLoading(loading => ({ ...loading, similar: false }))
    } catch (e) {
      console.error('similar', e)
      setLoading(loading => ({ ...loading, similar: false }))
    }
  }

  async function fetchSolarToday() {
    try {
      setSolarDataToday(await photoService.getSolarData(photo.id, new Date()))
      setLoading(loading => ({ ...loading, solarToday: false }))
    } catch (e) {
      console.error('solarToday', e)
      setLoading(loading => ({ ...loading, solarToday: false }))
    }
  }

  async function fetchTags() {
    setLoading({ ...loading, tags: true })
    try {
      const curPhoto = await photoService.getPhoto(id)
      setPhoto(curPhoto)
      setTagsExpanded(false)
      setRelevantTags(curPhoto.labels.slice(0, 5))
    } catch (e) {
      console.error(e)
    }
    setLoading({ ...loading, tags: false })
  }

  const _handleChange = (event, newValue) => {
    if (newValue === 1 && similarPhotos.length === 0) fetchSimilar()
    setCurTab(newValue)
  }

  const _handleTagsButton = () => {
    tagsExpanded ? setTagsExpanded(false) : setTagsExpanded(true)
  }

  const _handleOpen = () => {
    dispatch(applicationActions.openLightbox())
  }

  const _ownPhoto = () => {
    if (loggedIn && user && photo && photo.user && user.id === photo.user.id) {
      return true
    }
    return false
  }

  // eslint-disable-next-line no-shadow
  const CameraEntry = props => {
    return (
      props.value !== '' && (
        <p>
          {t(`forms.labels.${props.id}`)}:{' '}
          <span className={baseClasses.bold}>{props.value}</span>
        </p>
      )
    )
  }

  const _noCameraData = () => {
    if (
      !make &&
      !model &&
      !whiteBalance &&
      !fNumber &&
      !focalLength &&
      !exposureTime &&
      !lensMake &&
      !lensModel &&
      !flash &&
      !software &&
      !exposureMode &&
      !iso &&
      !dimensionX &&
      !dimensionY
    )
      return true
    return false
  }

  const _containsWeatherData = (weatherObj, type) => {
    let valid = false
    if (weatherObj === null) {
      valid = false
    } else if (
      (weatherObj.description !== null && weatherObj.description !== '') ||
      (weatherObj.temperature !== null && weatherObj.temperature !== '')
    ) {
      valid = true
    }
    if (type === 'photo')
      setValidWeather(validWeather => ({ ...validWeather, photo: valid }))
    else if (type === 'now')
      setValidWeather(validWeather => ({ ...validWeather, now: valid }))
  }

  const _changePage = () => {
    if (1 - solarDataPage === 1 && !solarDataToday) fetchSolarToday()
    setSolarDataPage(1 - solarDataPage)
  }

  const SwitchPage = () => {
    return (
      <Grid container justify="center" alignItems="center">
        <IconButton
          aria-label="back"
          className={classes.margin}
          onClick={_changePage}
          data-testid={'photo--solar--back'}
        >
          <ArrowBackIosIcon fontSize="small" />
        </IconButton>
        <Typography variant="overline" display="block" gutterBottom>
          {t('photo.tiles.sunriseSunset.changePage.general')}
        </Typography>
        <IconButton
          aria-label="next"
          className={classes.margin}
          onClick={_changePage}
          data-testid={'photo--solar--next'}
        >
          <ArrowForwardIosIcon fontSize="small" />
        </IconButton>
      </Grid>
    )
  }

  return loading.photo ? (
    <Loading />
  ) : (
    <div>
      <Map className="map--location" center={[photo.lat, photo.long]} zoom={12}>
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        <Marker key={photo.id} position={[photo.lat, photo.long]} />
      </Map>
      <Avatar
        alt={photo.name}
        src={photo.imageThumbnailTeaserSmall}
        wrappedInContainer
      />
      <Container
        component="main"
        maxWidth="md"
        className={`container_photo ${baseClasses.relative}`}
      >
        {_ownPhoto() && (
          <NavigationIconButton
            page={'photo'}
            id={photo.id}
            title={t('photo.edit')}
            aria={t('photo.aria.edit')}
            navigateToEdit
          />
        )}
        <NavigationIconButton
          page={'photo'}
          title={t('forms.return')}
          aria={t('forms.photo.aria.return')}
          navigateToEdit={false}
        />
        <CssBaseline />
        <Typography component="h1" variant="h2" data-testid={'photo--headline'}>
          {photo.name}
          <Favourite photo={photo} />
        </Typography>
        <Typography
          component="span"
          variant="body2"
          className={`${baseClasses.justifyContentCenter} ${
            photo.user.licenseUrl
              ? baseClasses.marginBottom1
              : baseClasses.marginBottom2
          }`}
        >
          {t('photo.by')}
          &nbsp;
          <UserCredit user={photo.user} />
        </Typography>
        {photo.user.licenseUrl && (
          <span
            className={`${baseClasses.justifyContentCenter} ${baseClasses.marginBottom2}`}
          >
            <LicenseCredit user={photo.user} />
          </span>
        )}
        {photo.labels && (
          <React.Fragment>
            <Typography
              component="span"
              variant="body2"
              className={`${baseClasses.justifyContentCenter} ${baseClasses.marginBottom2} ${baseClasses.verticalCenter} ${baseClasses.flexWrap}`}
            >
              {relevantTags.map(label => (
                <Chip
                  key={`photo-label-${photo.id}-${label.title}`}
                  label={label.title}
                  size="small"
                  link={`/photos/tags/${label.title}`}
                  clickable
                />
              ))}
              {photo.labels.length > 5 && (
                <React.Fragment>
                  <Tooltip
                    title={
                      tagsExpanded
                        ? t('photo.tags.counter.hide', {
                            count: photo.labels.length - 5,
                          })
                        : t('photo.tags.counter.show', {
                            count: photo.labels.length - 5,
                          })
                    }
                    placement="top"
                  >
                    <IconButton
                      aria-label={
                        tagsExpanded
                          ? t('photo.tags.general.hide')
                          : t('photo.tags.general.show')
                      }
                      color="primary"
                      onClick={_handleTagsButton}
                      className={baseClasses.margin05}
                      data-testid={'photo--tagsButton'}
                    >
                      {tagsExpanded ? <RemoveCircleIcon /> : <AddCircleIcon />}
                    </IconButton>
                  </Tooltip>
                </React.Fragment>
              )}
            </Typography>
          </React.Fragment>
        )}
        <Grid
          item
          xs={12}
          style={{ display: 'flex', justifyContent: 'center' }}
        >
          <img
            src={photo.imageThumbnailPreview}
            alt={`${id}`}
            className={classes.photo_preview}
            onClick={_handleOpen}
            data-testid={'photo--preview'}
          />
          <Lightbox id={photo.id} url={photo.image} />
        </Grid>
        <Typography component="h3" variant="h3">
          {t('photo.tiles.headlines.detail')}
        </Typography>
        <Grid container spacing={3}>
          {loading.photo ? (
            <Loading additionalClasses={classes.width50} />
          ) : (
            <Suspense fallback={<Loading />}>
              <Tile
                headline={t('photo.tiles.headlines.photodetails')}
                icon={ImageSearchIcon}
              >
                <p style={{ textAlign: 'left' }}>
                  {t('photo.tiles.info.date')}:{' '}
                  <span className={baseClasses.bold}>
                    {t('utilities.date.long', {
                      date: new Date(photo.originalCreationDate),
                    })}
                  </span>
                </p>
                <p>{photo.description}</p>
              </Tile>
            </Suspense>
          )}
          <Tile
            headline={t('photo.tiles.headlines.weather')}
            icon={WbCloudyIcon}
          >
            {loading.weather && <Loading />}
            {!loading.weather && !validWeather.photo && !validWeather.now && (
              <p>{t('photo.tiles.weather.error')}</p>
            )}
            {!loading.weather && validWeather.photo && (
              <Suspense fallback={<Loading />}>
                <TileWeather
                  headline={t('photo.tiles.headlines.time.photo')}
                  weather={weather ? weather.description : null}
                  temp={weather ? weather.temperature : null}
                />
              </Suspense>
            )}
            {!loading.weather && validWeather.photo && validWeather.now && (
              <hr />
            )}
            {!loading.weather && validWeather.now && (
              <Suspense fallback={<Loading />}>
                <TileWeather
                  headline={t('photo.tiles.headlines.time.today')}
                  weather={currentWeather ? currentWeather.description : null}
                  temp={currentWeather ? currentWeather.temperature : null}
                />
              </Suspense>
            )}
          </Tile>
          <Tile
            headline={t('photo.tiles.headlines.sunriseSunset')}
            icon={WbSunnyIcon}
          >
            {solarDataPage === 0 ? (
              <React.Fragment>
                <Typography
                  component="h5"
                  variant="h5"
                  className={baseClasses.tilesHeadline}
                >
                  {t('photo.tiles.headlines.time.photo')}
                </Typography>
                {loading.solar ? (
                  <Loading />
                ) : solarData ? (
                  <Suspense fallback={<Loading />}>
                    <SunriseSunset data={solarData} />
                  </Suspense>
                ) : (
                  <p>{t('photo.tiles.sunriseSunset.error')}</p>
                )}
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Typography
                  component="h5"
                  variant="h5"
                  className={baseClasses.tilesHeadline}
                  data-testid={'photo--solar--today'}
                >
                  {t('photo.tiles.headlines.time.today')}
                </Typography>
                {loading.solarToday ? (
                  <Loading />
                ) : solarDataToday ? (
                  <Suspense fallback={<Loading />}>
                    <SunriseSunset data={solarDataToday} />
                  </Suspense>
                ) : (
                  <p>{t('photo.tiles.sunriseSunset.error')}</p>
                )}
              </React.Fragment>
            )}
            <SwitchPage />
          </Tile>
          <Tile
            headline={t('photo.tiles.headlines.camera')}
            icon={PhotoCameraIcon}
          >
            {loading.cameraData ? (
              <Loading />
            ) : _noCameraData() ? (
              <p>{t('forms.photo.noCamera')}</p>
            ) : (
              <Suspense fallback={<Loading />}>
                <CameraEntry id={'make'} value={make} />
                <CameraEntry id={'model'} value={model} />
                <CameraEntry id={'lensMake'} value={lensMake} />
                <CameraEntry id={'lensModel'} value={lensModel} />
                <CameraEntry id={'focalLength'} value={focalLength} />
                <CameraEntry id={'fNumber'} value={fNumber} />
                <CameraEntry id={'whiteBalance'} value={whiteBalance} />
                <CameraEntry id={'exposureTime'} value={exposureTime} />
                <CameraEntry id={'exposureMode'} value={exposureMode} />
                <CameraEntry id={'iso'} value={iso} />
                <CameraEntry id={'flash'} value={flash} />
                <CameraEntry id={'software'} value={software} />
                <CameraEntry id={'dimensionX'} value={dimensionX} />
                <CameraEntry id={'dimensionY'} value={dimensionY} />
              </Suspense>
            )}
          </Tile>
        </Grid>
        <Typography component="h3" variant="h3">
          {t('photo.recommendations.headline')}
        </Typography>
        <Grid item xs={12} className={baseClasses.justifyContentCenter}>
          <Paper position="static" className={baseClasses.paper}>
            <Tabs
              value={curTab}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
              onChange={_handleChange}
              aria-label={t('photo.aria.tabs')}
            >
              <Tab
                key={0}
                label={t('photo.recommendations.nearby.headlineShort')}
                data-testid={'photo--tab--nearby'}
              />
              <Tab
                key={1}
                label={t('photo.recommendations.similar.headlineShort')}
                data-testid={'photo--tab--similar'}
              />
            </Tabs>
            <TabPanel
              value={curTab}
              index={0}
              key={0}
              className={baseClasses.fullWidth}
            >
              <Typography component="h2" variant="h3">
                {t('photo.recommendations.nearby.headlineLong')}
              </Typography>
              {loading.near ? (
                <Loading />
              ) : (
                <Suspense fallback={<Loading />}>
                  <RecommendationsGrid>
                    {nearbyPhotos.length <= 1 ? (
                      <p
                        className={`${baseClasses.textAlignCenter} ${baseClasses.fullWidth}`}
                      >
                        {t('photo.recommendations.nearby.error')}
                      </p>
                    ) : (
                      nearbyPhotos.map(nearbyPhoto => {
                        if (nearbyPhoto.id === photo.id) {
                          return null
                        }
                        return (
                          <RecommendationCard
                            key={`similar-photo-${photo.id}-${nearbyPhoto.id}`}
                            photo={nearbyPhoto}
                            user={nearbyPhoto.user}
                          />
                        )
                      })
                    )}
                  </RecommendationsGrid>
                </Suspense>
              )}
            </TabPanel>
            <TabPanel
              value={curTab}
              index={1}
              key={1}
              className={baseClasses.fullWidth}
            >
              <Typography component="h2" variant="h3">
                {t('photo.recommendations.similar.headlineLong')}
              </Typography>
              {loading.similar && curTab === 1 ? (
                <Loading />
              ) : (
                <Suspense fallback={<Loading />}>
                  <RecommendationsGrid>
                    {similarPhotos.length === 0 ? (
                      <p
                        className={`${baseClasses.textAlignCenter} ${baseClasses.fullWidth}`}
                      >
                        {t('photo.recommendations.similar.error')}
                      </p>
                    ) : (
                      similarPhotos.map(similarPhoto => {
                        if (similarPhoto.id === photo.id) {
                          return null
                        }
                        return (
                          <RecommendationCard
                            key={`similar-photo-${photo.id}-${similarPhoto.id}`}
                            photo={similarPhoto}
                            user={similarPhoto.user}
                          />
                        )
                      })
                    )}
                  </RecommendationsGrid>
                </Suspense>
              )}
            </TabPanel>
          </Paper>
        </Grid>
      </Container>
    </div>
  )
}
