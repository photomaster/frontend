import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import PhotoEdit from './photoEdit'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import validators from 'helpers/validators'
import { photosActions, applicationActions } from 'actions'
import photoService from 'service/photoService'
import weatherService from 'service/weatherService'
import PhotoModel from 'models/photo'
import WeatherModel from 'models/weatherState'
import PhotoData from 'models/photoData'

const TestUser = {
  id: 1,
  username: 'test_username',
  displayName: 'test_displayName',
  description: 'desc',
  email: 'test@mail.com',
  password: 'test_password',
  password2: 'test_password',
  bannerImage: '/banner',
  avatarImage: '/avatar',
}

const TestDate = new Date('2020-11-25T05:09:04Z')
const TestDateConverted = 'November 25 2020 06:09'

const TestPhotoContent = {
  id: 1,
  url: '/photo1',
  name: 'title1',
  description: 'desc1',
  geo: {
    type: 'Point',
    coordinates: [47.811195, 13.033229],
  },
  photodata: '/photodata1',
  currentWeather: null,
  originalCreationDate: TestDate,
  weatherAtCreation: '/some/url/to/weatherState/33/',
  image: '/image1',
  imageThumbnailTeaser: '/thumbnail1',
  imageThumbnailTeaserSmall: '/thumbnailsmall1',
  imageThumbnailPreview: '/thumbnailpreview1',
  labels: [
    {
      title: 'label1',
      score: 1,
    },
    {
      title: 'label2',
      score: 1,
    },
  ],
  user: TestUser,
}

const TestPhoto = new PhotoModel(TestPhotoContent)
const TestPhotoData = new PhotoData({
  id: 1,
  url: '/photosData/1',
  photo: '/photo/1',
  make: 'make',
  model: 'model',
  lensMake: 'lensMake',
  lensModel: 'lensModel',
  focalLength: 'focalLength',
  fNumber: 'fNumber',
  whiteBalance: 'whiteBalance',
  lightSource: 'lightSource',
  exposureTime: 'exposureTime',
  exposureProgram: 'exposureProgram',
  exposureMode: 'exposureMode',
  isoSpeed: 'isoSpeed',
  flash: 'flash',
  pixelXDimension: 200,
  pixelYDimension: 100,
  orientation: 3,
  software: 'software',
})
const TestWeatherState = new WeatherModel({
  description: 'clear sky',
  temperature: 10,
})

const mockTranslation = jest.fn(str => str)
const mockValidatorTrue = jest.fn(_ => true)
const mockValidatorFalse = jest.fn(_ => false)
const mockScrollTo = jest.fn()
const mockConsole = jest.fn(msg => msg)
const mockDispatch = jest.fn()
const mockGetPhoto = jest.fn(id => Promise.resolve(TestPhoto))
const mockGetWeatherAtCreation = jest.fn(() =>
  Promise.resolve(TestWeatherState),
)
const mockGetPhotoData = jest.fn(() => Promise.resolve(TestPhotoData))

window.scrollTo = mockScrollTo
console.error = mockConsole

/* material-UI datepicker prints every change in the log --> mock console to avoid messages in tests */
console.warn = jest.fn()

jest.mock('react-i18next', () => ({
  ...jest.requireActual('react-i18next'),
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        language: 'en',
        changeLanguage: () => new Promise(() => {}),
        on: jest.fn((_, fn) => fn()),
      },
    }
  },
}))

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

jest.mock('service/photoService', () => jest.fn())
photoService.getPhoto = mockGetPhoto
photoService.getPhotoData = mockGetPhotoData

jest.mock('service/weatherService', () => jest.fn())
weatherService.getWeather = mockGetWeatherAtCreation

function renderContainer(
  photoPageId = 1,
  activePhoto = TestPhotoContent,
  loggedIn = true,
  updating = false,
  errors = {},
) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: {
            loggedIn: loggedIn,
            errors: errors,
            user: TestUser,
          },
          photos: {
            active: activePhoto,
            updating: updating,
          },
        })}
      >
        <PhotoEdit match={{ params: { id: photoPageId } }} />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Photo Edit View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('requests photo if not available via redux', async () => {
    renderContainer(1, null)

    expect(mockGetPhoto).toHaveBeenCalledWith(1)
  })
  it('redirects if not own photo', async () => {
    const { container } = renderContainer(2)

    const photoEdit = container.querySelector('[data-testid="photoEdit"]')

    expect(photoEdit).toBeFalsy()
  })
  it('displays loading component', async () => {
    photoService.getPhoto.mockImplementationOnce(jest.fn(() => null))

    const { getByTestId } = renderContainer(1, null, true, true)

    const loading = getByTestId('loading')

    expect(loading).toBeTruthy()
  })
  it('dispatches action to open lightbox on click on photo', async () => {
    const mockOpenLightbox = jest.fn().mockImplementation(() => {
      return { type: 'opening lightbox' }
    })
    applicationActions.openLightbox = mockOpenLightbox

    const { getByTestId } = renderContainer()
    const photo = await waitForElement(() => getByTestId('photoEdit--preview'))

    fireEvent.click(photo)

    expect(mockOpenLightbox).toHaveBeenCalledTimes(1)
  })
  it('dispatches action to update photo on click', async () => {
    const mockUpdate = jest.fn().mockImplementation(() => {
      return { type: 'updating photo' }
    })
    photosActions.update = mockUpdate

    const { findByTestId } = renderContainer()
    const submitButton = await waitForElement(() =>
      findByTestId('photoEdit--submitButton'),
    )

    fireEvent.click(submitButton)

    expect(mockUpdate).toHaveBeenCalledTimes(1)
    expect(mockUpdate).toHaveBeenCalledWith(
      TestPhoto,
      TestWeatherState,
      TestPhotoData,
    )
  })
  it('catches error of updating photo', async () => {
    const mockUpdate = jest.fn().mockImplementation(() => {
      throw 'some error'
    })
    photosActions.update = mockUpdate

    const { container, findByTestId } = renderContainer()
    const submitButton = await waitForElement(() =>
      findByTestId('photoEdit--submitButton'),
    )

    fireEvent.click(submitButton)

    expect(mockUpdate).toHaveBeenCalledTimes(1)
    expect(mockConsole).toHaveBeenCalledWith('some error')
    expect(container).toBeTruthy()
  })
  it('dispatches action to delete photo on click', async () => {
    const mockDelete = jest.fn().mockImplementation(() => {
      return { type: 'deleting photo' }
    })
    photosActions.remove = mockDelete

    const { findByTestId } = renderContainer()
    const deleteButton = await waitForElement(() =>
      findByTestId('photoEdit--deleteButton'),
    )

    fireEvent.click(deleteButton)

    expect(mockDelete).toHaveBeenCalledTimes(1)
    expect(mockDelete).toHaveBeenCalledWith(TestPhoto)
  })
  it('catches error of deleting photo', async () => {
    const mockDelete = jest.fn().mockImplementation(() => {
      throw 'some error'
    })
    photosActions.remove = mockDelete

    const { container, findByTestId } = renderContainer()
    const deleteButton = await waitForElement(() =>
      findByTestId('photoEdit--deleteButton'),
    )

    fireEvent.click(deleteButton)

    expect(mockDelete).toHaveBeenCalledTimes(1)
    expect(mockConsole).toHaveBeenCalledWith('some error')
    expect(container).toBeTruthy()
  })
  describe('disables button to submit if', () => {
    it('title is not filled in', async () => {
      validators.titleIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const title = await waitForElement(() => findByTestId('photoEdit--title'))
      const submitButton = await waitForElement(() =>
        findByTestId('photoEdit--submitButton'),
      )

      fireEvent.change(title.querySelector('input'), {
        target: { value: '' },
      })

      expect(title.querySelector('input').getAttribute('value')).toEqual('')
      expect(submitButton).toBeDisabled()

      fireEvent.change(title.querySelector('input'), {
        target: { value: 'some title' },
      })

      expect(title.querySelector('input').getAttribute('value')).toEqual(
        'some title',
      )
      expect(submitButton).toBeEnabled()
    })
    it('desc is not filled in', async () => {
      validators.descIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const desc = await waitForElement(() => findByTestId('photoEdit--desc'))
      const submitButton = await waitForElement(() =>
        findByTestId('photoEdit--submitButton'),
      )

      fireEvent.change(desc.querySelectorAll('textarea')[0], {
        target: { value: '' },
      })

      expect(desc.querySelectorAll('textarea')[0].textContent).toEqual('')
      expect(submitButton).toBeDisabled()

      fireEvent.change(desc.querySelectorAll('textarea')[0], {
        target: { value: 'some desc' },
      })

      expect(desc.querySelectorAll('textarea')[0].textContent).toEqual(
        'some desc',
      )
      expect(submitButton).toBeEnabled()
    })
    xit('coordinates are invalid', async () => {
      // TODO how to make invalid coordinates?
    })
  })
  describe('validates and sets new value for', () => {
    it('title', async () => {
      validators.titleIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const title = await waitForElement(() => findByTestId('photoEdit--title'))

      fireEvent.change(title.querySelector('input'), {
        target: { value: 'new_title' },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(title.querySelector('input').getAttribute('value')).toEqual(
        'new_title',
      )
    })
    it('dateTime', async () => {
      const { findByTestId, queryByText } = renderContainer()
      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--dateTime'),
      )

      fireEvent.click(textfield.querySelector('input'))
      fireEvent.click(queryByText('7'))
      fireEvent.click(queryByText('OK'))

      expect(textfield.querySelector('input').getAttribute('value')).toEqual(
        'November 7 2020 06:09',
      )
    })
    it('desc', async () => {
      validators.descIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const desc = await waitForElement(() => findByTestId('photoEdit--desc'))

      fireEvent.change(desc.querySelectorAll('textarea')[0], {
        target: { value: 'new_desc' },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(desc.querySelectorAll('textarea')[0].textContent).toEqual(
        'new_desc',
      )
    })
    xit('coordinates', async () => {
      // TODO how to interact with map?
    })
    it('weather', async () => {
      const baseDOM = renderContainer()
      const select = await waitForElement(() =>
        baseDOM.getByTestId('photoEdit--weather'),
      )

      fireEvent.mouseDown(select.querySelector('[role="button"]'))

      const option = await waitForElement(() =>
        baseDOM.getByTestId('photoEdit--select--clouds_few'),
      )

      fireEvent.click(option)

      expect(select.querySelector('input').getAttribute('value')).toEqual(
        'few clouds',
      )
    })
    it('temperature', async () => {
      validators.numberIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--temp'),
      )

      fireEvent.change(textfield.querySelector('input'), {
        target: { value: 20 },
      })

      expect(textfield.querySelector('input').getAttribute('value')).toEqual(
        '20',
      )
    })
  })
  describe('displays error for', () => {
    it('title', async () => {
      validators.titleIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const title = await waitForElement(() => findByTestId('photoEdit--title'))

      fireEvent.change(title.querySelector('input'), {
        target: { value: 'a' },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(title.querySelector('.Mui-error')).toBeTruthy()
    })
    it('desc', async () => {
      validators.descIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const desc = await waitForElement(() => findByTestId('photoEdit--desc'))

      fireEvent.change(desc.querySelectorAll('textarea')[0], {
        target: { value: 'x' },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(desc.querySelector('.Mui-error')).toBeTruthy()
    })
    xit('coordinates', async () => {
      // TODO how to interact with map?
    })
    it('temperature', async () => {
      validators.numberIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--temp'),
      )

      fireEvent.change(textfield.querySelector('input'), {
        target: { value: 'two' },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(textfield.querySelector('.Mui-error')).toBeTruthy()
    })
    it('api errors', async () => {
      const { findByTestId } = renderContainer(
        1,
        TestPhotoContent,
        true,
        false,
        {
          update: { dimensionX: ['dimensionX error'] },
        },
      )
      const message = await waitForElement(() => findByTestId('alert'))

      expect(message).toBeTruthy()
      expect(message.textContent).toContain('dimensionX error')
    })
  })
  it('displays button to navigate to last page', async () => {
    const { getByTestId } = renderContainer()

    const button = await waitForElement(() =>
      getByTestId('navigationIconButton'),
    )

    expect(button).toBeTruthy()
  })
  it('translates the text', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('forms.return')
    expect(mockTranslation).toHaveBeenCalledWith('forms.photo.headlines.main')
    expect(mockTranslation).toHaveBeenCalledWith('forms.photo.introduction')
    expect(mockTranslation).toHaveBeenCalledWith('forms.photo.buttons.delete')
    expect(mockTranslation).toHaveBeenCalledWith('forms.photo.buttons.save')
  })
})
