import React, { useState, useEffect } from 'react'
import '../../components/recommendations/recommendations.scss'
import { Redirect } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core'
import validators from 'helpers/validators'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import Loading from 'components/utilities/loading'
import { photoService } from 'service'
import Lightbox from 'components/lightbox/lightbox'
import NavigationIconButton from 'components/utilities/navigationIconButton'
import ErrorMessage from 'components/formElements/errorMessage'
import AnimatedMarker from 'components/map/animatedMarker'
import PhotoModel from 'models/photo'
import { applicationActions, photosActions } from '../../actions'

import Step1 from './photoEditSteps/photoEditStep1'
import Step2 from './photoEditSteps/photoEditStep2'
import Step3 from './photoEditSteps/photoEditStep3'
import Step4 from './photoEditSteps/photoEditStep4'
import Step5 from './photoEditSteps/photoEditStep5'

const useStyles = makeStyles(theme => ({
  photo_preview: {
    maxWidth: '100%',
    maxHeight: '50vh',
    '&:hover': {
      cursor: 'zoom-in',
    },
  },
  marginTopBottom: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  buttonAdj: {
    marginTop: '-12px',
  },
}))

export default function PhotoEdit(props) {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const [id, setId] = useState(props.match.params.id)
  const loggedIn = useSelector(state => state.currentUser.loggedIn)
  const user = useSelector(state => state.currentUser.user)
  const activePhoto = useSelector(state => state.photos.active)
  const [photo, setPhoto] = useState(null)
  const [loading, setLoading] = useState({
    photo: true,
    weather: true,
    cameraData: true,
    submit: false,
  })
  const [weatherObj, setWeatherObj] = useState(null)
  const [weather, setWeather] = useState(null)
  const [temperature, setTemperature] = useState(null)
  const [title, setTitle] = useState('')
  const [dateTime, setDateTime] = useState(null)
  const [desc, setDesc] = useState('')
  const [coordinates, setCoordinates] = useState([])
  const [marker, setMarker] = useState(null)
  const [zoom, setZoom] = useState(12)
  const [tags, setTags] = useState([])
  const [tag, setTag] = useState('')
  const [errors, setErrors] = useState({
    title: false,
    desc: false,
    temp: false,
  })
  const [weatherConditions, setWeatherConditions] = useState([])
  const [make, setMake] = useState('')
  const [model, setModel] = useState('')
  const [lensMake, setLensMake] = useState('')
  const [lensModel, setLensModel] = useState('')
  const [focalLength, setFocalLength] = useState('')
  const [fNumber, setFNumber] = useState('')
  const [whiteBalance, setWhiteBalance] = useState('')
  const [exposureTime, setExposureTime] = useState('')
  const [exposureMode, setExposureMode] = useState('')
  const [iso, setIso] = useState('')
  const [flash, setFlash] = useState('')
  const [dimensionX, setDimensionX] = useState('')
  const [dimensionY, setDimensionY] = useState('')
  const [software, setSoftware] = useState('')
  const [photoData, setPhotoData] = useState(null)
  const active = useSelector(state => state.photos.updating)
  const [deleting, setDeleting] = useState(false)
  const apiErrors = useSelector(state => state.currentUser.errors)

  const conditions = [
    {
      short: 'clear',
      long: 'clear sky',
    },
    {
      short: 'clouds_few',
      long: 'few clouds',
    },
    {
      short: 'clouds_scattered',
      long: 'scattered clouds',
    },
    {
      short: 'clouds_broken',
      long: 'broken clouds',
    },
    {
      short: 'rain_shower',
      long: 'shower rain',
    },
    {
      short: 'rain',
      long: 'rain',
    },
    {
      short: 'thunderstorm',
      long: 'thunderstorm',
    },
    {
      short: 'snow',
      long: 'snow',
    },
    {
      short: 'mist',
      long: 'mist',
    },
  ]

  if (id != props.match.params.id) {
    setId(props.match.params.id)
  }

  useEffect(() => {
    setLoading({
      photo: true,
      weather: true,
      cameraData: true,
      submit: false,
    })
    window.scrollTo(0, 0)
    let mounted = true
    if (mounted) fetchData()
    return function cleanup() {
      mounted = false
    }
  }, [id])

  useEffect(() => {
    _updateMarker()
  }, [coordinates])

  const _ownPhoto = () => {
    if (loggedIn && user && photo && photo.user && user.id === photo.user.id) {
      return true
    }
    console.warn(`Hey! This is not your photo, go play somewhere else ☝`)
    return false
  }

  async function fetchData() {
    try {
      let curPhoto = null
      if (activePhoto && activePhoto.id == id) {
        curPhoto = new PhotoModel(activePhoto)
      } else {
        curPhoto = await photoService.getPhoto(id)
      }

      setPhoto(curPhoto)
      setTitle(curPhoto.name)
      setDateTime(curPhoto.originalCreationDate)
      setDesc(curPhoto.description)
      setCoordinates([curPhoto.lat, curPhoto.long])
      setTags(
        curPhoto.labels.map((curTag, index) => ({
          key: index,
          label: curTag.title,
        })),
      )
      setLoading(loading => ({ ...loading, photo: false }))

      try {
        const weatherObject = await curPhoto.weatherAtCreation
        setWeatherObj(weatherObject)
        setWeather(weatherObject.description)

        if (
          weatherObject.description &&
          filterByValue(conditions, weatherObject.description).length === 0
        ) {
          conditions.push({ long: weatherObject.description })
        }
        setWeatherConditions(conditions)
        setTemperature(weatherObject.temperature)
        setLoading(loading => ({ ...loading, weather: false }))
      } catch (e) {
        console.error('weather', e)
        setWeatherConditions(conditions)
        setLoading(loading => ({ ...loading, weather: false }))
      }

      try {
        const curPhotoData = await curPhoto.photoData
        setPhotoData(curPhotoData)
        setMake(curPhotoData.make || '')
        setModel(curPhotoData.model || '')
        setWhiteBalance(curPhotoData.whiteBalance || '')
        setFNumber(curPhotoData.fNumber || '')
        setFocalLength(curPhotoData.focalLength || '')
        setExposureTime(curPhotoData.exposureTime || '')
        setLensMake(curPhotoData.lensMake || '')
        setLensModel(curPhotoData.lensModel || '')
        setFlash(curPhotoData.flash || '')
        setSoftware(curPhotoData.software || '')
        setExposureMode(curPhotoData.exposureMode || '')
        setIso(curPhotoData.isoSpeed || '')
        setDimensionX(curPhotoData.pixelXDimension || '')
        setDimensionY(curPhotoData.pixelYDimension || '')
        setLoading(loading => ({ ...loading, cameraData: false }))
      } catch (e) {
        console.error('camera data', e)
        setLoading(loading => ({ ...loading, cameraData: false }))
      }

      dispatch(photosActions.setActivePhoto(curPhoto._props))
    } catch (e) {
      console.error(e)
    }
  }

  async function _handleSubmit(e) {
    e.preventDefault()
    setLoading(loading => ({ ...loading, submit: true }))
    setDeleting(false)

    if (_filledInCompletely && !Object.values(errors).includes(true)) {
      try {
        /* photo */
        const photoUpdated = photo
        photoUpdated.name = title
        photoUpdated.originalCreationDate = dateTime
          ? dateTime.toISOString()
          : null
        photoUpdated.description = desc
        photoUpdated.lat = coordinates[0]
        photoUpdated.long = coordinates[1]
        photoUpdated.labels = tags.map(curTag => {
          return {
            title: curTag.label,
          }
        })

        /* weather */
        if (weatherObj) {
          weatherObj.description = weather || null
          weatherObj.temperature = temperature || null
        }

        /* camera data */
        if (photoData) {
          photoData.make = make || null
          photoData.model = model || null
          photoData.whiteBalance = whiteBalance || null
          photoData.fNumber = fNumber || null
          photoData.focalLength = focalLength || null
          photoData.exposureTime = exposureTime || null
          photoData.lensMake = lensMake || null
          photoData.lensModel = lensModel || null
          photoData.flash = flash || null
          photoData.software = software || null
          photoData.exposureMode = exposureMode || null
          photoData.isoSpeed = iso || null
          photoData.pixelXDimension = dimensionX || null
          photoData.pixelYDimension = dimensionY || null
        }

        dispatch(photosActions.update(photoUpdated, weatherObj, photoData))
      } catch (error) {
        console.error(error)
      }
    }

    setLoading(loading => ({ ...loading, submit: false }))
  }

  async function _handleDelete(e) {
    e.preventDefault()
    setDeleting(true)

    try {
      dispatch(photosActions.remove(photo))
    } catch (error) {
      console.error(error)
    }
  }

  const _handleOpen = () => {
    dispatch(applicationActions.openLightbox())
  }

  const _handleTitleChange = event => {
    const value = event.target.value
    let errorTitle = false
    if (value && !validators.titleIsValid(value)) {
      errorTitle = true
    }
    setTitle(value)
    setErrors({ ...errors, title: errorTitle })
  }

  const _handleDateTimeChange = date => {
    setDateTime(date)
  }

  const _handleDescChange = event => {
    const value = event.target.value
    let errorDesc = false
    if (value && !validators.descIsValid(value)) {
      errorDesc = true
    }
    setDesc(value)
    setErrors({ ...errors, desc: errorDesc })
  }

  function _updateMarker() {
    let updatedMarker = null

    if (coordinates && coordinates.length === 2) {
      updatedMarker = <AnimatedMarker position={coordinates} />
    }
    setMarker(updatedMarker)
  }

  const _handleClick = e => {
    const { lat, lng } = e.latlng
    setCoordinates([lat, lng])
  }

  const _handleZoom = e => {
    setZoom(e.target._zoom)
  }

  const _handleWeatherChange = e => {
    setWeather(e.target.value)
  }

  const _handleTempChange = event => {
    const value = event.target.value
    let errorTemp = false
    if (value && !validators.numberIsValid(value)) {
      errorTemp = true
    }
    setTemperature(value)
    setErrors({ ...errors, temp: errorTemp })
  }

  /**
   * Returns the values of the array that contain the value
   * @param {string[]} array - Array to search through
   * @param {string} value - String to search for
   */
  function filterByValue(array, value) {
    return array.filter(
      data =>
        JSON.stringify(data)
          .toLowerCase()
          .indexOf(value.toLowerCase()) !== -1,
    )
  }

  const _filledInCompletely = () => {
    if (!title || !desc || coordinates.length !== 2) return false
    return true
  }

  return loading.photo ? (
    <Loading />
  ) : !_ownPhoto() ? (
    <Redirect to={`/photos/${id}`} />
  ) : (
    <div style={{ marginTop: '8rem' }} data-testid={'photoEdit'}>
      <Container
        component="main"
        maxWidth="md"
        className={baseClasses.relative}
      >
        <NavigationIconButton
          page={'photo'}
          title={t('forms.return')}
          aria={t('forms.photo.aria.return')}
          navigateToEdit={false}
        />
        <Typography component="h1" variant="h1">
          {t('forms.photo.headlines.main')}
        </Typography>
        <Typography variant="body1" className={baseClasses.marginBottom4}>
          {t('forms.photo.introduction')}
        </Typography>
        <Grid
          item
          xs={12}
          style={{ display: 'flex', justifyContent: 'center' }}
        >
          <img
            src={photo.imageThumbnailPreview}
            alt={`${id}`}
            className={classes.photo_preview}
            onClick={_handleOpen}
            data-testid={'photoEdit--preview'}
          />
          <Lightbox id={photo.id} url={photo.image} />
        </Grid>
        <form noValidate>
          <Step1
            data={{
              title: {
                val: title,
                func: _handleTitleChange,
              },
              dateTime: {
                val: dateTime,
                func: _handleDateTimeChange,
              },
              desc: {
                val: desc,
                func: _handleDescChange,
              },
              errors: errors,
            }}
          />
          <Step2
            data={{
              coordinates: coordinates,
              zoom: zoom,
              marker: marker,
              onClick: _handleClick,
              onZoomend: _handleZoom,
            }}
          />
          <Step3
            data={{
              tag: {
                val: tag,
                setter: setTag,
              },
              tags: {
                val: tags,
                setter: setTags,
              },
            }}
          />
          <Step4
            data={{
              loading: loading,
              errors: errors,
              weather: {
                obj: weatherObj,
                val: weather,
                conditions: weatherConditions,
                func: _handleWeatherChange,
              },
              temperature: {
                val: temperature,
                func: _handleTempChange,
              },
            }}
          />
          <Step5
            data={{
              loading: loading,
              photoData: photoData,
              make: {
                val: make,
                setter: setMake,
              },
              model: {
                val: model,
                setter: setModel,
              },
              lensMake: {
                val: lensMake,
                setter: setLensMake,
              },
              lensModel: {
                val: lensModel,
                setter: setLensModel,
              },
              focalLength: {
                val: focalLength,
                setter: setFocalLength,
              },
              fNumber: {
                val: fNumber,
                setter: setFNumber,
              },
              whiteBalance: {
                val: whiteBalance,
                setter: setWhiteBalance,
              },
              exposureTime: {
                val: exposureTime,
                setter: setExposureTime,
              },
              exposureMode: {
                val: exposureMode,
                setter: setExposureMode,
              },
              iso: {
                val: iso,
                setter: setIso,
              },
              flash: {
                val: flash,
                setter: setFlash,
              },
              software: {
                val: software,
                setter: setSoftware,
              },
              dimensionX: {
                val: dimensionX,
                setter: setDimensionX,
              },
              dimensionY: {
                val: dimensionY,
                setter: setDimensionY,
              },
            }}
          />
          {apiErrors && apiErrors.update && (
            <ErrorMessage text={apiErrors.update} noTranslate mt />
          )}
          <Grid container spacing={3} className={classes.marginTopBottom}>
            <Grid item xs={12} sm={6} className={baseClasses.relative}>
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                className={baseClasses.redBackground}
                disabled={active}
                onClick={e => _handleDelete(e)}
                data-testid={'photoEdit--deleteButton'}
              >
                {t('forms.photo.buttons.delete')}
              </Button>
              {active && deleting && (
                <CircularProgress
                  size={24}
                  className={`${baseClasses.buttonProgress} ${classes.buttonAdj}`}
                />
              )}
            </Grid>
            <Grid item xs={12} sm={6} className={baseClasses.relative}>
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                disabled={
                  !_filledInCompletely() ||
                  Object.values(errors).includes(true) ||
                  active
                }
                onClick={e => _handleSubmit(e)}
                data-testid={'photoEdit--submitButton'}
              >
                {t('forms.photo.buttons.save')}
              </Button>
              {active && !deleting && (
                <CircularProgress
                  size={24}
                  className={`${baseClasses.buttonProgress} ${classes.buttonAdj}`}
                />
              )}
            </Grid>
          </Grid>
        </form>
      </Container>
    </div>
  )
}
