import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import SunriseSunset from './sunriseSunset'

const Data = {
  astronomicalTwilightBegin: '2020-11-25T05:09:04Z',
  astronomicalTwilightEnd: '2020-11-25T16:26:35Z',
  civilTwilightBegin: '2020-11-25T05:47:36Z',
  civilTwilightEnd: '2020-11-25T15:48:03Z',
  date: '2020-11-25',
  dayLength: 31846,
  nauticalTwilightBegin: '2020-11-25T05:09:04Z',
  nauticalTwilightEnd: '2020-11-25T16:26:35Z',
  solarNoon: '2020-11-25T10:47:49Z',
  sunrise: '2020-11-25T06:22:26Z',
  sunset: '2020-11-25T15:13:12Z',
}

const mockTranslation = jest.fn(str => str)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer() {
  const result = render(
    <Router>
      <SunriseSunset data={Data} />
    </Router>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('SunriseSunset', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('renders a total of 9 entries', async () => {
    const { container } = renderContainer()
    const entries = await waitForElement(() =>
      container.querySelectorAll('[data-testid="sunriseSunset"] > div'),
    )

    expect(entries).toHaveLength(9)
  })
  it('translates the text', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalledTimes(18)
    expect(mockTranslation).toHaveBeenCalledWith(
      'photo.tiles.sunriseSunset.civil_twilight_begin',
    )
  })
  it('formats the time', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalledTimes(18)
    expect(mockTranslation).toHaveBeenCalledWith('utilities.time.long', {
      time: new Date(Data['civilTwilightBegin']),
    })
  })
  it('sets different background colours', async () => {
    const { getByTestId } = renderContainer()
    const entryBlue = await waitForElement(() =>
      getByTestId(
        'sunriseSunset--entry--photo.tiles.sunriseSunset.civil_twilight_begin',
      ),
    )
    const entryGold = await waitForElement(() =>
      getByTestId('sunriseSunset--entry--photo.tiles.sunriseSunset.sunrise'),
    )
    const entryYellow = await waitForElement(() =>
      getByTestId('sunriseSunset--entry--photo.tiles.sunriseSunset.solar_noon'),
    )
    const entryOrange = await waitForElement(() =>
      getByTestId('sunriseSunset--entry--photo.tiles.sunriseSunset.sunset'),
    )

    expect(entryBlue.getAttribute('style')).toContain(
      'background-color: lightsteelblue',
    )
    expect(entryGold.getAttribute('style')).toContain(
      'background-color: lightgoldenrodyellow',
    )
    expect(entryYellow.getAttribute('style')).toContain(
      'background-color: lightyellow',
    )
    expect(entryOrange.getAttribute('style')).toContain(
      'background-color: bisque',
    )
  })
})
