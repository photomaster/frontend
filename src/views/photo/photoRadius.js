import React, { useState, useEffect, lazy, Suspense } from 'react'
import Container from '@material-ui/core/Container'
import Loading from 'components/utilities/loading'
import { photoService } from 'service'
import NotFound from 'views/not-found/not-found'
import useInfiniteScroll from '../../helpers/scrollInfinite'

const RecommendationCard = lazy(() =>
  import('components/recommendationCard/recommendationCard.js'),
)

const RecommendationsGrid = lazy(() =>
  import('components/recommendationCard/recommendationsGrid'),
)

export default function PhotoRadiusPage(props) {
  const [limit] = useState(9)
  const [initialLoad, setInitialLoad] = useState(true)
  const [photos, setPhotos] = useState([])
  const [offset, setOffset] = useState(0)
  const [fetchedAll, setFetchedAll] = useState(false)
  // eslint-disable-next-line no-unused-vars
  const [_, setIsFetching] = useInfiniteScroll(
    () => setOffset(offset + limit),
    fetchedAll,
  )

  useEffect(() => {
    ;(async () => {
      const lat = props.match.params.lat
      const long = props.match.params.long

      const loadedPhotos = await photoService.getNearPhotos(lat, long, 0.01, {
        limit,
        offset,
      })

      if (loadedPhotos.length < limit) {
        setFetchedAll(true)
      }

      setIsFetching(false)
      setPhotos([...photos, ...loadedPhotos])
      setInitialLoad(false)
    })()
  }, [offset])

  return (
    <div className="App">
      <Container component="main" maxWidth="md" className="App">
        <Suspense fallback={<Loading />}>
          {!initialLoad && photos.length === 0 ? (
            <NotFound />
          ) : (
            <RecommendationsGrid>
              {photos.map(photo => (
                <RecommendationCard
                  key={`photo-list-${photo.id}`}
                  photo={photo}
                  user={photo.user}
                />
              ))}
            </RecommendationsGrid>
          )}
        </Suspense>
      </Container>
    </div>
  )
}
