import React, { useReducer } from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
  wait,
} from '@testing-library/react'
import Photo from './photo'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import { applicationActions } from 'actions'
import photoService from 'service/photoService'
import weatherService from 'service/weatherService'
import PhotoModel from 'models/photo'
import WeatherModel from 'models/weatherState'
import PhotoData from 'models/photoData'

const TestUser = {
  id: 1,
  username: 'test_username',
  displayName: 'test_displayName',
  description: 'test desc',
  email: 'test@mail.com',
  password: 'test_password',
  password2: 'test_password',
  bannerImage: '/banner1',
  avatarImage: '/avatar1',
}
const OtherUser = {
  id: 2,
  username: 'other_username',
  displayName: 'other_displayName',
  description: 'other desc',
  email: 'other@mail.com',
  password: 'other_password',
  password2: 'other_password',
  bannerImage: '/banner2',
  avatarImage: '/avatar2',
}

const TestDate = new Date('2020-11-25T05:09:04Z')

const TestPhotoContent = {
  id: 1,
  url: '/photo1',
  name: 'title1',
  description: 'desc1',
  geo: {
    type: 'Point',
    coordinates: [47.811195, 13.033229],
  },
  photodata: '/photodata1',
  solarData: '/some/url/to/solar/data/1/',
  currentWeather: null,
  originalCreationDate: TestDate,
  weatherAtCreation: '/some/url/to/weatherState/33/',
  image: '/image1',
  imageThumbnailTeaser: '/thumbnail1',
  imageThumbnailTeaserSmall: '/thumbnailsmall1',
  imageThumbnailPreview: '/thumbnailpreview1',
  labels: [
    {
      title: 'label1',
      score: 1,
    },
    {
      title: 'label2',
      score: 1,
    },
    {
      title: 'label3',
      score: 1,
    },
    {
      title: 'label4',
      score: 1,
    },
    {
      title: 'label5',
      score: 1,
    },
    {
      title: 'label6',
      score: 1,
    },
  ],
  user: TestUser,
}
const TestPhoto = new PhotoModel(TestPhotoContent)
const RecommendedPhoto = new PhotoModel({
  id: 2,
  url: '/photo2',
  name: 'title2',
  description: 'desc2',
  image: '/image2',
  imageThumbnailTeaser: '/thumbnai21',
  imageThumbnailTeaserSmall: '/thumbnailsmall2',
  imageThumbnailPreview: '/thumbnailpreview2',
  user: OtherUser,
})
const TestPhotoData = new PhotoData({
  id: 1,
  url: '/photosData/1',
  photo: '/photo/1',
  make: 'make',
  model: 'model',
  lensMake: 'lensMake',
  lensModel: 'lensModel',
  focalLength: 'focalLength',
  fNumber: 'fNumber',
  whiteBalance: 'whiteBalance',
  lightSource: 'lightSource',
  exposureTime: 'exposureTime',
  exposureProgram: 'exposureProgram',
  exposureMode: 'exposureMode',
  isoSpeed: 'isoSpeed',
  flash: 'flash',
  pixelXDimension: 200,
  pixelYDimension: 100,
  orientation: 3,
  software: 'software',
})
const TestWeatherState = new WeatherModel({
  description: 'clear sky',
  temperature: 10,
})
const TestSolarData = {
  astronomicalTwilightBegin: '2020-11-25T05:09:04Z',
  astronomicalTwilightEnd: '2020-11-25T16:26:35Z',
  civilTwilightBegin: '2020-11-25T05:47:36Z',
  civilTwilightEnd: '2020-11-25T15:48:03Z',
  date: '2020-11-25',
  dayLength: 31846,
  nauticalTwilightBegin: '2020-11-25T05:09:04Z',
  nauticalTwilightEnd: '2020-11-25T16:26:35Z',
  solarNoon: '2020-11-25T10:47:49Z',
  sunrise: '2020-11-25T06:22:26Z',
  sunset: '2020-11-25T15:13:12Z',
}

const mockTranslation = jest.fn(str => str)
const mockConsole = jest.fn(msg => msg)
const mockDispatch = jest.fn()
const mockGetPhoto = jest.fn(id => Promise.resolve(TestPhoto))
const mockGetPhotoData = jest.fn(() => Promise.resolve(TestPhotoData))
const mockGetNearPhotos = jest.fn(() =>
  Promise.resolve([TestPhoto, RecommendedPhoto]),
)
const mockGetSimilarPhotos = jest.fn(() =>
  Promise.resolve([TestPhoto, RecommendedPhoto]),
)
const mockGetSolarData = jest.fn(id => Promise.resolve(TestSolarData))
const mockGetWeather = jest.fn(() => Promise.resolve(TestWeatherState))

window.scrollTo = jest.fn()
console.error = mockConsole

jest.mock('react-i18next', () => ({
  ...jest.requireActual('react-i18next'),
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        language: 'en',
        changeLanguage: () => new Promise(() => {}),
        on: jest.fn((_, fn) => fn()),
      },
    }
  },
}))

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

jest.mock('service/photoService', () => jest.fn())
photoService.getPhoto = mockGetPhoto
photoService.getPhotoData = mockGetPhotoData
photoService.getNearPhotos = mockGetNearPhotos
photoService.getSimilarPhotos = mockGetSimilarPhotos
photoService.getSolarData = mockGetSolarData

jest.mock('service/weatherService', () => jest.fn())
weatherService.getWeather = mockGetWeather

function renderContainer(
  photoPageId = 1,
  activePhoto = TestPhotoContent,
  loggedIn = true,
  user = TestUser,
) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: {
            loggedIn: loggedIn,
            user: user,
          },
          photos: {
            active: activePhoto,
          },
        })}
      >
        <Photo match={{ params: { id: photoPageId } }} />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Photo View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('requests photo if not available via redux', async () => {
    renderContainer(1, null)

    expect(mockGetPhoto).toHaveBeenCalledWith(1)
  })
  it('requests data from the API (near photos, solar data, weather, photo data)', async () => {
    const { container } = renderContainer()

    await wait(() =>
      expect(container.textContent).toContain(
        'photo.tiles.sunriseSunset.astronomical_twilight_begin',
      ),
    )
    await wait(() =>
      expect(container.textContent).toContain(
        'photo.tiles.weather.temperature',
      ),
    )
    await wait(() =>
      expect(container.textContent).toContain('forms.labels.make'),
    )

    expect(mockGetNearPhotos).toHaveBeenLastCalledWith(
      TestPhotoContent.geo.coordinates[1],
      TestPhotoContent.geo.coordinates[0],
      10,
    )
    expect(mockGetSolarData).toHaveBeenLastCalledWith(1)
    expect(mockGetWeather).toHaveBeenLastCalledWith(33)
    expect(mockGetPhotoData).toHaveBeenLastCalledWith(TestPhoto)
  })
  it('requests similar photos on click on tab', async () => {
    const { getByTestId } = renderContainer()
    const tabButton = await waitForElement(() =>
      getByTestId('photo--tab--similar'),
    )

    fireEvent.click(tabButton)

    expect(mockGetSimilarPhotos).toHaveBeenCalledTimes(1)
    expect(mockGetSimilarPhotos).toHaveBeenLastCalledWith(TestPhoto)
  })
  xit('requests solar data of today on click on button', async () => {
    const { container, getByTestId } = renderContainer()
    const button = await waitForElement(() => getByTestId('photo--solar--next'))

    await wait(() =>
      expect(container.textContent).toContain(
        'photo.tiles.sunriseSunset.astronomical_twilight_begin',
      ),
    )
    //await wait(() => expect(container.textContent).toContain('photo.tiles.weather.temperature'))
    //await wait(() => expect(container.textContent).toContain('forms.labels.make'))

    fireEvent.click(button)

    // TODO times out :(
    await wait(() =>
      expect(container.textContent).not.toContain(
        'photo.tiles.headlines.time.photo',
      ),
    )

    //await wait(() => expect(getByTestId('photo--solar--today')).toBeTruthy())

    //await wait(() => expect(getByTestId('photo--solar--today')).toBeTruthy())
    /*const headline = await waitForElement(() =>
      getByTestId('photo--solar--today'),
    )

    expect(headline).toBeTruthy()*/
    expect(mockGetSolarData).toHaveBeenCalledTimes(2)
    expect(mockGetSolarDataToday).toHaveBeenLastCalledWith(1, new Date())
  })
  it('dispatches action to open lightbox on click on photo', async () => {
    const mockOpenLightbox = jest.fn().mockImplementation(() => {
      return { type: 'opening lightbox' }
    })
    applicationActions.openLightbox = mockOpenLightbox

    const { getByTestId } = renderContainer()
    const photo = await waitForElement(() => getByTestId('photo--preview'))

    fireEvent.click(photo)

    expect(mockOpenLightbox).toHaveBeenCalledTimes(1)
  })
  it('shows more tags on click on button', async () => {
    const { getByTestId, getAllByTestId } = renderContainer()
    const button = await waitForElement(() => getByTestId('photo--tagsButton'))
    const initialChips = await waitForElement(() => getAllByTestId('chip'))
    expect(initialChips.length).toEqual(5)

    fireEvent.click(button)

    const chipsAfterClick = await waitForElement(() => getAllByTestId('chip'))
    expect(chipsAfterClick.length).toEqual(6)
  })
  it('shows less tags on click on button', async () => {
    const { getByTestId, getAllByTestId } = renderContainer()
    const button = await waitForElement(() => getByTestId('photo--tagsButton'))

    fireEvent.click(button)

    const expandedChips = await waitForElement(() => getAllByTestId('chip'))
    expect(expandedChips.length).toEqual(6)

    fireEvent.click(button)

    const chipsAfterClick = await waitForElement(() => getAllByTestId('chip'))
    expect(chipsAfterClick.length).toEqual(5)
  })
  it('links to the owner of the photo', async () => {
    const { getByTestId } = renderContainer()

    const userCredit = await waitForElement(() => getByTestId('userCredit'))

    expect(userCredit.getAttribute('href')).toEqual('/users/1')
  })
  it('displays loading component', async () => {
    photoService.getPhoto.mockImplementationOnce(jest.fn(() => null))

    const { getByTestId } = renderContainer(1, null)

    const loading = getByTestId('loading')

    expect(loading).toBeTruthy()
  })
  it('displays button to edit own photo', async () => {
    const { getAllByTestId } = renderContainer()

    const buttons = await waitForElement(() =>
      getAllByTestId('navigationIconButton--button'),
    )
    const editButton = buttons[0]

    expect(editButton.getAttribute('title')).toEqual('photo.edit')
  })
  it('does not display button to edit of not owned photo', async () => {
    const { getAllByTestId } = renderContainer(
      1,
      TestPhotoContent,
      true,
      OtherUser,
    )

    const buttons = await waitForElement(() =>
      getAllByTestId('navigationIconButton--button'),
    )

    expect(buttons.length).toEqual(1)
    expect(buttons[0].getAttribute('title')).not.toEqual('photo.edit')
  })
  it('displays button to navigate to last page', async () => {
    const { getAllByTestId } = renderContainer()

    const buttons = await waitForElement(() =>
      getAllByTestId('navigationIconButton--button'),
    )
    const returnButton = buttons[1]

    expect(returnButton.getAttribute('title')).toEqual('forms.return')
  })
  it('displays a preview of the photo', async () => {
    const { getByTestId } = renderContainer()

    const photo = await waitForElement(() => getByTestId('photo--preview'))

    expect(photo).toBeTruthy()
  })
  it('displays the title of the photo as headline', async () => {
    const { container } = renderContainer()

    const headline = await waitForElement(() => container.querySelector('h1'))

    expect(headline.textContent).toEqual('title1')
  })
  it('translates the text', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('forms.return')
    expect(mockTranslation).toHaveBeenCalledWith(
      'photo.tiles.sunriseSunset.changePage.general',
    )
    expect(mockTranslation).toHaveBeenCalledWith('photo.aria.edit')
    expect(mockTranslation).toHaveBeenCalledWith('photo.by')
    expect(mockTranslation).toHaveBeenCalledWith('photo.tiles.headlines.detail')
  })
})
