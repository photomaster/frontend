import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
  tileContent: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: theme.spacing(1),
    '& p': {
      margin: '0',
      marginBottom: theme.spacing(1),
    },
  },
}))

export default function TileWeather(props) {
  const baseClasses = baseStyles()
  const classes = useStyles()
  const { t } = useTranslation()
  const { headline, weather, temp } = props

  return (
    <React.Fragment>
      <Typography
        component="h5"
        variant="h5"
        className={baseClasses.tilesHeadline}
        data-testid={'tileWeather--headline'}
      >
        {headline}
      </Typography>
      <div className={classes.tileContent} data-testid={'tileWeather--content'}>
        {weather && (
          <p>
            {t('photo.tiles.weather.weather')}:{' '}
            <span className={baseClasses.bold}>{weather}</span>
          </p>
        )}
        {temp && (
          <p>
            {t('photo.tiles.weather.temperature')}:{' '}
            <span className={baseClasses.bold}>{temp} °C</span>
          </p>
        )}
      </div>
    </React.Fragment>
  )
}

TileWeather.propTypes = {
  headline: PropTypes.string.isRequired,
  weather: PropTypes.string,
  temp: PropTypes.number,
}
