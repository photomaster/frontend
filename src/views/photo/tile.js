import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    whiteSpace: 'nowrap',
    marginBottom: theme.spacing(1),
    '& h5': {
      wordWrap: 'break-word',
      width: '100%',
      whiteSpace: 'normal',
    },
    '& hr': {
      border: '1px solid lightgray',
      background: 'lightgray',
    },
    '& p': {
      whiteSpace: 'normal',
      wordWrap: 'break-word',
    },
  },
  headline: {
    display: 'flex',
    alignItems: 'center',
    '& h4': {
      marginLeft: theme.spacing(2),
      wordWrap: 'break-word',
      width: '75%',
      whiteSpace: 'normal',
    },
  },
}))

export default function Tile(props) {
  const classes = useStyles()
  const Icon = props.icon
  const { headline, children } = props

  return (
    <Grid item xs={12} sm={6} data-testid={'tile'}>
      <Paper className={classes.paper}>
        <div className={classes.headline}>
          <Icon style={{ fontSize: 50 }} data-testid={'tile--icon'} />
          <Typography component="h4" variant="h4">
            {headline}
          </Typography>
        </div>
        {children}
      </Paper>
    </Grid>
  )
}

Tile.propTypes = {
  headline: PropTypes.string.isRequired,
  icon: PropTypes.any.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}
