import React from 'react'
import 'components/recommendations/recommendations.scss'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core'
import { errorMessages } from 'helpers/validators'
import { useTranslation } from 'react-i18next'
import DateFnsUtils from '@date-io/date-fns'
import { MuiPickersUtilsProvider, DateTimePicker } from '@material-ui/pickers'

const useStyles = makeStyles(theme => ({
  marginTopLarge: {
    marginTop: theme.spacing(6),
  },
}))

export default function Step1(props) {
  const classes = useStyles()
  const { t } = useTranslation()
  const { data } = props

  return (
    <React.Fragment>
      <Typography
        component="h2"
        variant="h2"
        className={classes.marginTopLarge}
      >
        {t('forms.photo.headlines.general')}
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={7}>
          <TextField
            required
            fullWidth
            id="title"
            label={t('forms.labels.title')}
            value={data.title.val}
            onChange={data.title.func}
            error={data.errors.title}
            helperText={data.errors.title ? errorMessages.title : ''}
            data-testid={'photoEdit--title'}
          />
        </Grid>
        <Grid item xs={12} sm={5}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DateTimePicker
              disableFuture
              showTodayButton
              ampm={false}
              fullWidth
              label={t('forms.labels.dateTime')}
              format="MMMM d yyyy HH:mm"
              value={data.dateTime.val}
              onChange={data.dateTime.func}
              onError={console.warn}
              helperText={t('forms.helperTexts.dateTime.hint')}
              data-testid={'photoEdit--dateTime'}
            />
          </MuiPickersUtilsProvider>
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="desc"
            label={t('forms.labels.desc')}
            multiline
            fullWidth
            rows={5}
            rowsMax={10}
            required
            value={data.desc.val}
            error={data.errors.desc}
            helperText={data.errors.desc ? errorMessages.desc : ''}
            onChange={data.desc.func}
            data-testid={'photoEdit--desc'}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  )
}
