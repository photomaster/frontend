import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import Step1 from './photoEditStep1'

const TestDate = new Date('2020-11-25T05:09:04Z')
const TestDateConverted = 'November 25 2020 06:09'

const mockTranslation = jest.fn(str => str)
const mockHandleTitleChange = jest.fn()
const mockHandleDateTimeChange = jest.fn()
const mockHandleDescChange = jest.fn()

/* material-UI datepicker prints every change in the log --> mock console to avoid messages in tests */
console.warn = jest.fn()

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer(
  errors = { title: false, dateTime: false, desc: false },
) {
  const result = render(
    <Router>
      <Step1
        data={{
          title: {
            val: 'title',
            func: mockHandleTitleChange,
          },
          dateTime: {
            val: TestDate,
            func: mockHandleDateTimeChange,
          },
          desc: {
            val: 'desc',
            func: mockHandleDescChange,
          },
          errors: errors,
        }}
      />
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Photo Edit Step 1 View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('shows a translated headline', async () => {
    const { container } = renderContainer()

    const headline = await waitForElement(() => container.querySelector('h2'))

    expect(headline).toBeTruthy()
    expect(headline.textContent).toEqual('forms.photo.headlines.general')
    expect(mockTranslation).toHaveBeenCalledWith(
      'forms.photo.headlines.general',
    )
  })
  it('translates the labels', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalledWith('forms.labels.title')
    expect(mockTranslation).toHaveBeenCalledWith('forms.labels.dateTime')
    expect(mockTranslation).toHaveBeenCalledWith('forms.labels.desc')
  })
  describe('displays current value of', () => {
    it('title', async () => {
      const { findByTestId } = renderContainer()

      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--title'),
      )

      expect(textfield.querySelector('input').getAttribute('value')).toEqual(
        'title',
      )
    })
    it('dateTime', async () => {
      const { findByTestId } = renderContainer()

      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--dateTime'),
      )

      expect(textfield.querySelector('input').getAttribute('value')).toEqual(
        TestDateConverted,
      )
    })
    it('desc', async () => {
      const { findByTestId } = renderContainer()

      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--desc'),
      )

      expect(textfield.querySelector('textarea').textContent).toEqual('desc')
    })
  })
  describe('calls function to handle change of', () => {
    it('title', async () => {
      const { findByTestId } = renderContainer()

      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--title'),
      )

      fireEvent.change(textfield.querySelector('input'), {
        target: { value: 'a new title' },
      })

      expect(mockHandleTitleChange).toHaveBeenCalledTimes(1)
    })
    it('dateTime', async () => {
      const { findByTestId, queryByText } = renderContainer()

      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--dateTime'),
      )

      fireEvent.click(textfield.querySelector('input'))
      fireEvent.click(queryByText('7'))
      fireEvent.click(queryByText('OK'))

      expect(mockHandleDateTimeChange).toHaveBeenCalledTimes(1)
    })
    it('desc', async () => {
      const { findByTestId } = renderContainer()

      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--desc'),
      )

      fireEvent.change(textfield.querySelector('textarea'), {
        target: { value: 'a new desc' },
      })

      expect(mockHandleDescChange).toHaveBeenCalledTimes(1)
    })
  })
  describe('displays errors for', () => {
    it('title', async () => {
      const { findByTestId } = renderContainer({
        title: true,
        dateTime: false,
        desc: false,
      })

      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--title'),
      )

      expect(textfield.querySelector('.Mui-error')).toBeTruthy()
    })
    it('desc', async () => {
      const { findByTestId } = renderContainer({
        title: false,
        dateTime: false,
        desc: true,
      })

      const textfield = await waitForElement(() =>
        findByTestId('photoEdit--desc'),
      )

      expect(textfield.querySelector('.Mui-error')).toBeTruthy()
    })
  })
})
