import React from 'react'
import 'components/recommendations/recommendations.scss'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import { Map, TileLayer } from 'react-leaflet'
import Search from 'components/map/search'

const useStyles = makeStyles(theme => ({
  marginTopLarge: {
    marginTop: theme.spacing(6),
  },
  map: {
    height: '50vh',
    maxHeight: '500px',
  },
}))

export default function Step2(props) {
  const classes = useStyles()
  const { t } = useTranslation()
  const { data } = props

  return (
    <React.Fragment>
      <Typography
        component="h2"
        variant="h2"
        className={classes.marginTopLarge}
      >
        {t('forms.photo.headlines.location')}
      </Typography>
      <Map
        className={classes.map}
        center={
          data.coordinates && data.coordinates.length > 0
            ? data.coordinates
            : [47.811195, 13.033229]
        }
        zoom={data.zoom}
        animate
        onClick={data.onClick}
        onZoomend={data.onZoomend}
      >
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        <Search variant="bar" />
        {data.marker}
      </Map>
    </React.Fragment>
  )
}
