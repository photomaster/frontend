import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import Step3 from './photoEditStep3'

const Tags = [
  { key: 0, label: 'tag0' },
  { key: 1, label: 'tag1' },
  { key: 2, label: 'tag2' },
]

const mockTranslation = jest.fn(str => str)
const mockSetTag = jest.fn()
const mockSetTags = jest.fn()

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer() {
  const result = render(
    <Router>
      <Step3
        data={{
          tag: {
            val: 'tag',
            setter: mockSetTag,
          },
          tags: {
            val: Tags,
            setter: mockSetTags,
          },
        }}
      />
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Photo Edit Step 3 View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('shows a translated headline', async () => {
    const { container } = renderContainer()

    const headline = await waitForElement(() => container.querySelector('h2'))

    expect(headline).toBeTruthy()
    expect(headline.textContent).toEqual('forms.photo.headlines.tags')
    expect(mockTranslation).toHaveBeenCalledWith('forms.photo.headlines.tags')
  })
  it('displays given tags', async () => {
    const { container } = renderContainer()

    expect(container.textContent).toContain('tag0')
    expect(container.textContent).toContain('tag1')
    expect(container.textContent).toContain('tag2')
  })
})
