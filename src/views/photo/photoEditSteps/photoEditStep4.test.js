import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import Step4 from './photoEditStep4'

const mockTranslation = jest.fn(str => str)
const mockHandleWeatherChange = jest.fn()
const mockHandleTempChange = jest.fn()

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer(
  loading = { weather: false },
  errors = { temp: false },
  weather = 'clear sky',
  temp = 10,
) {
  const result = render(
    <Router>
      <Step4
        data={{
          loading: loading,
          errors: errors,
          weather: {
            obj: {},
            val: weather,
            conditions: [
              {
                short: 'clear',
                long: 'clear sky',
              },
              {
                short: 'clouds_few',
                long: 'few clouds',
              },
              {
                long: 'weather by the API',
              },
            ],
            func: mockHandleWeatherChange,
          },
          temperature: {
            val: temp,
            func: mockHandleTempChange,
          },
        }}
      />
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Photo Edit Step 4 View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('shows a translated headline', async () => {
    const { container } = renderContainer()

    const headline = await waitForElement(() => container.querySelector('h2'))

    expect(headline).toBeTruthy()
    expect(headline.textContent).toEqual('forms.photo.headlines.weather')
    expect(mockTranslation).toHaveBeenCalledWith(
      'forms.photo.headlines.weather',
    )
  })
  it('translates labels and weather conditions', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalledWith('forms.labels.temp')
    expect(mockTranslation).toHaveBeenCalledWith(
      'forms.photo.weather.conditions.clear',
    )
    expect(mockTranslation).toHaveBeenCalledWith(
      'forms.photo.weather.conditions.clouds_few',
    )
  })
  it('displays loading component while loading', async () => {
    const { container, getByTestId } = renderContainer({ weather: true })

    const loading = await waitForElement(() => getByTestId('loading'))
    const headline = container.querySelector('h2')

    expect(loading).toBeTruthy()
    expect(headline).toBeFalsy()
  })
  it('can handle no weather value available', async () => {
    const { container, getByTestId } = renderContainer(
      { weather: false },
      { temp: false },
      null,
    )

    const select = await waitForElement(() => getByTestId('photoEdit--weather'))

    expect(container).toBeTruthy()
    expect(select.querySelector('input').getAttribute('value')).toEqual('')
  })
  it('can handle no temperature value available', async () => {
    const { container, getByTestId } = renderContainer(
      { weather: false },
      { temp: false },
      'clear sky',
      null,
    )

    const textfield = await waitForElement(() => getByTestId('photoEdit--temp'))

    expect(container).toBeTruthy()
    expect(textfield.querySelector('input').getAttribute('value')).toEqual('')
  })
  it('displays error state if temperature is not a number', async () => {
    const { getByTestId } = renderContainer(
      { weather: false },
      { temp: true },
      'clear sky',
      'not a number',
    )

    const textfield = await waitForElement(() => getByTestId('photoEdit--temp'))

    expect(textfield.querySelector('.Mui-error')).toBeTruthy()
  })
})
