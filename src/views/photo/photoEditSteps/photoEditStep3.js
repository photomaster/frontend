import React from 'react'
import 'components/recommendations/recommendations.scss'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import TagsAutocomplete from 'components/formElements/tagsAutocomplete'

const useStyles = makeStyles(theme => ({
  marginTopLarge: {
    marginTop: theme.spacing(6),
  },
}))

export default function Step3(props) {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const { t } = useTranslation()
  const { data } = props

  return (
    <React.Fragment>
      <Typography
        component="h2"
        variant="h2"
        className={classes.marginTopLarge}
      >
        {t('forms.photo.headlines.tags')}
      </Typography>
      <Grid container spacing={3} className={baseClasses.alignItemsCenter}>
        <TagsAutocomplete
          tag={data.tag.val}
          tags={data.tags.val}
          setTag={data.tag.setter}
          setTags={data.tags.setter}
        />
      </Grid>
    </React.Fragment>
  )
}
