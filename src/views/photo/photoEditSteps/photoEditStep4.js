import React, { lazy, Suspense } from 'react'
import 'components/recommendations/recommendations.scss'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core'
import { errorMessages } from 'helpers/validators'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import Loading from 'components/utilities/loading'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'

const FormControl = lazy(() => import('@material-ui/core/FormControl'))

const useStyles = makeStyles(theme => ({
  marginTopLarge: {
    marginTop: theme.spacing(6),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}))

export default function Step4(props) {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const { t } = useTranslation()
  const { data } = props

  return (
    <React.Fragment>
      {data.loading.weather && <Loading />}
      {!data.loading.weather && data.weather.obj && (
        <Suspense fallback={<Loading />}>
          <Typography
            component="h2"
            variant="h2"
            className={classes.marginTopLarge}
          >
            {t('forms.photo.headlines.weather')}
          </Typography>
          <Grid container spacing={3} justify={'center'}>
            <Grid
              item
              xs={6}
              md={4}
              className={baseClasses.justifyContentCenter}
            >
              <FormControl
                className={classes.formControl}
                data-testid={'photoEdit--weather'}
              >
                <InputLabel id="weather">
                  {t('forms.photo.headlines.weather')}
                </InputLabel>
                <Select
                  labelId="weather"
                  id="weather-select"
                  value={data.weather.val || ''}
                  onChange={data.weather.func}
                >
                  {data.weather.conditions.map(condition => (
                    <MenuItem
                      key={condition.short || 'custom'}
                      value={condition.long}
                      data-testid={`photoEdit--select--${condition.short ||
                        'custom'}`}
                    >
                      {condition.short
                        ? t(`forms.photo.weather.conditions.${condition.short}`)
                        : condition.long}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={6} md={4}>
              <TextField
                id="temp"
                label={t('forms.labels.temp')}
                fullWidth
                value={data.temperature.val || ''}
                onChange={data.temperature.func}
                error={data.errors.temp}
                helperText={data.errors.temp ? errorMessages.temp : ''}
                data-testid={'photoEdit--temp'}
              />
            </Grid>
          </Grid>
        </Suspense>
      )}
    </React.Fragment>
  )
}
