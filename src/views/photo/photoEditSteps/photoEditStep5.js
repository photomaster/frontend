import React, { lazy, Suspense } from 'react'
import 'components/recommendations/recommendations.scss'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import Loading from 'components/utilities/loading'

const TextFieldCamera = lazy(() =>
  import('components/formElements/textFieldCamera.js'),
)

const useStyles = makeStyles(theme => ({
  marginTopLarge: {
    marginTop: theme.spacing(6),
  },
  marginTopMedium: {
    marginTop: theme.spacing(3),
  },
}))

export default function Step5(props) {
  const classes = useStyles()
  const { t } = useTranslation()
  const { data } = props

  return (
    <React.Fragment>
      <Typography
        component="h2"
        variant="h2"
        className={classes.marginTopLarge}
      >
        {t('forms.photo.headlines.camera')}
      </Typography>
      {data.loading.cameraData && <Loading />}
      {!data.loading.cameraData && data.photoData && (
        <Suspense fallback={<Loading />}>
          <Grid container spacing={3}>
            <TextFieldCamera
              id={'make'}
              value={data.make.val}
              func={e => data.make.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'model'}
              value={data.model.val}
              func={e => data.model.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'lensMake'}
              value={data.lensMake.val}
              func={e => data.lensMake.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'lensModel'}
              value={data.lensModel.val}
              func={e => data.lensModel.setter(e.target.value)}
            />
          </Grid>
        </Suspense>
      )}
      <Typography
        component="h3"
        variant="h3"
        className={classes.marginTopMedium}
      >
        {t('forms.photo.headlines.settings')}
      </Typography>
      {data.loading.cameraData && <Loading />}
      {!data.loading.cameraData && data.photoData && (
        <Suspense fallback={<Loading />}>
          <Grid container spacing={3}>
            <TextFieldCamera
              id={'focalLength'}
              value={data.focalLength.val}
              func={e => data.focalLength.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'fNumber'}
              value={data.fNumber.val}
              func={e => data.fNumber.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'whiteBalance'}
              value={data.whiteBalance.val}
              func={e => data.whiteBalance.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'exposureTime'}
              value={data.exposureTime.val}
              func={e => data.exposureTime.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'exposureMode'}
              value={data.exposureMode.val}
              func={e => data.exposureMode.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'iso'}
              value={data.iso.val}
              func={e => data.iso.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'flash'}
              value={data.flash.val}
              func={e => data.flash.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'software'}
              value={data.software.val}
              func={e => data.software.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'dimensionX'}
              value={data.dimensionX.val}
              func={e => data.dimensionX.setter(e.target.value)}
            />
            <TextFieldCamera
              id={'dimensionY'}
              value={data.dimensionY.val}
              func={e => data.dimensionY.setter(e.target.value)}
            />
          </Grid>
        </Suspense>
      )}
    </React.Fragment>
  )
}
