import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import Step5 from './photoEditStep5'

const mockTranslation = jest.fn(str => str)
const mockSetMake = jest.fn()
const mockSetModel = jest.fn()
const mockSetLensMake = jest.fn()
const mockSetLensModel = jest.fn()
const mockSetFocalLength = jest.fn()
const mockSetFNumber = jest.fn()
const mockSetWhiteBalance = jest.fn()
const mockSetExposureTime = jest.fn()
const mockSetExposureMode = jest.fn()
const mockSetIso = jest.fn()
const mockSetFlash = jest.fn()
const mockSetSoftware = jest.fn()
const mockSetDimensionX = jest.fn()
const mockSetDimensionY = jest.fn()

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer(loading = { cameraData: false }) {
  const result = render(
    <Router>
      <Step5
        data={{
          loading: loading,
          photoData: {},
          make: {
            val: 'make',
            setter: mockSetMake,
          },
          model: {
            val: 'model',
            setter: mockSetModel,
          },
          lensMake: {
            val: 'lensMake',
            setter: mockSetLensMake,
          },
          lensModel: {
            val: 'lensModel',
            setter: mockSetLensModel,
          },
          focalLength: {
            val: 'focalLength',
            setter: mockSetFocalLength,
          },
          fNumber: {
            val: 'fNumber',
            setter: mockSetFNumber,
          },
          whiteBalance: {
            val: 'whiteBalance',
            setter: mockSetWhiteBalance,
          },
          exposureTime: {
            val: 'exposureTime',
            setter: mockSetExposureTime,
          },
          exposureMode: {
            val: 'exposureMode',
            setter: mockSetExposureMode,
          },
          iso: {
            val: 'iso',
            setter: mockSetIso,
          },
          flash: {
            val: 'flash',
            setter: mockSetFlash,
          },
          software: {
            val: 'software',
            setter: mockSetSoftware,
          },
          dimensionX: {
            val: 200,
            setter: mockSetDimensionX,
          },
          dimensionY: {
            val: 100,
            setter: mockSetDimensionY,
          },
        }}
      />
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Photo Edit Step 5 View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('shows translated headlines', async () => {
    const { container } = renderContainer()

    const headline = await waitForElement(() => container.querySelector('h2'))
    const subHeadline = await waitForElement(() =>
      container.querySelector('h3'),
    )

    expect(headline).toBeTruthy()
    expect(subHeadline).toBeTruthy()
    expect(headline.textContent).toEqual('forms.photo.headlines.camera')
    expect(subHeadline.textContent).toEqual('forms.photo.headlines.settings')
    expect(mockTranslation).toHaveBeenCalledWith('forms.photo.headlines.camera')
    expect(mockTranslation).toHaveBeenCalledWith(
      'forms.photo.headlines.settings',
    )
  })
  it('displays loading components while loading', async () => {
    const { getAllByTestId } = renderContainer({ cameraData: true })

    const loading = await waitForElement(() => getAllByTestId('loading'))

    expect(loading).toBeTruthy()
    expect(loading.length).toEqual(2)
  })
  describe('shows current values of', () => {
    it('camera fields', async () => {
      const { getByTestId } = renderContainer()

      const textFieldMake = await waitForElement(() =>
        getByTestId('textFieldCamera--make'),
      )
      const textFieldModel = await waitForElement(() =>
        getByTestId('textFieldCamera--model'),
      )
      const textFieldLensMake = await waitForElement(() =>
        getByTestId('textFieldCamera--lensMake'),
      )
      const textFieldLensModel = await waitForElement(() =>
        getByTestId('textFieldCamera--lensModel'),
      )

      expect(
        textFieldMake.querySelector('input').getAttribute('value'),
      ).toEqual('make')
      expect(
        textFieldModel.querySelector('input').getAttribute('value'),
      ).toEqual('model')
      expect(
        textFieldLensMake.querySelector('input').getAttribute('value'),
      ).toEqual('lensMake')
      expect(
        textFieldLensModel.querySelector('input').getAttribute('value'),
      ).toEqual('lensModel')
    })
    it('settings fields', async () => {
      const { getByTestId } = renderContainer()

      const textFieldFocalLength = await waitForElement(() =>
        getByTestId('textFieldCamera--focalLength'),
      )
      const textFieldFNumber = await waitForElement(() =>
        getByTestId('textFieldCamera--fNumber'),
      )
      const textFieldWhiteBalance = await waitForElement(() =>
        getByTestId('textFieldCamera--whiteBalance'),
      )
      const textFieldExposureTime = await waitForElement(() =>
        getByTestId('textFieldCamera--exposureTime'),
      )
      const textFieldExposureMode = await waitForElement(() =>
        getByTestId('textFieldCamera--exposureMode'),
      )
      const textFieldIso = await waitForElement(() =>
        getByTestId('textFieldCamera--iso'),
      )
      const textFieldFlash = await waitForElement(() =>
        getByTestId('textFieldCamera--flash'),
      )
      const textFieldSoftware = await waitForElement(() =>
        getByTestId('textFieldCamera--software'),
      )
      const textFieldDimensionX = await waitForElement(() =>
        getByTestId('textFieldCamera--dimensionX'),
      )
      const textFieldDimensionY = await waitForElement(() =>
        getByTestId('textFieldCamera--dimensionY'),
      )

      expect(
        textFieldFocalLength.querySelector('input').getAttribute('value'),
      ).toEqual('focalLength')
      expect(
        textFieldFNumber.querySelector('input').getAttribute('value'),
      ).toEqual('fNumber')
      expect(
        textFieldWhiteBalance.querySelector('input').getAttribute('value'),
      ).toEqual('whiteBalance')
      expect(
        textFieldExposureTime.querySelector('input').getAttribute('value'),
      ).toEqual('exposureTime')
      expect(
        textFieldExposureMode.querySelector('input').getAttribute('value'),
      ).toEqual('exposureMode')
      expect(textFieldIso.querySelector('input').getAttribute('value')).toEqual(
        'iso',
      )
      expect(
        textFieldFlash.querySelector('input').getAttribute('value'),
      ).toEqual('flash')
      expect(
        textFieldSoftware.querySelector('input').getAttribute('value'),
      ).toEqual('software')
      expect(
        textFieldDimensionX.querySelector('input').getAttribute('value'),
      ).toEqual('200')
      expect(
        textFieldDimensionY.querySelector('input').getAttribute('value'),
      ).toEqual('100')
    })
  })
  describe('calls function to handle change of', () => {
    it('make', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--make'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new make' },
      })

      expect(mockSetMake).toHaveBeenLastCalledWith('new make')
    })
    it('model', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--model'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new model' },
      })

      expect(mockSetModel).toHaveBeenLastCalledWith('new model')
    })
    it('lensMake', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--lensMake'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new lensMake' },
      })

      expect(mockSetLensMake).toHaveBeenLastCalledWith('new lensMake')
    })
    it('lensModel', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--lensModel'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new lensModel' },
      })

      expect(mockSetLensModel).toHaveBeenLastCalledWith('new lensModel')
    })
    it('focalLength', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--focalLength'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new focalLength' },
      })

      expect(mockSetFocalLength).toHaveBeenLastCalledWith('new focalLength')
    })
    it('fNumber', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--fNumber'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new fNumber' },
      })

      expect(mockSetFNumber).toHaveBeenLastCalledWith('new fNumber')
    })
    it('whiteBalance', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--whiteBalance'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new whiteBalance' },
      })

      expect(mockSetWhiteBalance).toHaveBeenLastCalledWith('new whiteBalance')
    })
    it('exposureTime', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--exposureTime'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new exposureTime' },
      })

      expect(mockSetExposureTime).toHaveBeenLastCalledWith('new exposureTime')
    })
    it('exposureMode', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--exposureMode'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new exposureMode' },
      })

      expect(mockSetExposureMode).toHaveBeenLastCalledWith('new exposureMode')
    })
    it('iso', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--iso'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new iso' },
      })

      expect(mockSetIso).toHaveBeenLastCalledWith('new iso')
    })
    it('flash', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--flash'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new flash' },
      })

      expect(mockSetFlash).toHaveBeenLastCalledWith('new flash')
    })
    it('software', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--software'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new software' },
      })

      expect(mockSetSoftware).toHaveBeenLastCalledWith('new software')
    })
    it('dimensionX', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--dimensionX'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new dimensionX' },
      })

      expect(mockSetDimensionX).toHaveBeenLastCalledWith('new dimensionX')
    })
    it('dimensionY', async () => {
      const { getByTestId } = renderContainer()

      const textField = await waitForElement(() =>
        getByTestId('textFieldCamera--dimensionY'),
      )

      fireEvent.change(textField.querySelector('input'), {
        target: { value: 'new dimensionY' },
      })

      expect(mockSetDimensionY).toHaveBeenLastCalledWith('new dimensionY')
    })
  })
})
