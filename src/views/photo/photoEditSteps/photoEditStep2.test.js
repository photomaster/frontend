import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import Step2 from './photoEditStep2'
import i18nextConfig from 'i18nTesting'
import AnimatedMarker from 'components/map/animatedMarker'

const Coordinates = [47.811195, 13.033229]

const mockTranslation = jest.fn(str => str)
const mockHandleClick = jest.fn()
const mockHandleZoom = jest.fn()

jest.mock('react-i18next', () => ({
  ...jest.requireActual('react-i18next'),
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        language: 'en',
        changeLanguage: () => new Promise(() => {}),
        on: jest.fn((_, fn) => fn()),
      },
    }
  },
}))

function renderContainer(coordinates = Coordinates) {
  const result = render(
    <Router>
      <Step2
        data={{
          coordinates: coordinates,
          zoom: 12,
          marker: <AnimatedMarker position={Coordinates} />,
          onClick: mockHandleClick,
          onZoomend: mockHandleZoom,
        }}
      />
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Photo Edit Step 2 View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('shows a translated headline', async () => {
    const { container } = renderContainer()

    const headline = await waitForElement(() => container.querySelector('h2'))

    expect(headline).toBeTruthy()
    expect(headline.textContent).toEqual('forms.photo.headlines.location')
    expect(mockTranslation).toHaveBeenCalledWith(
      'forms.photo.headlines.location',
    )
  })
  it('can handle no coordinates given', async () => {
    const { container } = renderContainer(null)

    expect(container).toBeTruthy()
  })
  xit('calls function on click on map', async () => {
    const { container } = renderContainer()

    // TODO how to interact with map?

    expect(container).toBeTruthy()
  })
})
