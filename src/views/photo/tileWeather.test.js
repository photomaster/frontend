import React from 'react'
import { render, cleanup, waitForElement } from '@testing-library/react'
import TileWeather from './tileWeather'

const mockTranslation = jest.fn(str => str)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer(weather = 'sunny', temp = 10) {
  const result = render(
    <TileWeather headline={'a headline'} weather={weather} temp={temp} />,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('TileWeather', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('renders headline', async () => {
    const { getByTestId } = renderContainer()
    const headline = await waitForElement(() =>
      getByTestId('tileWeather--headline'),
    )

    expect(headline.textContent).toEqual('a headline')
  })
  it('displays weather', async () => {
    const { getByTestId } = renderContainer()
    const content = await waitForElement(() =>
      getByTestId('tileWeather--content'),
    )

    expect(content.textContent).toContain('sunny')
  })
  it('displays temperature', async () => {
    const { getByTestId } = renderContainer()
    const content = await waitForElement(() =>
      getByTestId('tileWeather--content'),
    )

    expect(content.textContent).toContain('10 °')
  })
  it('can handle no available data (null)', async () => {
    const { container } = renderContainer(null, null)

    expect(container).toBeTruthy()
  })
  it('translates the text', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalledWith('photo.tiles.weather.weather')
    expect(mockTranslation).toHaveBeenCalledWith(
      'photo.tiles.weather.temperature',
    )
  })
})
