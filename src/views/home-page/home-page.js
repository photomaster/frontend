import React, { useState, useEffect, lazy, Suspense } from 'react'
import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import LeafletMap from 'components/map/map'
import Explanation from 'components/explanation/explanation'
import { useSelector, useDispatch } from 'react-redux'
import Loading from 'components/utilities/loading'
import { photosActions, userActions } from '../../actions'

import useInfiniteScroll from '../../helpers/scrollInfinite'

const Recommendations = lazy(() =>
  import('components/recommendations/recommendations.js'),
)

export default function HomePage(props) {
  const dispatch = useDispatch()
  const userLocation = useSelector(state => state.currentUser.location)
  const nearPhotos = useSelector(state => state.photos.near)
  const nearPhotosLoading = useSelector(state => state.photos.loading)
  const limit = useState(9)
  const [offset, setOffset] = useState(limit)
  const [counter, setCounter] = useState(limit)
  const [fetchedAll, setFetchedAll] = useState(false)
  // eslint-disable-next-line no-unused-vars
  const [isFetching, setIsFetching] = useInfiniteScroll(
    loadMoreData,
    fetchedAll,
  )

  useEffect(() => {
    // check if token was invalid and user should be logged out
    if (props.location.state && props.location.state.logout === true) {
      dispatch(userActions.logout())
    }

    let mounted = true
    if (mounted) {
      if (!userLocation) {
        dispatch(userActions.getUserLocation())
      } else if (!nearPhotos) {
        dispatch(photosActions.getAllNearPhotos(userLocation.coordinates))
      }
    }
    return function cleanup() {
      mounted = false
    }
  }, [])

  useEffect(() => {
    if (userLocation) {
      dispatch(photosActions.getAllNearPhotos(userLocation.coordinates))
    }
  }, [userLocation])

  useEffect(() => {
    if (!nearPhotosLoading) setIsFetching(false)
    if (nearPhotos && nearPhotos.length > 0) {
      if (counter === nearPhotos.length) setFetchedAll(true)
      else setCounter(nearPhotos.length)
    }
  }, [nearPhotosLoading])

  function loadMoreData() {
    if (userLocation) {
      dispatch(
        photosActions.getNearPhotosOffset(
          userLocation.coordinates,
          limit,
          offset,
        ),
      )
      setOffset(offset + limit)
    }
  }

  return (
    <div className="App">
      <LeafletMap />
      <Container component="main" maxWidth="md" className="App">
        <CssBaseline />
        <Explanation />
        {nearPhotos.length > 0 && (
          <Suspense fallback={<Loading />}>
            <Recommendations photos={nearPhotos} />
          </Suspense>
        )}
      </Container>
    </div>
  )
}
