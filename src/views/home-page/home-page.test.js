import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import HomePage from './home-page'
import i18nextConfig from '../../i18nTesting'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import { userActions, photosActions } from 'actions'

const User = {
  id: 100,
  displayName: 'someone else',
  avatarImage: 'url',
}

const ManyNearPhotos = [
  {
    id: 1,
    url: '/photo1',
    user: User,
    name: 'name1',
    description: 'desc1',
    imageThumbnailTeaser: '/url1',
  },
  {
    id: 2,
    url: '/photo2',
    user: User,
    name: 'name2',
    description: 'desc2',
    imageThumbnailTeaser: '/url2',
  },
]

const NoNearPhotos = []

const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

function renderContainer(
  nearPhotos = ManyNearPhotos,
  loadingNearPhotos = false,
  userLocationAvailable = true,
  logout = false,
) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: {
            location: userLocationAvailable
              ? {
                  coordinates: [47.811, 13.033],
                }
              : null,
          },
          photos: {
            loading: loadingNearPhotos,
            near: nearPhotos,
          },
        })}
      >
        <HomePage location={{ state: { logout: logout } }} />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('HomePage View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('dispatches logout action', async () => {
    const mockLogout = jest.fn().mockImplementation(() => {
      return { type: 'logging out' }
    })
    userActions.logout = mockLogout

    renderContainer(ManyNearPhotos, false, true, true)

    expect(mockLogout).toHaveBeenCalledTimes(1)
  })
  it('requests user location if not available', async () => {
    const mockGetUserLocation = jest.fn().mockImplementation(() => {
      return { type: 'getting user location' }
    })
    userActions.getUserLocation = mockGetUserLocation

    renderContainer(ManyNearPhotos, false, false)

    expect(mockGetUserLocation).toHaveBeenCalledTimes(1)
  })
  it('requests near photos if not available', async () => {
    const mockGetAllNearPhotos = jest.fn().mockImplementation(() => {
      return { type: 'getting near photos' }
    })
    photosActions.getAllNearPhotos = mockGetAllNearPhotos

    renderContainer(NoNearPhotos)

    expect(mockGetAllNearPhotos).toHaveBeenCalledTimes(1)
  })
  it('displays recommendations if photos are available', async () => {
    const { findByTestId } = renderContainer()
    const recommendations = await waitForElement(() =>
      findByTestId('recommendationsGrid'),
    )
    const photo1 = await waitForElement(() =>
      findByTestId('recommendationCard--CardActions-1'),
    )
    const photo2 = await waitForElement(() =>
      findByTestId('recommendationCard--CardActions-2'),
    )

    expect(recommendations).toBeTruthy()
    expect(photo1).toBeTruthy()
    expect(photo2).toBeTruthy()
  })
  xit('loads more data', async () => {
    const mockGetNearPhotosOffset = jest.fn().mockImplementation(() => {
      return { type: 'getting near photos' }
    })
    photosActions.getNearPhotosOffset = mockGetNearPhotosOffset

    renderContainer()

    expect(mockGetNearPhotosOffset).toHaveBeenCalledTimes(1)
    expect(mockGetNearPhotosOffset).toHaveBeenLastCalledWith(
      [47.811, 13.033],
      9,
      9,
    )
  })
})
