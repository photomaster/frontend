import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  queryByText,
  waitForElement,
  fireEvent,
  wait,
} from '@testing-library/react'
import '@testing-library/jest-dom'
import Upload from './upload'
import exifHandler from './exifHandler'
import photoService from 'service/photoService'
import Photo from 'models/photo'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import { history } from 'helpers/history'
import validators from 'helpers/validators'
import autocompleteService from 'service/autocompleteService'

const mockTranslation = jest.fn(str => str)
const mockValidatorTrue = jest.fn(_ => true)
const mockValidatorFalse = jest.fn(_ => false)
const mockConsole = jest.fn(str => str)

autocompleteService.getSuggestions = jest.fn(() =>
  Promise.resolve(['sug1', 'sug2', 'sug3']),
)

jest.mock('./exifHandler')
jest.mock('service/photoService')
jest.mock('helpers/history')
jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        language: 'en',
        changeLanguage: () => new Promise(() => {}),
        on: jest.fn((_, fn) => fn()),
      },
    }
  },
}))

console.error = mockConsole

/* material-UI datepicker prints every change in the log --> mock console to avoid messages in tests */
console.warn = jest.fn()

function renderContainer(loggedIn = true) {
  const result = render(
    <Provider
      store={createStore(rootReducer, {
        currentUser: {
          loggedIn: loggedIn,
        },
      })}
    >
      <Router>
        <Upload override={true} />
      </Router>
    </Provider>,
  )
  return result
}

async function renderContainerAndSelectTestImage() {
  const result = renderContainer()
  const file = new File(['(⌐□_□)'], 'chucknorris.png', { type: 'image/png' })

  photoService.save.mockReturnValue(
    Promise.resolve(
      new Photo({
        id: 1,
        image: '/image1',
        labels: [
          { title: 'label1', score: 1 },
          { title: 'label2', score: 1 },
          { title: 'label3', score: 1 },
          { title: 'label4', score: 0.1 },
        ],
      }),
    ),
  )

  fireEvent.change(result.getByTestId('upload--dropzone'), {
    target: {
      files: [file],
    },
  })

  await waitForElement(() => result.getByTestId('upload--preview'))

  return result
}

async function renderContainerStep2() {
  const result = await renderContainerAndSelectTestImage()
  const nextButton = await waitForElement(() =>
    result.getByTestId('upload--button--next'),
  )

  fireEvent.click(nextButton)

  await waitForElement(() => result.getByTestId('upload--map'))

  return result
}

async function renderContainerSelectTestImageFillMetadata(
  geo = [],
  title = '',
  description = '',
) {
  const result = await renderContainerStep2()

  fireEvent.change(result.getByTestId('upload--title').querySelector('input'), {
    target: {
      value: title,
    },
  })
  fireEvent.change(
    result.getByTestId('upload--desc').querySelector('textarea'),
    {
      target: {
        value: description,
      },
    },
  )

  return result
}

async function renderContainerStep3() {
  const result = await renderContainerSelectTestImageFillMetadata(
    [13, 14],
    'a title',
    'a description',
  )
  const nextButton = await waitForElement(() =>
    result.getByTestId('upload--button--next'),
  )

  fireEvent.click(nextButton)

  await waitForElement(() => result.getByTestId('autocomplete'))

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Upload View', () => {
  it('renders without crashing', () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('contains a headline', () => {
    const { container } = renderContainer()
    const headline = container.querySelector('h1')

    expect(headline).toBeTruthy()
    expect(headline.textContent).toEqual('forms.upload.header')
  })
  it('translates the text', () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('forms.upload.header')
    expect(mockTranslation).toHaveBeenCalledWith('forms.upload.introduction')
    expect(mockTranslation).toHaveBeenCalledWith('forms.upload.steps.details')
  })
  describe('Step 1 (Select photo)', () => {
    it('contains an upload area', () => {
      const { getByTestId } = renderContainer()
      const area = getByTestId('upload--dropzone')

      expect(area).toBeTruthy()
    })
    it('displays image preview', async () => {
      const { getByTestId } = await renderContainerAndSelectTestImage()
      const preview = await waitForElement(() => getByTestId('upload--preview'))

      expect(preview).toBeTruthy()
    })
    it('disables next-button without image', () => {
      const { getByTestId } = renderContainer()
      const nextButton = getByTestId('upload--button--next')

      expect(nextButton).toBeDisabled()
    })
    it('disables back button', () => {
      const { getByTestId } = renderContainer()
      const backButton = getByTestId('upload--button--back')

      expect(backButton).toBeDisabled()
    })
  })
  describe('Step 2 (Provide details)', () => {
    it('shows a map to select the location', async () => {
      exifHandler.getCoordinates.mockImplementationOnce(() =>
        Promise.reject(new Error()),
      )

      const { container } = await renderContainerStep2()

      const map = await waitForElement(() =>
        container.querySelector('.leaflet-container'),
      )

      expect(map).toBeTruthy()
    })
    it('shows an error message if no gps data could be extracted from the photo', async () => {
      exifHandler.getCoordinates.mockImplementationOnce(() =>
        Promise.reject(new Error()),
      )

      const { getByTestId } = await renderContainerStep2()
      const alert = getByTestId('alert')

      expect(alert).toBeTruthy()
      expect(alert.getAttribute('class')).toContain('MuiAlert-standardError')
    })
    it('shows a success message if the gps data could be extraced from the photo', async () => {
      exifHandler.getCoordinates.mockReturnValue(Promise.resolve([47, 13]))

      const { getByTestId } = await renderContainerStep2()
      const alert = getByTestId('alert')

      expect(alert).toBeTruthy()
      expect(alert.getAttribute('class')).toContain('MuiAlert-standardSuccess')
    })
    it('shows a marker if the gps data could be extraced from the photo', async () => {
      exifHandler.getCoordinates.mockReturnValue(Promise.resolve([47, 13]))

      const { container, getByTestId } = await renderContainerStep2()
      const marker = container.querySelector('.leaflet-marker-icon')

      expect(marker).toBeTruthy()
      expect(marker.nodeName).toEqual('IMG')
    })
    describe('sets new value for', () => {
      it('title', async () => {
        validators.titleIsValid = mockValidatorTrue

        const { findByTestId } = await renderContainerStep2()
        const title = await waitForElement(() => findByTestId('upload--title'))

        fireEvent.change(title.querySelector('input'), {
          target: { value: 'new_title' },
        })

        expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
        expect(title.querySelector('input').getAttribute('value')).toEqual(
          'new_title',
        )
      })
      it('dateTime', async () => {
        exifHandler.getDateTime.mockImplementationOnce(() =>
          Promise.resolve(new Date('2020-11-25T05:09:04Z')),
        )

        const { findByTestId, queryByText } = await renderContainerStep2()
        const textfield = await waitForElement(() =>
          findByTestId('upload--dateTime'),
        )

        fireEvent.click(textfield.querySelector('input'))
        fireEvent.click(queryByText('7'))
        fireEvent.click(queryByText('OK'))

        expect(textfield.querySelector('input').getAttribute('value')).toEqual(
          'November 7 2020 06:09',
        )
      })
      it('description', async () => {
        validators.descIsValid = mockValidatorTrue

        const { findByTestId } = await renderContainerStep2()
        const desc = await waitForElement(() => findByTestId('upload--desc'))

        fireEvent.change(desc.querySelector('textarea'), {
          target: { value: 'new_desc' },
        })

        expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
        expect(desc.querySelectorAll('textarea')[0].textContent).toEqual(
          'new_desc',
        )
      })
    })
    describe('shows error message for', () => {
      it('title', async () => {
        validators.titleIsValid = mockValidatorFalse

        const { findByTestId } = await renderContainerStep2()
        const title = await waitForElement(() => findByTestId('upload--title'))

        fireEvent.change(title.querySelector('input'), {
          target: { value: 'a' },
        })

        expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
        expect(title.querySelector('.Mui-error')).toBeTruthy()
      })
      it('description', async () => {
        validators.descIsValid = mockValidatorFalse

        const { findByTestId } = await renderContainerStep2()
        const desc = await waitForElement(() => findByTestId('upload--desc'))

        fireEvent.change(desc.querySelector('textarea'), {
          target: { value: 'x' },
        })

        expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
        expect(desc.querySelector('.Mui-error')).toBeTruthy()
      })
    })
    it('disables next-button without location, title or description', async () => {
      const { getByTestId } = await renderContainerStep2()
      const nextButton = getByTestId('upload--button--next')

      expect(nextButton).toBeDisabled()
    })
    it('enables back button', async () => {
      const { getByTestId } = await renderContainerStep2()
      const backButton = getByTestId('upload--button--back')

      expect(backButton).toBeEnabled()
    })
  })
  describe('Step 3 (Add tags)', () => {
    it('shows a list of predicted labels', async () => {
      const { container } = await renderContainerStep3()

      await waitForElement(() => queryByText(container, 'label1'))
      await waitForElement(() => queryByText(container, 'label2'))
      await waitForElement(() => queryByText(container, 'label3'))
    })
    it('does not display labels with low confidence', async () => {
      const { container } = await renderContainerStep3()

      expect(queryByText(container, 'label4')).toBeNull()
    })
    it('adds user labels to the list', async () => {
      const {
        getByTestId,
        getAllByTestId,
        getByText,
      } = await renderContainerStep3()
      const autocomplete = await waitForElement(() =>
        getByTestId('autocomplete'),
      )
      const input = await waitForElement(() =>
        getByTestId('autocomplete').querySelector('input'),
      )
      const button = await waitForElement(() =>
        getByTestId('upload--tags--container').querySelector('button'),
      )

      fireEvent.change(input, { target: { value: 'a new tag!' } })
      fireEvent.blur(autocomplete)
      fireEvent.click(button)

      const newTag = await waitForElement(() => getByText('a new tag!'))

      expect(newTag).toBeTruthy()
      expect(getAllByTestId('chip').length).toEqual(4)
    })
    it('does not allow to add duplicates', async () => {
      const { getByTestId, getAllByTestId } = await renderContainerStep3()
      const autocomplete = await waitForElement(() =>
        getByTestId('autocomplete'),
      )
      const input = await waitForElement(() =>
        getByTestId('autocomplete').querySelector('input'),
      )
      const button = await waitForElement(() =>
        getByTestId('upload--tags--container').querySelector('button'),
      )

      fireEvent.change(input, { target: { value: 'label2' } })
      fireEvent.blur(autocomplete)
      fireEvent.click(button)

      const errorHelperText = await waitForElement(() =>
        autocomplete.querySelector('p'),
      )

      expect(errorHelperText.getAttribute('class')).toContain('Mui-error')
      expect(errorHelperText.textContent).toEqual('forms.helperTexts.tagExists')
      expect(getAllByTestId('chip').length).toEqual(3)
    })
    it('removes labels from the list on click on x', async () => {
      const { getAllByTestId, queryByText } = await renderContainerStep3()
      const chips = await waitForElement(() => getAllByTestId('chip'))
      const x = await waitForElement(() => chips[0].querySelector('svg'))

      expect(queryByText('label1')).toBeTruthy()

      fireEvent.click(x)

      expect(queryByText('label1')).toBeFalsy()
    })
    it('sucessfully uploads a new photo to the api endpoint', async () => {
      exifHandler.getCoordinates.mockReturnValue(Promise.resolve([47, 13]))
      const mockPush = jest.fn()
      history.push = mockPush

      const { getByTestId } = await renderContainerStep3()

      fireEvent.click(getByTestId('upload--button--submit'))

      await wait(() => expect(mockPush).toHaveBeenCalledTimes(1))

      const arg =
        photoService.save.mock.calls[photoService.save.mock.calls.length - 1][0]
      expect(arg).toBeInstanceOf(Object)
      expect(arg.lat).toEqual(47)
      expect(arg.long).toEqual(13)
      expect(arg.name).toEqual('a title')
      expect(arg.description).toEqual('a description')
    })
    it('enables back button', async () => {
      const { getByTestId } = await renderContainerStep3()
      const backButton = getByTestId('upload--button--back')

      expect(backButton).toBeEnabled()
    })
  })
})
