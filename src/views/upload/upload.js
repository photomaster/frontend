import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import CloudUploadIcon from '@material-ui/icons/CloudUpload'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import TextField from '@material-ui/core/TextField'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import Paper from '@material-ui/core/Paper'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Box from '@material-ui/core/Box'
import { Map, TileLayer } from 'react-leaflet'
import './upload.scss'
import photoService from 'service/photoService'
import locationService from 'service/locationService'
import Alert from 'components/utilities/alert'
import CircularProgress from '@material-ui/core/CircularProgress'
import 'date-fns'
import DateFnsUtils from '@date-io/date-fns'
import { MuiPickersUtilsProvider, DateTimePicker } from '@material-ui/pickers'
import Loading from 'components/utilities/loading'
import Photo from 'models/photo'
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core'
import Lightbox from 'components/lightbox/lightbox'
import validators from 'helpers/validators'
import baseStyles from 'styles/baseClasses'
import { DropzoneArea } from 'material-ui-dropzone'
import { useTranslation } from 'react-i18next'
import AnimatedMarker from 'components/map/animatedMarker'
import TagsAutocomplete from 'components/formElements/tagsAutocomplete'
import Search from '../../components/map/search'
import exifHandler from './exifHandler'
import { history } from '../../helpers/history'
import { applicationActions } from '../../actions'

const useStyles = makeStyles(theme => ({
  map: {
    height: '40vh',
    width: '40vh',
  },
  formElement: {
    padding: '1rem 0',
  },
  fullWidth_larger_xs: {
    [theme.breakpoints.up('sm')]: {
      width: '100%',
    },
  },
  paper: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    padding: theme.spacing(2),
    margin: theme.spacing(2),
  },
  center_input: {
    display: 'flex',
    justifyContent: 'flex-end',
    [theme.breakpoints.only('xs')]: {
      justifyContent: 'center',
    },
  },
  center_button: {
    [theme.breakpoints.only('xs')]: {
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex',
    },
  },
  button_container: {
    [theme.breakpoints.only('xs')]: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
  },
  button_container_inner: {
    [theme.breakpoints.only('xs')]: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
    },
  },
  buttonProgress: {
    color: theme.palette.primary[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  card_square: {
    height: '20rem',
    width: '20rem',
    maxWidth: '70vw',
    '&:hover': {
      cursor: 'zoom-in',
    },
  },
  card_landscape: {
    height: '10rem',
    width: '20rem',
    maxWidth: '70vw',
    '&:hover': {
      cursor: 'zoom-in',
    },
  },
  card_portrait: {
    height: '20rem',
    width: '10rem',
    maxWidth: '70vw',
    '&:hover': {
      cursor: 'zoom-in',
    },
  },
  media: {
    height: '100%',
  },
  media_vertical: {
    width: '100%',
  },
  dropzone: {
    minHeight: 0,
    width: 'auto',
    padding: theme.spacing(2),
  },
  dropzoneDisabled: {
    cursor: 'auto',
  },
  dropzoneParagraph: {
    '& + .MuiDropzoneArea-icon': {
      color: theme.palette.primary.main,
    },
  },
}))

export default function Upload() {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const loggedIn = useSelector(state => state.currentUser.loggedIn)
  const [photo, setPhoto] = useState(null)
  const [file, setFile] = useState('')
  const [imagePreviewUrl, setImagePreviewUrl] = useState('')
  const [orientation, setOrientation] = useState(1)
  const [landscape, setLandscape] = useState(true)
  const [dimensions, setDimensions] = useState(null)
  const [aspectRatio, setAspectRatio] = useState('square')
  const [coordinates, setCoordinates] = useState([])
  const [location, setLocation] = useState('')
  const [dateTime, setDateTime] = useState(null)
  const [title, setTitle] = useState('')
  const [desc, setDesc] = useState('')
  const [tags, setTags] = useState([])
  const [tagsFromGoogle, setTagsFromGoogle] = useState(false)
  const [tag, setTag] = useState('')
  const [showMap, setShowMap] = useState(false)
  const [marker, setMarker] = useState(null)
  const [zoom, setZoom] = useState(12)
  const [activeStep, setActiveStep] = useState(0)
  const [loading, setLoading] = useState({
    active: false,
    visibility: null,
  })
  const [success, setSuccess] = useState(false)
  const imageRef = React.createRef()
  const [errors, setErrors] = useState({
    title: false,
    desc: false,
    image: null,
  })

  useEffect(() => {
    _updateMarker()
  }, [coordinates])

  useEffect(() => {
    orientation === 5 ||
    orientation === 6 ||
    orientation === 7 ||
    orientation === 8
      ? setLandscape(false)
      : setLandscape(true)
  }, [orientation])

  useEffect(() => {
    if (dimensions && dimensions.width && dimensions.height) {
      if (dimensions.width == dimensions.height) setAspectRatio('square')
      else if (dimensions.width > dimensions.height) setAspectRatio('landscape')
      else setAspectRatio('portrait')
    }
  }, [dimensions])

  useEffect(() => {
    if (success) history.push(`/photos/${photo.id}`)
  }, [success])

  async function _handleSubmit(e, visibility) {
    e.preventDefault()
    setLoading({ active: true, visibility: visibility })

    if (!(photo instanceof Photo)) {
      throw new Error('photo is not a valid photo!')
    }

    if (coordinates && coordinates.length === 2) {
      try {
        const photoToUpload = photo
        photoToUpload.name = title
        photoToUpload.description = desc
        photoToUpload.originalCreationDate = dateTime.value
          ? dateTime.value.toISOString()
          : null
        photoToUpload.lat = coordinates[0]
        photoToUpload.long = coordinates[1]
        photoToUpload.visibility = visibility
        photoToUpload.labels = tags.map(curTag => {
          return {
            title: curTag.label,
          }
        })

        const photoReturned = await photoService.save(photoToUpload)
        setPhoto(photoReturned)
        setFile('')
        setImagePreviewUrl('')
        setLoading({ active: false, visibility: null })
        setSuccess(true)
      } catch (err) {
        console.error('upload error', err)
      }
    } else {
      console.error('Please provide the location the image was taken at.')
    }
  }

  async function _handleImageChange(e) {
    if (e == null || loading.active) return

    const reader = new FileReader()
    const curFile = e

    let curCoordinates = []
    const curLocation = ''
    let curDateTime = null
    let curTags = []
    let newPhoto = null
    let curOrientation = 1
    setLoading({ active: true })
    setErrors({ ...errors, image: null })

    try {
      curCoordinates = await exifHandler.getCoordinates(curFile)
    } catch (err) {}

    try {
      curDateTime = await exifHandler.getDateTime(curFile)
    } catch (err) {}

    try {
      curOrientation = await exifHandler.getImageOrientation(curFile)
    } catch (err) {}

    try {
      newPhoto = await photoService.save({
        image: curFile,
      })
      curTags = newPhoto.labels
        .filter(curTag => {
          if (curTag.score > 0.7) return curTag
          return false
        })
        .map((curTag, index) => ({ key: index, label: curTag.title }))
    } catch (err) {
      if ('detail' in err.json()) {
        setErrors({ ...errors, image: err.json().detail })
      } else {
        // todo: show generic error message??
      }
      setLoading({ active: false })
    }

    try {
      // to get dimensions of image (PixelXDimension is unreliable if image got edited)
      const img = new Image()
      img.src = window.URL.createObjectURL(curFile)

      img.onload = () => {
        setDimensions({ width: img.width, height: img.height })
      }
    } catch (err) {
      console.error('dimensions error', err)
    }

    reader.onloadend = async () => {
      setPhoto(newPhoto)
      setShowMap(true)
      setCoordinates(curCoordinates)
      setLocation(curLocation)
      setDateTime({ value: curDateTime, fromImage: !!curDateTime })
      setFile(curFile)
      setImagePreviewUrl(reader.result)
      setOrientation(curOrientation)
      setTags(curTags)
      setTagsFromGoogle(curTags.length > 0)
      setLoading({ active: false })
    }

    reader.readAsDataURL(curFile)
  }

  function _updateMarker() {
    let updatedMarker = null

    if (coordinates && coordinates.length === 2) {
      updatedMarker = <AnimatedMarker position={coordinates} />
    }
    setMarker(updatedMarker)
  }

  async function _updateLocation() {
    let updatedLocation = ''

    try {
      updatedLocation = await locationService.getLocationByCoordinates(
        coordinates,
      )
    } catch (err) {}

    setLocation(updatedLocation)
  }

  async function _handleLocationChange(event) {
    let curLocation = event.target.value
    let curCoordinates = []

    try {
      curLocation = await locationService.getLocationByName(curLocation)
      curCoordinates = await locationService.getCoordinatesByName(curLocation)
    } catch (err) {}

    setLocation(curLocation)
    setCoordinates(curCoordinates)
  }

  const _handleTitleChange = event => {
    const value = event.target.value
    let errorTitle = false
    if (value && !validators.titleIsValid(value)) {
      errorTitle = true
    }
    setTitle(value)
    setErrors({ ...errors, title: errorTitle })
  }

  const _handleDescChange = event => {
    const value = event.target.value
    let errorDesc = false
    if (value && !validators.descIsValid(value)) {
      errorDesc = true
    }
    setDesc(value)
    setErrors({ ...errors, desc: errorDesc })
  }

  const _handleDateTimeChange = date => {
    setDateTime({ value: date, fromImage: false })
  }

  const _handleClick = e => {
    const { lat, lng } = e.latlng
    setCoordinates([lat, lng])
    _updateLocation()
  }

  const _handleZoom = e => {
    setZoom(e.target._zoom)
  }

  function _getImagePreview() {
    if (
      (imagePreviewUrl && !loading.active) ||
      (imagePreviewUrl && activeStep != 0)
    ) {
      return (
        <div className="imgPreview" data-testid={'upload--preview'}>
          <Card
            className={`${
              aspectRatio === 'square'
                ? classes.card_square
                : aspectRatio === 'landscape'
                ? classes.card_landscape
                : aspectRatio === 'portrait'
                ? classes.card_portrait
                : ''
            }`}
            onClick={_handlePreviewOpen}
          >
            <CardMedia
              ref={imageRef}
              className={`${classes.media} ${
                landscape === false ? classes.media_vertical : ''
              }`}
              image={imagePreviewUrl}
              title={t('forms.upload.image.title')}
            />
          </Card>
          <Lightbox
            id={photo ? photo.id : 0}
            url={imagePreviewUrl}
            ariaLabelled={t('forms.upload.lightbox.aria.label')}
            ariaDescribed={t('forms.upload.lightbox.aria.described')}
            alt={t('forms.upload.lightbox.alt')}
          />
        </div>
      )
    }
    if (loading.active && activeStep === 0) {
      return (
        <div className="imgPreview">
          <Loading />
        </div>
      )
    }
    return (
      <div className="imgPreview">
        <Alert severity="info">{t('forms.upload.alerts.selectPhoto')}</Alert>
      </div>
    )
  }

  const _handlePreviewOpen = () => {
    dispatch(applicationActions.openLightbox())
  }

  function _getSteps() {
    return [
      t('forms.upload.steps.photo'),
      t('forms.upload.steps.details'),
      t('forms.upload.steps.tags'),
    ]
  }

  function _getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <div
            className={`${baseClasses.margin2} ${baseClasses.fullWidth} ${baseClasses.alignItemsCenter} ${baseClasses.flexDirectionColumn}`}
          >
            <DropzoneArea
              acceptedFiles={['image/*']}
              filesLimit={1}
              maxFileSize={5000000}
              dropzoneText={t('forms.upload.dropzone.text')}
              showPreviewsInDropzone={false}
              showAlerts={['error', 'info']}
              maxWidth={false}
              inputProps={{ 'data-testid': 'upload--dropzone' }}
              dropzoneClass={`${classes.dropzone} ${
                loading.active
                  ? `${classes.dropzoneDisabled} MuiDropzoneArea-active`
                  : ''
              }`}
              dropzoneParagraphClass={classes.dropzoneParagraph}
              onChange={files => _handleImageChange(files[0])}
            />
            {errors && errors.image && (
              <div className="imgPreview">
                <Alert severity="error">{errors.image}</Alert>
              </div>
            )}
            {_getImagePreview()}
          </div>
        )
      case 1:
        return (
          <div
            className={`${baseClasses.margin2} ${baseClasses.fullWidth} ${baseClasses.alignItemsCenter} ${baseClasses.flexDirectionColumn}`}
            style={{ display: 'block' }}
          >
            {_getImagePreview()}
            {showMap && (
              <div
                className={baseClasses.fullWidth}
                data-testid={'upload--map'}
              >
                {coordinates && coordinates.length > 0 ? (
                  <Alert severity="success">
                    {t('forms.upload.alerts.locationSuccess')}
                  </Alert>
                ) : (
                  <Alert severity="error">
                    {t('forms.upload.alerts.locationError')}
                  </Alert>
                )}
                <Map
                  className="map--upload"
                  center={
                    coordinates && coordinates.length > 0
                      ? coordinates
                      : [47.811195, 13.033229]
                  }
                  zoom={zoom}
                  animate
                  onClick={_handleClick}
                  onZoomend={_handleZoom}
                >
                  <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
                  <Search variant="bar" />
                  {marker}
                </Map>
                {false && (
                  <div
                    className={`${baseClasses.alignItemsCenter} ${baseClasses.flexDirectionColumn}`}
                  >
                    <FormControl size="medium" margin="normal">
                      <InputLabel htmlFor="location">Location TODO</InputLabel>
                      <Input
                        id="location"
                        value={location}
                        onChange={_handleLocationChange}
                      />
                    </FormControl>
                  </div>
                )}
              </div>
            )}
            <Box m={2}>
              <Grid container justify="space-around" spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    id="title"
                    label={t('forms.labels.title')}
                    fullWidth
                    required
                    value={title}
                    error={errors.title}
                    helperText={
                      errors.title ? validators.errorMessages.title : ''
                    }
                    onChange={_handleTitleChange}
                    data-testid={'upload--title'}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <DateTimePicker
                      disableFuture
                      showTodayButton
                      ampm={false}
                      fullWidth
                      label={t('forms.labels.dateTime')}
                      format="MMMM d yyyy HH:mm"
                      value={dateTime.value}
                      onChange={_handleDateTimeChange}
                      onError={console.warn}
                      helperText={
                        dateTime.fromImage
                          ? t('forms.helperTexts.dateTime.extracted')
                          : t('forms.helperTexts.dateTime.hint')
                      }
                      data-testid={'upload--dateTime'}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="desc"
                    label={t('forms.labels.desc')}
                    multiline
                    fullWidth
                    rows={5}
                    rowsMax={10}
                    required
                    value={desc}
                    error={errors.desc}
                    helperText={
                      errors.desc ? validators.errorMessages.desc : ''
                    }
                    onChange={_handleDescChange}
                    data-testid={'upload--desc'}
                  />
                </Grid>
              </Grid>
            </Box>
          </div>
        )
      case 2:
        return (
          <div
            className={`${baseClasses.margin2} ${baseClasses.fullWidth} ${baseClasses.alignItemsCenter} ${baseClasses.flexDirectionColumn}`}
          >
            {_getImagePreview()}
            <Typography
              variant="body1"
              gutterBottom
              className={baseClasses.margin05}
            >
              {t('forms.upload.tags.introduction')}
              {tagsFromGoogle ? t('forms.upload.tags.google') : ``}
            </Typography>
            <Grid
              container
              spacing={1}
              className={baseClasses.alignItemsCenter}
              data-testid={'upload--tags--container'}
            >
              <TagsAutocomplete
                tag={tag}
                tags={tags}
                setTag={setTag}
                setTags={setTags}
              />
            </Grid>
          </div>
        )
      default:
        return t('forms.upload.steps.error')
    }
  }

  const _handleNextStep = () => {
    setActiveStep(activeStep + 1)
  }

  const _handleBackStep = () => {
    setActiveStep(activeStep - 1)
  }

  const _handleResetStep = () => {
    setActiveStep(0)
  }

  const steps = _getSteps()

  return (
    <div className="previewComponent">
      <Container component="main" maxWidth="md" className="upload">
        <CssBaseline />
        <Typography component="h1" variant="h1">
          {t('forms.upload.header')}
        </Typography>
        <Typography variant="body1">
          {t('forms.upload.introduction')}
        </Typography>
        {!loggedIn ? (
          <React.Fragment>
            <Alert severity="info">
              <span>
                {t('forms.upload.alerts.loginNeeded.text1')}
                <NavLink to="/login" style={{ textDecoration: 'none' }}>
                  {t('forms.upload.alerts.loginNeeded.link1')}
                </NavLink>
                {t('forms.upload.alerts.loginNeeded.text2')}
                <NavLink to="/register" style={{ textDecoration: 'none' }}>
                  {t('forms.upload.alerts.loginNeeded.link2')}
                </NavLink>
                {t('forms.upload.alerts.loginNeeded.text3')}
              </span>
            </Alert>
          </React.Fragment>
        ) : (
          <Paper className={classes.paper}>
            <form
              className={`${baseClasses.fullWidth} ${baseClasses.alignItemsCenter} ${baseClasses.flexDirectionColumn}`}
              onSubmit={e => e.preventDefault()}
            >
              <Stepper
                activeStep={activeStep}
                alternativeLabel
                className={`${classes.fullWidth_larger_xs} ${baseClasses.backgroundNone}`}
              >
                {steps.map(label => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
              <div className={baseClasses.fullWidth}>
                {activeStep === steps.length ? (
                  <div>
                    <Typography
                      className={`${baseClasses.marginTop1} ${baseClasses.marginBottom1}`}
                    >
                      {t('forms.upload.steps.complete')}
                    </Typography>
                    <Button
                      onClick={_handleResetStep}
                      data-testid={'upload--button--reset'}
                    >
                      {t('forms.upload.buttons.reset')}
                    </Button>
                  </div>
                ) : (
                  <div
                    className={`${baseClasses.alignItemsCenter} ${baseClasses.flexDirectionColumn}`}
                  >
                    {_getStepContent(activeStep)}
                    <div className={classes.button_container}>
                      <Button
                        className={baseClasses.margin05}
                        disabled={activeStep === 0}
                        onClick={_handleBackStep}
                        data-testid={'upload--button--back'}
                      >
                        {t('forms.upload.buttons.back')}
                      </Button>
                      {activeStep === steps.length - 1 ? (
                        <span className={classes.button_container_inner}>
                          <span className={baseClasses.relative}>
                            <Button
                              variant="contained"
                              color="primary"
                              className={`${classes.button} ${baseClasses.margin05}`}
                              startIcon={<CloudUploadIcon />}
                              disabled={loading.active || success}
                              onClick={e => _handleSubmit(e, 3)}
                              data-testid={'upload--button--submit'}
                            >
                              {t('forms.upload.buttons.upload')}
                            </Button>
                            {loading.active && loading.visibility === 3 && (
                              <CircularProgress
                                size={24}
                                className={classes.buttonProgress}
                              />
                            )}
                          </span>
                        </span>
                      ) : (
                        <Button
                          variant="contained"
                          color="primary"
                          disabled={
                            (errors && errors.image) ||
                            file === '' ||
                            loading.active ||
                            (activeStep === 1 &&
                              (coordinates.length === 0 ||
                                title === '' ||
                                desc === ''))
                          }
                          onClick={_handleNextStep}
                          data-testid={'upload--button--next'}
                        >
                          {t('forms.upload.buttons.next')}
                        </Button>
                      )}
                    </div>
                  </div>
                )}
              </div>
            </form>
          </Paper>
        )}
      </Container>
    </div>
  )
}
