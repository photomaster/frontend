import React from 'react'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import '@testing-library/jest-dom'
import exifHandler from './exifHandler'
import ImgLandscape1 from 'assets/testing/landscape/Landscape_1.jpg'
import ImgLandscape3 from 'assets/testing/landscape/Landscape_3.jpg'
import ImgLandscape6 from 'assets/testing/landscape/Landscape_6.jpg'
import ImgLandscape8 from 'assets/testing/landscape/Landscape_8.jpg'
import ImgPortrait1 from 'assets/testing/portrait/Portrait_1.jpg'
import ImgPortrait3 from 'assets/testing/portrait/Portrait_3.jpg'
import ImgPortrait6 from 'assets/testing/portrait/Portrait_6.jpg'
import ImgPortrait8 from 'assets/testing/portrait/Portrait_8.jpg'

const EXIF = require('exif-js')
//const fs = require('fs')

// orientation images by https://github.com/recurser/exif-orientation-examples
// orientations 1/3/6/8 are most common, as the rest (2/4/5/7) are mirrored

const mockConsole = jest.fn(str => str)
console.error = mockConsole

async function handleImageChange(e) {
  const reader = new FileReader()
  const file = e
  return file
}

/*function renderContainer() {
  const result = render(
    <input
      accept="image/*"
      id="img-file"
      data-testid={'imageInput'}
      type="file"
      onChange={(e) => handleImageChange(e)}
    />
  )
  return result
}*/

function toArrayBuffer(buffer) {
  var ab = new ArrayBuffer(buffer.length)
  var view = new Uint8Array(ab)
  for (var i = 0; i < buffer.length; ++i) {
    view[i] = buffer[i]
  }
  return ab
}

function renderContainer(src) {
  const result = render(<img src={src} data-testid={'image'} />)
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
xdescribe('EXIF Handler', () => {
  describe('location data', () => {})
  describe('date time data', () => {})
  describe('orientation data', () => {})
  describe('dimensions data', () => {
    it('returns correct orientation for landscape image with orientation 1', async () => {
      //const imgFile = readFileSync('assets/testing/landscape/Landscape_1.jpg')

      const blob = new Blob([ImgLandscape1], { type: 'image/jpeg' })
      const img = new Image()
      //img.src = 'assets/testing/landscape/Landscape_1.jpg'
      img.src = URL.createObjectURL(blob)
      //console.log(img)

      //const fileString = 'assets/testing/landscape/Landscape_1.jpg'
      /*const blob = await fetch(fileString).then(res => {
        return res.blob();
      });*/
      //const file = new File([blob], 'chucknorris.png', { type: 'image/jpeg' })

      //const response = readImage('assets/testing/landscape/Landscape_1.jpg',  function(base64) { console.info(base64) });

      //console.log(response)
      const result = await exifHandler.getImageOrientation(img)
      expect(result).toEqual(1)
    })
    it('returns correct orientation for landscape image with orientation 1', async () => {
      const { getByTestId } = renderContainer()
      const input = await waitForElement(() => getByTestId('imageInput'))

      fireEvent.change(input, {
        target: {
          files: [file],
        },
      })

      const result = await exifHandler.getImageOrientation(response)
      expect(result).toEqual(1)
    })
    it('returns correct orientation for landscape image with orientation 1', async () => {
      const { getByTestId } = renderContainer(ImgLandscape1)
      const image = await waitForElement(() => getByTestId('image'))
      //const file = new File([image], 'chucknorris.png', { type: 'image/jpg' })

      image.exifData = { orientation: 1 }

      //console.log(image)

      const result = await exifHandler.getImageOrientation(image)
      expect(result).toEqual(1)
    })
    it('returns correct orientation for landscape image with orientation 1', async () => {
      const { getByTestId } = renderContainer(ImgLandscape1)
      const image = await waitForElement(() => getByTestId('image'))
      let result = null
      const res = await EXIF.getData(image, function() {
        result = EXIF.getTag(this, 'Orientation')
        console.log(`this is the result ${result}`)
        return result
      })
      expect(res).toEqual(1)
      expect(result).toEqual(1)
    })
    it('returns correct orientation for landscape image with orientation 1', async () => {
      const blob = new Blob([ImgLandscape1], { type: 'image/jpeg' })
      const f = EXIF.readFromBinaryFile(ImgLandscape1)

      const result = await exifHandler.getImageOrientation(f)
      expect(result).toEqual(1)
    })
    it('returns correct orientation for landscape image with orientation 1', async () => {
      const fs = require('fs')
      const result = await fs.readFile(
        'src/assets/testing/landscape/Landscape_1.jpg',
        async function(err, buffer) {
          if (err) throw err

          const f = EXIF.readFromBinaryFile(toArrayBuffer(buffer))
          const result = await exifHandler.getImageOrientation(f)
          expect(result).toEqual(1)
          expect(result).toEqual(2)
          expect(true).toBeFalsy()

          console.log(`hey ${result}`)
        },
      )
      expect(result).toEqual('sjfslfs')
    })
    it('returns correct orientation for landscape image with orientation 1', async () => {
      const { promises: fs } = require('fs')

      async function getContent(filePath, encoding = 'utf-8') {
        if (!filePath) {
          throw new Error('filePath required')
        }

        return fs.readFile(filePath) //, { encoding })
      }

      function toBase64(arr) {
        //arr = new Uint8Array(arr) if it's an ArrayBuffer
        return btoa(
          arr.reduce((data, byte) => data + String.fromCharCode(byte), ''),
        )
      }

      const content = await getContent(
        'src/assets/testing/landscape/Landscape_1.jpg',
      )
      const result = await exifHandler.getImageOrientation(content)
      expect(true).toBeFalsy()
      expect(result).toEqual(1)
      expect(content).toEqual('sfs')
    })
    it('returns correct orientation for landscape image with orientation 1', async () => {
      const fs = require('fs').promises
      async function loadImage(src) {
        const data = await fs.readFile(src, 'binary')
        return new Buffer(data)
      }

      const buffer = await loadImage(
        'src/assets/testing/landscape/Landscape_1.jpg',
      )
      const f = await EXIF.readFromBinaryFile(toArrayBuffer(buffer))
      const result = await exifHandler.getImageOrientation(f)
      expect(result).toEqual(1)
      expect(result).toEqual(2)
      expect(true).toBeFalsy()
    })
    it('returns correct orientation for landscape image with orientation 1', async () => {
      const fs = require('fs')
      const fsPromises = fs.promises

      async function loadImage() {
        const data = await fsPromises.readFile(
          'src/assets/testing/landscape/Landscape_1.jpg',
          'binary',
        )
        return new Buffer(data)
      }

      const buffer = await loadImage()
      const arrayBuffer = toArrayBuffer(buffer)
      console.log(arrayBuffer)
      //const file = new File([buffer], 'chucknorris.png', { type: 'image/jpeg' })
      const result = await exifHandler.getImageOrientation(file)
      expect(result).toEqual(1)
    })
    it('returns correct orientation for landscape image with orientation 1', async () => {
      let img = new Image()
      img.src = URL.createObjectURL(ImgLandscape1)

      img.onload = async () => {
        const result = await exifHandler.getImageOrientation(file)
        expect(result).toEqual(1)
        expect(true).toBeFalsy()
      }
    })
  })
})
