const EXIF = require('exif-js')

async function containsLocationData(img) {
  const coordinates = await getLocationData(img)
  if (
    coordinates &&
    coordinates.length === 2 &&
    coordinates[0] !== null &&
    coordinates[1] !== null
  ) {
    return true
  }
  console.error('NO EXIF DATA FOUND!')
  return false
}

async function getCoordinates(img) {
  try {
    return getLocationData(img)
  } catch (_) {
    return null
  }
}

async function getDateTime(img) {
  try {
    return getDateTimeData(img)
  } catch (_) {
    return null
  }
}

async function getImageOrientation(img) {
  try {
    return getOrientationData(img)
  } catch (_) {
    return null
  }
}

async function getImageDimensions(img) {
  try {
    return getDimensionsData(img)
  } catch (_) {
    return null
  }
}

/* EXIF */
function getLocationData(img) {
  if ('exifdata' in img) {
    delete img.exifdata
  }

  return new Promise((resolve, reject) => {
    EXIF.getData(img, function handleLocationData() {
      const exifData = img.exifdata

      if (!('GPSLatitude' in exifData)) {
        reject(new Error('Does not contain GPSLatitude'))
        return
      }

      // Calculate latitude decimal
      const latDegree = exifData.GPSLatitude[0].numerator
      const latMinute = exifData.GPSLatitude[1].numerator
      const latSecond =
        exifData.GPSLatitude[2].numerator / exifData.GPSLatitude[2].denominator
      const latDirection = exifData.GPSLatitudeRef

      const latFinal = convertDMSToDD(
        latDegree,
        latMinute,
        latSecond,
        latDirection,
      )

      // Calculate longitude decimal
      const lonDegree = exifData.GPSLongitude[0].numerator
      const lonMinute = exifData.GPSLongitude[1].numerator
      const lonSecond =
        exifData.GPSLongitude[2].numerator /
        exifData.GPSLongitude[2].denominator
      const lonDirection = exifData.GPSLongitudeRef

      const lonFinal = convertDMSToDD(
        lonDegree,
        lonMinute,
        lonSecond,
        lonDirection,
      )

      resolve([latFinal, lonFinal])
    })
  })
}

/* Conversion to LatLong */
function convertDMSToDD(degrees, minutes, seconds, direction) {
  let dd = degrees + minutes / 60 + seconds / 3600

  if (direction === 'S' || direction === 'W') {
    dd *= -1
  }

  return dd
}

function getDateTimeData(img) {
  if ('exifdata' in img) {
    delete img.exifdata
  }

  return new Promise((resolve, reject) => {
    EXIF.getData(img, function handleDateTimeData() {
      const exifData = img.exifdata

      if (!('DateTime' in exifData)) {
        reject(new Error('Does not contain DateTime'))
        return
      }

      // Convert date time format
      const dateTimeRaw = exifData.DateTime
      const splitDate = dateTimeRaw.split(/\D/)
      const dateTime = new Date(
        splitDate[0],
        splitDate[1] - 1,
        splitDate[2],
        splitDate[3],
        splitDate[4],
        splitDate[5],
      )
      resolve(dateTime)
    })
  })
}

function getOrientationData(img) {
  if ('exifdata' in img) {
    delete img.exifdata
  }

  return new Promise((resolve, reject) => {
    EXIF.getData(img, function handleOrientationData() {
      const exifData = img.exifdata

      if (!('Orientation' in exifData)) {
        reject(new Error('Does not contain Orientation'))
        return
      }

      // Convert date time format
      const orientation = exifData.Orientation
      resolve(orientation)
    })
  })
}

function getDimensionsData(img) {
  if ('exifdata' in img) {
    delete img.exifdata
  }

  return new Promise((resolve, reject) => {
    EXIF.getData(img, function handleDimensionsData() {
      const exifData = img.exifdata

      if (
        !('PixelXDimension' in exifData) ||
        !('PixelYDimension' in exifData)
      ) {
        reject(new Error('Does not contain Dimensions'))
        return
      }

      const x = exifData.PixelXDimension
      const y = exifData.PixelYDimension
      resolve([x, y])
    })
  })
}

const functions = {
  containsLocationData: containsLocationData,
  getDateTime: getDateTime,
  getCoordinates: getCoordinates,
  getImageOrientation: getImageOrientation,
  getImageDimensions: getImageDimensions,
  binary: function binary(file) {
    const f = EXIF.readFromBinaryFile(file)
    console.info(f)
  },
}

export default functions
