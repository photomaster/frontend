import React, { useState, useEffect } from 'react'
import '../../components/recommendations/recommendations.scss'
import { Redirect } from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import PhotoCamera from '@material-ui/icons/PhotoCamera'
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core'
import validators from 'helpers/validators'
import baseStyles from 'styles/baseClasses'
import User from 'models/user'
import { useTranslation } from 'react-i18next'
import NavigationIconButton from 'components/utilities/navigationIconButton'
import ErrorMessage from 'components/formElements/errorMessage'
import Avatar from 'components/utilities/avatar'
import { userActions } from '../../actions'

const useStyles = makeStyles(theme => ({
  upload: {
    position: 'absolute',
    display: 'flex',
    zIndex: 10,
  },
  profileImage: {
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
  },
  bannerImage: {
    bottom: `calc(${theme.spacing(7)}px + 3px)`,
    right: 0,
    '&::after': {
      content: '""',
      background: 'white',
      width: theme.spacing(16),
      height: theme.spacing(10),
      position: 'absolute',
      opacity: 0.5,
      zIndex: -1,
      transform: `rotate(-45deg) translate(-${theme.spacing(
        2,
      )}px, -${theme.spacing(3)}px)`,
    },
  },
  bannerMessage: {
    position: 'absolute',
    top: theme.spacing(20),
    left: 0,
    right: 0,
    color: 'white',
    textAlign: 'center',
    padding: `0 ${theme.spacing(2)}px`,
  },
  overflowXHidden: {
    overflowX: 'hidden',
    overflowY: 'hidden',
    padding: '40px 0px 0px 40px',
  },
}))

export default function UserEdit(props) {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const [id, setId] = useState(props.match.params.id)
  const loggedIn = useSelector(state => state.currentUser.loggedIn)
  const user = useSelector(state => state.currentUser.user)
  const [loading, setLoading] = useState({ active: true })
  const [displayName, setDisplayname] = useState(
    loggedIn ? user.displayName : '',
  )
  const [username] = useState(loggedIn ? user.username : '')
  const [email, setEmail] = useState(loggedIn ? user.email : '')
  const [bio, setBio] = useState(
    loggedIn && user.description !== null ? user.description : '',
  )
  const [password, setPassword] = useState('')
  const [password2, setPassword2] = useState('')
  const [profileImage, setProfileImage] = useState(loggedIn ? user.img : '')
  const [bannerImage, setBannerImage] = useState(loggedIn ? user.banner : '')
  const [file, setFile] = useState({ profileImage: null, bannerImage: null })
  const [errors, setErrors] = useState({
    displayName: false,
    bio: false,
    email: false,
    password: false,
    password2: false,
  })
  const active = useSelector(state => state.currentUser.updating)
  const apiErrors = useSelector(state => state.currentUser.errors)

  if (id != props.match.params.id) {
    setId(props.match.params.id)
  }

  useEffect(() => {
    setLoading({ active: true })
    window.scrollTo(0, 0)
  }, [id])

  async function _handleImageChange(e, profileImageInput) {
    e.preventDefault()

    const reader = new FileReader()
    const eventFile = e.target.files[0]
    if (profileImageInput) setLoading({ active: true, profileImage: true })
    else setLoading({ active: true, bannerImage: true })

    reader.onloadend = async () => {
      if (profileImageInput) {
        setProfileImage(reader.result)
        setFile({ ...file, profileImage: eventFile })
        setLoading({ ...loading, profileImage: false })
        if (!loading.bannerImage) setLoading({ ...loading, active: false })
      } else {
        setBannerImage(reader.result)
        setFile({ ...file, bannerImage: eventFile })
        setLoading({ ...loading, bannerImage: false })
        if (!loading.profileImage) setLoading({ ...loading, active: false })
      }
    }

    reader.readAsDataURL(eventFile)
  }

  async function _handleSubmit(e) {
    e.preventDefault()

    try {
      let toUpdate = { id, displayName, email }
      if (password) toUpdate = { ...toUpdate, password }
      if (bio) toUpdate = { ...toUpdate, description: bio }
      if (profileImage)
        toUpdate = { ...toUpdate, avatarImage: file.profileImage }
      if (bannerImage) toUpdate = { ...toUpdate, bannerImage: file.bannerImage }
      dispatch(userActions.update(new User(toUpdate)))
    } catch (error) {
      console.error(error)
    }
  }

  const _ownUserprofile = () => {
    if (loggedIn && user && user.id == id) {
      return true
    }
    return false
  }

  const _handleDisplaynameChange = event => {
    const value = event.target.value
    let errorDisplayname = false
    if (!validators.displayNameIsValid(value)) {
      errorDisplayname = true
    }
    setDisplayname(value)
    setErrors({ ...errors, displayName: errorDisplayname })
  }

  const _handleEmailChange = event => {
    const value = event.target.value
    let errorEmail = false
    if (!validators.emailIsValid(value)) {
      errorEmail = true
    }
    setEmail(value)
    setErrors({ ...errors, email: errorEmail })
  }

  const _handleBioChange = event => {
    const value = event.target.value
    let errorBio = false
    if (value && !validators.bioIsValid(value)) {
      errorBio = true
    }
    setBio(value)
    setErrors({ ...errors, bio: errorBio })
  }

  const _handlePasswordChange = event => {
    const value = event.target.value
    let errorPassword = false
    if (value && !validators.passwordIsValid(value)) {
      errorPassword = true
    }
    setPassword(value)

    let errorPassword2 = false
    if (password2 && !validators.passwordsMatch(value, password2)) {
      errorPassword2 = true
    }
    setErrors({ ...errors, password: errorPassword, password2: errorPassword2 })
  }

  const _handlePasswort2Change = event => {
    const value = event.target.value
    let errorPassword2 = false
    if (value && value != password) {
      errorPassword2 = true
    }
    setPassword2(value)
    setErrors({ ...errors, password2: errorPassword2 })
  }

  const _filledInCompletely = () => {
    if (!displayName || !email) return false
    return true
  }

  return !_ownUserprofile() ? (
    <Redirect to={`/users/${id}`} />
  ) : (
    <React.Fragment>
      <div className={baseClasses.relative} data-testid={'userEdit'}>
        <div
          className={baseClasses.banner}
          style={
            bannerImage
              ? { backgroundImage: `url(${bannerImage})` }
              : { backgroundImage: `url(${user.bannerImage})` }
          }
          data-testid={'userEdit--banner'}
        />
        {!bannerImage && (
          <span className={classes.bannerMessage}>
            {t('forms.profile.bannerHint')} &#8600;
          </span>
        )}
        <span
          className={`${classes.upload} ${classes.bannerImage} ${classes.overflowXHidden}`}
        >
          <input
            accept="image/*"
            className={baseClasses.displayNone}
            id="bannerImage"
            type="file"
            onChange={e => _handleImageChange(e, false)}
            data-testid={'userEdit--banner--input'}
          />
          <label htmlFor="bannerImage">
            <IconButton
              color="primary"
              aria-label={t('forms.profile.aria.banner')}
              component="span"
              disabled={loading.active && loading.bannerImage}
              data-testid={'userEdit--banner--button'}
            >
              <PhotoCamera />
            </IconButton>
          </label>
        </span>
        <div className={baseClasses.relative}>
          <Avatar
            alt={t('profile.avatar.alt')}
            src={profileImage ?? user.avatarImage}
            wrappedInContainer
            iconBottom
          />
          <span className={`${classes.upload} ${classes.profileImage}`}>
            <input
              accept="image/*"
              className={baseClasses.displayNone}
              id="profileImage"
              type="file"
              onChange={e => _handleImageChange(e, true)}
              data-testid={'userEdit--profileImage--input'}
            />
            <label htmlFor="profileImage">
              <IconButton
                color="primary"
                aria-label={t('forms.profile.aria.upload')}
                component="span"
                disabled={loading.active && loading.profileImage}
                data-testid={'userEdit--profileImage--button'}
              >
                <PhotoCamera />
              </IconButton>
            </label>
          </span>
        </div>
      </div>
      <Container
        component="main"
        maxWidth="xs"
        className={`upload ${baseClasses.marginTop0} ${baseClasses.relative}`}
      >
        <CssBaseline />
        <NavigationIconButton
          page={'user'}
          title={t('forms.return')}
          aria={t('forms.profile.aria.return')}
          navigateToEdit={false}
        />
        <Typography component="h1" variant="h1">
          {t('forms.profile.header')}
        </Typography>
        <Typography variant="body1" className={baseClasses.marginBottom4}>
          {t('forms.profile.introduction')}
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoComplete="username"
                name="username"
                variant="outlined"
                fullWidth
                id="username"
                label={t('forms.labels.username')}
                value={username}
                helperText={t('forms.helperTexts.username')}
                disabled
                data-testid={'userEdit--username'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                autoComplete="nickname"
                name="displayName"
                variant="outlined"
                required
                fullWidth
                id="displayName"
                label={t('forms.labels.displayName')}
                value={displayName}
                onChange={_handleDisplaynameChange}
                error={errors.displayName}
                helperText={
                  errors.displayName
                    ? validators.errorMessages.displayName
                    : t('forms.helperTexts.displayName')
                }
                disabled={active}
                data-testid={'userEdit--displayName'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                multiline
                rows={6}
                rowsMax={20}
                id="bio"
                label={t('forms.labels.bio')}
                name="bio"
                value={bio}
                onChange={_handleBioChange}
                error={errors.bio}
                helperText={
                  errors.bio
                    ? validators.errorMessages.bio
                    : t('forms.helperTexts.bio')
                }
                disabled={active}
                data-testid={'userEdit--description'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label={t('forms.labels.email')}
                name="email"
                autoComplete="email"
                value={email}
                onChange={_handleEmailChange}
                error={errors.email}
                helperText={errors.email ? validators.errorMessages.email : ''}
                disabled={active}
                data-testid={'userEdit--email'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                name="password"
                label={t('forms.labels.passwordNew')}
                type="password"
                id="password"
                value={password}
                onChange={_handlePasswordChange}
                error={errors.password}
                helperText={
                  errors.password ? validators.errorMessages.password : ''
                }
                disabled={active}
                data-testid={'userEdit--password'}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                name="password2"
                label={t('forms.labels.passwordNew2')}
                type="password"
                id="password2"
                value={password2}
                onChange={_handlePasswort2Change}
                error={errors.password2}
                helperText={
                  errors.password2
                    ? validators.errorMessages.passwordsMatch
                    : ''
                }
                disabled={active}
                data-testid={'userEdit--password2'}
              />
            </Grid>
          </Grid>
          {apiErrors && apiErrors.update && (
            <ErrorMessage text={apiErrors.update} noTranslate mt />
          )}
          <span className={baseClasses.relative}>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={baseClasses.submit}
              disabled={
                !_filledInCompletely() ||
                Object.values(errors).includes(true) ||
                active
              }
              onClick={e => _handleSubmit(e)}
              data-testid={'userEdit--submitButton'}
            >
              {t('forms.profile.button')}
            </Button>
            {active && (
              <CircularProgress
                size={24}
                className={baseClasses.buttonProgress}
                data-testid={'userEdit--loading'}
              />
            )}
          </span>
        </form>
      </Container>
    </React.Fragment>
  )
}
