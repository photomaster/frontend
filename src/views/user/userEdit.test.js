import React, { useReducer } from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
  wait,
} from '@testing-library/react'
import UserEdit from './userEdit'
import User from 'models/user'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import validators from 'helpers/validators'
import { userActions } from 'actions'

const TestUser = {
  id: 1,
  username: 'test_username',
  displayName: 'test_displayName',
  description: 'desc',
  email: 'test@mail.com',
  password: 'test_password',
  password2: 'test_password',
  bannerImage: '/banner',
  avatarImage: '/avatar',
}

const ImageFile = new File(['(⌐□_□)'], 'chucknorris.png', { type: 'image/png' })

const mockTranslation = jest.fn(str => str)
const mockValidatorTrue = jest.fn(_ => true)
const mockValidatorFalse = jest.fn(_ => false)
const mockScrollTo = jest.fn()
const mockConsole = jest.fn(msg => msg)
const mockDispatch = jest.fn()

window.scrollTo = mockScrollTo

console.error = mockConsole

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

function renderContainer(
  userPageId = 1,
  loggedIn = true,
  updating = false,
  errors = {},
) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: {
            loggedIn: loggedIn,
            updating: updating,
            errors: errors,
            user: TestUser,
          },
        })}
      >
        <UserEdit match={{ params: { id: userPageId } }} />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('User Edit View', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('redirects if not own profile', async () => {
    const { container } = renderContainer(2)

    const userEdit = container.querySelector('[data-testid="userEdit"]')

    expect(userEdit).toBeFalsy()
  })
  it('can handle banner image upload', async () => {
    const { getByTestId } = renderContainer()
    const input = await waitForElement(() =>
      getByTestId('userEdit--banner--input'),
    )
    const banner = await waitForElement(() => getByTestId('userEdit--banner'))

    fireEvent.change(input, { target: { files: [ImageFile] } })

    await wait(() =>
      expect(banner.getAttribute('style')).not.toContain(
        'background-image: url(/banner)',
      ),
    )

    expect(banner.getAttribute('style')).toContain(
      'background-image: url(data:',
    )
  })
  xit('can handle profile image upload', async () => {
    const { getByTestId, debug } = renderContainer()
    const input = await waitForElement(() =>
      getByTestId('userEdit--profileImage--input'),
    )
    const avatarContainer = await waitForElement(() =>
      getByTestId('avatar--container'),
    )
    const avatarImage = await waitForElement(() =>
      avatarContainer.querySelector('img'),
    )

    fireEvent.change(input, { target: { files: [ImageFile] } })

    debug(avatarImage)

    await wait(() =>
      expect(avatarImage.getAttribute('src')).not.toContain('/avatar'),
    )

    debug(avatarImage)

    expect(avatarImage.getAttribute('src')).toContain('data:')
  })
  xit('dispatches action to update user on click', async () => {
    const mockUpdate = jest.fn().mockImplementation(() => {
      return { type: 'updating user' }
    })
    userActions.update = mockUpdate
    validators.passwordIsValid = mockValidatorTrue

    const { findByTestId } = renderContainer()
    const submitButton = await waitForElement(() =>
      findByTestId('userEdit--submitButton'),
    )
    const password = await waitForElement(() =>
      findByTestId('userEdit--password'),
    )
    const password2 = await waitForElement(() =>
      findByTestId('userEdit--password2'),
    )

    fireEvent.change(password2.querySelector('input'), {
      target: { value: 'a_secret' },
    })
    fireEvent.change(password.querySelector('input'), {
      target: { value: 'a_secret' },
    })
    fireEvent.click(submitButton)

    expect(mockUpdate).toHaveBeenCalledTimes(1)
    expect(mockUpdate).toHaveBeenCalledWith(
      new User({
        id: 1,
        displayName: 'test_displayName',
        email: 'test@mail.com',
        password: 'a_secret',
        description: 'desc',
        bannerImage: {},
        avatarImage: {},
      }),
    )
  })
  it('dispatches action to update user (not everything filled in)', async () => {
    const mockUpdate = jest.fn().mockImplementation(() => {
      return { type: 'updating user' }
    })
    userActions.update = mockUpdate
    validators.passwordIsValid = mockValidatorTrue

    const { findByTestId } = renderContainer()
    const submitButton = await waitForElement(() =>
      findByTestId('userEdit--submitButton'),
    )
    const password = await waitForElement(() =>
      findByTestId('userEdit--password'),
    )
    const password2 = await waitForElement(() =>
      findByTestId('userEdit--password2'),
    )

    fireEvent.change(password2.querySelector('input'), {
      target: { value: 'a_secret' },
    })
    fireEvent.change(password.querySelector('input'), {
      target: { value: 'a_secret' },
    })
    fireEvent.click(submitButton)

    expect(mockUpdate).toHaveBeenCalledTimes(1)
    expect(mockUpdate).toHaveBeenCalledWith(
      new User({
        id: 1,
        displayName: 'test_displayName',
        email: 'test@mail.com',
        password: 'a_secret',
        description: 'desc',
      }),
    )
  })
  describe('disables button to submit if', () => {
    it('displayName is not filled in', async () => {
      validators.displayNameIsValid = mockValidatorTrue

      const { getByTestId, findByTestId } = renderContainer()
      const displayName = await waitForElement(() =>
        findByTestId('userEdit--displayName'),
      )
      const submitButton = await waitForElement(() =>
        getByTestId('userEdit--submitButton'),
      )

      fireEvent.change(displayName.querySelector('input'), {
        target: { value: '' },
      })

      expect(submitButton).toBeDisabled()

      fireEvent.change(displayName.querySelector('input'), {
        target: { value: 'some_displayName' },
      })

      expect(submitButton).toBeEnabled()
    })
    it('email is not filled in', async () => {
      validators.emailIsValid = mockValidatorTrue

      const { getByTestId, findByTestId } = renderContainer()
      const email = await waitForElement(() => findByTestId('userEdit--email'))
      const submitButton = await waitForElement(() =>
        getByTestId('userEdit--submitButton'),
      )

      fireEvent.change(email.querySelector('input'), {
        target: { value: '' },
      })

      expect(submitButton).toBeDisabled()

      fireEvent.change(email.querySelector('input'), {
        target: { value: 'some@mail.com' },
      })

      expect(submitButton).toBeEnabled()
    })
  })
  describe('validates and sets new value for', () => {
    it('displayName', async () => {
      validators.displayNameIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const displayName = await waitForElement(() =>
        findByTestId('userEdit--displayName'),
      )

      fireEvent.change(displayName.querySelector('input'), {
        target: { value: 'new_displayName' },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(displayName.querySelector('input').getAttribute('value')).toEqual(
        'new_displayName',
      )
    })
    it('email', async () => {
      validators.emailIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const email = await waitForElement(() => findByTestId('userEdit--email'))

      fireEvent.change(email.querySelector('input'), {
        target: { value: 'new@mail.com' },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(email.querySelector('input').getAttribute('value')).toEqual(
        'new@mail.com',
      )
    })
    it('description', async () => {
      validators.bioIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const description = await waitForElement(() =>
        findByTestId('userEdit--description'),
      )

      fireEvent.change(description.querySelectorAll('textarea')[0], {
        target: { value: 'new_desc' },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(description.querySelectorAll('textarea')[0].textContent).toEqual(
        'new_desc',
      )
    })
    it('password (password2 empty)', async () => {
      validators.passwordIsValid = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const password = await waitForElement(() =>
        findByTestId('userEdit--password'),
      )

      fireEvent.change(password.querySelector('input'), {
        target: { value: TestUser.password },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(1)
      expect(password.querySelector('input').getAttribute('value')).toEqual(
        'test_password',
      )
    })
    it('password (password2 given)', async () => {
      validators.passwordIsValid = mockValidatorTrue
      validators.passwordsMatch = mockValidatorTrue

      const { findByTestId } = renderContainer()
      const password = await waitForElement(() =>
        findByTestId('userEdit--password'),
      )
      const password2 = await waitForElement(() =>
        findByTestId('userEdit--password2'),
      )

      fireEvent.change(password2.querySelector('input'), {
        target: { value: TestUser.password2 },
      })
      fireEvent.change(password.querySelector('input'), {
        target: { value: TestUser.password },
      })

      expect(mockValidatorTrue).toHaveBeenCalledTimes(2)
      expect(password.querySelector('input').getAttribute('value')).toEqual(
        'test_password',
      )
    })
    it('password2', async () => {
      const { findByTestId } = renderContainer()
      const password2 = await waitForElement(() =>
        findByTestId('userEdit--password2'),
      )

      fireEvent.change(password2.querySelector('input'), {
        target: { value: TestUser.password2 },
      })

      expect(password2.querySelector('input').getAttribute('value')).toEqual(
        'test_password',
      )
    })
  })
  it('username is read-only', async () => {
    const { findByTestId } = renderContainer()
    const username = await waitForElement(() =>
      findByTestId('userEdit--username'),
    )

    expect(username.querySelector('input')).toBeDisabled()
  })
  describe('displays error for', () => {
    it('displayName', async () => {
      validators.displayNameIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const displayName = await waitForElement(() =>
        findByTestId('userEdit--displayName'),
      )

      fireEvent.change(displayName.querySelector('input'), {
        target: { value: 'new_displayName' },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(displayName.querySelector('.Mui-error')).toBeTruthy()
    })
    it('email', async () => {
      validators.emailIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const email = await waitForElement(() => findByTestId('userEdit--email'))

      fireEvent.change(email.querySelector('input'), {
        target: { value: 'new@mail.com' },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(email.querySelector('.Mui-error')).toBeTruthy()
    })
    it('description', async () => {
      validators.bioIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const description = await waitForElement(() =>
        findByTestId('userEdit--description'),
      )

      fireEvent.change(description.querySelectorAll('textarea')[0], {
        target: { value: 'new_desc' },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(description.querySelector('.Mui-error')).toBeTruthy()
    })
    it('password (password2 empty)', async () => {
      validators.passwordIsValid = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const password = await waitForElement(() =>
        findByTestId('userEdit--password'),
      )

      fireEvent.change(password.querySelector('input'), {
        target: { value: TestUser.password },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(password.querySelector('.Mui-error')).toBeTruthy()
    })
    it('password (password2 given)', async () => {
      validators.passwordIsValid = mockValidatorTrue
      validators.passwordsMatch = mockValidatorFalse

      const { findByTestId } = renderContainer()
      const password = await waitForElement(() =>
        findByTestId('userEdit--password'),
      )
      const password2 = await waitForElement(() =>
        findByTestId('userEdit--password2'),
      )

      fireEvent.change(password2.querySelector('input'), {
        target: { value: TestUser.password2 },
      })
      fireEvent.change(password.querySelector('input'), {
        target: { value: TestUser.password },
      })

      expect(mockValidatorFalse).toHaveBeenCalledTimes(1)
      expect(password2.querySelector('.Mui-error')).toBeTruthy()
    })
    it('password2', async () => {
      const { findByTestId } = renderContainer()
      const password2 = await waitForElement(() =>
        findByTestId('userEdit--password2'),
      )

      fireEvent.change(password2.querySelector('input'), {
        target: { value: TestUser.password2 },
      })

      expect(password2.querySelector('.Mui-error')).toBeTruthy()
    })
    it('api errors', async () => {
      const { findByTestId } = renderContainer(1, true, false, {
        update: { email: ['email error'] },
      })
      const message = await waitForElement(() => findByTestId('alert'))

      expect(message).toBeTruthy()
      expect(message.textContent).toContain('email error')
    })
  })
  it('displays button to navigate to last page', async () => {
    const { getByTestId } = renderContainer()

    const button = await waitForElement(() =>
      getByTestId('navigationIconButton'),
    )

    expect(button).toBeTruthy()
  })
  it('translates the text', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('forms.profile.bannerHint')
    expect(mockTranslation).toHaveBeenCalledWith('profile.avatar.alt')
    expect(mockTranslation).toHaveBeenCalledWith('forms.return')
    expect(mockTranslation).toHaveBeenCalledWith('forms.profile.header')
    expect(mockTranslation).toHaveBeenCalledWith('forms.profile.introduction')
    expect(mockTranslation).toHaveBeenCalledWith('forms.labels.username')
  })
})
