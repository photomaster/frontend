import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  wait,
  waitForElementToBeRemoved,
} from '@testing-library/react'
import User from './user'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import photoService from 'service/photoService'
import userService from 'service/userService'

const TestUser = {
  id: 1,
  username: 'test_username',
  displayName: 'test_displayName',
  description: 'desc',
  email: 'test@mail.com',
  password: 'test_password',
  password2: 'test_password',
  bannerImage: '/banner',
  avatarImage: '/avatar',
}

const Photos = Array(...Array(5)).map((_, i) => {
  return {
    id: i,
    url: `/photo${i}`,
    user: TestUser,
    name: `name${i}`,
    description: `desc${i}`,
    imageThumbnailTeaser: `/url${i}`,
  }
})

const mockTranslation = jest.fn(str => str)
const mockGetPhotosOfUser = jest.fn(id => Photos)
const mockGetUser = jest.fn(id => TestUser)
const mockThrowError = jest.fn(_ => {
  throw 'some error'
})
const mockScrollTo = jest.fn()
const mockConsole = jest.fn(msg => msg)

window.scrollTo = mockScrollTo

console.error = mockConsole

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => jest.fn(),
}))

jest.mock('service/photoService', () => jest.fn())
photoService.getPhotosOfUser = mockGetPhotosOfUser

jest.mock('service/userService', () => jest.fn())
userService.getUser = mockGetUser

function renderContainer(userPageId = 1, loggedIn = true) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: {
            loggedIn: loggedIn,
            user: TestUser,
          },
        })}
      >
        <User match={{ params: { id: userPageId } }} />
      </Provider>
    </Router>,
  )

  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('User View', () => {
  it('renders without crashing', async () => {
    const { container, getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('user--top')).toBeTruthy())

    expect(container).toBeTruthy()
  })
  it('takes the user from the redux store for own profile page', async () => {
    const { getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('user--top')).toBeTruthy())

    expect(mockGetUser).toHaveBeenCalledTimes(0)
  })
  it('requests the user from the API for a different user page', async () => {
    const { getByTestId } = renderContainer(2)
    await wait(() => expect(getByTestId('user--top')).toBeTruthy())

    expect(mockGetUser).toHaveBeenCalledTimes(1)
  })
  it('does not crash if userService is not working', async () => {
    userService.getUser = mockThrowError

    const { container, getByTestId } = renderContainer(2)
    await wait(() => expect(getByTestId('user--top')).toBeTruthy())

    expect(container).toBeTruthy()
    expect(mockConsole).toHaveBeenCalledWith('some error')
  })
  it('requests photos from the photoService', async () => {
    const { getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('user--top')).toBeTruthy())

    expect(mockGetPhotosOfUser).toHaveBeenCalledTimes(1)
  })
  it('does not crash if photoService is not working', async () => {
    photoService.getPhotosOfUser = mockThrowError

    const { container, getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('user--top')).toBeTruthy())

    expect(container).toBeTruthy()
    expect(mockConsole).toHaveBeenCalledWith('some error')
  })
  describe('edit button', () => {
    it('is available for own profile', async () => {
      const { getByTestId } = renderContainer()
      await wait(() => expect(getByTestId('user--top')).toBeTruthy())

      const button = await waitForElement(() =>
        getByTestId('navigationIconButton'),
      )

      expect(button).toBeTruthy()
    })
    it('is unavailable for other profile', async () => {
      const { container, getByTestId } = renderContainer(2)
      await wait(() => expect(getByTestId('user--top')).toBeTruthy())

      const button = container.querySelector(
        '[data-testid="navigationIconButton"]',
      )

      expect(button).toBeFalsy()
    })
  })
  describe('displays correct', () => {
    it('displayName as headline', async () => {
      const { container, getByTestId } = renderContainer()
      await wait(() => expect(getByTestId('user--top')).toBeTruthy())

      const headline = await waitForElement(() => container.querySelector('h1'))

      expect(headline.textContent).toEqual('test_displayName')
    })
    it('banner image', async () => {
      const { getByTestId } = renderContainer()
      await wait(() => expect(getByTestId('user--top')).toBeTruthy())

      const banner = await waitForElement(() => getByTestId('user--banner'))

      expect(banner).toBeTruthy()
      expect(banner.getAttribute('style')).toContain(
        'background-image: url(/banner)',
      )
    })
    it('avtar image', async () => {
      const { getByTestId } = renderContainer()
      await wait(() => expect(getByTestId('user--top')).toBeTruthy())

      const avatar = await waitForElement(() => getByTestId('avatar'))

      expect(avatar).toBeTruthy()
      expect(avatar.querySelector('img').getAttribute('src')).toEqual('/avatar')
    })
    it('description', async () => {
      const { container, getByTestId } = renderContainer()
      await wait(() => expect(getByTestId('user--top')).toBeTruthy())

      const desc = await waitForElement(() => getByTestId('user--desc'))

      expect(desc).toBeTruthy()
      expect(desc.textContent).toEqual('desc')
    })
  })
  xit('displays photos returned by photoService', async () => {
    // TODO: lazy loading components do not work

    photoService.getPhotosOfUser = mockGetPhotosOfUser

    const {
      container,
      getByTestId,
      findByTestId,
      rerender,
      debug,
    } = renderContainer()
    await wait(() => expect(getByTestId('user--bottom')).toBeTruthy())

    //await import('components/recommendationCard/recommendationsGrid')
    //await import('components/recommendationCard/recommendationCard')

    //await wait()

    expect(mockGetPhotosOfUser).toHaveBeenCalledTimes(1)

    waitForElementToBeRemoved(() => getByTestId('loading')).then(() =>
      console.log('Element no longer in DOM'),
    )

    //rerender()
    //await wait(() => expect(getByTestId('heey')).toBeTruthy())

    debug(container)

    //await RecommendationsGrid
    //await RecommendationCard
    //await wait(() => expect(getByTestId('recommendationsGrid')).toBeTruthy())

    const grid = await waitForElement(() => findByTestId('recommendationsGrid'))
    const photos = await waitForElement(() =>
      container.querySelectorAll('[data-testid="recommendationCard--card"]'),
    )

    expect(grid).toBeTruthy()
    expect(mockGetPhotosOfUser).toHaveBeenCalledTimes(1)
    expect(photos.length).toEqual(5)
  })
  it('can handle no photo results', async () => {
    const mockGetPhotosOfUserNone = jest.fn(id => [])
    photoService.getPhotosOfUser = mockGetPhotosOfUserNone

    const { container, getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('user--top')).toBeTruthy())

    expect(mockTranslation).toHaveBeenCalledWith('profile.noPhotos')
  })
  it('displays loading icon', async () => {
    const { container, getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('user--top')).toBeTruthy())

    expect(container).toBeTruthy()
  })
  it('translates the text', async () => {
    const { container, getByTestId } = renderContainer()
    await wait(() => expect(getByTestId('user--top')).toBeTruthy())

    expect(mockTranslation).toHaveBeenCalled()
    expect(mockTranslation).toHaveBeenCalledWith('profile.avatar.alt')
    expect(mockTranslation).toHaveBeenCalledWith('profile.edit')
    expect(mockTranslation).toHaveBeenCalledWith('profile.popular')
  })
})
