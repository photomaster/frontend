import React, { useState, useEffect, lazy, Suspense } from 'react'
import '../../components/recommendations/recommendations.scss'
import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import { useSelector } from 'react-redux'
import baseStyles from 'styles/baseClasses'
import userService from 'service/userService'
import { useTranslation } from 'react-i18next'
import NavigationIconButton from 'components/utilities/navigationIconButton'
import Avatar from 'components/utilities/avatar'
import Loading from '../../components/utilities/loading'
import photoService from '../../service/photoService'

const RecommendationsGrid = lazy(() =>
  import('components/recommendationCard/recommendationsGrid.js'),
)
const RecommendationCard = lazy(() =>
  import('components/recommendationCard/recommendationCard.js'),
)

export default function User(props) {
  const baseClasses = baseStyles()
  const { t } = useTranslation()

  const [id, setId] = useState(props.match.params.id)
  const loggedIn = useSelector(state => state.currentUser.loggedIn)
  const loggedID = useSelector(state =>
    state.currentUser.user ? state.currentUser.user.id : null,
  )
  const [user, setUser] = useState('')
  const loggedUser = useSelector(state => state.currentUser.user)
  const [photos, setPhotos] = useState([])
  const [loading, setLoading] = useState({
    user: true,
    photos: true,
  })

  if (id != props.match.params.id) {
    setId(props.match.params.id)
  }

  useEffect(() => {
    setLoading({
      user: true,
      photos: true,
    })
    window.scrollTo(0, 0)
    fetchData()
  }, [id])

  async function fetchData() {
    try {
      if (loggedIn && loggedUser && loggedUser.id == id) {
        setUser(loggedUser)
      } else {
        setUser(await userService.getUser(props.match.params.id))
      }
      setLoading(loading => ({ ...loading, user: false }))
    } catch (e) {
      console.error(e)
      setLoading(loading => ({ ...loading, user: false }))
    }

    try {
      setPhotos(await photoService.getPhotosOfUser(id))
      setLoading(loading => ({ ...loading, photos: false }))
    } catch (e) {
      console.error(e)
      setLoading(loading => ({ ...loading, photos: false }))
    }
  }

  const _ownUserprofile = () => {
    if (loggedIn && user && user.id === loggedID) {
      return true
    }
    return false
  }

  return loading.user ? (
    <Loading />
  ) : (
    <React.Fragment>
      <div className={baseClasses.relative} data-testid={'user--top'}>
        <div
          className={baseClasses.banner}
          style={{ backgroundImage: `url(${user.bannerImage})` }}
          data-testid={'user--banner'}
        />
        <Avatar
          alt={t('profile.avatar.alt')}
          src={user.avatarImage}
          wrappedInContainer
        />
      </div>
      <Container
        component="main"
        maxWidth="md"
        className={`upload ${baseClasses.marginTop0} ${baseClasses.relative}`}
        data-testid={'user--bottom'}
      >
        <CssBaseline />
        {_ownUserprofile() && (
          <NavigationIconButton
            page={'user'}
            id={user.id}
            title={t('profile.edit')}
            aria={t('profile.aria.edit')}
            navigateToEdit
          />
        )}
        <Typography component="h1" variant="h1">
          {user.displayName}
        </Typography>
        <Typography
          variant="body1"
          className={`${baseClasses.textAlignCenter} ${baseClasses.fullWidth} ${baseClasses.padding2}`}
          data-testid={'user--desc'}
        >
          {user.description}
        </Typography>
        <Typography
          component="h2"
          variant="h2"
          className={baseClasses.headline}
        >
          {t('profile.popular')}
        </Typography>
        {loading.photos ? (
          <Loading />
        ) : (
          <Suspense fallback={<Loading />}>
            <RecommendationsGrid>
              {photos.length == 0 ? (
                <Typography
                  variant="body1"
                  className={`${baseClasses.textAlignCenter} ${baseClasses.fullWidth} ${baseClasses.padding2}`}
                >
                  {t('profile.noPhotos')}
                </Typography>
              ) : (
                photos.map(photo => (
                  <RecommendationCard
                    key={photo.id}
                    photo={photo}
                    user={photo.user}
                  />
                ))
              )}
            </RecommendationsGrid>
          </Suspense>
        )}
      </Container>
    </React.Fragment>
  )
}
