import WeatherState from './favorite'
import TestIcon from '@material-ui/icons/CloudOutlined'
import TestIcon2 from '@material-ui/icons/FavoriteBorder'

jest.mock('service/baseService')
jest.mock('models/photo')

const props = {
  locationId: 1,
  locationName: 'test location',
  description: 'desc',
  icon: TestIcon,
  temperature: 10,
}

describe('WeatherState model', () => {
  it('creates a new weather state', () => {
    const model = new WeatherState(props)
    expect(model).toBeInstanceOf(WeatherState)
  })
  describe('sets new values for', () => {
    it('description', () => {
      const model = new WeatherState(props)
      model.description = 'something new'
      expect(model.description).toEqual('something new')
    })
    it('icon', () => {
      const model = new WeatherState(props)
      model.icon = TestIcon2
      expect(model.icon).toEqual(TestIcon2)
    })
    it('temperature', () => {
      const model = new WeatherState(props)
      model.temperature = 20
      expect(model.temperature).toEqual(20)
    })
  })
})
