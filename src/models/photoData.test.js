import { baseService } from 'service/baseService'
import PhotoData from './photoData'
jest.mock('service/baseService')

describe('PhotoData model', () => {
  it('should forward save request to baseService', () => {
    const model = new PhotoData({
      url: 'https://foobar.com/photoData/1.json',
      model: 'a camera model',
    })
    model.save()
    expect(baseService.put).toHaveBeenCalledWith(model._props.url, model._props)
  })

  it('should throw an error if model has no url property', () => {
    const model = new PhotoData({
      model: 'a camera model',
    })

    expect(() => model.save()).toThrow()
  })
})
