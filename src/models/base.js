import { baseService } from 'service/baseService'

export default class Base {
  /**
   * @param {Object} props
   */
  constructor(props = {}) {
    this._props = props
  }

  /**
   * @returns {null|Number}
   */
  get id() {
    return this._props.id
  }

  delete(routeName = null) {
    return baseService.deleteModel(this, routeName)
  }
}
