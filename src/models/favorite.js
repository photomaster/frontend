import Base from './base'
import Photo from './photo'

export default class Favorite extends Base {
  constructor(props) {
    super(props)
    if (props.photo) {
      this._props.photo = new Photo(props.photo)
    }
  }

  /**
   * @returns {Photo|null}
   */
  get photo() {
    return this._props.photo
  }

  get dateAdded() {
    return this._props.dateAdded
  }
}
