import Base from './base'
import { baseService } from 'service/baseService'
import { ROUTE_PHOTOS_DETAIL } from 'service/routes'

jest.mock('service/baseService')
beforeEach(() => {})

describe('BaseModel', () => {
  it('should should create a base model', () => {
    const model = new Base({
      id: 5,
    })
    expect(model.id).toEqual(5)
  })

  it('should create props object if not set', () => {
    const model = new Base()
    expect(model.id).toEqual(undefined)
  })

  it('should not allow ids to be changed', () => {
    const model = new Base({
      id: 5,
    })

    expect(model.id).toEqual(5)
    expect(() => (model.id = 42)).toThrow()
    expect(model.id).toEqual(5)
  })

  it('should forward delete request to baseService', () => {
    const model = new Base({
      url: 'https://foobar.com/base/1.json',
      id: 1,
      foo: 'bar',
    })
    model.delete()
    expect(baseService.deleteModel).toHaveBeenCalledWith(model, null)

    const modelWithoutUrl = new Base({
      id: 1,
      foo: 'bar',
    })
    modelWithoutUrl.delete(ROUTE_PHOTOS_DETAIL)
    expect(baseService.deleteModel).toHaveBeenLastCalledWith(
      modelWithoutUrl,
      ROUTE_PHOTOS_DETAIL,
    )
  })
})
