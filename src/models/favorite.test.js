import Photo from 'models/photo'
import Favorite from './favorite'
import photoMock from 'views/photo/photo.mock.json'
jest.mock('service/baseService')
jest.mock('models/photo')

describe('Favorite model', () => {
  it('creates a new photo out of the given data', () => {
    const model = new Favorite({
      photo: photoMock,
    })
    expect(model.photo).toBeInstanceOf(Photo)
  })
  it('returns the photo', () => {
    const model = new Favorite({
      photo: photoMock,
    })
    const photo = model.photo
    expect(photo).toBeInstanceOf(Photo)
  })
})
