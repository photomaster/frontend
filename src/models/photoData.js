import Base from 'models/base'
import { baseService } from 'service/baseService'

export default class PhotoData extends Base {
  get make() {
    return this._props.make
  }

  set make(value) {
    this._props.make = value
  }

  get model() {
    return this._props.model
  }

  set model(value) {
    this._props.model = value
  }

  get whiteBalance() {
    return this._props.whiteBalance
  }

  set whiteBalance(value) {
    this._props.whiteBalance = value
  }

  get fNumber() {
    return this._props.fNumber
  }

  set fNumber(value) {
    this._props.fNumber = value
  }

  get focalLength() {
    return this._props.focalLength
  }

  set focalLength(value) {
    this._props.focalLength = value
  }

  get exposureTime() {
    return this._props.exposureTime
  }

  set exposureTime(value) {
    this._props.exposureTime = value
  }

  get flash() {
    return this._props.flash
  }

  set flash(value) {
    this._props.flash = value
  }

  get lensMake() {
    return this._props.lensMake
  }

  set lensMake(value) {
    this._props.lensMake = value
  }

  get lensModel() {
    return this._props.lensModel
  }

  set lensModel(value) {
    this._props.lensModel = value
  }

  get software() {
    return this._props.software
  }

  set software(value) {
    this._props.software = value
  }

  get exposureMode() {
    return this._props.exposureMode
  }

  set exposureMode(value) {
    this._props.exposureMode = value
  }

  get isoSpeed() {
    return this._props.isoSpeed
  }

  set isoSpeed(value) {
    this._props.isoSpeed = value
  }

  get pixelXDimension() {
    return this._props.pixelXDimension
  }

  set pixelXDimension(value) {
    this._props.pixelXDimension = value
  }

  get pixelYDimension() {
    return this._props.pixelYDimension
  }

  set pixelYDimension(value) {
    this._props.pixelYDimension = value
  }

  save() {
    if (!this._props.url) {
      throw new Error(
        'Thou shalt not build this object manually :-) . Use the one you get from calling "await photo.photoData"',
      )
    }
    return baseService.put(this._props.url, this._props)
  }
}
