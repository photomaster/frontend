import userService from 'service/userService'
import Base from './base'

export default class User extends Base {
  /**
   * @returns {null|String}
   */
  get username() {
    return this._props.username
  }

  /**
   * @returns {null|String}
   */
  get email() {
    return this._props.email
  }

  /**
   * @param {String} email email of user
   */
  set email(email) {
    this._props.email = email
  }

  /**
   * Returns displayName of user. If displayName is falsey, it returns the username instead
   * @returns {String} displayName or username
   */
  get displayName() {
    if (!('displayName' in this._props) || !this._props.displayName) {
      return this._props.username
    }
    return this._props.displayName
  }

  /**
   * @param {null|String} displayName display name
   */
  set displayName(displayName) {
    this._props.displayName = displayName
  }

  /**
   * @returns {null|String}
   */
  get description() {
    return this._props.description
  }

  /**
   * @param {null|String} description
   */
  set description(description) {
    this._props.description = description
  }

  /**
   * @returns {null|String|File}
   */
  get avatarImage() {
    return this._props.avatarImage
  }

  /**
   * @param {null|String|File} avatarImage
   */
  set avatarImage(avatarImage) {
    this._props.avatarImage = avatarImage
  }

  /**
   * @returns {null|String|File}
   */
  get bannerImage() {
    return this._props.bannerImage
  }

  /**
   * @param {null|String|File} description
   */
  set bannerImage(bannerImage) {
    this._props.bannerImage = bannerImage
  }

  /**
   * @param {String} password
   */
  set password(password) {
    this._props.password = password
  }

  save() {
    return userService.save(this)
  }
}
