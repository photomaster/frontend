import moment from 'moment'
// eslint-disable-next-line import/no-cycle
import photoService from 'service/photoService'
import weatherService from 'service/weatherService'
import Base from './base'

const fallbackUser = Object.freeze({
  id: -1,
  displayName: 'Anonymous',
})

export default class Photo extends Base {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props)

    if (typeof this._props.geo !== 'object') {
      this._props.geo = {
        type: 'Point',
        coordinates: [],
      }
    }
  }

  /**
   * Sends photo object to api. It does not update the current photo with but instead return a new photo object!
   * @returns {Photo} updated version of photo
   */
  save() {
    return photoService.save(this)
  }

  /**
   * @returns {String}
   */
  get name() {
    return this._props.name
  }

  /**
   * @param {String} name
   */
  set name(name) {
    this._props.name = name
  }

  /**
   * @returns {String}
   */
  get description() {
    return this._props.description
  }

  /**
   * @param {String} description
   */
  set description(description) {
    this._props.description = description
  }

  /**
   * @returns {Object}
   */
  get geo() {
    return this._props.geo
  }

  /**
   * @returns {Number}
   */
  get lat() {
    return this._props.geo.coordinates[1]
  }

  /**
   * @param {Number} lat
   */
  set lat(lat) {
    this._props.geo.coordinates[1] = lat
  }

  /**
   * @returns {Number}
   */
  get long() {
    return this._props.geo.coordinates[0]
  }

  /**
   * @param {Number} long
   */
  set long(long) {
    this._props.geo.coordinates[0] = long
  }

  /**
   * @readonly
   * @returns {Object|null}
   */
  get user() {
    if (this._props.externalUsername) {
      return {
        id: -1,
        displayName: this._props.externalUsername,
        url: this._props.externalUrl,
        licenseUrl: this._props.licenseUrl,
        licenseName: this._props.licenseName,
      }
    }

    return this._props.user ?? fallbackUser
  }

  /**
   * @returns {String} url of weather object
   */
  get currentWeatherUrl() {
    return this._props.currentWeather
  }

  /**
   * @returns {Object} weather object
   */
  get currentWeather() {
    if (!this._props.currentWeather) {
      return null
    }

    const splitWeather = this._props.currentWeather.split('/')
    const weatherId = splitWeather[splitWeather.length - 2]

    return weatherService.getWeather(parseInt(weatherId))
  }

  get weatherAtCreation() {
    if (!this._props.weatherAtCreation) {
      return Promise.reject(
        new Error('no weather data is associated with this photo'),
      )
    }
    const splitWeather = this._props.weatherAtCreation.split('/')
    const weatherId = splitWeather[splitWeather.length - 2]

    return weatherService.getWeather(parseInt(weatherId))
  }

  get photoDataUrl() {
    return this._props.photodata
  }

  get photoData() {
    if (!this.photoDataUrl) {
      return Promise.reject(new Error('Cannot build photo data url.'))
    }
    return photoService.getPhotoData(this)
  }

  get solarData() {
    if (!this.id) {
      return Promise.reject(new Error('Photo has not been saved yet!'))
    }
    return photoService.getSolarData(this.id)
  }

  /**
   * @returns {File|String}
   */
  get image() {
    return this._props.image
  }

  /**
   * @param {File} file
   */
  set image(file) {
    // eslint-disable-next-line no-unsafe-negation
    if (!file instanceof File) {
      throw new Error('image must be a File object!')
    }
    this._props.image = file
  }

  /**
   * @returns {String}
   */
  get imageThumbnailPreview() {
    return this._props.imageThumbnailPreview
  }

  /**
   * @returns {String}
   */
  get imageThumbnailTeaser() {
    return this._props.imageThumbnailTeaser
  }

  /**
   * @returns {String}
   */
  get imageThumbnailTeaserSmall() {
    return this._props.imageThumbnailTeaserSmall
  }

  /**
   * @returns {Array.<Object>}
   */
  get labels() {
    return this._props.labels
  }

  /**
   * @param {Array.<Object>} labels
   */
  set labels(labels) {
    this._props.labels = labels
  }

  /**
   * @returns {Array.<String>} list of label titles
   */
  labelNames() {
    return this._props.labels.map(label => label.title)
  }

  /**
   * @param {String} title
   */
  getLabel(title) {
    return this._props.labels.find(label => label.title === title)
  }

  /**
   * @return {Boolean} indicates whether or not the label is already included
   * @param {String} title the title to look up
   */
  hasLabel(title) {
    return this.getLabel(title) !== undefined
  }

  /**
   *
   * @param {String} title the title of the new label
   * @param {Number} score optionally privide a custom score between 0 and 1
   */
  addLabel(title, score = 1.0) {
    if (this.hasLabel(title)) {
      return
    }

    this._props.labels.push({
      title,
      score,
    })
  }

  /**
   * @param {String} title the title of the label
   */
  removeLabel(title) {
    const idx = this._props.labels.findIndex(label => label.title === title)
    if (idx === -1) {
      return
    }

    this._props.labels.splice(idx, 1)
  }

  /**
   * @returns {Number} Visibilty state (draft, private, public)
   * @see PhotoService
   */
  get visibility() {
    return this._props.visibility
  }

  /**
   * @param {Number} visibilty visibility state (draft, private, public)
   * @throws {Error}
   * @see PhotoService
   */
  set visibility(visibility) {
    if (visibility > 3 || visibility < 1) {
      throw new Error(
        "Invalid visibility state! Use one of these: 'draft': 1, 'private': 2 or 'public': 3",
      )
    }
    this._props.visibility = visibility
  }

  /**
   * @returns {Date|String} Returns the original creation date of the image provided by the user provided by the user.
   */
  get originalCreationDate() {
    if (!this._props.originalCreationDate) {
      return null
    }
    if (!(this._props.originalCreationDate instanceof Date)) {
      return moment(this._props.originalCreationDate).toDate()
    }
    return this._props.originalCreationDate
  }

  /**
   * @param {Number|String|Date} originalCreationDate
   */
  set originalCreationDate(originalCreationDate) {
    this._props.originalCreationDate = originalCreationDate
  }
}
