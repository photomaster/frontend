import Base from './base'

export default class WeatherState extends Base {
  /**
   * @returns {Number|null}
   */
  get locationId() {
    return this._props.locationId
  }

  /**
   * @returns {String|null}
   */
  get locationName() {
    return this._props.locationName
  }

  /**
   * @returns {String|null}
   */
  get description() {
    return this._props.description
  }

  /**
   * @param description {String|null}
   */
  set description(description) {
    this._props.description = description
  }

  /**
   * @returns {String|null}
   */
  get icon() {
    return this._props.icon
  }

  /**
   * @param icon {String|null}
   */
  set icon(icon) {
    this._props.icon = icon
  }

  /**
   * @returns {Number|null}
   */
  get temperature() {
    return this._props.temperature
  }

  /**
   * @param temperature {Number|null}
   */
  set temperature(temperature) {
    this._props.temperature = temperature
  }

  /**
   * @returns {String|null}
   */
  get sunrise() {
    return this._props.sunrise
  }

  /**
   * @returns {String|null}
   */
  get sunset() {
    return this._props.sunset
  }
}
