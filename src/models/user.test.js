import User from 'models/user'
import userService from 'service/userService'

jest.mock('service/userService')

describe('User Model', () => {
  it('Correctly accesses wrapped properies', () => {
    const props = {
      id: 1,
      username: 'username',
      displayName: 'anon',
      email: 'test@test.at',
      description: 'Lorem ipsum',
      avatarImage: 'avatar.png',
      bannerImage: 'banner.png',
    }
    const user = new User(props)

    expect(user).toEqual(expect.objectContaining(props))
  })

  it('uses the username as fallback if display name is empty', () => {
    const user = new User({
      username: 'username',
    })

    expect(user.username).toEqual(user.displayName)

    const user1 = new User({
      username: null,
    })

    expect(user1.username).toEqual(user1.displayName)
  })

  it('offers only public write access to password', () => {
    const user = new User()
    user.password = 'foobar'

    expect(user._props.password).toEqual('foobar')
    expect(user.password).toBeUndefined()
  })

  it('should call userService when calling the save method', async () => {
    userService.save.mockResolvedValue(true)
    const user = new User()

    const r = await user.save()
    expect(userService.save.mock.calls).toHaveLength(1)
    expect(r).toEqual(true)

    userService.save.mockReset()
  })
})
