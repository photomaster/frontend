import Photo from './photo'
import photoService from 'service/photoService'
import weatherService from 'service/weatherService'
import PhotoData from './photoData'
import WeatherState from './weatherState'

jest.mock('service/photoService')
jest.mock('service/weatherService')

const getCompleteProps = () => {
  return {
    id: 42,
    name: 'name',
    description: 'description',
    geo: {
      type: 'Point',
      coordinates: [1, 2],
    },
    currentWeather: 'currenWeather',
    labels: [
      {
        title: 'Label 1',
        score: 0.9,
      },
      {
        title: 'Label 2',
        score: 0.75,
      },
      {
        title: 'Label 3',
        score: 0.5,
      },
    ],
    visibility: 3,
    originalCreationDate: '2020-04-01T09:52:51.271000Z',
  }
}

describe('Photo Model', () => {
  it('handles simple properties the right way', () => {
    const propsComplete = getCompleteProps()
    const p = new Photo(propsComplete)

    expect(p.id).toEqual(propsComplete.id)
    expect(p.title).toEqual(propsComplete.title)
    expect(p.description).toEqual(propsComplete.description)
    expect(p.geo).toEqual(propsComplete.geo)
    expect(p.currentWeatherUrl).toEqual(propsComplete.currentWeather)
    expect(p.labels).toEqual(propsComplete.labels)
    expect(p.visibility).toEqual(propsComplete.visibility)
  })

  it('correctly resolves access to the underling geo object', () => {
    const propsComplete = getCompleteProps()
    const p = new Photo(propsComplete)

    expect(p.lat).toEqual(propsComplete.geo.coordinates[1])
    expect(p.long).toEqual(propsComplete.geo.coordinates[0])

    p.lat = 42
    expect(p.lat).toEqual(42)
    expect(p.lat).toEqual(propsComplete.geo.coordinates[1])

    p.long = 43
    expect(p.long).toEqual(43)
    expect(p.lat).toEqual(propsComplete.geo.coordinates[1])
  })

  it('is able to check if a photo includes a label', () => {
    const propsComplete = getCompleteProps()
    const p = new Photo(propsComplete)

    expect(p.hasLabel('Label 1')).toBeTruthy()
    expect(p.hasLabel('Label 2')).toBeTruthy()
    expect(p.hasLabel('Label 3')).toBeTruthy()
    expect(p.hasLabel('Label 4')).toBeFalsy()
    expect(p.hasLabel('')).toBeFalsy()
    expect(p.hasLabel(null)).toBeFalsy()
    expect(p.hasLabel(false)).toBeFalsy()
    expect(p.hasLabel(undefined)).toBeFalsy()
  })

  it('adds labels to photo', () => {
    const propsComplete = getCompleteProps()
    const p = new Photo(propsComplete)
    expect(p.labels.length).toEqual(3)

    p.addLabel('a new Label')

    expect(p.labels.length).toEqual(4)
    expect(p.hasLabel('a new Label')).toBeTruthy()

    p.addLabel('not important', 0.25)
    expect(p.labels.length).toEqual(5)
    expect(p.hasLabel('not important')).toBeTruthy()
    expect(p.getLabel('not important').score).toEqual(0.25)
  })

  it('does not add a label if it already exists', () => {
    const propsComplete = getCompleteProps()
    const p = new Photo(propsComplete)

    p.addLabel('a new Label')
    p.addLabel('a new Label')
    p.addLabel('a new Label')

    expect(p.labels.length).toEqual(4)
    expect(p.hasLabel('a new Label')).toBeTruthy()
  })

  it('removes labels', () => {
    const propsComplete = getCompleteProps()
    const p = new Photo(propsComplete)

    p.removeLabel('Label 1')
    expect(p.labels.length).toEqual(2)

    p.removeLabel('Label 2')
    expect(p.labels.length).toEqual(1)

    p.removeLabel('not included')
    expect(p.labels.length).toEqual(1)

    p.removeLabel('not included as well')
    expect(p.labels.length).toEqual(1)
  })

  it('sets visibility state', () => {
    const propsComplete = getCompleteProps()
    const p = new Photo(propsComplete)

    p.visibility = 1
    expect(p.visibility).toEqual(1)

    expect(() => p.visibility(0)).toThrowError()
    expect(() => p.visibility(5)).toThrowError()
    expect(() => p.visibility(-1)).toThrowError()
    expect(() => p.visibility(null)).toThrowError()
    expect(() => p.visibility(undefined)).toThrowError()
  })

  it('sets originalCreationDate and transforms it to Date object', () => {
    const propsComplete = getCompleteProps()
    const p = new Photo(propsComplete)
    expect(p.originalCreationDate).toBeInstanceOf(Date)

    p.originalCreationDate = '2000-06-01T11:02:44'
    expect(p.originalCreationDate).toBeInstanceOf(Date)
    expect(p.originalCreationDate.getFullYear()).toEqual(2000)
    expect(p.originalCreationDate.getDate()).toEqual(1)
    expect(p.originalCreationDate.getMonth()).toEqual(5)
    expect(p.originalCreationDate.getHours()).toEqual(11)
    expect(p.originalCreationDate.getMinutes()).toEqual(2)
    expect(p.originalCreationDate.getSeconds()).toEqual(44)
  })

  it('actually saves the objects after calling save()', () => {
    photoService.save.mockReturnValue(Promise.resolve(true))

    const propsComplete = getCompleteProps()
    const p = new Photo(propsComplete)

    p.save()

    expect(photoService.save.mock.calls[0]).toBeDefined()
    expect(photoService.save.mock.calls[0][0]).toBe(p)
  })

  it('creates geo objects if not provided in constructor', () => {
    const p = new Photo({})

    expect(p.geo).toBeDefined()
    expect(p.geo.coordinates).toHaveLength(0)
    expect(p.lat).toBeUndefined()
    expect(p.long).toBeUndefined()

    p.lat = 42
    expect(p.lat).toEqual(42)

    p.long = 13
    expect(p.long).toEqual(13)
  })

  it('loads its solar data', async () => {
    photoService.getSolarData.mockReturnValue = Promise.resolve({ foo: 123 })
    const p = new Photo({
      id: 42,
    })
    await p.solarData
    expect(photoService.getSolarData).toBeCalledTimes(1)
  })

  it('throws an error if the solar data of a non-saved photo is requests', async () => {
    const p = new Photo()
    try {
      await p.solarData
      expect(false).toBeTruthy()
    } catch {
      expect(true).toBeTruthy()
    }
  })

  it('loads photo data', async () => {
    photoService.getPhotoData.mockImplementationOnce(() =>
      Promise.resolve(
        new PhotoData({
          make: 'Apple',
          id: 42,
          url: 'https://foobar.com/photoData/42/',
        }),
      ),
    )
    const p = new Photo({
      id: 1,
      url: 'https://foobar.com/photo/1/',
      photodata: 'https://foobar.com/photoData/42/',
    })
    const photoData = await p.photoData
    expect(photoData).toBeInstanceOf(PhotoData)
    expect(photoData).toHaveProperty('make', 'Apple')
  })

  it('loads weather state at creation date', async () => {
    weatherService.getWeather.mockImplementationOnce(() =>
      Promise.resolve(
        new WeatherState({
          id: 42,
          url: 'https://foobar.com/weatherState/42/',
        }),
      ),
    )

    const p = new Photo({
      id: 1,
      url: 'https://foobar.com/photo/1/',
      weatherAtCreation: 'https://foobar.com/weatherState/42/',
    })
    const weatherState = await p.weatherAtCreation
    expect(weatherState).toBeInstanceOf(WeatherState)
    expect(weatherService.getWeather).toHaveBeenCalledWith(42)
  })

  it('should inject data of external users into user model', () => {
    const photo = new Photo({
      user: null,
      externalUsername: 'flickr user 1',
      externalUrl: 'http://flickr.com',
      licenseUrl: 'http://creativecommons.org',
      licenseName: 'Attribution Licence',
    })

    expect(photo.user).not.toBeFalsy()
    expect(photo.user.id).toEqual(-1)
    expect(photo.user.displayName).toEqual('flickr user 1')
    expect(photo.user.url).toEqual('http://flickr.com')
    expect(photo.user.licenseUrl).toEqual('http://creativecommons.org')
    expect(photo.user.licenseName).toEqual('Attribution Licence')
  })

  it('should return external user if both user and externalUsername are set', () => {
    const photo = new Photo({
      user: {
        id: 1,
        displayName: 'internal user',
        url: 'http://photomaster.rocks',
      },
      externalUsername: 'flickr user 1',
      externalUrl: 'http://flickr.com',
    })

    expect(photo.user.displayName).toEqual('flickr user 1')
    expect(photo.user.url).toEqual('http://flickr.com')
  })

  it('should return a fallback user if photo has no user', () => {
    const photo = new Photo({
      user: null,
    })

    expect(photo.user).not.toBeFalsy()
    expect(photo.user.id).toEqual(-1)
  })
})
