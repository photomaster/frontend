import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cyan from '@material-ui/core/colors/cyan'
import lightGreen from '@material-ui/core/colors/lightGreen'
import red from '@material-ui/core/colors/red'
import amber from '@material-ui/core/colors/amber'
import grey from '@material-ui/core/colors/grey'

export default createMuiTheme({
  palette: {
    primary: {
      main: cyan[900],
      light: cyan[600],
      dark: cyan[900],
    },
    secondary: {
      main: lightGreen[900],
      light: lightGreen[600],
      dark: lightGreen[900],
    },
    error: red,
    warning: amber,
    darkBackground: grey[900],
  },
  typography: {
    h1: {
      fontFamily: "'Permanent Marker', sans-serif",
      fontSize: '3rem',
      textAlign: 'center',
      marginTop: '1rem',
      marginBottom: '1rem',
      wordBreak: 'break-word',
    },
    h2: {
      fontFamily: "'Permanent Marker', sans-serif",
      fontSize: '2.5rem',
      textAlign: 'center',
      marginTop: '1rem',
      marginBottom: '1rem',
      wordBreak: 'break-word',
    },
    h3: {
      fontFamily: "'Permanent Marker', sans-serif",
      fontSize: '2rem',
      textAlign: 'center',
      marginTop: '1rem',
      marginBottom: '1rem',
      wordBreak: 'break-word',
    },
    h4: {
      fontFamily: "'Permanent Marker', sans-serif",
      fontSize: '1.8rem',
      textAlign: 'left',
      marginTop: '1rem',
      marginBottom: '1rem',
      wordBreak: 'break-word',
    },
    h5: {
      wordBreak: 'break-word',
    },
  },
})
