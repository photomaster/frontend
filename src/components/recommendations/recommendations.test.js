import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import Recommendations from './recommendations'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'

const Photos = [
  {
    id: 1,
    user: {
      id: 10,
      url: '/user10',
      displayName: 'User10',
    },
    imageThumbnailTeaser: '/img1',
    name: 'Name1',
    description: 'desc1',
  },
  {
    id: 2,
    user: {
      id: 20,
      url: '/user20',
      displayName: 'User20',
    },
    imageThumbnailTeaser: '/img2',
    name: 'Name2',
    description: 'desc2',
  },
]

const mockTranslation = jest.fn(str => str)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer(photos = Photos, allowedAccess = true) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: {
            location: {
              access: allowedAccess,
              coordinates: [47.9476328, 14.8967515],
            },
          },
        })}
      >
        <Recommendations photos={photos} />
      </Provider>
    </Router>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Recommendations', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()
    const { container: containerLoading } = renderContainer(null)

    expect(container).toBeTruthy()
    expect(containerLoading).toBeTruthy()
  })
  describe('translates the corresponding text', () => {
    it('when access to location is given', () => {
      renderContainer()

      expect(mockTranslation).toHaveBeenCalledWith('recommendations.header')
      expect(mockTranslation).toHaveBeenCalledWith(
        'recommendations.introduction.specific',
      )
      expect(mockTranslation).toHaveBeenCalledWith(
        'recommendations.introduction.text',
      )
      expect(mockTranslation).not.toHaveBeenCalledWith(
        'recommendations.introduction.general',
      )
    })
    it('when no access to location is given', () => {
      renderContainer(Photos, false)

      expect(mockTranslation).toHaveBeenCalledWith('recommendations.header')
      expect(mockTranslation).toHaveBeenCalledWith(
        'recommendations.introduction.general',
      )
      expect(mockTranslation).toHaveBeenCalledWith(
        'recommendations.introduction.text',
      )
      expect(mockTranslation).not.toHaveBeenCalledWith(
        'recommendations.introduction.specific',
      )
    })
  })
  it('displays loading component when no photos are available', async () => {
    const { getByTestId } = renderContainer([])

    const loading = await waitForElement(() => getByTestId('loading'))

    expect(loading).toBeTruthy()
  })
  it('calls RecommendationCard for all given photos', async () => {
    const { getByTestId } = renderContainer()

    const img1 = await waitForElement(() =>
      getByTestId('recommendationCard--CardActions-1'),
    )
    const img2 = await waitForElement(() =>
      getByTestId('recommendationCard--CardActions-2'),
    )

    expect(img1).toBeTruthy()
    expect(img2).toBeTruthy()
  })
})
