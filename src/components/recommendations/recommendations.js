import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import baseStyles from 'styles/baseClasses'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import Loading from '../utilities/loading'
import RecommendationsGrid from '../recommendationCard/recommendationsGrid'
import './recommendations.scss'
import RecommendationCard from '../recommendationCard/recommendationCard'

export default function Recommendations(props) {
  const baseClasses = baseStyles()
  const { photos } = props
  const { t } = useTranslation()
  const [loading, setLoading] = useState(true)
  const userLocation = useSelector(state => state.currentUser.location)

  useEffect(() => {
    if (!photos || (photos && photos.length == 0)) setLoading(true)
    else setLoading(false)
  }, [photos])

  return (
    <React.Fragment>
      <Typography component="h2" variant="h2" className={baseClasses.headline}>
        {t('recommendations.header')}
      </Typography>
      <Box mb={2}>
        <Typography variant="body1">
          {`${
            userLocation && userLocation.access
              ? t('recommendations.introduction.specific')
              : t('recommendations.introduction.general')
          } 
            ${t('recommendations.introduction.text')}`}
        </Typography>
      </Box>
      {loading ? (
        <Loading />
      ) : (
        <RecommendationsGrid>
          {photos.map(photo => (
            <RecommendationCard
              key={photo.id}
              photo={photo}
              user={photo.user}
            />
          ))}
        </RecommendationsGrid>
      )}
    </React.Fragment>
  )
}

Recommendations.propTypes = {
  photos: PropTypes.array,
}
