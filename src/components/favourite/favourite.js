import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import IconButton from '@material-ui/core/IconButton'
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder'
import FavoriteIcon from '@material-ui/icons/Favorite'
import photoService from 'service/photoService'
import { useSelector, useDispatch } from 'react-redux'
import Tooltip from '@material-ui/core/Tooltip'
import { photosActions } from '../../actions'

export default function Favourite(props) {
  const dispatch = useDispatch()
  const { t } = useTranslation()
  const { photo } = props
  const [isFavourite, setIsFavourite] = useState(null)
  const [loading, setLoading] = useState(false)
  const loggedIn = useSelector(state => state.currentUser.loggedIn)
  const user = useSelector(state => state.currentUser.user)
  const favourites = useSelector(state => state.photos.favourites || null)

  useEffect(() => {
    if (loggedIn && favourites === null)
      dispatch(photosActions.updateFavourites())
  }, [])

  useEffect(() => {
    if (typeof isFavourite !== 'boolean' && favourites && !_ownPhoto()) {
      _isFavourite()
    }
  }, [favourites])

  async function _isFavourite() {
    try {
      if (favourites) {
        const favourite = favourites.find(f => f.photo.id === photo.id)
        if (favourite) setIsFavourite(true)
        else setIsFavourite(false)
      }
    } catch (e) {
      console.error('favourite', e)
    }
  }

  async function _handleClick() {
    if (loading) return
    setLoading(true)
    if (!isFavourite) {
      try {
        await photoService.addPhotoToFavorites(photo)
        dispatch(photosActions.updateFavourites())
        setIsFavourite(true)
      } catch (e) {
        console.error('set favourite', e)
      }
    } else {
      try {
        if (favourites) {
          const favPhoto = favourites.find(f => f.photo.id === photo.id)
          if (favPhoto) {
            await favPhoto.delete()
            dispatch(photosActions.updateFavourites())
            setIsFavourite(false)
          }
        }
      } catch (e) {
        console.error('delete favourite', e)
      }
    }
    setLoading(false)
  }

  const _ownPhoto = () => {
    if (loggedIn && user && photo && photo.user && user.id === photo.user.id) {
      return true
    }
    return false
  }

  return !loggedIn || _ownPhoto() || !favourites ? (
    ''
  ) : (
    <Tooltip
      title={
        isFavourite
          ? t('forms.photo.buttons.removeFavourite')
          : t('forms.photo.buttons.addFavourite')
      }
      placement="top"
    >
      <IconButton
        aria-label="heart"
        size="medium"
        onClick={_handleClick}
        data-testid={'favourite--button'}
      >
        {isFavourite ? (
          <FavoriteIcon
            fontSize="inherit"
            color="primary"
            data-testid={'favourite--iconFull'}
          />
        ) : (
          <FavoriteBorderIcon
            fontSize="inherit"
            color="primary"
            data-testid={'favourite--iconBorder'}
          />
        )}
      </IconButton>
    </Tooltip>
  )
}
