import React from 'react'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
  act,
} from '@testing-library/react'
import Favourite from './favourite'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import photoService from 'service/photoService'

const You = {
  id: 90,
  displayName: 'user',
  avatarImage: 'url',
}

const OtherUser = {
  id: 100,
  displayName: 'someone else',
  avatarImage: 'url',
}

const FavPhoto = {
  id: 10,
  url: '/photo10',
  user: OtherUser,
}

const NotFavPhoto = {
  id: 20,
  url: '/photo20',
  user: OtherUser,
}

const OwnPhoto = {
  id: 30,
  url: '/photo30',
  user: You,
}

const InvalidPhoto = {
  url: '/photo30',
  user: You,
}

const mockTranslation = jest.fn(str => str)
const mockDispatch = jest.fn()
const mockAddPhotoToFavorites = jest.fn(photo => Promise.resolve(photo))
const mockDelete = jest.fn(() => Promise.resolve())
const mockReject = jest.fn(() => Promise.reject())

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

jest.mock('service/photoService', () => jest.fn())
photoService.addPhotoToFavorites = mockAddPhotoToFavorites

function renderContainer(
  photo,
  loggedIn = true,
  loading = false,
  deleteFunc = mockDelete,
) {
  const baseState = {
    currentUser: {
      loggedIn: loggedIn,
      user: You,
    },
    photos: {
      loadingFavs: loading,
      favourites: [
        {
          dateAdded: '"2020-10-25T11:36:11.049156Z"',
          id: 1,
          photo: FavPhoto,
          delete: deleteFunc,
        },
      ],
    },
  }

  const result = render(
    <Provider store={createStore(rootReducer, baseState)}>
      <Favourite photo={photo} />
    </Provider>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Favourite', () => {
  it('renders without crashing', async () => {
    const { container: containerFav } = renderContainer(FavPhoto)
    const { container: containerNonFav } = renderContainer(NotFavPhoto)
    const { container: containerOwnPhoto } = renderContainer(OwnPhoto)

    expect(containerFav).toBeTruthy()
    expect(containerNonFav).toBeTruthy()
    expect(containerOwnPhoto).toBeTruthy()
  })
  it('does not return the heart-button and tooltip of own photos', async () => {
    const { container } = renderContainer(OwnPhoto)

    expect(container).toBeTruthy()
    expect(container).toBeEmpty()
  })
  describe('displays the correct icon', () => {
    it('of a favourite photo', async () => {
      const { getByTestId } = renderContainer(FavPhoto)
      const icon = await waitForElement(() =>
        getByTestId('favourite--iconFull'),
      )

      expect(icon).toBeTruthy()
    })
    it('of a non-favourite photo', async () => {
      const { getByTestId } = renderContainer(NotFavPhoto)
      const icon = await waitForElement(() =>
        getByTestId('favourite--iconBorder'),
      )

      expect(icon).toBeTruthy()
    })
  })
  describe('displays the correct tooltip', () => {
    it('of a favourite photo', async () => {
      const baseDom = renderContainer(FavPhoto)
      const button = await waitForElement(() =>
        baseDom.getByTestId('favourite--button'),
      )

      fireEvent.mouseOver(button)

      const tooltip = await waitForElement(() => baseDom.getByRole('tooltip'))

      expect(tooltip).toBeTruthy()
      expect(tooltip.textContent).toEqual('forms.photo.buttons.removeFavourite')
      expect(mockTranslation).toHaveBeenCalledWith(
        'forms.photo.buttons.removeFavourite',
      )
    })
    it('of a non-favourite photo', async () => {
      const baseDom = renderContainer(NotFavPhoto)
      const button = await waitForElement(() =>
        baseDom.getByTestId('favourite--button'),
      )

      fireEvent.mouseOver(button)

      const tooltip = await waitForElement(() => baseDom.getByRole('tooltip'))

      expect(tooltip).toBeTruthy()
      expect(tooltip.textContent).toEqual('forms.photo.buttons.addFavourite')
      expect(mockTranslation).toHaveBeenCalledWith(
        'forms.photo.buttons.addFavourite',
      )
    })
  })
  it('calls function to add photo to favourites on click', async () => {
    const { getByTestId } = renderContainer(NotFavPhoto)
    const button = await waitForElement(() => getByTestId('favourite--button'))

    act(() => {
      fireEvent.click(button)
    })

    const iconFull = await waitForElement(() =>
      getByTestId('favourite--iconFull'),
    )

    expect(iconFull).toBeTruthy()
    expect(mockDispatch).toHaveBeenCalledTimes(1)
    expect(mockAddPhotoToFavorites).toHaveBeenCalledWith(NotFavPhoto)
  })
  it('calls function to remove photo from favourites on click', async () => {
    const { getByTestId } = renderContainer(FavPhoto)
    const button = await waitForElement(() => getByTestId('favourite--button'))

    act(() => {
      fireEvent.click(button)
    })

    const iconBorder = await waitForElement(() =>
      getByTestId('favourite--iconBorder'),
    )

    expect(iconBorder).toBeTruthy()
    expect(mockDelete).toHaveBeenCalledTimes(1)
    expect(mockDispatch).toHaveBeenCalledTimes(1)
    expect(mockAddPhotoToFavorites).toHaveBeenCalledTimes(0)
  })
  describe('does not update if there is something wrong', () => {
    it('with a favourite photo', async () => {
      const { getByTestId } = renderContainer(FavPhoto, true, false, mockReject)
      const button = await waitForElement(() =>
        getByTestId('favourite--button'),
      )

      act(() => {
        fireEvent.click(button)
      })

      const iconFull = await waitForElement(() =>
        getByTestId('favourite--iconFull'),
      )

      expect(iconFull).toBeTruthy()
      expect(mockReject).toHaveBeenCalledTimes(1)
      expect(mockDispatch).toHaveBeenCalledTimes(0)
    })
    it('with a non favourite photo', async () => {
      photoService.addPhotoToFavorites = mockReject

      const { getByTestId } = renderContainer(NotFavPhoto)
      const button = await waitForElement(() =>
        getByTestId('favourite--button'),
      )

      act(() => {
        fireEvent.click(button)
      })

      const iconBorder = await waitForElement(() =>
        getByTestId('favourite--iconBorder'),
      )

      expect(iconBorder).toBeTruthy()
      expect(mockReject).toHaveBeenCalledWith(NotFavPhoto)
      expect(mockDispatch).toHaveBeenCalledTimes(0)
    })
  })
})
