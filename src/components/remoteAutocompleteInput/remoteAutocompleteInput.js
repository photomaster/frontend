import React, { useState, useEffect } from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import PropTypes from 'prop-types'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import CircularProgress from '@material-ui/core/CircularProgress'
import autocompleteService from 'service/autocompleteService'

const useStyles = makeStyles(() => ({
  input: {
    flex: 1,
  },
}))

export default function RemoteAutocompleteInput(props) {
  const {
    fieldName,
    value,
    placeholder,
    onChange,
    variant,
    size,
    label,
    error,
    helperText,
    filterOptions,
    autoFocus,
  } = props
  const classes = useStyles(props)
  const [loading, setLoading] = useState(false)
  const [options, setOptions] = useState([])
  const [internalValue, setInternalValue] = useState(value)

  useEffect(() => {
    setInternalValue(value)
  }, [value])

  useEffect(() => {
    if (loading) {
      return
    }
    ;(async () => {
      if (!internalValue) {
        setOptions([])
        return
      }

      setLoading(true)

      try {
        let fetchedOptions = await autocompleteService.getSuggestions(
          internalValue,
          fieldName,
        )
        if (typeof filterOptions === 'function') {
          fetchedOptions = filterOptions(fetchedOptions)
        }
        setOptions(fetchedOptions)
      } catch (e) {
        setOptions([])
      }
      setLoading(false)
    })()

    return () => setLoading(false)
  }, [internalValue])

  return (
    <Autocomplete
      className={classes.input}
      options={options}
      value={internalValue}
      freeSolo
      disableClearable
      onKeyPress={e => {
        if (e.key === 'Enter') {
          onChange(e, internalValue)
        }
      }}
      onInputChange={(e, val) => {
        setInternalValue(val)
      }}
      onChange={onChange}
      onBlur={e => {
        // user enters an unknown value.
        // check if selection has changed
        if (internalValue !== value) {
          // fire custom onChange event to notify parent component about change
          onChange(e, internalValue)
        }
      }}
      data-testid={'autocomplete'}
      renderInput={params => (
        <TextField
          {...params}
          autoFocus={autoFocus}
          variant={variant}
          placeholder={placeholder}
          error={error}
          helperText={helperText}
          size={size}
          label={label}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
          data-testid={'autocomplete-textfield'}
        />
      )}
    />
  )
}

RemoteAutocompleteInput.propTypes = {
  fieldName: PropTypes.string,
  size: PropTypes.string,
  label: PropTypes.string,
  variant: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  filterOptions: PropTypes.func,
  autoFocus: PropTypes.bool,
}

RemoteAutocompleteInput.defaultProps = {
  placeholder: '',
  size: 'medium',
  label: '',
  variant: 'standard',
  helperText: '',
  error: false,
  onChange: () => {},
  filterOptions: v => v,
  autoFocus: false,
}
