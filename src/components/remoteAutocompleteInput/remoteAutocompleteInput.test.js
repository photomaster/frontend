import React from 'react'
import {
  render,
  screen,
  cleanup,
  waitForElement,
  fireEvent,
  act,
} from '@testing-library/react'
import '@testing-library/jest-dom'
import RemoteAutocompleteInput from './remoteAutocompleteInput'
import autocompleteService from 'service/autocompleteService'

const Tag = 'Tree'

const mockOnChange = jest.fn((e, tag) => (e, tag))
const mockFilter = jest.fn(options => options)
const mockGetSuggestions = jest.fn(() =>
  Promise.resolve(['sug1', 'sug2', 'sug3']),
)
const mockReject = jest.fn(() => Promise.reject())

jest.mock('service/autocompleteService', () => jest.fn())
autocompleteService.getSuggestions = mockGetSuggestions

function renderContainer(
  error = false,
  useDefaults = false,
  filterOptions = mockFilter,
) {
  if (useDefaults) {
    const result = render(<RemoteAutocompleteInput />)
    return result
  } else {
    const result = render(
      <RemoteAutocompleteInput
        fieldName={'labels'}
        label={'label'}
        placeholder={'placeholder'}
        error={error}
        helperText={'helperText'}
        value={Tag}
        filterOptions={filterOptions}
        onChange={mockOnChange}
        variant={'filled'}
        size={'small'}
      />,
    )
    return result
  }
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('RemoteAutocompleteInput', () => {
  it('renders without crashing', async () => {
    const { container: containerValues } = renderContainer()
    const { container: containerDefaults } = renderContainer(false, true)

    expect(containerValues).toBeTruthy()
    expect(containerDefaults).toBeTruthy()
  })
  it('catches possible error in autocomplete service', async () => {
    autocompleteService.getSuggestions = mockReject

    const { container } = renderContainer()

    expect(mockReject).toHaveBeenCalledTimes(1)
    expect(container).toBeTruthy()
  })
  it('calls change function on keypress enter', async () => {
    const { getByTestId } = renderContainer()
    const autocomplete = await waitForElement(() => getByTestId('autocomplete'))

    act(() => {
      fireEvent.keyPress(autocomplete, {
        keyCode: 'Enter',
        key: 'Enter',
        code: 13,
        charCode: 13,
      })
    })

    expect(mockOnChange).toHaveBeenCalledTimes(1)
  })
  it('does not call change function on keypress space', async () => {
    const { getByTestId } = renderContainer()
    const autocomplete = await waitForElement(() => getByTestId('autocomplete'))

    act(() => {
      fireEvent.keyPress(autocomplete, {
        keyCode: 'Space',
        key: 'Space',
        code: 32,
        charCode: 32,
      })
    })

    expect(mockOnChange).toHaveBeenCalledTimes(0)
  })
  it('calls change function on blur', async () => {
    const { getByTestId } = renderContainer()
    const autocomplete = await waitForElement(() => getByTestId('autocomplete'))
    const input = await waitForElement(() =>
      autocomplete.querySelector('input'),
    )

    act(() => {
      fireEvent.change(input, { target: { value: 'new' } })
    })

    fireEvent.blur(autocomplete)

    expect(mockOnChange).toHaveBeenCalledTimes(1)
  })
  it('does not call change function on blur when the value is the same', async () => {
    const { getByTestId } = renderContainer()
    const autocomplete = await waitForElement(() => getByTestId('autocomplete'))
    const input = await waitForElement(() =>
      autocomplete.querySelector('input'),
    )

    act(() => {
      fireEvent.change(input, { target: { value: Tag } })
      fireEvent.blur(autocomplete)
    })

    expect(mockOnChange).toHaveBeenCalledTimes(0)
  })
  it('requests suggestions when the value changes', async () => {
    autocompleteService.getSuggestions = mockGetSuggestions

    const { getByTestId } = renderContainer()
    const autocomplete = await waitForElement(() => getByTestId('autocomplete'))
    const input = await waitForElement(() =>
      autocomplete.querySelector('input'),
    )

    fireEvent.change(input, { target: { value: 'new' } })

    expect(mockGetSuggestions).toHaveBeenLastCalledWith('new', 'labels')
  })
  it('displays suggestions on input change', async () => {
    autocompleteService.getSuggestions = mockGetSuggestions

    const { getByTestId } = renderContainer()
    const autocomplete = await waitForElement(() => getByTestId('autocomplete'))
    const input = await waitForElement(() =>
      autocomplete.querySelector('input'),
    )

    expect(screen.queryByText('sug1')).not.toBeInTheDocument()
    expect(screen.queryByText('sug2')).not.toBeInTheDocument()
    expect(screen.queryByText('sug3')).not.toBeInTheDocument()

    fireEvent.change(input, { target: { value: 's' } })

    expect(screen.queryByText('sug1')).toBeInTheDocument()
    expect(screen.queryByText('sug2')).toBeInTheDocument()
    expect(screen.queryByText('sug3')).toBeInTheDocument()
  })
  describe('filters options', () => {
    it('with given function', async () => {
      autocompleteService.getSuggestions = mockGetSuggestions

      const { getByTestId } = renderContainer()
      const autocomplete = await waitForElement(() =>
        getByTestId('autocomplete'),
      )
      const input = await waitForElement(() =>
        autocomplete.querySelector('input'),
      )

      fireEvent.change(input, { target: { value: 'new' } })

      expect(mockFilter).toHaveBeenCalledWith(['sug1', 'sug2', 'sug3'])
    })
    it('with default function', async () => {
      autocompleteService.getSuggestions = mockGetSuggestions

      const { getByTestId } = renderContainer(false, true)
      const autocomplete = await waitForElement(() =>
        getByTestId('autocomplete'),
      )
      const input = await waitForElement(() =>
        autocomplete.querySelector('input'),
      )

      fireEvent.change(input, { target: { value: 'new' } })

      expect(mockFilter).not.toHaveBeenCalledWith(['sug1', 'sug2', 'sug3'])
    })
    it('does not crash if filterOptions is no function', async () => {
      autocompleteService.getSuggestions = mockGetSuggestions

      const { container, getByTestId } = renderContainer(false, false, null)
      const autocomplete = await waitForElement(() =>
        getByTestId('autocomplete'),
      )
      const input = await waitForElement(() =>
        autocomplete.querySelector('input'),
      )

      fireEvent.change(input, { target: { value: 'new' } })

      expect(container).toBeTruthy()
      expect(input).toBeTruthy()
      expect(mockFilter).not.toHaveBeenCalledWith(['sug1', 'sug2', 'sug3'])
    })
  })
})
