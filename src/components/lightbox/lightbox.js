import React from 'react'
import baseStyles from 'styles/baseClasses'
import makeStyles from '@material-ui/core/styles/makeStyles'
import PropTypes from 'prop-types'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import { useSelector, useDispatch } from 'react-redux'
import { applicationActions } from '../../actions'
import './lightbox.scss'

const useStyles = makeStyles(() => ({
  lightboxPaper: {
    '&:focus': {
      outline: 'none',
    },
  },
  photo_fullSize: {
    maxWidth: '100vw',
    maxHeight: '100vh',
    '&:hover': {
      cursor: 'zoom-out',
    },
  },
}))

export default function Lightbox(props) {
  const classes = useStyles(props)
  const baseClasses = baseStyles()
  const dispatch = useDispatch()
  const lightboxState = useSelector(state => state.applicationState.lightbox)

  const { id, url, ariaLabelled, ariaDescribed, alt } = props

  const handleClose = () => {
    dispatch(applicationActions.closeLightbox())
  }

  return (
    <React.Fragment>
      <Modal
        aria-labelledby={ariaLabelled || `Image ${id}`}
        aria-describedby={ariaDescribed || `Image ${id}`}
        className={`${baseClasses.alignItemsCenter} ${baseClasses.justifyContentCenter}`}
        open={lightboxState.open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
        data-testid={`lightbox--${id}--modal`}
      >
        <Fade in={lightboxState.open}>
          <div className={classes.lightboxPaper}>
            <img
              src={url}
              alt={alt || `${id}`}
              className={classes.photo_fullSize}
              onClick={handleClose}
              data-testid={`lightbox--${id}--img`}
            />
          </div>
        </Fade>
      </Modal>
    </React.Fragment>
  )
}

Lightbox.propTypes = {
  id: PropTypes.number.isRequired,
  url: PropTypes.string.isRequired,
  ariaLabelled: PropTypes.string,
  ariaDescribed: PropTypes.string,
  alt: PropTypes.string,
}
