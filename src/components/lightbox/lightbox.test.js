import React from 'react'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import Lightbox from './lightbox'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'

const Photo = {
  id: 1,
  url: '/url',
  ariaLabelled: 'aria labelled',
  ariaDescribed: 'aria described',
  alt: 'alt text',
}

const IncompletePhoto = {
  id: 1,
  url: '/url',
}

const mockDispatch = jest.fn()

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

function renderContainer(photo = Photo, lightboxOpen = true) {
  const result = render(
    <Provider
      store={createStore(rootReducer, {
        applicationState: {
          lightbox: {
            open: lightboxOpen,
          },
        },
      })}
    >
      <Lightbox
        id={photo.id}
        url={photo.url}
        ariaLabelled={photo.ariaLabelled}
        ariaDescribed={photo.ariaDescribed}
        alt={photo.alt}
      />
    </Provider>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Lightbox', () => {
  it('renders without crashing', async () => {
    const baseDOMPhoto = renderContainer()
    const baseDOMIncompletePhoto = renderContainer(IncompletePhoto)

    expect(baseDOMPhoto).toBeTruthy()
    expect(baseDOMIncompletePhoto).toBeTruthy()
  })
  it('renders correct image with attributes', async () => {
    const baseDOM = renderContainer()

    const img = await waitForElement(() =>
      baseDOM.getByTestId('lightbox--1--img'),
    )

    expect(img.getAttribute('src')).toEqual('/url')
    expect(img.getAttribute('alt')).toEqual('alt text')
  })
  it('closes lightbox on click on image', async () => {
    const baseDOM = renderContainer()

    const img = await waitForElement(() =>
      baseDOM.getByTestId('lightbox--1--img'),
    )

    fireEvent.click(img)

    expect(mockDispatch).toHaveBeenCalledTimes(1)
  })
  it('closes lightbox on click on backdrop', async () => {
    const baseDOM = renderContainer()

    const backdrop = await waitForElement(
      () =>
        baseDOM
          .getByTestId('lightbox--1--modal')
          .getElementsByTagName('div')[0],
    )

    fireEvent.click(backdrop)

    expect(mockDispatch).toHaveBeenCalledTimes(1)
  })
})
