import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'leaflet'
import { Marker } from 'react-leaflet'
import icon from '../../assets/icons/icon_256x256.svg'

export default function AnimatedMarker(props) {
  const { position } = props
  const photoIcon = new Icon({
    iconUrl: icon,
    iconSize: [25, 25],
    className: 'leaflet-custom-marker-icon',
  })

  const _handleAnimation = () => {
    const el = document.getElementsByClassName('leaflet-custom-marker-icon')[0]
    if (el) {
      el.style.animationPlayState = 'running'

      setTimeout(() => {
        el.style.animationPlayState = 'paused'
      }, 2000)
    }
  }

  return (
    <Marker position={position} icon={photoIcon} onadd={_handleAnimation()} />
  )
}

AnimatedMarker.propTypes = {
  position: PropTypes.array.isRequired,
}
