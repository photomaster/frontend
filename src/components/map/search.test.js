import React from 'react'
import { Map } from 'react-leaflet'
import { render, cleanup } from '@testing-library/react'
import Search from './search'
import i18nextConfig from '../../i18nTesting'
import { useTranslation } from 'react-i18next'

const mockTranslation = jest.fn(str => str)
const mockOn = jest.fn((_, fn) => fn())

jest.mock('react-i18next', () => ({
  ...jest.requireActual('react-i18next'),
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        language: 'en',
        changeLanguage: () => new Promise(() => {}),
        on: mockOn,
      },
    }
  },
}))

function renderContainer(variant) {
  const result = render(
    <Map>{variant ? <Search variant={variant} /> : <Search />}</Map>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Search', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()
    const { container: containerWithVariant } = renderContainer('bar')

    expect(container).toBeTruthy()
    expect(containerWithVariant).toBeTruthy()
  })
  it('is rendered in the correct variant', async () => {
    const { container: containerDefaultVariant } = renderContainer()
    const { container: containerBarVariant } = renderContainer('bar')
    const { container: containerButtonVariant } = renderContainer('button')

    expect(
      containerDefaultVariant.querySelector('.leaflet-geosearch-button'),
    ).toBeTruthy()
    expect(
      containerBarVariant.querySelector('.leaflet-geosearch-bar'),
    ).toBeTruthy()
    expect(
      containerButtonVariant.querySelector('.leaflet-geosearch-button'),
    ).toBeTruthy()
  })
  it('reacts to change of language', async () => {
    renderContainer()

    const { i18n } = useTranslation()
    i18n.changeLanguage('de')

    expect(mockOn).toHaveBeenCalledTimes(1)
  })
  it('translates the search label', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalledWith('forms.upload.map.search')
  })
})
