import React from 'react'
import { Map } from 'react-leaflet'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import AnimatedMarker from './animatedMarker'

let coordinates = [47.811195, 13.033229]
let marker = <AnimatedMarker position={coordinates} />

const resetMarker = () => {
  coordinates = [coordinates[0] + 1, coordinates[1] + 1]
  //marker = null
  marker = <AnimatedMarker position={coordinates} />
}

function renderContainer() {
  const result = render(
    <Map center={coordinates} zoom={12} onClick={resetMarker}>
      {marker}
    </Map>,
  )
  return result
}

afterEach(cleanup)
describe('AnimatedMarker', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('does not animate initial placement of marker', async () => {
    const { container } = renderContainer()
    const marker = await waitForElement(() => container.querySelector('img'))

    expect(marker).toBeTruthy()
    expect(marker.getAttribute('style')).not.toContain('animation-play-state')
  })
  xit('animates replacement of marker', async () => {
    const { container } = renderContainer()
    const mapContainer = container.firstChild
    const mapPane = mapContainer.firstChild
    const marker = await waitForElement(() => container.querySelector('img'))

    //resetMarker()
    fireEvent.click(mapPane)

    expect(marker.getAttribute('style')).toContain(
      'animation-play-state: running',
    )

    const sleep = m => new Promise(r => setTimeout(r, m))
    await sleep(2000)

    expect(marker.getAttribute('style')).toContain(
      'animation-play-state: paused',
    )
  })
})
