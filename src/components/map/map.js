/* eslint-disable react/no-this-in-sfc */
import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import { history } from 'helpers/history'
import { Map, Popup, TileLayer } from 'react-leaflet'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import './map.scss'
import { makeStyles } from '@material-ui/core'
import MapClusterComponent from 'components/mapCluster/mapCluster'
import userService from 'service/userService'
import { useSelector } from 'react-redux'
import Loading from '../utilities/loading'
import locationService from '../../service/locationService'
import Search from './search'

const useStyles = makeStyles(() => ({
  root: {
    maxWidth: 345,
  },
  marginTop: {
    marginTop: '100px',
  },
  media: {
    height: 140,
  },
  centerText: {
    textAlign: 'center',
  },
  textColor: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
}))

export default function LeafletMap() {
  const classes = useStyles()

  const [zoom, setZoom] = useState(12)
  const [locations, setLocations] = useState([])
  const [activePhoto, setActivePhoto] = useState(null)
  const [loading, setLoading] = useState(true)
  const [center, setCenter] = useState([47.811, 13.033])
  const userLocation = useSelector(state => state.currentUser.location)

  useEffect(() => {
    // eslint-disable-next-line no-unused-vars
    let mounted = true
    if (!userLocation) {
      userService.getUserLocation.then(pos => {
        setCenter(pos.position)
      })
    } else {
      setCenter(userLocation.coordinates)
    }

    return function cleanup() {
      mounted = false
    }
  }, [])

  async function fetchData(topLeft, bottomRight, zoomLevel) {
    try {
      setLocations(
        await locationService.getClusters(topLeft, bottomRight, zoomLevel),
      )
      setLoading(false)
    } catch (e) {
      if (e.name !== 'AbortError') {
        console.error(e)
      }
    }

    if (window) _resize()
  }

  function onClusterClick(cluster) {
    if (zoom >= 18) {
      history.push(
        `/photos/radius/${cluster.centroid.coordinates[1]}-${cluster.centroid.coordinates[0]}`,
      )
      return
    }

    setZoom(zoom + 1)
    setCenter([
      cluster.centroid.coordinates[1],
      cluster.centroid.coordinates[0],
    ])
  }

  function _getMarkers() {
    return locations.map(location => (
      <MapClusterComponent
        key={location.id}
        zoom={zoom}
        cluster={location}
        onPhotoClick={setActivePhoto}
        onClusterClick={onClusterClick}
      />
    ))
  }

  function onMoveend() {
    const topLeft = this.getBounds().getNorthWest()
    const bottomRight = this.getBounds().getSouthEast()

    setZoom(this.getZoom())
    fetchData(topLeft, bottomRight, this.getZoom())
  }

  function _resize() {
    window.dispatchEvent(new Event('resize'))
  }

  return (
    <Map
      className="map--index"
      minZoom={3}
      onMoveend={onMoveend}
      center={center}
      zoom={zoom}
    >
      <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
      <Search />
      {loading ? <Loading /> : _getMarkers()}
      {activePhoto && (
        <Popup
          position={[
            activePhoto.geo.coordinates[1],
            activePhoto.geo.coordinates[0],
          ]}
          onClose={() => {
            setActivePhoto(null)
          }}
        >
          <Card className={classes.root}>
            <CardActionArea>
              <NavLink
                style={{ textDecoration: 'none' }}
                activeClassName="active"
                to={`/photos/${activePhoto.id}`}
              >
                <CardMedia
                  className={classes.media}
                  image={activePhoto.imageThumbnailTeaser}
                  title={activePhoto.name}
                />
                <CardContent>
                  <Typography
                    variant="h6"
                    component="p"
                    className={`${classes.centerText} ${classes.textColor}`}
                  >
                    {activePhoto.name}
                  </Typography>
                </CardContent>
              </NavLink>
            </CardActionArea>
          </Card>
        </Popup>
      )}
    </Map>
  )
}
