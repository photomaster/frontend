import { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useLeaflet } from 'react-leaflet'
import { OpenStreetMapProvider, GeoSearchControl } from 'leaflet-geosearch'
import '../../../node_modules/leaflet-geosearch/assets/css/leaflet.css'
import { useTranslation } from 'react-i18next'

export default function Search(props) {
  const { t, i18n } = useTranslation()
  const { map } = useLeaflet()
  const provider = new OpenStreetMapProvider({
    params: {
      'accept-language': i18n.language,
    },
  })

  const [curLng, setCurLng] = useState(i18n.language)

  i18n.on('languageChanged', function handleLanguageChange(lng) {
    if (curLng != i18n.language) {
      setCurLng(lng)
    }
  })

  useEffect(() => {
    const searchControl = new GeoSearchControl({
      provider: provider,
      style: props.variant ? props.variant : 'button',
      showMarker: false,
      autoClose: true,
      searchLabel: t('forms.upload.map.search'),
    })

    map.addControl(searchControl)
    return () => map.removeControl(searchControl)
  }, [props, curLng])

  return null
}

Search.propTypes = {
  variant: PropTypes.oneOf(['bar', 'button']),
}
