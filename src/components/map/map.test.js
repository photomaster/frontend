import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  act,
  fireEvent,
} from '@testing-library/react'
import LeafletMap from './map'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'
import i18nextConfig from '../../i18nTesting'
import locationService from 'service/locationService'
import userService from 'service/userService'

const Locations = [
  {
    centroid: {
      coordinates: [47.8115, 13.033],
      type: 'Point',
    },
    id: 1,
    photos: [
      {
        id: 1,
        imageThumbnailTeaser: '/url',
        name: 'name',
        geo: {
          coordinates: [47.7563, 13.066499],
        },
      },
    ],
    size: 1,
  },
  {
    centroid: {
      coordinates: [47.811, 13.0335],
      type: 'Point',
    },
    id: 2,
    photos: [],
    size: 51,
  },
]

const mockGetClusters = jest.fn((topLeft, bottomRight, zoom) =>
  Promise.resolve(Locations),
)
const mockGetUserLocation = jest.fn(() =>
  Promise.resolve({ access: true, position: [47.811, 13.033] }),
)

jest.mock('service/locationService', () => jest.fn())
locationService.getClusters = mockGetClusters
jest.mock('service/userService', () => jest.fn())
userService.getUserLocation = mockGetUserLocation

function renderContainer(locationAvailable = true) {
  const result = render(
    <Router>
      <Provider
        store={createStore(rootReducer, {
          currentUser: {
            location: locationAvailable
              ? {
                  access: true,
                  coordinates: [47.7563, 13.066498],
                }
              : null,
          },
        })}
      >
        <LeafletMap />
      </Provider>
    </Router>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Map', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  xit('requests user location if not available', async () => {
    userService.getUserLocation = mockGetUserLocation
    renderContainer(false)

    expect(mockGetUserLocation).toHaveBeenCalledTimes(1)
  })
  it('requests locations on moveend', async () => {
    const { container, findByTestId } = renderContainer()
    const markerPhoto = await waitForElement(() =>
      container.querySelector('img'),
    )
    const markerBubble = await waitForElement(() =>
      findByTestId('mapCluster--bubble--51'),
    )

    act(() => {
      fireEvent.dragStart(markerPhoto)
      fireEvent.dragEnter(markerBubble)
      fireEvent.dragOver(markerBubble)
      fireEvent.dragLeave(markerPhoto)
      fireEvent.dragExit(markerPhoto)
    })

    expect(mockGetClusters).toHaveBeenCalledTimes(1)
  })
  it('does not crash if getClusters fails', async () => {
    const mockReject = jest.fn((topLeft, bottomRight, zoom) =>
      Promise.reject({ name: 'error' }),
    )
    locationService.getClusters.mockImplementationOnce(mockReject)
    const { container } = renderContainer()

    expect(container).toBeTruthy()
    expect(mockReject).toHaveBeenCalledTimes(1)
  })
  it('zooms closer to cluster on click', async () => {
    const { findByTestId, getByRole } = renderContainer()
    const tile = await waitForElement(() => getByRole('presentation'))
    const marker = await waitForElement(() =>
      findByTestId('mapCluster--bubble--51'),
    )
    const tileInitialSrc = tile.getAttribute('src')

    act(() => {
      fireEvent.click(marker)
    })

    expect(tile.getAttribute('src')).not.toEqual(tileInitialSrc)
  })
  it('displays popup after click on photo icon', async () => {
    const { container } = renderContainer()
    const markerPhoto = await waitForElement(() =>
      container.querySelector('img.leaflet-marker-icon'),
    )
    const popupPane = await waitForElement(() =>
      container.querySelector('.leaflet-popup-pane'),
    )

    expect(popupPane.firstElementChild).toBeFalsy()

    act(() => {
      fireEvent.click(markerPhoto)
    })

    expect(popupPane.firstElementChild).toBeTruthy()
  })
})
