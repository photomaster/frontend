import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import i18nextConfig from '../../i18nTesting'
import Footer from './footer'

function renderContainer() {
  const result = render(
    <Router>
      <Footer />
    </Router>,
  )
  return result
}

afterEach(cleanup)
describe('Footer', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('displays the current year', async () => {
    const { container } = renderContainer()
    const buildString = await waitForElement(() =>
      container.querySelector('span'),
    )

    expect(buildString.textContent).toContain(new Date().getFullYear())
  })
  it('contains link to homepage', async () => {
    const { getByTestId } = renderContainer()
    const link = await waitForElement(() => getByTestId('link--/'))

    expect(link).toBeTruthy()
    expect(link.getAttribute('href')).toEqual('/')
  })
  it('contains link to feedback page', async () => {
    const { getByTestId } = renderContainer()
    const link = await waitForElement(() => getByTestId('link--/feedback'))

    expect(link).toBeTruthy()
    expect(link.getAttribute('href')).toEqual('/feedback')
  })
  it('contains link to privacy page', async () => {
    const { getByTestId } = renderContainer()
    const link = await waitForElement(() => getByTestId('link--/privacy'))

    expect(link).toBeTruthy()
    expect(link.getAttribute('href')).toEqual('/privacy')
  })
  it('contains link to conditions page', async () => {
    const { getByTestId } = renderContainer()
    const link = await waitForElement(() => getByTestId('link--/conditions'))

    expect(link).toBeTruthy()
    expect(link.getAttribute('href')).toEqual('/conditions')
  })
})
