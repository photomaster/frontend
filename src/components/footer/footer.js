import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import logoFullText from '../../assets/icons/icon_text-light.svg'
import './footer.scss'

export default function Footer() {
  const { t } = useTranslation()

  const Link = props => {
    const { to, exact, additionalClasses, children } = props
    return (
      <NavLink
        to={to}
        exact={exact}
        className={additionalClasses || ''}
        activeClassName={!exact ? 'active' : ''}
        data-testid={`link--${to}`}
      >
        {children}
      </NavLink>
    )
  }

  Link.propTypes = {
    to: PropTypes.string.isRequired,
    exact: PropTypes.bool,
    additionalClasses: PropTypes.string,
    children: PropTypes.node,
  }

  Link.defaultProps = {
    exact: false,
  }

  return (
    <footer className="footer">
      <nav className="footer--nav">
        <div className="logo">
          <Link to={'/'} exact>
            <img
              src={logoFullText}
              className="footer--logo"
              alt="PhotoMaster"
            />
          </Link>
        </div>
        <div className="links">
          <Link to="/feedback" additionalClasses="footer--menuItem">
            {t('pages.feedback')}
          </Link>
          <Link to="/privacy" additionalClasses="footer--menuItem">
            {t('pages.privacy')}
          </Link>
          <Link to="/conditions" additionalClasses="footer--menuItem">
            {t('pages.conditions')}
          </Link>
        </div>
      </nav>
      <span>&copy;{new Date().getFullYear()} | dev-build</span>
    </footer>
  )
}
