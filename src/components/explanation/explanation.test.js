import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import Explanation from './explanation'

const mockTranslation = jest.fn(str => str)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer() {
  const result = render(
    <Router>
      <Explanation />
    </Router>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Explanation', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('renders header', async () => {
    const { findByTestId } = renderContainer()
    const headline = await waitForElement(() =>
      findByTestId('explanation--headline'),
    )

    expect(headline.textContent).toEqual('explanation.overview.header')
  })
  it('renders introduction text', async () => {
    const { getByTestId } = renderContainer()
    const text = await waitForElement(() =>
      getByTestId('explanation--box').querySelector('p'),
    )

    expect(text.textContent).toEqual('explanation.overview.introduction')
    expect(mockTranslation).toHaveBeenCalledWith(
      'explanation.overview.introduction',
    )
  })
  it('renders 5 ExplanationLinks', async () => {
    const { findAllByTestId } = renderContainer()
    const links = await waitForElement(() => findAllByTestId('explanationLink'))

    expect(links.length).toEqual(5)
  })
})
