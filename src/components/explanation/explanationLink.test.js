import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import ExampleIcon from '@material-ui/icons/CloudOutlined'
import ExplanationLink from './explanationLink'

const mockTranslation = jest.fn(str => str)

const LinkWithAllAttributes = {
  icon: ExampleIcon,
  color: 'secondary',
  text: 'text all attributes',
  link: '/url',
  additionalClasses: 'someClass',
}

const LinkWithSomeAttributes = {
  icon: ExampleIcon,
  color: null,
  text: 'text some attributes',
  link: '/url',
  additionalClasses: null,
}

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer(link) {
  const result = render(
    <Router>
      <ExplanationLink
        Icon={link.icon}
        color={link.color}
        text={link.text}
        link={link.link}
        additionalClasses={link.additionalClasses}
      />
    </Router>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('ExplanationLink', () => {
  it('renders without crashing', async () => {
    const { container: containerAll } = renderContainer(LinkWithAllAttributes)
    const { container: containerSome } = renderContainer(LinkWithSomeAttributes)

    expect(containerAll).toBeTruthy()
    expect(containerSome).toBeTruthy()
  })
  it('renders an icon', async () => {
    const { container } = renderContainer(LinkWithAllAttributes)
    const icon = await waitForElement(() =>
      container
        .querySelector('[data-testid="explanationLink"]')
        .querySelector('svg'),
    )

    expect(icon).toBeTruthy()
  })
  it('displays the text without translation', async () => {
    const { getByTestId } = renderContainer(LinkWithAllAttributes)
    const paragraph = await waitForElement(() =>
      getByTestId('explanationLink').querySelector('p'),
    )

    expect(paragraph.textContent).toEqual('text all attributes')
    expect(mockTranslation).not.toHaveBeenCalledWith('text all attributes')
  })
  it('points to the correct url', async () => {
    const { getByTestId } = renderContainer(LinkWithAllAttributes)
    const link = await waitForElement(() =>
      getByTestId('explanationLink').querySelector('a'),
    )

    expect(link.getAttribute('href')).toEqual('/url')
  })
  it('sets additional classes', async () => {
    const { getByTestId } = renderContainer(LinkWithAllAttributes)
    const div = await waitForElement(() => getByTestId('explanationLink'))

    expect(div.getAttribute('class')).toContain('someClass')
  })
})
