import React from 'react'
import { NavLink } from 'react-router-dom'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'

export default function ExplanationLink(props) {
  const baseClasses = baseStyles()
  const { Icon, color, text, link, additionalClasses } = props
  const { t } = useTranslation()

  return (
    <div
      className={`${baseClasses.padding2} 
        ${baseClasses.flexWrap}
        ${baseClasses.justifyContentCenter}
        ${baseClasses.alignItemsBaseline}
        ${baseClasses.alignContentBetween}
        ${additionalClasses || ''}`}
      data-testid={'explanationLink'}
    >
      <Icon style={{ fontSize: 50 }} color={color || 'primary'} />
      <Typography component="p" variant="subtitle2">
        {text}
      </Typography>
      <NavLink
        to={link}
        style={{ textDecoration: 'none' }}
        className={baseClasses.marginTop1}
      >
        <Button variant="outlined" color="primary">
          {t('explanation.overview.link')}
        </Button>
      </NavLink>
    </div>
  )
}

ExplanationLink.propTypes = {
  Icon: PropTypes.any.isRequired,
  color: PropTypes.oneOf([
    'inherit',
    'primary',
    'secondary',
    'action',
    'error',
    'disabled',
    null,
  ]),
  text: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  additionalClasses: PropTypes.string,
}
