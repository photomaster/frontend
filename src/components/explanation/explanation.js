import React from 'react'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core'
import Box from '@material-ui/core/Box'
import ImageSearchOutlinedIcon from '@material-ui/icons/ImageSearchOutlined'
import EmojiObjectsOutlinedIcon from '@material-ui/icons/EmojiObjectsOutlined'
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined'
import PhotoCameraOutlinedIcon from '@material-ui/icons/PhotoCameraOutlined'
import CloudOutlinedIcon from '@material-ui/icons/CloudOutlined'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import ExplanationLink from './explanationLink'

const useStyles = makeStyles(theme => ({
  box_container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(20),
      height: theme.spacing(21),
    },
  },
}))

export default function Explanation() {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const { t } = useTranslation()

  return (
    <div data-testid={'explanation'}>
      <Typography
        component="h2"
        variant="h2"
        className={baseClasses.headline}
        data-testid={'explanation--headline'}
      >
        {t('explanation.overview.header')}
      </Typography>
      <Box mb={2} data-testid={'explanation--box'}>
        <Typography variant="body1">
          {t('explanation.overview.introduction')}
        </Typography>
      </Box>
      <div className={classes.box_container}>
        <ExplanationLink
          Icon={LocationOnOutlinedIcon}
          text={t('explanation.topics.locations.header')}
          link={`/info#locations`}
        />
        <ExplanationLink
          Icon={ImageSearchOutlinedIcon}
          text={t('explanation.topics.explore.header')}
          link={`/info#explore`}
        />
        <ExplanationLink
          Icon={CloudOutlinedIcon}
          text={t('explanation.topics.external.header')}
          link={`/info#external`}
        />
        <ExplanationLink
          Icon={EmojiObjectsOutlinedIcon}
          text={t('explanation.topics.inspiration.header')}
          link={`/info#inspiration`}
        />
        {/* <ExplanationLink
          Icon={FavoriteBorderOutlinedIcon}
          text={t('explanation.topics.recommendations.header')}
          link={`/info#recommendations`}
        /> */}
        <ExplanationLink
          Icon={PhotoCameraOutlinedIcon}
          text={t('explanation.topics.share.header')}
          link={`/info#share`}
        />
      </div>
    </div>
  )
}
