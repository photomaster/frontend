import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import MaterialChip from '@material-ui/core/Chip'
import baseStyles from 'styles/baseClasses'
import './chip.scss'

export default function Chip(props) {
  const baseClasses = baseStyles()
  const {
    label,
    variant,
    size,
    link,
    clickable,
    onClick,
    onDelete,
    color,
    additionalClasses,
  } = props

  const SingleChip = () => {
    return (
      <MaterialChip
        label={label}
        variant={variant}
        size={size}
        clickable={clickable}
        onClick={onClick}
        onDelete={onDelete}
        color={color}
        className={`${baseClasses.margin05} ${additionalClasses}`}
        data-testid={'chip'}
      />
    )
  }

  if (link) {
    return (
      <NavLink
        style={{ textDecoration: 'none' }}
        to={link}
        key={`link-to-${label}`}
        data-testid={'chip--link'}
      >
        <SingleChip />
      </NavLink>
    )
  }
  return <SingleChip />
}

Chip.propTypes = {
  label: PropTypes.string.isRequired,
  variant: PropTypes.oneOf(['default', 'outlined']),
  size: PropTypes.oneOf(['small', 'medium']),
  link: PropTypes.string,
  clickable: PropTypes.bool,
  onClick: PropTypes.func,
  onDelete: PropTypes.func,
  color: PropTypes.oneOf(['default', 'primary', 'secondary']),
  additionalClasses: PropTypes.string,
}

Chip.defaultProps = {
  variant: 'default',
  size: 'small',
  clickable: false,
  color: 'default',
}
