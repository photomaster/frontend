import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import Chip from './chip'

const mockOnClick = jest.fn()
const mockOnDelete = jest.fn()

const LinkChip = {
  label: 'Link Label',
  variant: 'default',
  size: 'small',
  link: '/url',
  clickable: true,
  onClick: null,
  onDelete: null,
  color: 'default',
  additionalClasses: 'linkClass',
}

const InteractiveChip = {
  label: 'Interactive Label',
  variant: 'outlined',
  size: 'medium',
  link: null,
  clickable: true,
  onClick: mockOnClick,
  onDelete: mockOnDelete,
  color: 'primary',
  additionalClasses: 'interactiveClass',
}

function renderContainer(chip) {
  const result = render(
    <Router>
      <Chip
        label={chip.label}
        variant={chip.variant}
        size={chip.size}
        link={chip.link}
        clickable={chip.clickable}
        onClick={chip.onClick}
        onDelete={chip.onDelete}
        color={chip.color}
        additionalClasses={chip.additionalClasses}
      />
    </Router>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('Chip', () => {
  it('renders without crashing', async () => {
    const { container: linkContainer } = renderContainer(LinkChip)
    const { container: intContainer } = renderContainer(InteractiveChip)

    expect(linkContainer).toBeTruthy()
    expect(intContainer).toBeTruthy()
  })
  it('displays correct label', async () => {
    const { getByTestId } = renderContainer(LinkChip)
    const chip = await waitForElement(() => getByTestId('chip'))

    expect(chip.querySelector('span').textContent).toEqual('Link Label')
  })
  it('wraps chip inside a link', async () => {
    const { getByTestId } = renderContainer(LinkChip)
    const link = await waitForElement(() => getByTestId('chip--link'))

    expect(link).toBeTruthy()
    expect(link.getAttribute('href')).toEqual('/url')
  })
  it('calls onClick function correctly', async () => {
    const { getByTestId } = renderContainer(InteractiveChip)
    const chip = await waitForElement(() => getByTestId('chip'))

    fireEvent.click(chip)

    expect(mockOnClick).toHaveBeenCalledTimes(1)
  })
  it('calls onDelete function correctly', async () => {
    const { getByTestId } = renderContainer(InteractiveChip)
    const deleteIcon = await waitForElement(() =>
      getByTestId('chip').querySelector('svg'),
    )

    fireEvent.click(deleteIcon)

    expect(mockOnDelete).toHaveBeenCalledTimes(1)
  })
})
