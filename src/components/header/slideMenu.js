import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import AccountCircle from '@material-ui/icons/AccountCircle'
import KeyIcon from '@material-ui/icons/VpnKey'
import PeopleAltIcon from '@material-ui/icons/PeopleAlt'
import EditIcon from '@material-ui/icons/Edit'
import DoorIcon from '@material-ui/icons/MeetingRoom'
import FavoriteIcon from '@material-ui/icons/Favorite'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { userActions } from '../../actions'
import ProfileMenuEntry from './profileMenuEntry'
import LanguageSwitcher from './languageSwitcher'
import './header.scss'

export default function SlideMenu(props) {
  const dispatch = useDispatch()
  const { t } = useTranslation()
  const { menuOpen, setMenuOpen } = props
  const currentUser = useSelector(state => state.currentUser)

  useEffect(() => {
    window.addEventListener('keydown', _keyDown)
    return () => {
      window.removeEventListener('keydown', _keyDown)
    }
  })

  const _handleLogout = () => {
    dispatch(userActions.logout())
  }

  const _closeMenu = () => {
    setMenuOpen(false)
  }

  const _keyDown = ({ key }) => {
    if (key === 'Escape') {
      _closeMenu()
    }
  }

  return (
    <React.Fragment>
      <div
        role={'navigation'}
        className={`backdrop ${menuOpen ? 'show' : 'hide'}`}
        onClick={_closeMenu}
        data-testid={'slideMenu--backdrop'}
      />
      <div className={`profileMenu ${menuOpen ? 'show' : 'hide'}`}>
        <ul className="profileMenu--list" onClick={_closeMenu}>
          {currentUser.loggedIn ? (
            <React.Fragment>
              <ProfileMenuEntry
                Icon={AccountCircle}
                text={t('header.profile')}
                to={`/users/${currentUser.user.id}`}
                title={t('header.profile')}
              />
              <ProfileMenuEntry
                Icon={EditIcon}
                text={t('header.edit')}
                to={`/users/${currentUser.user.id}/edit`}
                title={t('header.edit')}
              />
              <ProfileMenuEntry
                Icon={FavoriteIcon}
                text={t('header.favourites')}
                to={`/favorites`}
                title={t('header.favourites')}
              />
              <ProfileMenuEntry
                Icon={DoorIcon}
                text={t('pages.logout')}
                func={_handleLogout}
                title={t('pages.logout')}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <ProfileMenuEntry
                Icon={KeyIcon}
                text={t('pages.login')}
                to={`/login`}
                title={t('pages.login')}
              />
              <ProfileMenuEntry
                Icon={PeopleAltIcon}
                text={t('forms.register.button')}
                to={`/register`}
                title={t('forms.register.header')}
              />
            </React.Fragment>
          )}
        </ul>
        <LanguageSwitcher />
      </div>
    </React.Fragment>
  )
}

SlideMenu.propTypes = {
  menuOpen: PropTypes.bool.isRequired,
  setMenuOpen: PropTypes.func.isRequired,
}
