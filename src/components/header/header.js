import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import AddAPhoto from '@material-ui/icons/AddAPhoto'
import AccountCircle from '@material-ui/icons/AccountCircle'
import SearchIcon from '@material-ui/icons/Search'
import CloseIcon from '@material-ui/icons/Close'
import { makeStyles } from '@material-ui/core'
import { useSelector } from 'react-redux'
import useDocumentScrollThrottled from 'helpers/scroll'
import { useTranslation } from 'react-i18next'
import Avatar from 'components/utilities/avatar'
import logoIconOnly from '../../assets/icons/icon_256x256.svg'
import logoFullText from '../../assets/icons/icon_text.svg'
import SlideMenu from './slideMenu'
import './header.scss'

const useStyles = makeStyles(theme => ({
  icon: {
    height: '2rem',
    width: 'auto',
    fill: theme.palette.primary.main,
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    backgroundColor: theme.palette.primary.main,
  },
  ul: {
    listStyle: 'none',
    margin: 0,
    paddingInlineStart: 0,
  },
}))

export default function Header() {
  const classes = useStyles()
  const { t } = useTranslation()

  const currentUser = useSelector(state => state.currentUser)
  const [smallerHeader, setSmallerHeader] = useState(false)
  const [menuOpen, setMenuOpen] = useState(false)

  const MIN_SCROLL = 100
  const TIMEOUT_DELAY = 400

  const currentStyle = smallerHeader ? 'small' : 'large'
  const currentLogo = smallerHeader ? logoIconOnly : logoFullText

  // TODO: GlobalEventHandlers.onscroll?

  useDocumentScrollThrottled(callbackData => {
    // eslint-disable-next-line no-unused-vars
    const { previousScrollTop, currentScrollTop } = callbackData
    const isMinimumScrolled = currentScrollTop > MIN_SCROLL

    setTimeout(() => {
      setSmallerHeader(isMinimumScrolled)
    }, TIMEOUT_DELAY)
  })

  const _toggleMenu = () => {
    setMenuOpen(!menuOpen)
  }

  const HeaderLink = props => {
    const { to, title, exact, onClick, children } = props
    return (
      <li>
        <NavLink
          to={to || ''}
          title={title}
          onClick={onClick || null}
          className={!exact ? 'header--menuItem' : ''}
          activeClassName={!exact ? 'active' : ''}
          data-testid={`header--${to || 'func'}`}
        >
          {children}
        </NavLink>
      </li>
    )
  }

  HeaderLink.propTypes = {
    to: PropTypes.string,
    title: PropTypes.string.isRequired,
    exact: PropTypes.bool.isRequired,
    onClick: PropTypes.func,
    children: PropTypes.node,
  }

  return (
    <React.Fragment>
      <header className={`header ${currentStyle}`}>
        <nav className={`header--nav ${currentStyle}`}>
          <ul className={`flex-left logo-full ${classes.ul}`}>
            <HeaderLink to={'/'} title={t('header.homepage')} exact>
              <img
                src={currentLogo}
                className={`header--logo ${currentStyle}`}
                alt="PhotoMaster"
              />
            </HeaderLink>
          </ul>
          <ul className={`flex-left logo-small ${classes.ul}`}>
            <HeaderLink to={'/'} title={t('header.homepage')} exact>
              <img
                src={logoIconOnly}
                className={`header--logo ${currentStyle}`}
                alt="PhotoMaster"
              />
            </HeaderLink>
          </ul>
          <ul className={`flex-right ${classes.ul}`}>
            <HeaderLink to={'/search'} title={t('header.search')} exact={false}>
              <SearchIcon className={classes.icon} />
            </HeaderLink>
            <HeaderLink to="/upload" title={t('header.add')} exact={false}>
              <AddAPhoto className={classes.icon} />
            </HeaderLink>
            <HeaderLink
              title={t('header.menu')}
              exact={false}
              onClick={e => {
                e.preventDefault()
                _toggleMenu()
              }}
            >
              {menuOpen ? (
                <CloseIcon
                  className={classes.icon}
                  data-testid={'header--close'}
                />
              ) : currentUser.user && currentUser.user.avatarImage ? (
                <Avatar
                  alt={currentUser.user.displayName}
                  src={currentUser.user.avatarImage}
                  additionalClasses={`${classes.avatar}`}
                />
              ) : (
                <AccountCircle className={classes.icon} />
              )}
            </HeaderLink>
          </ul>
        </nav>
      </header>
      <SlideMenu menuOpen={menuOpen} setMenuOpen={setMenuOpen} />
    </React.Fragment>
  )
}
