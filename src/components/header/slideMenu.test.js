import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import SlideMenu from './slideMenu'
import i18nextConfig from '../../i18nTesting'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'

const mockSetMenuOpen = jest.fn()
const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

function renderContainer(open, loggedIn) {
  const result = render(
    <Provider
      store={createStore(rootReducer, {
        currentUser: { loggedIn: loggedIn, user: { id: 1 } },
      })}
    >
      <Router>
        <SlideMenu menuOpen={open} setMenuOpen={mockSetMenuOpen} />
      </Router>
    </Provider>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('SlideMenu', () => {
  it('renders without crashing', async () => {
    const { container: containerOpen } = renderContainer(true, false)
    const { container: containerClosed } = renderContainer(false, false)

    expect(containerOpen).toBeTruthy()
    expect(containerClosed).toBeTruthy()
  })
  it('closes menu on escape key only', async () => {
    renderContainer(true, false)

    fireEvent.keyDown(window, { keyCode: 'Enter', key: 'Enter' })
    expect(mockSetMenuOpen).toHaveBeenCalledTimes(0)

    fireEvent.keyDown(window, { keyCode: 'Escape', key: 'Escape' })
    expect(mockSetMenuOpen).toHaveBeenCalledWith(false)
  })
  it('closes menu on click on backdrop', async () => {
    const { getByTestId } = renderContainer(true, false)
    const backdrop = await waitForElement(() =>
      getByTestId('slideMenu--backdrop'),
    )

    fireEvent.click(backdrop)

    expect(mockSetMenuOpen).toHaveBeenCalledWith(false)
  })
  it('initiates logout on click', async () => {
    const { getByTestId } = renderContainer(true, true)
    const logoutLink = await waitForElement(() =>
      getByTestId('profileMenuEntry--func'),
    )

    fireEvent.click(logoutLink)

    expect(mockDispatch).toHaveBeenCalledTimes(1)
  })
  describe('displays correct entries', () => {
    it('if user is logged in', async () => {
      const { container: containerLoggedIn, getByTestId } = renderContainer(
        true,
        true,
      )
      const profileLink = await waitForElement(() =>
        getByTestId('profileMenuEntry--/users/1'),
      )
      const profileEditLink = await waitForElement(() =>
        getByTestId('profileMenuEntry--/users/1/edit'),
      )
      const favoritesLink = await waitForElement(() =>
        getByTestId('profileMenuEntry--/favorites'),
      )
      const logoutLink = await waitForElement(() =>
        getByTestId('profileMenuEntry--func'),
      )

      expect(profileLink).toBeTruthy()
      expect(profileEditLink).toBeTruthy()
      expect(favoritesLink).toBeTruthy()
      expect(logoutLink).toBeTruthy()
      expect(containerLoggedIn).not.toContain(
        'data-testid="profileMenuEntry--/login"',
      )
      expect(containerLoggedIn).not.toContain(
        'data-testid="profileMenuEntry--/register"',
      )
    })
    it('if the user is logged out', async () => {
      const { container: containerLoggedOut, getByTestId } = renderContainer(
        true,
        false,
      )
      const loginLink = await waitForElement(() =>
        getByTestId('profileMenuEntry--/login'),
      )
      const registerLink = await waitForElement(() =>
        getByTestId('profileMenuEntry--/register'),
      )

      expect(loginLink).toBeTruthy()
      expect(registerLink).toBeTruthy()
      expect(containerLoggedOut).not.toContain(
        'data-testid="profileMenuEntry--/users/1"',
      )
      expect(containerLoggedOut).not.toContain(
        'data-testid="profileMenuEntry--/users/1/edit"',
      )
      expect(containerLoggedOut).not.toContain(
        'data-testid="profileMenuEntry--/favorites"',
      )
      expect(containerLoggedOut).not.toContain(
        'data-testid="profileMenuEntry--func"',
      )
    })
  })
})
