import React, { useState } from 'react'
import i18n from 'i18next'
import { useTranslation } from 'react-i18next'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import Typography from '@material-ui/core/Typography'
import LanguageIcon from '@material-ui/icons/Language'

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
  },
  text: {
    padding: `0 ${theme.spacing(1)}px 0 ${theme.spacing(2)}px`,
    color: theme.palette.primary.main,
  },
}))

const options = {
  de: 'Deutsch',
  en: 'English',
}

export default function LanguageSwitcher() {
  const classes = useStyles()
  const { t } = useTranslation()
  const [anchorEl, setAnchorEl] = useState(null)
  const [selectedLang, setSelectedLang] = useState(i18n.language)

  const _changeLanguage = key => {
    i18n.changeLanguage(key)
  }

  const _handleClickListItem = event => {
    setAnchorEl(event.currentTarget)
  }

  const _handleMenuItemClick = (event, key) => {
    setSelectedLang(key)
    setAnchorEl(null)
    _changeLanguage(key)
  }

  const _handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <div className={`${classes.root} languageSwitcher`}>
      <LanguageIcon style={{ fontSize: 30 }} color={'primary'} />
      <Typography variant="subtitle2" className={classes.text}>
        {t('languageSwitcher.language')}
      </Typography>
      <Button
        aria-controls="language-menu"
        aria-haspopup="true"
        onClick={_handleClickListItem}
        data-testid={'languageSwitcher--button'}
      >
        {options[selectedLang]}
      </Button>
      <Menu
        id="language-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={_handleClose}
        data-testid={'languageSwitcher--menu'}
      >
        {Object.entries(options).map(([key, lang]) => (
          <MenuItem
            key={key}
            selected={key === selectedLang}
            onClick={event => _handleMenuItemClick(event, key)}
            data-testid={`languageSwitcher--${key}`}
          >
            {lang}
          </MenuItem>
        ))}
      </Menu>
    </div>
  )
}
