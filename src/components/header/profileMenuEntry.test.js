import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import ProfileMenuEntry from './profileMenuEntry'
import KeyIcon from '@material-ui/icons/VpnKey'

const mockFn = jest.fn()

const LinkEntry = {
  Icon: KeyIcon,
  text: 'link text',
  to: '/url',
  func: null,
  title: 'Link Entry',
  additionalClasses: null,
}

const FuncEntry = {
  Icon: KeyIcon,
  text: 'func text',
  to: null,
  func: mockFn,
  title: 'Func Entry',
  additionalClasses: 'funcClass',
}

function renderContainer(entry) {
  const result = render(
    <Router>
      <ProfileMenuEntry
        Icon={entry.Icon}
        text={entry.text}
        to={entry.to}
        func={entry.func}
        title={entry.title}
        additionalClasses={entry.additionalClasses}
      />
    </Router>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('ProfileMenuEntry', () => {
  it('renders without crashing', async () => {
    const { container: linkContainer } = renderContainer(LinkEntry)
    const { container: funcContainer } = renderContainer(FuncEntry)

    expect(linkContainer).toBeTruthy()
    expect(funcContainer).toBeTruthy()
  })
  it('sets correct attributes', async () => {
    const { container: linkContainer } = renderContainer(LinkEntry)
    const { container: funcContainer } = renderContainer(FuncEntry)

    const linkEntry = await waitForElement(() =>
      linkContainer.querySelector('[data-testid="profileMenuEntry--/url"]'),
    )
    const funcEntry = await waitForElement(() =>
      funcContainer.querySelector('[data-testid="profileMenuEntry--func"]'),
    )

    expect(linkEntry.getAttribute('title')).toEqual('Link Entry')
    expect(linkEntry.text).toEqual('link text')
    expect(linkEntry.getAttribute('href')).toEqual('/url')
    expect(funcEntry.getAttribute('href')).toEqual('/')
    expect(funcContainer.getElementsByClassName('funcClass')).toBeTruthy()
  })
  it('calls function on click', async () => {
    const { getByTestId } = renderContainer(FuncEntry)

    const entry = await waitForElement(() =>
      getByTestId('profileMenuEntry--func'),
    )

    fireEvent.click(entry)

    expect(mockFn).toHaveBeenCalledTimes(1)
  })
})
