import React from 'react'
import { NavLink } from 'react-router-dom'
import { makeStyles } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'

const useStyles = makeStyles(theme => ({
  link: {
    paddingLeft: theme.spacing(2),
    color: theme.palette.primary.main,
  },
}))

export default function ProfileMenuEntry(props) {
  const classes = useStyles()
  const { Icon, text, to, func, title, additionalClasses } = props

  return (
    <li
      className={`profileMenu--menuItem 
                  ${additionalClasses || ''}`}
    >
      <NavLink
        exact
        className="profileMenu--link"
        activeClassName={to ? 'active' : ''}
        to={to || ''}
        onClick={func || null}
        title={title}
        data-testid={`profileMenuEntry--${to || 'func'}`}
      >
        <Icon style={{ fontSize: 30 }} color={'primary'} />
        <Typography variant="subtitle2" className={classes.link}>
          {text}
        </Typography>
      </NavLink>
    </li>
  )
}

ProfileMenuEntry.propTypes = {
  Icon: PropTypes.any.isRequired,
  text: PropTypes.any.isRequired,
  to: PropTypes.string,
  func: PropTypes.func,
  title: PropTypes.any.isRequired,
  additionalClasses: PropTypes.string,
}
