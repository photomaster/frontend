import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
  act,
} from '@testing-library/react'
import Header from './header'
import i18nextConfig from '../../i18nTesting'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'

function renderContainer(loggedIn) {
  const result = render(
    <Provider
      store={createStore(rootReducer, {
        currentUser: {
          loggedIn: loggedIn,
          user: {
            id: 1,
            displayName: 'user',
            avatarImage: 'url',
          },
        },
      })}
    >
      <Router>
        <Header />
      </Router>
    </Provider>,
  )
  return result
}

afterEach(cleanup)
describe('Header', () => {
  it('renders without crashing', async () => {
    const { container: containerLoggedIn } = renderContainer(true)
    const { container: containeLoggedOut } = renderContainer(false)

    expect(containerLoggedIn).toBeTruthy()
    expect(containeLoggedOut).toBeTruthy()
  })
  it('contains links to various features', async () => {
    const { getByTestId, getAllByTestId } = renderContainer(false)
    const homepageLink = await waitForElement(() => getAllByTestId('header--/'))
    const searchLink = await waitForElement(() =>
      getByTestId('header--/search'),
    )
    const uploadLink = await waitForElement(() =>
      getByTestId('header--/upload'),
    )

    expect(homepageLink).toBeTruthy()
    expect(searchLink).toBeTruthy()
    expect(uploadLink).toBeTruthy()
  })
  it('displays avatar when logged in', async () => {
    const { getByTestId } = renderContainer(true)
    const avatar = await waitForElement(() => getByTestId('avatar'))

    expect(avatar).toBeTruthy()
  })
  it('opens menu on click', async () => {
    const { getByTestId } = renderContainer(true)
    const button = await waitForElement(() => getByTestId('header--func'))

    fireEvent.click(button)

    const closeIcon = await waitForElement(() => getByTestId('header--close'))

    expect(closeIcon).toBeTruthy()
  })
  xit('changes to smaller header when scrolled', async () => {
    const { container } = renderContainer(true)
    const nav = await waitForElement(() => container.querySelector('nav'))

    expect(nav.getAttribute('class')).toContain('large')

    act(() => {
      fireEvent.scroll(window, { target: { scrollY: 150 } })
    })

    //const sleep = m => new Promise(r => setTimeout(r, m))
    //await sleep(500)
    expect(nav.getAttribute('class')).toContain('small')
  })
})
