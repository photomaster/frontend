import React from 'react'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import LanguageSwitcher from './languageSwitcher'
import i18nextConfig from '../../i18nTesting'
import i18n from 'i18next'

function renderContainer() {
  const result = render(<LanguageSwitcher />)
  return result
}

beforeEach(() => {
  i18n.changeLanguage('en')
})
afterEach(cleanup)
describe('LanguageSwitcher', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('opens list of languages on click', async () => {
    const baseDOM = renderContainer()
    const button = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--button'),
    )
    const menu = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--menu'),
    )

    expect(menu.getAttribute('style')).toContain('visibility: hidden')

    fireEvent.click(button)

    expect(menu.getAttribute('style')).not.toContain('visibility: hidden')
  })
  it('changes selected language on click', async () => {
    const baseDOM = renderContainer()
    const button = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--button'),
    )
    const en = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--en'),
    )
    const de = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--de'),
    )

    fireEvent.click(button)

    expect(en.getAttribute('class')).toContain('Mui-selected')
    expect(de.getAttribute('class')).not.toContain('Mui-selected')

    fireEvent.click(de)

    expect(en.getAttribute('class')).not.toContain('Mui-selected')
    expect(de.getAttribute('class')).toContain('Mui-selected')
  })
  it('closes selection on escape without changing language', async () => {
    const baseDOM = renderContainer()
    const button = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--button'),
    )
    const menu = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--menu'),
    )
    const en = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--en'),
    )
    const de = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--de'),
    )

    fireEvent.click(button)

    expect(en.getAttribute('class')).toContain('Mui-selected')
    expect(de.getAttribute('class')).not.toContain('Mui-selected')

    fireEvent.keyDown(document, { keyCode: 'Escape', code: 'Escape' })

    setTimeout(() => {
      expect(en.getAttribute('class')).toContain('Mui-selected')
      expect(de.getAttribute('class')).not.toContain('Mui-selected')
      expect(menu.getAttribute('style')).toContain('visibility: hidden')
    }, 100)
  })
  xit('closes selection on click on backdrop without changing language', async () => {
    const baseDOM = renderContainer()
    const button = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--button'),
    )
    const menu = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--menu'),
    )
    const en = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--en'),
    )
    const de = await waitForElement(() =>
      baseDOM.getByTestId('languageSwitcher--de'),
    )

    fireEvent.click(button)

    expect(en.getAttribute('class')).toContain('Mui-selected')
    expect(de.getAttribute('class')).not.toContain('Mui-selected')

    const backdrop = await waitForElement(
      () =>
        baseDOM
          .getByTestId('languageSwitcher--menu')
          .getElementsByTagName('div')[0],
    )
    expect(backdrop.getAttribute('aria-hidden')).toEqual('true')

    fireEvent.click(backdrop)
    //backdrop.blur()

    setTimeout(() => {
      expect(en.getAttribute('class')).toContain('Mui-selected')
      expect(de.getAttribute('class')).not.toContain('Mui-selected')
      expect(menu.getAttribute('style')).toContain('visibility: hidden')
    }, 200)
  })
})
