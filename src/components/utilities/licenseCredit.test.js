import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import i18nextConfig from '../../i18nTesting'
import LicenseCredit from './licenseCredit'

const FlickrUser = {
  id: -1,
  displayName: 'FlickrUser',
  avatarImage: null,
  url: 'flickr.com',
  licenseUrl: 'creativecommons.org',
  licenseName: 'CC',
}

function renderContainer(user) {
  const result = render(
    <Router>
      <LicenseCredit user={user} />
    </Router>,
  )
  return result
}

afterEach(cleanup)
describe('LicenseCredit', () => {
  it('renders without crashing', async () => {
    const { getByTestId } = renderContainer(FlickrUser)
    const licenseCredit = await waitForElement(() =>
      getByTestId('licenseCredit--link'),
    )

    expect(licenseCredit).toBeTruthy()
  })
  it('provides a link to the license', async () => {
    const { getByTestId } = renderContainer(FlickrUser)
    const licenseCredit = await waitForElement(() =>
      getByTestId('licenseCredit--link'),
    )

    expect(licenseCredit.getAttribute('href')).toEqual('creativecommons.org')
    expect(licenseCredit.textContent).toEqual(
      'This photo is made available under a CC.',
    )
  })
})
