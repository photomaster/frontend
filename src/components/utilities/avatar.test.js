import React from 'react'
import { render, cleanup, waitForElement } from '@testing-library/react'
import Avatar from './avatar'

const AvatarWithImage = {
  alt: 'avatarWithImage',
  src: 'someURL',
  additionalClasses: 'someClass',
  wrappedInContainer: false,
  iconBottom: false,
}

const AvatarWithoutImage = {
  alt: 'avatarWithoutImage',
  src: null,
  additionalClasses: 'someClass',
  wrappedInContainer: false,
  iconBottom: false,
}

const AvatarInContainerWithIcon = {
  alt: 'avatarInContainer',
  src: 'someURL',
  additionalClasses: null,
  wrappedInContainer: true,
  iconBottom: true,
}

function renderBasicAvatarContainer(avatar) {
  const result = render(
    <Avatar
      alt={avatar.alt}
      src={avatar.src}
      additionalClasses={avatar.additionalClasses}
      wrappedInContainer={avatar.wrappedInContainer}
      iconBottom={avatar.iconBottom}
    />,
  )
  return result
}

function renderAvatarWithChildrenContainer(avatar) {
  const result = render(
    <Avatar
      alt={avatar.alt}
      src={avatar.src}
      additionalClasses={avatar.additionalClasses}
      wrappedInContainer={avatar.wrappedInContainer}
      iconBottom={avatar.iconBottom}
    >
      <p>Additional content</p>
    </Avatar>,
  )
  return result
}

afterEach(cleanup)
describe('Avatar', () => {
  describe('renders without crashing', () => {
    it('without avatar image', async () => {
      const { getByTestId } = renderBasicAvatarContainer(AvatarWithoutImage)
      const avatar = await waitForElement(() => getByTestId('avatar'))

      expect(avatar).toBeTruthy()
    })

    it('with avatar image', async () => {
      const { getByTestId } = renderBasicAvatarContainer(AvatarWithImage)
      const avatar = await waitForElement(() => getByTestId('avatar'))

      expect(avatar).toBeTruthy()
    })

    it('with children', async () => {
      const { getByTestId } = renderAvatarWithChildrenContainer(
        AvatarWithoutImage,
      )
      const avatar = await waitForElement(() => getByTestId('avatar'))

      expect(avatar).toBeTruthy()
    })
  })
  it('sets correct attributes', async () => {
    const { getByTestId } = renderBasicAvatarContainer(AvatarWithImage)
    const avatar = await waitForElement(() => getByTestId('avatar'))

    expect(avatar.querySelector('img').getAttribute('alt')).toEqual(
      'avatarWithImage',
    )
    expect(avatar.querySelector('img').getAttribute('src')).toEqual('someURL')
    expect(avatar.getAttribute('class')).toContain('someClass')
  })
  it('contains correct children', async () => {
    const { getByTestId } = renderAvatarWithChildrenContainer(
      AvatarWithoutImage,
    )
    const avatar = await waitForElement(() => getByTestId('avatar'))

    expect(avatar.querySelector('p').textContent).toEqual('Additional content')
  })
  it('can be wrapped inside a container', async () => {
    const { getByTestId } = renderBasicAvatarContainer(
      AvatarInContainerWithIcon,
    )
    const avatarContainer = await waitForElement(() =>
      getByTestId('avatar--container'),
    )

    expect(avatarContainer).toBeTruthy()
  })
  it('sets correct classes depending on attributes', async () => {
    const { container: container1 } = renderBasicAvatarContainer(
      AvatarWithoutImage,
    )
    const avatarWithoutImage = await waitForElement(() =>
      container1.querySelector('[data-testid="avatar"]'),
    )
    const { container: container2 } = renderBasicAvatarContainer(
      AvatarInContainerWithIcon,
    )
    const avatarWithIcon = await waitForElement(() =>
      container2.querySelector('[data-testid="avatar"]'),
    )

    expect(avatarWithoutImage.getAttribute('class')).not.toContain(
      'whiteBackground',
    )
    expect(avatarWithoutImage.getAttribute('class')).not.toContain('large')
    expect(avatarWithoutImage.getAttribute('class')).not.toContain(
      'backgroundBottom',
    )

    expect(avatarWithIcon.getAttribute('class')).toContain('whiteBackground')
    expect(avatarWithIcon.getAttribute('class')).toContain('large')
    expect(avatarWithIcon.getAttribute('class')).toContain('backgroundBottom')
  })
})
