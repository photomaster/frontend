import React from 'react'
import PropTypes from 'prop-types'
import MuiAlert from '@material-ui/lab/Alert'
import baseStyles from 'styles/baseClasses'

export default function Alert(props) {
  const baseClasses = baseStyles()
  const { severity, style, children } = props

  return (
    <MuiAlert
      elevation={6}
      severity={severity}
      style={style}
      className={baseClasses.margin1}
      data-testid={'alert'}
    >
      {children}
    </MuiAlert>
  )
}

Alert.propTypes = {
  severity: PropTypes.string.isRequired,
  style: PropTypes.object,
  children: PropTypes.node.isRequired,
}
