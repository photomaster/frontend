import React from 'react'
import { render, cleanup, waitForElement } from '@testing-library/react'
import Loading from './loading'

function renderContainer() {
  const result = render(<Loading additionalClasses={'additionalClass'} />)
  return result
}

afterEach(cleanup)
describe('Loading', () => {
  it('renders without crashing', async () => {
    const { getByTestId } = renderContainer()
    const loading = await waitForElement(() => getByTestId('loading'))

    expect(loading).toBeTruthy()
  })
  it('sets additional classes', async () => {
    const { getByTestId } = renderContainer()
    const loading = await waitForElement(() => getByTestId('loading'))

    expect(loading.getAttribute('class')).toContain('additionalClass')
  })
})
