import React from 'react'
import { render, cleanup, waitForElement } from '@testing-library/react'
import Alert from './alert'

function renderContainer(severity) {
  const result = render(
    <Alert severity={severity} style={{ marginTop: '100px' }}>
      <p>Some content</p>
    </Alert>,
  )
  return result
}

afterEach(cleanup)
describe('Alert', () => {
  it('renders without crashing', async () => {
    const { getByTestId } = renderContainer('error')
    const alert = await waitForElement(() => getByTestId('alert'))

    expect(alert).toBeTruthy()
  })
  it('has correct severity class', async () => {
    const { container: containerError } = renderContainer('error')
    const { container: containerInfo } = renderContainer('info')

    const alertError = await waitForElement(() =>
      containerError.querySelector('[data-testid="alert"]'),
    )
    const alertInfo = await waitForElement(() =>
      containerInfo.querySelector('[data-testid="alert"]'),
    )

    expect(alertError.getAttribute('class')).toContain('Error')
    expect(alertInfo.getAttribute('class')).toContain('Info')
  })
  it('contains correct children', async () => {
    const { getByTestId } = renderContainer('error')
    const alert = await waitForElement(() => getByTestId('alert'))

    expect(alert.querySelector('p').textContent).toEqual('Some content')
  })
  it('sets additional style', async () => {
    const { getByTestId } = renderContainer('error')
    const alert = await waitForElement(() => getByTestId('alert'))

    expect(alert.getAttribute('style')).toContain('margin-top: 100px')
  })
})
