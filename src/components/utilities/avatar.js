import React from 'react'
import PropTypes from 'prop-types'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Container from '@material-ui/core/Container'
import MaterialAvatar from '@material-ui/core/Avatar'
import baseStyles from 'styles/baseClasses'

const useStyles = makeStyles(() => ({
  whiteBackground: {
    background: 'white',
  },
  large: {
    height: 120,
    width: 120,
    marginTop: -60,
    zIndex: 10,
  },
  backgroundBottom: {
    '&::after': {
      content: '""',
      background: 'white',
      width: '100%',
      height: '40%',
      position: 'absolute',
      bottom: 0,
      opacity: 0.5,
    },
  },
}))

export default function Avatar(props) {
  const classes = useStyles(props)
  const baseClasses = baseStyles()
  const {
    alt,
    src,
    wrappedInContainer,
    iconBottom,
    additionalClasses,
    children,
  } = props

  // eslint-disable-next-line no-shadow
  const Wrapper = props => {
    if (wrappedInContainer)
      return (
        <Container
          className={baseClasses.justifyContentCenter}
          data-testid={'avatar--container'}
        >
          {props.children}
        </Container>
      )
    return <React.Fragment>{props.children}</React.Fragment>
  }

  return (
    <Wrapper>
      <MaterialAvatar
        alt={alt}
        src={src}
        className={`${src ? classes.whiteBackground : ''} 
                    ${wrappedInContainer ? classes.large : ''} 
                    ${iconBottom ? classes.backgroundBottom : ''}
                    ${additionalClasses || ''}`}
        data-testid={'avatar'}
      >
        {children}
      </MaterialAvatar>
    </Wrapper>
  )
}

Avatar.propTypes = {
  alt: PropTypes.string.isRequired,
  src: PropTypes.string,
  wrappedInContainer: PropTypes.bool,
  iconBottom: PropTypes.bool,
  additionalClasses: PropTypes.string,
  children: PropTypes.node,
}

Avatar.defaultProps = {
  wrappedInContainer: false,
  iconBottom: false,
}
