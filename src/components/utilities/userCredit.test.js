import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { render, cleanup, waitForElement } from '@testing-library/react'
import i18nextConfig from '../../i18nTesting'
import UserCredit from './userCredit'

const OwnUser = {
  id: 1,
  displayName: 'OwnUser',
  avatarImage: 'mediaURL',
}

const FlickrUser = {
  id: -1,
  displayName: 'FlickrUser',
  avatarImage: null,
  url: 'flickr.com',
}

function renderContainer(user) {
  const result = render(
    <Router>
      <UserCredit user={user} />
    </Router>,
  )
  return result
}

afterEach(cleanup)
describe('UserCredit', () => {
  describe('of own user', () => {
    it('renders without crashing', async () => {
      const { getByTestId } = renderContainer(OwnUser)
      const userCredit = await waitForElement(() => getByTestId('userCredit'))

      expect(userCredit).toBeTruthy()
    })
    it('sets correct attributes', async () => {
      const { getByTestId } = renderContainer(OwnUser)
      const userCredit = await waitForElement(() => getByTestId('userCredit'))

      expect(userCredit.getAttribute('title')).toEqual("OwnUser's profile")
      expect(userCredit.getAttribute('href')).toEqual('/users/1')
    })
  })
  describe('of Flickr user', () => {
    it('renders without crashing', async () => {
      const { getByTestId } = renderContainer(FlickrUser)
      const userCredit = await waitForElement(() =>
        getByTestId('userCredit--external'),
      )

      expect(userCredit).toBeTruthy()
    })
    it('sets correct attributes', async () => {
      const { getByTestId } = renderContainer(FlickrUser)
      const userCredit = await waitForElement(() =>
        getByTestId('userCredit--external'),
      )

      expect(userCredit.getAttribute('title')).toEqual(
        "FlickrUser's image on Flickr",
      )
      expect(userCredit.getAttribute('href')).toEqual('flickr.com')
    })
  })
})
