import React from 'react'
import PropTypes from 'prop-types'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { Link, NavLink } from 'react-router-dom'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import Avatar from 'components/utilities/avatar'

const useStyles = makeStyles(theme => ({
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
    margin: '0 0.25rem',
  },
  link: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
}))

export default function UserCredit(props) {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const { t } = useTranslation()
  const { user } = props

  // eslint-disable-next-line no-shadow
  const Link = props => {
    if ('url' in user && user.id === -1) {
      return (
        <a
          href={user.url}
          title={t('pages.imageFlickrUser', { user: user.displayName })}
          target="_blank"
          rel="noopener noreferrer"
          className={`${baseClasses.verticalCenter} ${classes.link}`}
          data-testid={'userCredit--external'}
        >
          {props.children}
        </a>
      )
    }
    return (
      <NavLink
        to={`/users/${user.id}`}
        title={t('pages.profileUser', { user: user.displayName })}
        className={`${baseClasses.verticalCenter} ${classes.link}`}
        data-testid={'userCredit'}
      >
        {props.children}
      </NavLink>
    )
  }

  return (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <Link>
      <Avatar
        alt={user.displayName}
        src={user.avatarImage}
        additionalClasses={classes.small}
      />
      <span>{user.displayName}</span>
    </Link>
  )
}

UserCredit.propTypes = {
  user: PropTypes.object.isRequired,
}

Link.propTypes = {
  children: PropTypes.node.isRequired,
}
