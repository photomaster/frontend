import React from 'react'
import PropTypes from 'prop-types'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import SettingsRoundedIcon from '@material-ui/icons/SettingsRounded'
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded'
import { makeStyles } from '@material-ui/core'
import { history } from '../../helpers/history'

const useStyles = makeStyles(theme => ({
  base: {
    position: 'absolute',
  },
  user: {
    position: 'absolute',
    top: -theme.spacing(3),
    right: theme.spacing(5),
  },
  photo: {
    position: 'absolute',
    top: -theme.spacing(4),
  },
  right: {
    right: theme.spacing(2),
  },
  left: {
    left: theme.spacing(2),
  },
  left_switch: {
    [theme.breakpoints.up('sm')]: {
      left: -theme.spacing(2),
    },
    left: theme.spacing(1),
  },
}))

export default function NavigationIconButton(props) {
  const classes = useStyles(props)
  const { page, id, title, aria, noStyle, navigateToEdit } = props
  let style
  if (page === 'user') {
    if (navigateToEdit) style = `${classes.user} ${classes.right}`
    else style = `${classes.user} ${classes.left_switch}`
  } else if (page === 'photo') {
    if (navigateToEdit) style = `${classes.photo} ${classes.right}`
    else style = `${classes.photo} ${classes.left}`
  } else style = classes.base

  const _handleEditClick = () => {
    if (page === 'user') history.push(`/users/${id}/edit`)
    else if (page === 'photo') history.push(`/photos/${id}/edit`)
    else console.warn("We don't know what you're looking for 🙈")
  }

  const _handleReturnClick = () => {
    history.goBack()
  }

  return (
    <span className={noStyle ? '' : style} data-testid="navigationIconButton">
      <Tooltip
        title={title}
        placement="top"
        data-testid="navigationIconButton--tooltip"
      >
        <IconButton
          aria-label={aria}
          color="primary"
          onClick={navigateToEdit ? _handleEditClick : _handleReturnClick}
          data-testid="navigationIconButton--button"
        >
          {navigateToEdit ? <SettingsRoundedIcon /> : <ArrowBackRoundedIcon />}
        </IconButton>
      </Tooltip>
    </span>
  )
}

NavigationIconButton.propTypes = {
  page: PropTypes.string.isRequired,
  id: PropTypes.number,
  title: PropTypes.string.isRequired,
  aria: PropTypes.string.isRequired,
  noStyle: PropTypes.bool,
  navigateToEdit: PropTypes.bool.isRequired,
}
