import React from 'react'
import PropTypes from 'prop-types'
import makeStyles from '@material-ui/core/styles/makeStyles'
import CircularProgress from '@material-ui/core/CircularProgress'
import baseStyles from 'styles/baseClasses'

const useStyles = makeStyles(() => ({
  zIndex_top: {
    zIndex: 1000,
  },
}))

export default function Loading(props) {
  const classes = useStyles(props)
  const baseClasses = baseStyles()
  const { additionalClasses } = props

  return (
    <div
      className={`${baseClasses.justifyContentCenter}
                  ${baseClasses.verticalCenter} 
                  ${additionalClasses || ''}`}
      data-testid={'loading'}
    >
      <CircularProgress className={classes.zIndex_top} />
    </div>
  )
}

Loading.propTypes = {
  additionalClasses: PropTypes.string,
}
