import React from 'react'
import {
  render,
  cleanup,
  getByTestId,
  fireEvent,
  waitForElement,
} from '@testing-library/react'
import NavigationIconButton from './navigationIconButton'
import { history } from 'helpers/history'

const EditButtonUserPage = {
  page: 'user',
  id: 1,
  title: 'Title of 1',
  aria: 'Aria of 1',
  noStyle: false,
  navigateToEdit: true,
}

const EditButtonPhotoPage = {
  page: 'photo',
  id: 2,
  title: 'Title of 2',
  aria: 'Aria of 2',
  noStyle: true,
  navigateToEdit: true,
}

const EditButtonWrongPage = {
  page: 'noRealPage',
  id: 3,
  title: 'Title of 3',
  aria: 'Aria of 3',
  noStyle: false,
  navigateToEdit: true,
}

const ReturnUserButton = {
  page: 'user',
  id: 1,
  title: 'Title of 1',
  aria: 'Aria of 1',
  noStyle: false,
  navigateToEdit: false,
}

const ReturnPhotoButton = {
  page: 'photo',
  id: 2,
  title: 'Title of 2',
  aria: 'Aria of 2',
  noStyle: false,
  navigateToEdit: false,
}

jest.mock('helpers/history', () => ({
  history: {
    push: jest.fn(),
    goBack: jest.fn(),
  },
}))

function renderContainer(attributes) {
  const result = render(
    <NavigationIconButton
      page={attributes.page}
      id={attributes.id}
      title={attributes.title}
      aria={attributes.aria}
      noStyle={attributes.noStyle}
      navigateToEdit={attributes.navigateToEdit}
    />,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('NavigationIconButton', () => {
  it('renders without crashing', async () => {
    const { getByTestId } = renderContainer(EditButtonUserPage)
    const editButton = await waitForElement(() =>
      getByTestId('navigationIconButton'),
    )

    expect(editButton).toBeTruthy()
  })
  it('sets correct attributes', async () => {
    const baseDom = renderContainer(EditButtonUserPage)
    const editButton = await waitForElement(() =>
      baseDom.getByTestId('navigationIconButton'),
    )

    fireEvent.mouseOver(getByTestId(editButton, 'navigationIconButton--button'))

    expect(await baseDom.findByText('Title of 1')).toBeInTheDocument()
    expect(
      getByTestId(editButton, 'navigationIconButton--button').getAttribute(
        'aria-label',
      ),
    ).toEqual('Aria of 1')
    expect(editButton.getAttribute('class')).toContain('user')
  })
  describe('navigating to edit', () => {
    it('forwards to correct page on click (user edit)', async () => {
      const { getByTestId } = renderContainer(EditButtonUserPage)
      const editButton = await waitForElement(() =>
        getByTestId('navigationIconButton--button'),
      )

      fireEvent.click(editButton)

      expect(history.push).toHaveBeenCalledWith('/users/1/edit')
    })
    it('forwards to correct page on click (photo edit)', async () => {
      const { getByTestId } = renderContainer(EditButtonPhotoPage)
      const editButton = await waitForElement(() =>
        getByTestId('navigationIconButton--button'),
      )

      fireEvent.click(editButton)

      expect(history.push).toHaveBeenCalledWith('/photos/2/edit')
    })
    it('does not forward on click (wrong page given)', async () => {
      const { getByTestId } = renderContainer(EditButtonWrongPage)
      const editButton = await waitForElement(() =>
        getByTestId('navigationIconButton--button'),
      )

      fireEvent.click(editButton)

      expect(history.push).toHaveBeenCalledTimes(0)
    })
  })
  describe('navigating to last page', () => {
    it('goes back in history (user edit)', async () => {
      const { getByTestId } = renderContainer(ReturnUserButton)
      const returnButton = await waitForElement(() =>
        getByTestId('navigationIconButton--button'),
      )

      fireEvent.click(returnButton)

      expect(history.goBack).toHaveBeenCalledTimes(1)
    })
    it('goes back in history (photo edit)', async () => {
      const { getByTestId } = renderContainer(ReturnPhotoButton)
      const returnButton = await waitForElement(() =>
        getByTestId('navigationIconButton--button'),
      )

      fireEvent.click(returnButton)

      expect(history.goBack).toHaveBeenCalledTimes(1)
    })
  })
})
