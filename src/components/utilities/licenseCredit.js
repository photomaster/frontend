import React from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

export default function LicenseCredit(props) {
  const { t } = useTranslation()
  const { user } = props

  return (
    <a
      href={user.licenseUrl}
      title={t('photo.licenseLink')}
      target="_blank"
      rel="noopener noreferrer"
      data-testid={'licenseCredit--link'}
    >
      <span>{t('photo.license', { license: user.licenseName })}</span>
    </a>
  )
}

LicenseCredit.propTypes = {
  user: PropTypes.object.isRequired,
}
