import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import {
  render,
  cleanup,
  getByTitle,
  waitForElement,
} from '@testing-library/react'
import '@testing-library/jest-dom'
import RecommendationCard from './recommendationCard'
import photoMock from '../../views/photo/photo.mock.json'
import Photo from 'models/photo'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'

const PhotoOwner = {
  id: 1,
  url: 'http://localhost:8080/api/users/1/',
  username: 'lukas',
  displayName: 'Lugge',
  description: 'nice1',
  avatarImage: 'http://localhost:8080/media/avatars/go_Qy228Zw.png',
  bannerImage:
    'http://localhost:8080/media/banners/star-wars-backgrounds-12-1.jpg',
}

const OtherUser = {
  id: 2,
  url: 'http://localhost:8080/api/users/2/',
  username: 'lisa',
  displayName: 'Lisa',
  description: 'desc1',
  avatarImage: 'http://localhost:8080/media/avatars/go_Qy228Zz.png',
  bannerImage: 'http://localhost:8080/media/banners/some-image.jpg',
}

const TestPhoto = new Photo(photoMock)

const mockDispatch = jest.fn()
const mockTranslation = jest.fn(str => str)

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer(loggedIn = true, loggedInUser = PhotoOwner) {
  const result = render(
    <Provider
      store={createStore(rootReducer, {
        currentUser: {
          loggedIn: loggedIn,
          user: loggedInUser,
        },
      })}
    >
      <Router>
        <RecommendationCard photo={TestPhoto} user={PhotoOwner} />
      </Router>
    </Provider>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('RecommendationCard', () => {
  it('renders without crashing', async () => {
    const { container: containerOwnPhoto } = renderContainer()
    const { container: containerOthersPhoto } = renderContainer(true, OtherUser)

    expect(containerOwnPhoto).toBeTruthy()
    expect(containerOthersPhoto).toBeTruthy()
  })
  it('displays the given image', async () => {
    const { container } = renderContainer()

    expect(getByTitle(container, photoMock.name)).toHaveStyle(
      `background-image: url(${photoMock.imageThumbnailTeaser})`,
    )
  })
  it('displays the given title', async () => {
    const { container } = renderContainer()

    expect(container.querySelector('h2').textContent).toEqual(photoMock.name)
  })
  it('links to the recommended photo', async () => {
    const { container } = renderContainer()

    expect(container.querySelector('a').getAttribute('href')).toEqual(
      `/photos/${photoMock.id}`,
    )
  })
  it('credits the creator of the photo', async () => {
    const { getByTestId } = renderContainer()

    expect(
      getByTestId(
        `recommendationCard--CardActions-${photoMock.id}`,
      ).querySelector('div').textContent,
    ).toContain('Lugge')
  })
  it('links to the creator of the photo', async () => {
    const { getByTestId } = renderContainer()

    expect(
      getByTestId(`recommendationCard--CardActions-${photoMock.id}`)
        .querySelector('a')
        .getAttribute('href'),
    ).toEqual(`/users/${PhotoOwner.id}`)
  })
  it('displays a maximum of 80 characters of the description', async () => {
    const { container } = renderContainer()

    // 80 desc + 1 space + 1 … = 82
    expect(container.querySelector('p').textContent.length).toBeLessThanOrEqual(
      82,
    )
  })
  it('displays "…" preceded by " " as the last characters in the description', async () => {
    const { container } = renderContainer()

    expect(container.querySelector('p').textContent.slice(-2)).toEqual(' …')
  })
  it('translates the UI', async () => {
    renderContainer()

    expect(mockTranslation).toHaveBeenCalledWith('photo.edit')
    expect(mockTranslation).toHaveBeenCalledWith('photo.aria.edit')
    expect(mockTranslation).toHaveBeenCalledWith('recommendations.by')
  })
  it('displays edit button of own photo', async () => {
    const { container } = renderContainer()

    const button = await waitForElement(() =>
      container.querySelector('[data-testid="navigationIconButton"]'),
    )

    expect(button).toBeTruthy()
  })
  it('does not display edit button for not owned photos', async () => {
    const { container } = renderContainer(true, OtherUser)

    const button = container.querySelector(
      '[data-testid="navigationIconButton"]',
    )

    expect(button).toBeFalsy()
  })
})
