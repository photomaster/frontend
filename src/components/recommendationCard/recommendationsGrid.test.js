import React from 'react'
import { render, cleanup, waitForElement } from '@testing-library/react'
import RecommendationsGrid from './recommendationsGrid'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'reducers'

const You = {
  id: 90,
  displayName: 'user',
  avatarImage: 'url',
}

const OtherUser = {
  id: 100,
  displayName: 'someone else',
  avatarImage: 'url',
}

const FavPhoto = {
  id: 10,
  url: '/photo10',
  user: OtherUser,
}

const Favourites = [
  {
    dateAdded: '"2020-10-25T11:36:11.049156Z"',
    id: 1,
    photo: FavPhoto,
    delete: jest.fn(),
  },
]

const mockDispatch = jest.fn()

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockDispatch,
}))

function renderContainer(loggedIn = true, favourites = Favourites) {
  const baseState = {
    currentUser: {
      loggedIn: loggedIn,
      user: You,
    },
    photos: {
      loadingFavs: false,
      favourites: favourites,
    },
  }

  const result = render(
    <Provider store={createStore(rootReducer, baseState)}>
      <RecommendationsGrid>
        <div data-testid={'theChild'}>Some text</div>
      </RecommendationsGrid>
    </Provider>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('RecommendationsGrid', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()
    const { container: containerLoading } = renderContainer(true, null)

    expect(container).toBeTruthy()
    expect(containerLoading).toBeTruthy()
  })
  it('displays loading component when favourites are not loaded yet', async () => {
    const { getByTestId } = renderContainer(true, null)

    const loading = await waitForElement(() => getByTestId('loading'))

    expect(loading).toBeTruthy()
  })
  it('requests to update favourites if they are not available', async () => {
    renderContainer(true, null)

    expect(mockDispatch).toHaveBeenCalledTimes(1)
  })
  it('does not request to update favourites if not logged in', async () => {
    renderContainer(false, null)

    expect(mockDispatch).toHaveBeenCalledTimes(0)
  })
  it('contains correct children', async () => {
    const { getByTestId } = renderContainer()

    const child = await waitForElement(() => getByTestId('theChild'))

    expect(child).toBeTruthy()
    expect(child.textContent).toEqual('Some text')
  })
})
