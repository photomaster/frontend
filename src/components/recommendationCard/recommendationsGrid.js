import React, { useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import { useSelector, useDispatch } from 'react-redux'
import Loading from '../utilities/loading'
import { photosActions } from '../../actions'

export default function RecommendationsGrid(props) {
  const dispatch = useDispatch()
  const loggedIn = useSelector(state => state.currentUser.loggedIn)
  const favourites = useSelector(state => state.photos.favourites || null)

  useEffect(() => {
    if (loggedIn && !favourites) dispatch(photosActions.updateFavourites())
  }, [])

  return loggedIn && !favourites ? (
    <Loading />
  ) : (
    <Grid
      container
      direction="row"
      justify="flex-start"
      wrap="wrap"
      data-testid={'recommendationsGrid'}
    >
      {props.children}
    </Grid>
  )
}
