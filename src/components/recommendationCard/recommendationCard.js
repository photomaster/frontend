import React, { useState } from 'react'
import baseStyles from 'styles/baseClasses'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { NavLink } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Favourite from '../favourite/favourite'
import UserCredit from '../utilities/userCredit'
import NavigationIconButton from '../utilities/navigationIconButton'
import './recommendationCard.scss'

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
    maxWidth: '345px',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  media: {
    height: 200,
  },
  textColor: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  left: {
    marginLeft: 'auto',
    display: 'flex',
  },
}))

export default function RecommendationCard(props) {
  const classes = useStyles(props)
  const baseClasses = baseStyles()
  const { t } = useTranslation()
  const [id] = useState(props.photo.id)
  const [url] = useState(props.photo.imageThumbnailTeaser)
  const [title] = useState(props.photo.name)
  const [user] = useState(props.user)
  const [desc] = useState(props.photo.description)
  const [photo] = useState(props.photo)
  const loggedIn = useSelector(state => state.currentUser.loggedIn)
  const loggedUser = useSelector(state => state.currentUser.user)

  let descShort = desc.substr(0, 80)
  descShort = descShort.substr(0, descShort.lastIndexOf(' ') + 1)

  const _ownPhoto = () => {
    if (loggedIn && loggedUser && user && loggedUser.id === user.id) {
      return true
    }
    return false
  }

  return (
    <Grid
      item
      xs={12}
      sm={6}
      md={4}
      style={{ display: 'flex', justifyContent: 'center' }}
    >
      <Card
        className={`${classes.root} ${baseClasses.margin1}`}
        key={id}
        data-testid={`recommendationCard--card`}
      >
        <CardActionArea>
          <NavLink
            style={{ textDecoration: 'none' }}
            activeClassName="active"
            to={`/photos/${id}`}
          >
            <CardMedia
              className={classes.media}
              image={url}
              title={title}
              role="img"
            />
            <CardContent>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                className={classes.textColor}
              >
                {title}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                {descShort}
                &hellip;
              </Typography>
            </CardContent>
          </NavLink>
        </CardActionArea>
        <CardActions
          disableSpacing
          data-testid={`recommendationCard--CardActions-${id}`}
          className={baseClasses.flexWrap}
        >
          <Favourite photo={photo} />
          {_ownPhoto() && (
            <NavigationIconButton
              page={'photo'}
              id={id}
              title={t('photo.edit')}
              aria={t('photo.aria.edit')}
              noStyle
              navigateToEdit
            />
          )}
          <div className={classes.left}>
            {t('recommendations.by')}
            &nbsp;
            <UserCredit user={user} />
          </div>
        </CardActions>
      </Card>
    </Grid>
  )
}

RecommendationCard.propTypes = {
  photo: PropTypes.shape({
    id: PropTypes.number.isRequired,
    imageThumbnailTeaser: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }).isRequired,
}
