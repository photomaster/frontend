import React from 'react'
import PropTypes from 'prop-types'
import Alert from 'components/utilities/alert'
import { useTranslation } from 'react-i18next'

export default function ErrorMessage(props) {
  const { t } = useTranslation()
  const { text, noTranslate, mt, mb } = props
  const standardError = 'forms.errors.general'
  let combinedMessages = ''

  if (noTranslate) {
    for (const typeKey in text) {
      for (const messageKey in text[typeKey]) {
        combinedMessages += `${text[typeKey][messageKey]} `
      }
    }
  }

  return (
    <Alert
      severity="error"
      style={
        mt ? { marginTop: '24px' } : mb ? { marginBottom: '24px' } : { '': '' }
      }
    >
      {noTranslate ? combinedMessages : t(text || standardError)}
    </Alert>
  )
}

ErrorMessage.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  noTranslate: PropTypes.bool,
  mt: PropTypes.bool,
  mb: PropTypes.bool,
}
