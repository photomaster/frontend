import React from 'react'
import { render, cleanup, waitForElement } from '@testing-library/react'
import TextFieldCamera from './textFieldCamera'

const mockFunc = jest.fn()
const mockTranslation = jest.fn(str => str)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

function renderContainer() {
  const result = render(
    <TextFieldCamera id={'id'} value={'value'} func={mockFunc} />,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('TextFieldCamera', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('translates the label', async () => {
    const { getByTestId } = renderContainer()
    const label = await waitForElement(() =>
      getByTestId('textFieldCamera--id').querySelector('label'),
    )

    expect(mockTranslation).toHaveBeenCalledWith('forms.labels.id')
    expect(label.textContent).toEqual('forms.labels.id')
  })
  it('translates the helperText', async () => {
    const { getByTestId } = renderContainer()
    const helperText = await waitForElement(() =>
      getByTestId('textFieldCamera--id').querySelector('[id="id-helper-text"]'),
    )

    expect(mockTranslation).toHaveBeenCalledWith(
      'forms.photo.helperTexts.example',
    )
    expect(mockTranslation).toHaveBeenCalledWith('forms.photo.helperTexts.id')
    expect(helperText.textContent).toEqual(
      'forms.photo.helperTexts.example: forms.photo.helperTexts.id',
    )
  })
  it('displays the current value', async () => {
    const { getByTestId } = renderContainer()
    const input = await waitForElement(() =>
      getByTestId('textFieldCamera--id').querySelector('input'),
    )

    expect(input.getAttribute('value')).toEqual('value')
  })
})
