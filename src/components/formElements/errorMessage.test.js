import React from 'react'
import { render, cleanup, waitForElement } from '@testing-library/react'
import ErrorMessage from './errorMessage'

const mockTranslation = jest.fn(str => str)

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: mockTranslation,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))

const StringMessage = {
  text: 'Message',
  noTranslate: false,
  mt: true,
  mb: false,
}

const ObjMessage = {
  text: {
    username: ['First message', 'Second message'],
    password: ['Third message'],
  },
  noTranslate: true,
  mt: false,
  mb: true,
}

const noMessage = {
  text: null,
  noTranslate: false,
  mt: false,
  mb: false,
}

function renderContainer(errorMessage) {
  const result = render(
    <ErrorMessage
      text={errorMessage.text}
      noTranslate={errorMessage.noTranslate}
      mt={errorMessage.mt}
      mb={errorMessage.mb}
    />,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('ErrorMessage', () => {
  it('renders without crashing', async () => {
    const { container: stringContainer } = renderContainer(StringMessage)
    const { container: objContainer } = renderContainer(ObjMessage)

    expect(stringContainer).toBeTruthy()
    expect(objContainer).toBeTruthy()
  })
  it('translates and displays given text', async () => {
    const { getByTestId } = renderContainer(StringMessage)
    const text = await waitForElement(() =>
      getByTestId('alert').querySelector('[class="MuiAlert-message"]'),
    )

    expect(mockTranslation).toHaveBeenCalledWith('Message')
    expect(text.textContent).toEqual('Message')
  })
  it('combines object messages without translation', async () => {
    const { getByTestId } = renderContainer(ObjMessage)
    const text = await waitForElement(() =>
      getByTestId('alert').querySelector('[class="MuiAlert-message"]'),
    )

    expect(mockTranslation).toHaveBeenCalledTimes(0)
    expect(text.textContent).toContain(
      'First message Second message Third message',
    )
  })
  it('displays generic message if no text is given', async () => {
    const { getByTestId } = renderContainer(noMessage)
    const text = await waitForElement(() =>
      getByTestId('alert').querySelector('[class="MuiAlert-message"]'),
    )

    expect(mockTranslation).toHaveBeenCalledWith('forms.errors.general')
    expect(text.textContent).toEqual('forms.errors.general')
  })
  it('sets margin correctly', async () => {
    const { container: mbContainer } = renderContainer(ObjMessage)
    const { container: mtContainer } = renderContainer(StringMessage)
    const mbMessage = await waitForElement(() =>
      mbContainer.querySelector('[data-testid="alert"]'),
    )
    const mtMessage = await waitForElement(() =>
      mtContainer.querySelector('[data-testid="alert"]'),
    )

    expect(mtMessage.getAttribute('style')).toContain('margin-top: 24px')
    expect(mtMessage.getAttribute('style')).not.toContain('margin-bottom: 24px')
    expect(mbMessage.getAttribute('style')).toContain('margin-bottom: 24px')
    expect(mbMessage.getAttribute('style')).not.toContain('margin-top: 24px')
  })
})
