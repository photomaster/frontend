import React from 'react'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
  act,
} from '@testing-library/react'
import TagsAutocomplete from './tagsAutocomplete'
import i18nextConfig from '../../i18nTesting'
import i18n from 'i18next'
import autocompleteService from 'service/autocompleteService'

const mockSetTag = jest.fn()
const mockSetTags = jest.fn()

autocompleteService.getSuggestions = jest.fn(() =>
  Promise.resolve(['sug1', 'sug2', 'sug3']),
)

function renderContainer(tag = '', useBaseTags = true) {
  let baseTags = [
    { key: 0, label: 'tag0' },
    { key: 1, label: 'tag1' },
    { key: 2, label: 'tag2' },
  ]
  const result = render(
    <TagsAutocomplete
      tag={tag}
      tags={useBaseTags ? baseTags : []}
      setTag={mockSetTag}
      setTags={mockSetTags}
    />,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('TagsAutocomplete', () => {
  it('renders without crashing', async () => {
    const { container } = renderContainer()

    expect(container).toBeTruthy()
  })
  it('displays all given tags as chips', async () => {
    const { getAllByTestId } = renderContainer()
    const chips = await waitForElement(() => getAllByTestId('chip'))

    expect(chips.length).toEqual(3)
    expect(chips[0].textContent).toEqual('tag0')
    expect(chips[1].textContent).toEqual('tag1')
    expect(chips[2].textContent).toEqual('tag2')
  })
  it('displays the given tag in the input field', async () => {
    const { getByTestId } = renderContainer('a tag')
    const input = await waitForElement(() =>
      getByTestId('autocomplete').querySelector('input'),
    )

    expect(input.value).toEqual('a tag')
  })
  it('updates the tag', async () => {
    const { getByTestId } = renderContainer('a new ta')
    const autocomplete = await waitForElement(() => getByTestId('autocomplete'))
    const input = await waitForElement(() =>
      getByTestId('autocomplete').querySelector('input'),
    )

    fireEvent.change(input, { target: { value: 'a new tag!' } })
    fireEvent.blur(autocomplete)

    expect(mockSetTag).toHaveBeenCalledTimes(1)
    expect(mockSetTag).toHaveBeenLastCalledWith('a new tag!')
  })
  it('adds tag to array with entries on click on button', async () => {
    const { getByTestId } = renderContainer('newTag')
    const button = await waitForElement(() => getByTestId('containedButton'))

    fireEvent.click(button)

    expect(mockSetTags).toHaveBeenCalledTimes(1)
    expect(mockSetTags).toHaveBeenLastCalledWith([
      { key: 0, label: 'tag0' },
      { key: 1, label: 'tag1' },
      { key: 2, label: 'tag2' },
      { key: 3, label: 'newTag' },
    ])
    expect(mockSetTag).toHaveBeenCalledTimes(1)
    expect(mockSetTag).toHaveBeenLastCalledWith('')
  })
  it('adds tag to array with no entries on click on button', async () => {
    const { getByTestId } = renderContainer('newTag', false)
    const button = await waitForElement(() => getByTestId('containedButton'))

    fireEvent.click(button)

    expect(mockSetTags).toHaveBeenCalledTimes(1)
    expect(mockSetTags).toHaveBeenLastCalledWith([{ key: 0, label: 'newTag' }])
    expect(mockSetTag).toHaveBeenCalledTimes(1)
    expect(mockSetTag).toHaveBeenLastCalledWith('')
  })
  it('does not add duplicates to array of tags', async () => {
    const { getByTestId } = renderContainer('tag0')
    const button = await waitForElement(() => getByTestId('containedButton'))

    fireEvent.click(button)

    expect(mockSetTags).toHaveBeenCalledTimes(0)
  })
  it('displays helper text if duplicate is provided', async () => {
    const { getByTestId } = renderContainer('tag0')

    const textfield = await waitForElement(() =>
      getByTestId('autocomplete-textfield'),
    )
    const inputBase = await waitForElement(() => textfield.querySelector('div'))
    const button = await waitForElement(() => getByTestId('containedButton'))

    fireEvent.click(button)

    const helperText = await waitForElement(() => textfield.querySelector('p'))

    expect(inputBase.getAttribute('class')).toContain('Mui-error')
    expect(helperText.textContent).toEqual('This tag already exists')
  })
  it('deletes a tag on click on x on chip', async () => {
    const { getAllByTestId } = renderContainer()
    const chips = await waitForElement(() => getAllByTestId('chip'))
    const x = await waitForElement(() => chips[0].querySelector('svg'))

    act(() => {
      fireEvent.click(x)
    })

    expect(mockSetTags).toHaveBeenCalledTimes(1)
    expect(mockSetTags).toHaveBeenLastCalledWith([
      { key: 1, label: 'tag1' },
      { key: 2, label: 'tag2' },
    ])
  })
  xit('submits the tag', async () => {
    const { getByTestId } = renderContainer('a new ta')
    const autocomplete = await waitForElement(() => getByTestId('autocomplete'))
    const input = await waitForElement(() =>
      getByTestId('autocomplete').querySelector('input'),
    )

    fireEvent.change(input, { target: { value: 'a new tag!' } })
    fireEvent.keyPress(autocomplete, {
      keyCode: 'Enter',
      code: 'Enter',
      key: 'Enter',
    })

    expect(mockSetTag).toHaveBeenCalledTimes(1)
    expect(mockSetTag).toHaveBeenLastCalledWith('a new tag!')
  })
})
