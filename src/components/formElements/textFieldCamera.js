import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { useTranslation } from 'react-i18next'

export default function TextFieldCamera(props) {
  const { t } = useTranslation()
  const { id, value, func } = props
  const example = t('forms.photo.helperTexts.example')
  const helperText = t(`forms.photo.helperTexts.${id}`)

  return (
    <Grid item xs={12} sm={6}>
      <TextField
        fullWidth
        id={id}
        label={t(`forms.labels.${id}`)}
        helperText={`${example}: ${helperText}`}
        value={value}
        onChange={func}
        data-testid={`textFieldCamera--${id}`}
      />
    </Grid>
  )
}

TextFieldCamera.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  func: PropTypes.func.isRequired,
}
