import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'

export default function ContainedButton(props) {
  const { text, onClick } = props

  return (
    <Button
      variant="contained"
      color="primary"
      onClick={onClick && onClick}
      data-testid={'containedButton'}
    >
      {text}
    </Button>
  )
}

ContainedButton.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func,
}
