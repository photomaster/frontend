import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import { makeStyles } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
import Chip from 'components/chip/chip'
import RemoteAutocompleteInput from 'components/remoteAutocompleteInput/remoteAutocompleteInput'
import baseStyles from 'styles/baseClasses'
import ContainedButton from './containedButton'

const useStyles = makeStyles(theme => ({
  center_input: {
    display: 'flex',
    justifyContent: 'flex-end',
    [theme.breakpoints.only('xs')]: {
      justifyContent: 'center',
    },
  },
  center_button: {
    [theme.breakpoints.only('xs')]: {
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex',
    },
  },
}))

export default function TagsAutocomplete(props) {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const { t } = useTranslation()
  const { tag, tags, setTag, setTags } = props
  const [submitTag, setSubmitTag] = useState(false)

  useEffect(() => {
    if (submitTag) _handleTagSubmit()
  }, [submitTag])

  const _tagAlreadyExists = () => {
    return (
      tags.filter(curTag => curTag.label.toLowerCase() === tag.toLowerCase())
        .length !== 0
    )
  }

  const _handleTagSubmit = () => {
    if (!_tagAlreadyExists() && tag.length > 0) {
      tags.push({
        key: tags.length > 0 ? tags[tags.length - 1].key + 1 : 0,
        label: tag,
      })
      setTags(tags)
      setTag('')
    }
    setSubmitTag(false)
  }

  const _handleTagDelete = tagToDelete => {
    setTags(tags.filter(curTag => curTag.key !== tagToDelete.key))
  }

  const Chips = () => {
    return tags.map(data => (
      <Chip
        key={data.key}
        label={data.label}
        size="small"
        onDelete={() => _handleTagDelete(data)}
      />
    ))
  }

  return (
    <React.Fragment>
      <Grid item xs={12} sm={6} md={3} className={classes.center_input}>
        <RemoteAutocompleteInput
          fieldName="labels"
          label={t('forms.labels.tag')}
          error={_tagAlreadyExists()}
          helperText={
            _tagAlreadyExists() ? t('forms.helperTexts.tagExists') : ''
          }
          value={tag}
          filterOptions={options => {
            const tagValues = tags.map(v => v.label.toLowerCase())
            return options.filter(
              option => !tagValues.includes(option.toLowerCase()),
            )
          }}
          onChange={(e, curTag) => {
            setTag(curTag)
            const type = e.nativeEvent.type
            if (type !== 'blur') setSubmitTag(true)
          }}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={3} className={classes.center_button}>
        <ContainedButton
          text={t('forms.upload.tags.add')}
          onClick={_handleTagSubmit}
        />
      </Grid>
      <Grid
        item
        xs={12}
        md={6}
        className={`${baseClasses.flexDirectionRow} ${baseClasses.justifyContentCenter} ${baseClasses.flexWrap}`}
      >
        <Chips />
      </Grid>
    </React.Fragment>
  )
}

TagsAutocomplete.propTypes = {
  tag: PropTypes.string.isRequired,
  tags: PropTypes.array.isRequired,
  setTag: PropTypes.func.isRequired,
  setTags: PropTypes.func.isRequired,
}
