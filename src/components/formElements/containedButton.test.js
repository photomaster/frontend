import React from 'react'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import ContainedButton from './containedButton'

const mockOnClick = jest.fn()

const BasicButton = {
  text: 'Basic Button',
}

const InteractiveButton = {
  text: 'Interactive Button',
  onClick: mockOnClick,
}

function renderContainer(button) {
  const result = render(
    <ContainedButton text={button.text} onClick={button.onClick} />,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('ContainedButton', () => {
  it('renders without crashing', async () => {
    const { container: basicContainer } = renderContainer(BasicButton)
    const { container: intContainer } = renderContainer(InteractiveButton)

    expect(basicContainer).toBeTruthy()
    expect(intContainer).toBeTruthy()
  })
  it('displays given text', async () => {
    const { getByTestId } = renderContainer(BasicButton)
    const button = await waitForElement(() => getByTestId('containedButton'))

    expect(button.textContent).toEqual('Basic Button')
  })
  it('calls given onClick function correctly', async () => {
    const { getByTestId } = renderContainer(InteractiveButton)
    const button = await waitForElement(() => getByTestId('containedButton'))

    fireEvent.click(button)

    expect(mockOnClick).toHaveBeenCalledTimes(1)
  })
})
