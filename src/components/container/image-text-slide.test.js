import React from 'react'
import Button from '@material-ui/core/Button'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import ImageTextSlide from './image-text-slide'
import TestImage from 'assets/undraw/terms.svg'

describe('ImageTextSlide', () => {
  it('should render text and image', async () => {
    render(<ImageTextSlide title={'Title'} text={'Lorem ipsum'} />)
    expect(screen.queryByText('Title')).toBeDefined()
    expect(screen.queryByText('Lorem ipsum')).toBeDefined()
  })

  it('should render a button', async () => {
    render(
      <ImageTextSlide
        button={
          <Button variant="contained" color="primary">
            Upload
          </Button>
        }
      />,
    )
    expect(screen.queryByText('Upload')).toBeDefined()
  })

  it('should render child components', async () => {
    render(
      <ImageTextSlide>
        <p>Hello there</p>
        <span>General Kenobi!</span>
      </ImageTextSlide>,
    )

    expect(screen.queryByText('Hello there')).toBeDefined()
    expect(screen.queryByText('Hello there').nodeName).toEqual('P')

    expect(screen.queryByText('General Kenobi!')).toBeDefined()
    expect(screen.queryByText('General Kenobi!').nodeName).toEqual('SPAN')
  })

  it('should render an image', async () => {
    render(<ImageTextSlide image={TestImage} title={'Test title'} />)

    expect(screen.queryByAltText('Test title')).toBeDefined()
    expect(screen.queryByAltText('Test title').nodeName).toEqual('IMG')
  })

  it('should use styling params', async () => {
    render(
      <ImageTextSlide mt={false} reverse={true}>
        <p>Hello there</p>
      </ImageTextSlide>,
    )

    expect(screen.queryByTestId('imageTextSlide')).toBeDefined()
    expect(
      screen.queryByTestId('imageTextSlide').getAttribute('class'),
    ).toContain('makeStyles-containerFirst-')
    expect(
      screen.queryByTestId('imageTextSlide').getAttribute('style'),
    ).toContain('flex-direction: row-reverse')
  })
})
