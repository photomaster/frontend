import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Grid from '@material-ui/core/Grid'
import baseStyles from 'styles/baseClasses'

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(18),
  },
  containerFirst: {
    marginTop: theme.spacing(9),
  },
  image: {
    width: '90%',
    height: 'auto',
  },
}))

export default function ImageTextSlide(props) {
  const classes = useStyles()
  const baseClasses = baseStyles()
  const { title, text, button, image, reverse, id, mt } = props

  return (
    <Grid
      container
      spacing={8}
      className={mt ? classes.container : classes.containerFirst}
      style={reverse ? { flexDirection: 'row-reverse' } : null}
      id={id}
      data-testid={'imageTextSlide'}
    >
      <Grid
        item
        sm={12}
        md={6}
        className={`${baseClasses.justifyContentCenter} ${baseClasses.alignItemsCenter}`}
      >
        {(image && <img className={classes.image} src={image} alt={title} />) ||
          props.children}
      </Grid>
      <Grid item sm={12} md={6} className={baseClasses.alignItemsCenter}>
        <Card className={baseClasses.padding2}>
          <CardContent>
            <Typography component="h3" variant="h3">
              {title}
            </Typography>
            <Typography variant="body2" component="p">
              {!!text && text}
            </Typography>
          </CardContent>
          {!!button && (
            <CardActions className={baseClasses.justifyContentCenter}>
              {button}
            </CardActions>
          )}
        </Card>
      </Grid>
    </Grid>
  )
}

ImageTextSlide.defaultProps = {
  image: null,
  reverse: false,
  title: '',
  text: null,
  button: null,
  id: '',
  mt: true,
}
