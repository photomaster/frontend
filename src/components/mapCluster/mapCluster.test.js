import React from 'react'
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from '@testing-library/react'
import MapClusterComponent from './mapCluster'
import { Map } from 'react-leaflet'

const ClusterSmall = {
  centroid: {
    coordinates: [47.7563, 13.066498],
    type: 'Point',
  },
  id: 1,
  photos: [
    {
      id: 1,
      geo: {
        coordinates: [47.7563, 13.066499],
      },
    },
  ],
  size: 1,
}

const ClusterLarge = {
  centroid: {
    coordinates: [47.7563, 13.066498],
    type: 'Point',
  },
  id: 1,
  photos: [],
  size: 10,
}

const ClusterLargeGreen = {
  centroid: {
    coordinates: [47.7563, 13.066498],
    type: 'Point',
  },
  id: 1,
  photos: [],
  size: 51,
}

const ClusterLargeYellow = {
  centroid: {
    coordinates: [47.7563, 13.066498],
    type: 'Point',
  },
  id: 1,
  photos: [],
  size: 251,
}

const ClusterLargeOrange = {
  centroid: {
    coordinates: [47.7563, 13.066498],
    type: 'Point',
  },
  id: 1,
  photos: [],
  size: 501,
}

const mockPhotoClick = jest.fn()
const mockClusterClick = jest.fn()

function renderContainer(Cluster = ClusterSmall, zoom = 12) {
  const result = render(
    <Map center={[47.7563, 13.066498]} zoom={zoom}>
      <MapClusterComponent
        key={1}
        zoom={zoom}
        cluster={Cluster}
        onPhotoClick={mockPhotoClick}
        onClusterClick={mockClusterClick}
      />
    </Map>,
  )
  return result
}

beforeEach(() => {
  jest.clearAllMocks()
})
afterEach(cleanup)
describe('MapCluster', () => {
  it('renders without crashing', async () => {
    const { container: ContainerSmall } = renderContainer()
    const { container: ContainerLarge } = renderContainer(ClusterLargeGreen)

    expect(ContainerSmall).toBeTruthy()
    expect(ContainerLarge).toBeTruthy()
  })
  it('renders photo markers for small clusters', async () => {
    const { container } = renderContainer()
    const marker = await waitForElement(() => container.querySelector('img'))

    expect(marker).toBeTruthy()
    expect(marker.getAttribute('class')).toContain('leaflet-marker-icon')
  })
  it('does not display photo markers if zoomed out', async () => {
    const { container } = renderContainer(ClusterSmall, 5)
    const marker = container.querySelector('img')

    expect(marker).toBeFalsy()
  })
  it('renders bubble markers for large clusters', async () => {
    const { findByTestId } = renderContainer(ClusterLargeGreen)
    const marker = await waitForElement(() =>
      findByTestId('mapCluster--bubble--51'),
    )

    expect(marker).toBeTruthy()
    expect(marker.textContent).toEqual('51')
  })
  it('calls function on photo click', async () => {
    const { container } = renderContainer()
    const marker = await waitForElement(() => container.querySelector('img'))

    fireEvent.click(marker)

    expect(mockPhotoClick).toHaveBeenCalledTimes(1)
  })
  it('calls function on bubble click', async () => {
    const { findByTestId } = renderContainer(ClusterLargeGreen)
    const marker = await waitForElement(() =>
      findByTestId('mapCluster--bubble--51'),
    )

    fireEvent.click(marker)

    expect(mockClusterClick).toHaveBeenCalledTimes(1)
  })
  it('renders bubble in different colors depending on size', async () => {
    const { findByTestId: findByTestIdGreen } = renderContainer(
      ClusterLargeGreen,
    )
    const { findByTestId: findByTestIdYellow } = renderContainer(
      ClusterLargeYellow,
    )
    const { findByTestId: findByTestIdOrange } = renderContainer(
      ClusterLargeOrange,
    )
    const markerGreen = await waitForElement(() =>
      findByTestIdGreen('mapCluster--bubble--51'),
    )
    const markerYellow = await waitForElement(() =>
      findByTestIdYellow('mapCluster--bubble--251'),
    )
    const markerOrange = await waitForElement(() =>
      findByTestIdOrange('mapCluster--bubble--501'),
    )

    expect(markerGreen.getAttribute('style')).toContain(
      'background-color:rgba(110, 204, 57, 0.6)',
    )
    expect(markerYellow.getAttribute('style')).toContain(
      'background-color:rgba(240, 194, 12, 0.6)',
    )
    expect(markerOrange.getAttribute('style')).toContain(
      'background-color:rgba(241, 128, 23, 0.6)',
    )
  })
  it('renders bubble in different sizes depending on size', async () => {
    const { findByTestId: findByTestIdSize1 } = renderContainer(ClusterLarge)
    const { findByTestId: findByTestIdSize2 } = renderContainer(
      ClusterLargeGreen,
    )
    const { findByTestId: findByTestIdSize3 } = renderContainer(
      ClusterLargeYellow,
    )
    const { findByTestId: findByTestIdSize4 } = renderContainer(
      ClusterLargeOrange,
    )
    const markerSize1 = await waitForElement(() =>
      findByTestIdSize1('mapCluster--bubble--10'),
    )
    const markerSize2 = await waitForElement(() =>
      findByTestIdSize2('mapCluster--bubble--51'),
    )
    const markerSize3 = await waitForElement(() =>
      findByTestIdSize3('mapCluster--bubble--251'),
    )
    const markerSize4 = await waitForElement(() =>
      findByTestIdSize4('mapCluster--bubble--501'),
    )

    expect(markerSize1.getAttribute('style')).toContain('width:30', 'height:30')
    expect(markerSize2.getAttribute('style')).toContain('width:40', 'height:40')
    expect(markerSize3.getAttribute('style')).toContain('width:50', 'height:50')
    expect(markerSize4.getAttribute('style')).toContain('width:75', 'height:75')
  })
})
