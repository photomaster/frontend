/* eslint react/prefer-stateless-function: 0 */
import React from 'react'
import PropTypes from 'prop-types'
import L, { Icon } from 'leaflet'
import { Marker } from 'react-leaflet'

import ReactDOMServer from 'react-dom/server'
import photoIcon from '../../assets/icons/icon_256x256.svg'

const icon = new Icon({
  iconUrl: photoIcon,
  iconSize: [25, 25],
})

class ClusterIcon extends React.Component {
  render() {
    const size = this.props.size || 0

    let visualSize = 30
    let backgroundColor = 'rgba(110, 204, 57, 0.6)' // green

    if (size > 500) {
      visualSize = 75
      backgroundColor = 'rgba(241, 128, 23, 0.6)' // orange
    } else if (size > 250) {
      visualSize = 50
      backgroundColor = 'rgba(240, 194, 12, 0.6)' // yellow
    } else if (size > 50) {
      visualSize = 40
    }

    const style = {
      width: visualSize,
      height: visualSize,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: '50%',
      backgroundColor: backgroundColor,
    }

    if (size <= 0) {
      return null
    }

    return (
      <div style={style} data-testid={`mapCluster--bubble--${size}`}>
        {size}
      </div>
    )
  }
}

ClusterIcon.propTypes = {
  size: PropTypes.number,
}

export default class MapClusterComponent extends React.Component {
  render() {
    const { cluster, zoom, onPhotoClick, onClusterClick } = this.props

    if (cluster.size <= 3) {
      if (zoom <= 9) {
        return null
      }

      return cluster.photos.map(photo => (
        <Marker
          key={`map-photo-${photo.id}`}
          position={[
            photo.geo.coordinates[1] + (Math.cos(photo.id) - 0.5) * 0.00001,
            photo.geo.coordinates[0] + (Math.cos(photo.id) - 0.5) * 0.00001,
          ]}
          icon={icon}
          onClick={() => {
            onPhotoClick(photo)
          }}
        />
      ))
    }

    const clusterIcon = L.divIcon({
      className: 'custom-icon',
      html: ReactDOMServer.renderToString(<ClusterIcon size={cluster.size} />),
    })

    return (
      <Marker
        key={`map-cluster-${cluster.id}`}
        onClick={() => onClusterClick(cluster)}
        position={[
          cluster.centroid.coordinates[1] +
            (Math.cos(cluster.id) - 0.5) * 0.00001,
          cluster.centroid.coordinates[0] +
            (Math.cos(cluster.id) - 0.5) * 0.00001,
        ]}
        icon={clusterIcon}
      />
    )
  }
}

MapClusterComponent.propTypes = {
  cluster: PropTypes.shape({
    centroid: PropTypes.shape({
      coordinates: PropTypes.array.isRequired,
      type: PropTypes.string.isRequired,
    }),
    id: PropTypes.number.isRequired,
    photos: PropTypes.array,
  }).isRequired,
  zoom: PropTypes.number.isRequired,
  onPhotoClick: PropTypes.func.isRequired,
  onClusterClick: PropTypes.func.isRequired,
}
