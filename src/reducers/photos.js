const photosReducer = (
  state = { active: null, near: [], loading: false },
  action,
) => {
  switch (action.type) {
    case 'getAllNearPhotos/success':
      return {
        ...state,
        near: action.payload,
      }
    case 'getAllNearPhotos/error':
      return {
        ...state,
        errors: { getAllNearPhotos: action.payload },
      }
    case 'getNearPhotosOffset/initiated':
      return {
        ...state,
        loading: true,
      }
    case 'getNearPhotosOffset/success':
      return {
        ...state,
        near: action.payload,
        loading: false,
      }
    case 'getNearPhotosOffset/error':
      return {
        ...state,
        errors: { getNearPhotos: action.payload },
        loading: false,
      }
    case 'activePhoto/set':
      return {
        ...state,
        active: action.payload,
      }
    case 'activePhoto/unset':
      return {
        ...state,
        active: null,
      }
    case 'update/photo/initiated':
      return {
        ...state,
        updating: true,
      }
    case 'update/photo/success':
      return {
        ...state,
        photo: action.payload,
        updating: false,
        errors: null,
      }
    case 'update/photo/error':
      return {
        ...state,
        errors: { update: action.payload },
        updating: false,
      }
    case 'update/weather/initiated':
      return {
        ...state,
        updating: true,
      }
    case 'update/weather/success':
      return {
        ...state,
        weather: action.payload,
        updating: false,
        errors: null,
      }
    case 'update/weather/error':
      return {
        ...state,
        errors: { update: action.payload },
        updating: false,
      }
    case 'update/camera/initiated':
      return {
        ...state,
        updating: true,
      }
    case 'update/camera/success':
      return {
        ...state,
        camera: action.payload,
        updating: false,
        errors: null,
      }
    case 'update/camera/error':
      return {
        ...state,
        errors: { update: action.payload },
        updating: false,
      }
    case 'remove/photo/initiated':
      return {
        ...state,
        updating: true,
      }
    case 'remove/photo/success':
      return {
        ...state,
        photo: null,
        updating: false,
        errors: null,
      }
    case 'remove/photo/error':
      return {
        ...state,
        errors: { update: action.payload },
        updating: false,
      }
    case 'updateFavourites/initiated':
      return {
        ...state,
        loadingFavs: true,
      }
    case 'updateFavourites/success':
      return {
        ...state,
        favourites: action.payload,
        loadingFavs: false,
        errors: null,
      }
    case 'updateFavourites/error':
      return {
        ...state,
        errors: { updateFavourites: action.payload },
        loadingFavs: false,
      }
    case 'updateFavourites/remove':
      return {
        ...state,
        favourites: null,
        loadingFavs: false,
        errors: null,
      }
    case 'reset/errors':
      return {
        ...state,
        errors: null,
      }
    default:
      return state
  }
}

export default photosReducer
