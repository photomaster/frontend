import authService from 'service/authService'

// Saves the current user
const authenticated = authService.isSignedIn()
let initialState = { loggedIn: false, registering: false, loggingIn: false }
if (authenticated) {
  const user =
    JSON.parse(sessionStorage.getItem('user')) ??
    JSON.parse(localStorage.getItem('user'))
  initialState = { ...initialState, loggedIn: true, user: user }
}

const currentUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'getUserLocation/success':
      return {
        ...state,
        location: action.payload,
      }
    case 'getUserLocation/denied':
      return {
        ...state,
        location: action.payload,
      }
    case 'getUserLocation/error':
      return {
        ...state,
        errors: { location: action.payload },
      }
    case 'register/initiated':
      return {
        ...state,
        user: action.payload,
        registering: true,
      }
    case 'register/success':
      return {
        ...state,
        user: action.payload,
        registering: false,
        loggedIn: true,
        errors: null,
      }
    case 'register/error':
      return {
        ...state,
        errors: { register: action.payload },
        registering: false,
      }
    case 'login/initiated':
      return {
        ...state,
        user: action.payload,
        loggingIn: true,
      }
    case 'login/success':
      return {
        ...state,
        user: action.payload,
        loggingIn: false,
        loggedIn: true,
        errors: null,
      }
    case 'login/error':
      return {
        ...state,
        errors: { login: action.payload },
        loggingIn: false,
        loggedIn: false,
      }
    case 'logout/success':
      return {
        ...state,
        user: {},
        loggedIn: false,
      }
    case 'update/user/initiated':
      return {
        ...state,
        updating: true,
      }
    case 'update/user/success':
      return {
        ...state,
        user: action.payload,
        updating: false,
        errors: null,
      }
    case 'update/user/error':
      return {
        ...state,
        errors: { update: action.payload },
        updating: false,
      }
    case 'reset/errors':
      return {
        ...state,
        errors: null,
      }
    default:
      return state
  }
}

export default currentUserReducer
