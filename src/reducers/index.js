import { combineReducers } from 'redux'
import currentUserReducer from './currentUser'
import photosReducer from './photos'
import applicationStateReducer from './applicationState'

const rootReducer = combineReducers({
  currentUser: currentUserReducer,
  photos: photosReducer,
  applicationState: applicationStateReducer,
})

export default rootReducer
