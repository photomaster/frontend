import { cleanup } from '@testing-library/react'
import rootReducer from 'reducers/'
import UserModel from 'models/user'
import PhotoModel from 'models/photo'
import PhotoDataModel from 'models/photoData'
import WeatherModel from 'models/weatherState'

const TestUser = new UserModel({
  id: 1,
  username: 'username',
  displayName: 'displayName',
  email: 'test@test.com',
  description: 'description',
  avatarImage: '/avatar',
  bannerImage: '/banner',
})
const TestPhoto = new PhotoModel({
  id: 1,
  url: '/photo1',
  name: 'title1',
  description: 'desc1',
  geo: {
    type: 'Point',
    coordinates: [47.811195, 13.033229],
  },
  photodata: '/photodata1',
  solarData: '/some/url/to/solar/data/1/',
  currentWeather: null,
  originalCreationDate: new Date('2020-11-25T05:09:04Z'),
  weatherAtCreation: '/some/url/to/weatherState/33/',
  image: '/image1',
  imageThumbnailTeaser: '/thumbnail1',
  imageThumbnailTeaserSmall: '/thumbnailsmall1',
  imageThumbnailPreview: '/thumbnailpreview1',
  labels: [
    {
      title: 'label1',
      score: 1,
    },
    {
      title: 'label2',
      score: 1,
    },
  ],
  user: TestUser,
})
const TestWeatherState = new WeatherModel({
  description: 'clear sky',
  temperature: 10,
})
const TestPhotoData = new PhotoDataModel({
  id: 1,
  url: '/photosData/1',
  photo: '/photo/1',
  make: 'make',
  model: 'model',
  lensMake: 'lensMake',
  lensModel: 'lensModel',
  focalLength: 'focalLength',
  fNumber: 'fNumber',
  whiteBalance: 'whiteBalance',
  lightSource: 'lightSource',
  exposureTime: 'exposureTime',
  exposureProgram: 'exposureProgram',
  exposureMode: 'exposureMode',
  isoSpeed: 'isoSpeed',
  flash: 'flash',
  pixelXDimension: 200,
  pixelYDimension: 100,
  orientation: 3,
  software: 'software',
})

afterEach(cleanup)
describe('photos reducers', () => {
  it('reset/errors resets the errors', async () => {
    expect(
      rootReducer(
        {},
        {
          type: 'reset/errors',
        },
      ),
    ).toMatchObject({
      photos: {
        errors: null,
      },
    })
  })
  describe('active photo', () => {
    it('activePhoto/set sets the active photo', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'activePhoto/set',
            payload: TestPhoto,
          },
        ),
      ).toMatchObject({
        photos: {
          active: TestPhoto,
        },
      })
    })
    it('activePhoto/unset removes the active photo', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'activePhoto/unset',
          },
        ),
      ).toMatchObject({
        photos: {
          active: null,
        },
      })
    })
  })
  describe('get near photos', () => {
    it('getAllNearPhotos/success sets the near photos', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'getAllNearPhotos/success',
            payload: [TestPhoto],
          },
        ),
      ).toMatchObject({
        photos: {
          near: [TestPhoto],
        },
      })
    })
    it('getAllNearPhotos/error to set error', async () => {
      const error = new Error('get all near photos error')
      expect(
        rootReducer(
          {},
          {
            type: 'getAllNearPhotos/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        photos: {
          errors: { getAllNearPhotos: error },
        },
      })
    })
  })
  describe('get near photos offset', () => {
    it('getNearPhotosOffset/initiated sets loading to true', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'getNearPhotosOffset/initiated',
          },
        ),
      ).toMatchObject({
        photos: {
          loading: true,
        },
      })
    })
    it('getNearPhotosOffset/success sets the near photos and resets loading', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'getNearPhotosOffset/success',
            payload: [TestPhoto],
          },
        ),
      ).toMatchObject({
        photos: {
          near: [TestPhoto],
          loading: false,
        },
      })
    })
    it('getNearPhotosOffset/error to set error and resets loading', async () => {
      const error = new Error('get near photos offset error')
      expect(
        rootReducer(
          {},
          {
            type: 'getNearPhotosOffset/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        photos: {
          errors: { getNearPhotos: error },
          loading: false,
        },
      })
    })
  })
  describe('update photo', () => {
    it('update/photo/initiated sets updating to true', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'update/photo/initiated',
          },
        ),
      ).toMatchObject({
        photos: {
          updating: true,
        },
      })
    })
    it('update/photo/success sets the photo and resets updating/error', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'update/photo/success',
            payload: TestPhoto,
          },
        ),
      ).toMatchObject({
        photos: {
          photo: TestPhoto,
          updating: false,
          errors: null,
        },
      })
    })
    it('update/photo/error sets error and resets updating', async () => {
      const error = new Error('update photo error')
      expect(
        rootReducer(
          {},
          {
            type: 'update/photo/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        photos: {
          errors: { update: error },
          updating: false,
        },
      })
    })
    it('update/weather/initiated sets updating to true', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'update/weather/initiated',
          },
        ),
      ).toMatchObject({
        photos: {
          updating: true,
        },
      })
    })
    it('update/weather/success sets weather and resets errors/updating', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'update/weather/success',
            payload: TestWeatherState,
          },
        ),
      ).toMatchObject({
        photos: {
          weather: TestWeatherState,
          updating: false,
          errors: null,
        },
      })
    })
    it('update/weather/error sets error and resets updating', async () => {
      const error = new Error('update weather error')
      expect(
        rootReducer(
          {},
          {
            type: 'update/weather/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        photos: {
          errors: { update: error },
          updating: false,
        },
      })
    })
    it('update/camera/initiated sets updating to true', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'update/camera/initiated',
          },
        ),
      ).toMatchObject({
        photos: {
          updating: true,
        },
      })
    })
    it('update/camera/success sets camera data and resets errors/updating', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'update/camera/success',
            payload: TestPhotoData,
          },
        ),
      ).toMatchObject({
        photos: {
          camera: TestPhotoData,
          updating: false,
          errors: null,
        },
      })
    })
    it('update/camera/error sets errors and resets updating', async () => {
      const error = new Error('update camera error')
      expect(
        rootReducer(
          {},
          {
            type: 'update/camera/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        photos: {
          errors: { update: error },
          updating: false,
        },
      })
    })
  })
  describe('remove photo', () => {
    it('remove/photo/initiated sets updating to true', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'remove/photo/initiated',
          },
        ),
      ).toMatchObject({
        photos: {
          updating: true,
        },
      })
    })
    it('remove/photo/success unsets photo and resets errors/updating', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'remove/photo/success',
          },
        ),
      ).toMatchObject({
        photos: {
          photo: null,
          updating: false,
          errors: null,
        },
      })
    })
    it('remove/photo/error sets error and resets updating', async () => {
      const error = new Error('remove photo error')
      expect(
        rootReducer(
          {},
          {
            type: 'remove/photo/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        photos: {
          errors: { update: error },
          updating: false,
        },
      })
    })
  })
  describe('update favourites', () => {
    it('updateFavourites/initiated sets loading to true', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'updateFavourites/initiated',
          },
        ),
      ).toMatchObject({
        photos: {
          loadingFavs: true,
        },
      })
    })
    it('updateFavourites/success sets favourites and resets errors/loading', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'updateFavourites/success',
            payload: [TestPhoto],
          },
        ),
      ).toMatchObject({
        photos: {
          favourites: [TestPhoto],
          loadingFavs: false,
          errors: null,
        },
      })
    })
    it('updateFavourites/remove unsets favourites and resets errors/loading', async () => {
      expect(
        rootReducer(
          {},
          {
            type: 'updateFavourites/remove',
          },
        ),
      ).toMatchObject({
        photos: {
          favourites: null,
          errors: null,
          loadingFavs: false,
        },
      })
    })
    it('updateFavourites/error sets error and resets loading', async () => {
      const error = new Error('remove photo error')
      expect(
        rootReducer(
          {},
          {
            type: 'updateFavourites/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        photos: {
          errors: { updateFavourites: error },
          loadingFavs: false,
        },
      })
    })
  })
})
