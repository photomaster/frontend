import { cleanup } from '@testing-library/react'
import rootReducer from 'reducers/'
import UserModel from 'models/user'

const TestUser = new UserModel({
  id: 1,
  username: 'username',
  displayName: 'displayName',
  email: 'test@test.com',
  description: 'description',
  avatarImage: '/avatar',
  bannerImage: '/banner',
})

beforeEach(() => {
  jest.resetModules()
  sessionStorage.clear()
})
afterEach(cleanup)
describe('user reducers', () => {
  it('reset/errors resets the errors', () => {
    expect(
      rootReducer(
        {},
        {
          type: 'reset/errors',
        },
      ),
    ).toMatchObject({
      currentUser: {
        errors: null,
      },
    })
  })
  describe('sets initial status based on authentication', () => {
    it('not logged in', () => {
      const reducer = require('./currentUser').default

      expect(reducer(undefined, {})).toEqual({
        loggedIn: false,
        registering: false,
        loggingIn: false,
      })
    })
    it('logged in (user obj available)', () => {
      sessionStorage.setItem('token', 'some token')
      sessionStorage.setItem('user', JSON.stringify(TestUser))
      const reducer = require('./currentUser').default

      expect(reducer(undefined, {})).toEqual({
        loggedIn: true,
        user: TestUser,
        registering: false,
        loggingIn: false,
      })
    })
    it('logged in (user obj not available)', () => {
      sessionStorage.setItem('token', 'some token')
      const reducer = require('./currentUser').default

      expect(reducer(undefined, {})).toEqual({
        loggedIn: true,
        user: null,
        registering: false,
        loggingIn: false,
      })
    })
  })
  describe('user location', () => {
    it('getUserLocation/success sets user location', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'getUserLocation/success',
            payload: { access: true, coordinates: [1, 2] },
          },
        ),
      ).toMatchObject({
        currentUser: {
          location: {
            access: true,
            coordinates: [1, 2],
          },
        },
      })
    })
    it('getUserLocation/denied sets user location', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'getUserLocation/denied',
            payload: { access: false, coordinates: [1, 2] },
          },
        ),
      ).toMatchObject({
        currentUser: {
          location: {
            access: false,
            coordinates: [1, 2],
          },
        },
      })
    })
    it('getUserLocation/error sets error', () => {
      const error = new Error('get user location error')
      expect(
        rootReducer(
          {},
          {
            type: 'getUserLocation/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        currentUser: {
          errors: { location: error },
        },
      })
    })
  })
  describe('register user', () => {
    it('register/initiated sets user/registering', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'register/initiated',
            payload: TestUser,
          },
        ),
      ).toMatchObject({
        currentUser: {
          user: TestUser,
          registering: true,
        },
      })
    })
    it('register/success sets user/logged status and resets errors/registering', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'register/success',
            payload: TestUser,
          },
        ),
      ).toMatchObject({
        currentUser: {
          user: TestUser,
          registering: false,
          loggedIn: true,
          errors: null,
        },
      })
    })
    it('register/error sets errors and resets registering', () => {
      const error = new Error('register error')
      expect(
        rootReducer(
          {},
          {
            type: 'register/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        currentUser: {
          errors: { register: error },
          registering: false,
        },
      })
    })
  })
  describe('login user', () => {
    it('login/initiated sets user/logging', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'login/initiated',
            payload: TestUser,
          },
        ),
      ).toMatchObject({
        currentUser: {
          user: TestUser,
          loggingIn: true,
        },
      })
    })
    it('login/success sets user/logged status and resets errors/logging', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'login/success',
            payload: TestUser,
          },
        ),
      ).toMatchObject({
        currentUser: {
          user: TestUser,
          loggingIn: false,
          loggedIn: true,
          errors: null,
        },
      })
    })
    it('login/error sets errors/logged status/logging', () => {
      const error = new Error('login error')
      expect(
        rootReducer(
          {},
          {
            type: 'login/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        currentUser: {
          errors: { login: error },
          loggingIn: false,
          loggedIn: false,
        },
      })
    })
  })
  describe('logout user', () => {
    it('logout/success sets logged status and resets user', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'logout/success',
          },
        ),
      ).toMatchObject({
        currentUser: {
          user: {},
          loggedIn: false,
        },
      })
    })
  })
  describe('update user', () => {
    it('update/initiated sets updating', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'update/user/initiated',
            payload: TestUser,
          },
        ),
      ).toMatchObject({
        currentUser: {
          updating: true,
        },
      })
    })
    it('update/success sets user and resets errors/updating', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'update/user/success',
            payload: TestUser,
          },
        ),
      ).toMatchObject({
        currentUser: {
          user: TestUser,
          updating: false,
          errors: null,
        },
      })
    })
    it('creates update/user/error when updating profile has failed', () => {
      const error = new Error('update user error')
      expect(
        rootReducer(
          {},
          {
            type: 'update/user/error',
            payload: error,
          },
        ),
      ).toMatchObject({
        currentUser: {
          errors: { update: error },
          updating: false,
        },
      })
    })
  })
})
