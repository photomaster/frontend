// Saves interactions with the app

const applicationStateReducer = (
  state = { lightbox: { open: false } },
  action,
) => {
  switch (action.type) {
    case 'lightboxInteraction/open':
      return {
        ...state,
        lightbox: action.payload,
      }
    case 'lightboxInteraction/close':
      return {
        ...state,
        lightbox: action.payload,
      }
    default:
      return state
  }
}

export default applicationStateReducer
