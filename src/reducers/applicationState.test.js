import { cleanup } from '@testing-library/react'
import rootReducer from 'reducers/'

afterEach(cleanup)
describe('application reducers', () => {
  describe('lightbox', () => {
    it('lightboxInteraction/open sets lightbox to true', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'lightboxInteraction/open',
            payload: { open: true },
          },
        ),
      ).toMatchObject({
        applicationState: {
          lightbox: {
            open: true,
          },
        },
      })
    })
    it('lightboxInteraction/close sets lightbox to false', () => {
      expect(
        rootReducer(
          {},
          {
            type: 'lightboxInteraction/close',
            payload: { open: false },
          },
        ),
      ).toMatchObject({
        applicationState: {
          lightbox: {
            open: false,
          },
        },
      })
    })
  })
})
