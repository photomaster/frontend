import { useState, useEffect } from 'react'
import { throttle } from 'lodash'

const useInfiniteScroll = (callback, fetchedAll) => {
  const [isFetching, setIsFetching] = useState(false)

  useEffect(() => {
    window.addEventListener('scroll', isScrollingThrottled)
    return () => window.removeEventListener('scroll', isScrollingThrottled)
  }, [])

  useEffect(() => {
    if (!isFetching || fetchedAll) return
    callback()
  }, [isFetching])

  function isScrolling() {
    const scrollTop =
      (document.documentElement && document.documentElement.scrollTop) ||
      document.body.scrollTop
    const scrollHeight =
      (document.documentElement && document.documentElement.scrollHeight) ||
      document.body.scrollHeight
    if (isFetching) return
    if (scrollTop + window.innerHeight >= scrollHeight - 230)
      setIsFetching(true)
  }

  const isScrollingThrottled = throttle(isScrolling, 1000)

  return [isFetching, setIsFetching]
}

export default useInfiniteScroll
