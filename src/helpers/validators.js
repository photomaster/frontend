export const passwordIsValid = () => {
  // password strength is handled by the backend
  return true
}

export const passwordsMatch = (value1, value2) => {
  return value1 === value2
}

export const emailIsValid = value => {
  const regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i //eslint-disable-line
  return regex.test(value)
}

export const bioIsValid = value => {
  return value.length >= 1 && value.length <= 600
}

export const usernameIsValid = value => {
  const regex = /(?!.*[\-\_]{2,})^[a-zA-Z0-9\-\_]{3,24}$/gm //eslint-disable-line
  return regex.test(value)
}

export const displayNameIsValid = value => {
  const regex = /(?!.*[\-\_]{2,})^[a-zA-Z0-9\-\_]{3,24}$/gm //eslint-disable-line
  return regex.test(value)
}

export const titleIsValid = value => {
  return value.length >= 3 && value.length <= 30
}

export const descIsValid = value => {
  return value.length >= 5 && value.length <= 255
}

export const numberIsValid = value => {
  const regex = /^(0|([1-9]\d*)(\.|\,)\d*|0?(\.|\,)\d*[1-9]\d*|[1-9]\d*)$/gm //eslint-disable-line
  return regex.test(value)
}

export const errorMessages = {
  password:
    'Minimum length 8 characters. Password must contain 1 uppercase letter, 1 lowercase letter and 1 number.',
  passwordsMatch: "Passwords don't match.",
  email: 'Email address is not valid.',
  bio: 'Maximum length 600 characters.',
  username:
    'Minimum length 3 characters. Can contain letters, numbers and the special characters underscore and dash. Special characters are not allowed consecutively or combined.',
  displayName:
    'Minimum length 3 characters. Can contain letters, numbers and the special characters underscore and dash. Special characters are not allowed consecutively or combined.',
  title: 'Minimum length 3 characters. Maximum length 30 characters.',
  desc: 'Minimum length 5 characters. Maximum length 255 characters.',
  number: 'Enter a valid number.',
}

export default {
  passwordIsValid,
  passwordsMatch,
  emailIsValid,
  bioIsValid,
  usernameIsValid,
  displayNameIsValid,
  titleIsValid,
  descIsValid,
  numberIsValid,
  errorMessages,
}
