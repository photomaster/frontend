import validators from 'helpers/validators'

const testData = [
  {
    function: validators.emailIsValid,
    message: validators.errorMessages.email,
    wrongValues: ['a@b', 'MailATMail.com', 'test@mail.a', ' ', ''],
    correctValues: [
      'mail@test.com',
      'test@user.at',
      'google@gmail.com',
      'a@b.net',
    ],
  },
  {
    function: validators.bioIsValid,
    message: validators.errorMessages.bio,
    wrongValues: ['a'.repeat(601), ''],
    correctValues: ['bio', 'a'.repeat(300), 'a'.repeat(600)],
  },
  {
    function: validators.usernameIsValid,
    message: validators.errorMessages.username,
    wrongValues: [
      'Me',
      '__Testuser__',
      '_-Anna-_',
      'Alone@Home',
      '#user',
      'user!',
      'Max Mustermann',
      ' ',
      '',
    ],
    correctValues: [
      '_TestUser_',
      'Ina',
      '_Nice-Username_',
      '123',
      'Max_Mustermann',
      'N0b0dy',
      'NOT_SHOUTING1',
    ],
  },
  {
    function: validators.displayNameIsValid,
    message: validators.errorMessages.displayName,
    wrongValues: [
      'Me',
      '__Testuser__',
      '_-Anna-_',
      'Alone@Home',
      '#user',
      'user!',
      'Max Mustermann',
      ' ',
      '',
    ],
    correctValues: [
      '_TestUser_',
      'Ina',
      '_Nice-Username_',
      '123',
      'Max_Mustermann',
      'N0b0dy',
      'NOT_SHOUTING1',
    ],
  },
  {
    function: validators.titleIsValid,
    message: validators.errorMessages.title,
    wrongValues: ['AT', 'a'.repeat(31), ' ', ''],
    correctValues: [
      'Title',
      'Title with spaces',
      'Title with special characters!',
      'a'.repeat(3),
      'a'.repeat(30),
    ],
  },
  {
    function: validators.descIsValid,
    message: validators.errorMessages.desc,
    wrongValues: ['Desc', 'a'.repeat(256), ' ', ''],
    correctValues: [
      'Description',
      'Description with spaces',
      'Description with special?',
      'a'.repeat(5),
      'a'.repeat(255),
    ],
  },
]

const passwordsData = {
  function: validators.passwordsMatch,
  message: validators.errorMessages.passwordsMatch,
  wrongValues: [
    {
      password1: 'T0p_Secret',
      password2: 'TOp_Secret',
    },
    {
      password1: '1234Aa78',
      password2: '1234Aa7B',
    },
  ],
  correctValues: [
    {
      password1: 'T0p_Secret',
      password2: 'T0p_Secret',
    },
    {
      password1: '1234Aa78',
      password2: '1234Aa78',
    },
  ],
}

describe('Validators', () => {
  it('returns false for wrong values', () => {
    testData.forEach(entry => {
      let func = entry.function
      entry.wrongValues.forEach(value => {
        expect(func(value)).toBeFalsy()
      })
    })

    passwordsData.wrongValues.forEach(entry => {
      expect(
        passwordsData.function(entry.password1, entry.password2),
      ).toBeFalsy()
    })
  })

  it('returns true for correct values', () => {
    testData.forEach(entry => {
      let func = entry.function
      entry.correctValues.forEach(value => {
        expect(func(value)).toBeTruthy()
      })
    })

    passwordsData.correctValues.forEach(entry => {
      expect(
        passwordsData.function(entry.password1, entry.password2),
      ).toBeTruthy()
    })
  })

  it('has corresponding error message for every validator', () => {
    testData.forEach(entry => {
      const message = entry.message
      expect(typeof message).toBe('string')
    })

    const message = passwordsData.message
    expect(typeof message).toBe('string')
  })
})
