import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import { initReactI18next } from 'react-i18next'
import detector from 'i18next-browser-languagedetector'
import moment from 'moment'
import 'moment/locale/de'

i18n
  .use(detector)
  .use(Backend)
  .use(initReactI18next)
  .init({
    detection: {
      order: ['localStorage', 'navigator'],
    },
    backend: {
      loadPath: '/assets/translations/{{lng}}/{{ns}}.json',
    },
    fallbackLng: 'en',
    whitelist: ['en', 'de'],
    debug: false,
    ns: ['main'],
    defaultNS: 'main',
    keySeparator: '.',
    interpolation: {
      escapeValue: false,
      formatSeparator: ',',
      format: function(value, format) {
        if (value instanceof Date && format === 'dateLong')
          return moment(value).format('LL')
        if (value instanceof Date && format === 'dateShort')
          return moment(value).format('l')
        if (value instanceof Date && format === 'timeLong')
          return moment(value).format('LTS')
        if (value instanceof Date && format === 'timeShort')
          return moment(value).format('LT')
        return value
      },
    },
    react: {
      wait: true,
    },
  })

i18n.on('initialized', function handleInitializied() {
  moment.locale(i18n.language)
})

i18n.on('languageChanged', function handleLanguageChange(lng) {
  moment.locale(lng)
})

export default i18n
