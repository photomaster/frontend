import { makeStyles } from '@material-ui/core'

const styles = makeStyles(theme => ({
  test: {
    backgroundColor: 'red',
  },
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  headline: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
  },
  relative: {
    position: 'relative',
  },
  displayNone: {
    display: 'none',
  },
  backgroundNone: {
    background: 'none',
  },
  fullWidth: {
    width: '100%',
  },
  margin05: {
    margin: theme.spacing(0.5),
  },
  margin1: {
    margin: theme.spacing(1),
  },
  margin2: {
    margin: theme.spacing(2),
  },
  marginTop0: {
    marginTop: 0,
  },
  marginTop1: {
    marginTop: theme.spacing(1),
  },
  marginTop3: {
    marginTop: theme.spacing(3),
  },
  marginTop10: {
    marginTop: theme.spacing(10),
  },
  marginBottom1: {
    marginBottom: theme.spacing(1),
  },
  marginBottom2: {
    marginBottom: '2rem',
  },
  marginBottom4: {
    marginBottom: theme.spacing(4),
  },
  padding: {
    padding: theme.spacing(1),
  },
  padding2: {
    padding: theme.spacing(2),
  },
  flex: {
    display: 'flex',
  },
  flexWrap: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  flexDirectionColumn: {
    display: 'flex',
    flexDirection: 'column',
  },
  flexDirectionRow: {
    display: 'flex',
    flexDirection: 'row',
  },
  justifyContentCenter: {
    display: 'flex',
    justifyContent: 'center',
  },
  alignItemsCenter: {
    display: 'flex',
    alignItems: 'center',
  },
  alignItemsBaseline: {
    display: 'flex',
    alignItems: 'baseline',
  },
  alignContentBetween: {
    alignContent: 'space-between',
  },
  verticalCenter: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
  },
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flexGrow: 1,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  buttonProgress: {
    color: theme.palette.primary[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -6,
    marginLeft: -12,
  },
  banner: {
    height: '50vh',
    background: theme.palette.darkBackground,
    backgroundSize: 'cover',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
  },
  tilesHeadline: {
    fontFamily: 'Roboto, sans-serif',
    fontSize: '1.2rem',
    textAlign: 'left',
    marginTop: '1rem',
    marginBottom: '1rem',
    fontWeight: 'bold',
  },
  bold: {
    fontWeight: 'bold',
  },
  redBackground: {
    backgroundColor: '#c62828 !important',
  },
  whiteBackground: {
    background: 'white',
  },
  textAlignCenter: {
    textAlign: 'center',
  },
  wordBreak: {
    wordBreak: 'break-word',
  },
  image: {
    width: '100%',
    height: 'auto',
    margin: `${theme.spacing(4)}px 0`,
  },
}))

export default styles
