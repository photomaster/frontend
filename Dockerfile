FROM node:10 as builder

ARG BUILD_ID=dev
ENV NODE_ENV=production

RUN mkdir /code
WORKDIR /code

COPY package.json /code
COPY package-lock.json /code

RUN npm ci

COPY . /code

RUN sed -i 's/dev-build/Build '"$BUILD_ID"'/' src/components/footer/footer.js

RUN npm run build:prod

# runtime container image
FROM nginx:1

ENV NGINX_PORT=80

COPY --from=builder /code/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /code/build /usr/share/nginx/html

EXPOSE 80/tcp