#!/bin/bash

set -e

cleanup() {
    docker-compose -f docker-compose.e2e.yml kill
    docker-compose -f docker-compose.e2e.yml down -v
}

trap cleanup EXIT

echo "docker: $(docker --version)"
echo "docker-compose: $(docker-compose --version)"

docker-compose -f docker-compose.e2e.yml pull
docker-compose -f docker-compose.e2e.yml build frontend

curl -o wait-for-it.sh https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh && chmod +x ./wait-for-it.sh 
docker-compose -f docker-compose.e2e.yml up -d web
./wait-for-it.sh -h 127.0.0.1 -p 8080 -t 60

docker-compose -f docker-compose.e2e.yml run --rm frontend

