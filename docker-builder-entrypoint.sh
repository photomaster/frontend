#!/bin/bash
set -e

export ENV_FILE=.env.e2e

npm i pm2 

./node_modules/.bin/pm2 --name project start npm -- run start:ci
# wait until bundler is bound to port
wait-for-it.sh -h 127.0.0.1 -p 3000 -t 60
# wait for the dev server to be ready
curl -I --connect-timeout 60 http://127.0.0.1:3000

npm run test:e2e --watchAll=false

./node_modules/.bin/pm2 kill 
